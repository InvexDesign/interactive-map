@extends('templates.master')

@section('page-title', isset($page_title) ? $page_title : 'Edit Facility Type')

@section('breadcrumbs')
    {!! App\Helpers\Breadcrumbs::generate([
        'Facility Types'      => ['backend.facility_types.index'],
        '#' . $facility_type->id    => ['backend.facility_types.show', $facility_type->id],
        'Edit'                 =>  null
    ]); !!}
@stop

@section('content')

    {!! Form::model($facility_type, ['route' => ['backend.facility_types.edit.post', $facility_type->id]]) !!}

        @include('backend.facility_types.partials.form')

        <div class="form-group">
            {!! Form::submit('Update Facility Type', ['class' => 'updatable btn btn-primary']) !!}
        </div>

    {!! Form::close() !!}

@stop