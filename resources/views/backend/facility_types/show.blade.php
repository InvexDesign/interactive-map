@extends('templates.master')

@section('page-title', isset($page_title) ? $page_title : 'View Facility Type')

@section('breadcrumbs')
    {!! App\Helpers\Breadcrumbs::generate([
        'Facility Types'      => ['backend.facility_types.index'],
        $facility_type->display()   =>  null
    ]); !!}
@stop

@section('content')
    <ul class="nav nav-tabs">
        <li class="active"><a data-toggle="tab" href="#facility_type">Facility Type</a></li>
        <li><a data-toggle="tab" href="#facilities">Facilities</a></li>
    </ul>

    <div class="tab-content">
        <div id="facility_type" class="tab-pane fade in active">
            {!! link_to_route('backend.facility_types.edit.get', 'Edit Facility Type', ['id' => $facility_type->id], ['class' => 'btn btn-primary']) !!}
            {!! link_to_route('backend.facility_types.destroy', 'Delete Facility Type', ['id' => $facility_type->id], ['class' => 'btn btn-danger']) !!}
            <br />
            <br />
            @include('backend.facility_types.partials.snapshot')
        </div>
        <div id="facilities" class="tab-pane fade">
            @include('backend.facilities.partials.table', ['facilities' => $facility_type->facilities])
        </div>
    </div>
@stop