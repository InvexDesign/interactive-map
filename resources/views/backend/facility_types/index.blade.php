@extends('templates.master')

@section('page-title', isset($page_title) ? $page_title : 'Facility Types')

@section('breadcrumbs')
    {!! App\Helpers\Breadcrumbs::generate([
        'Facility Types' => null,
    ]); !!}
@stop

@section('content')

    {!! link_to_route('backend.facility_types.create.get', 'Add Facility Type', [], ['class' => 'btn btn-primary']) !!}
    <br />
    <br />

    @include('backend.facility_types.partials.table')

@stop