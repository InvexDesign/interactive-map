<?php $table_id = isset($table_id) ? $table_id : 'facility_type_table'; ?>

<table id="{{ $table_id }}" class="datatable">
    <thead>
    <tr>
        <th>ID</th>
        <th>Name</th>
        <th>Actions</th>
    </tr>
    </thead>

    <tbody>
    @foreach($facility_types as $facility_type)
        <tr>
            <td>{!! link_to_route('backend.facility_types.show', $facility_type->id, [$facility_type->id]) !!}</td>
            <td>{!! link_to_route('backend.facility_types.show', $facility_type->name, [$facility_type->id]) !!}</td>
            <td class="icon-td">
                {!! Helper::icon_to_route('backend.facility_types.show', 'fa-eye', [$facility_type->id], ['title' => 'View Facility Type']) !!}
                {!! Helper::icon_to_route('backend.facility_types.edit.get', 'fa-pencil', [$facility_type->id], ['title' => 'Edit Facility Type']) !!}
                {!! Helper::icon_to_route('backend.facility_types.destroy', 'fa-ban', [$facility_type->id], ['title' => 'Delete Facility Type']) !!}
            </td>
        </tr>
    @endforeach
    </tbody>

</table>

<script>
    $(document).ready(function()
    {
        var hidden_columns;
        @include('includes.js.hidden_columns', ['default_hidden_columns' => ['ID' => 1]])

        var table_id = '{{ $table_id }}';
        datatables[table_id] = init_datatable_i(table_id, {hasToggles: false, hiddenColumns: hidden_columns, sortOrder: [[ 1, 'asc' ]]});

        @if(isset($has_filtering) && $has_filtering)
        $('.filter').change(function()
        {
            datatables[table_id].draw();
        });
        @endif
    });
</script>