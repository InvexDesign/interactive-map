@extends('templates.master')

@section('page-title', isset($page_title) ? $page_title : 'Add Facility Type')

@section('breadcrumbs')
    {!! App\Helpers\Breadcrumbs::generate([
            'Facility Types' => ['backend.facility_types.index'],
            'Add'            =>  null
    ]); !!}
@stop

@section('content')
    {!! Form::open(['route' => 'backend.facility_types.create.post']) !!}

        @include('backend.facility_types.partials.form')

        <div class="form-group">
            {!! Form::submit('Add Facility Type', ['class' => 'btn btn-primary']) !!}
        </div>

    {!! Form::close() !!}
@stop