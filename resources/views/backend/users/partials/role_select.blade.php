<div class="col-xs-3 form-group">
    {!! Form::label('role_id', 'Role *') !!}
    {!! Form::select('role_id', $role_options, isset($default_role_id) ? $default_role_id : null, ['class'=>'form-control']) !!}
    {!! $errors->first('role_id', '<span class="error">:message</span>') !!}
</div>