<table id="<?php echo isset($table_id) ? $table_id : 'user_table'; ?>" class="datatable">
    <thead>
    <tr>
        <th>ID</th>
        <th>Username</th>
        <th>Email</th>
        <th>Name</th>
        <th>Role</th>
        <th>Created</th>
    </tr>
    </thead>

    <tbody>
    @foreach($users as $user)
        <tr>
            <td>{!! link_to_route('backend.users.show', $user->id, ['id' => $user->id]) !!}</td>
            <td>{!! link_to_route('backend.users.show', $user->username, ['id' => $user->id]) !!}</td>
            <td><a href="mailto:{{ $user->email }}">{{ $user->email }}</a></td>
            <td>{{ $user->first_last }}</td>
            <td>{{ $user->getRole() ? $user->getRole()->display_name : 'None' }}</td>
            <td>{!! Helper::sortDatetime($user->created_at) !!}</td>
        </tr>
    @endforeach
    </tbody>
</table>


<?php $javascript_section = isset($js_section) ? 'javascript-for-partials-' . $js_section : 'javascript-for-partials'; ?>
@section($javascript_section)
<script>
    $(document).ready(function()
    {
        var hidden_columns;
        @include('includes.js.hidden_columns')

        init_datatable('user_table', true, hidden_columns, 25);
    });
</script>
@stop