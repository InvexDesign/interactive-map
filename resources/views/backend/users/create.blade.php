@extends('templates.master')

@section('page-title', isset($page_title) ? $page_title : 'Create User')

@section('breadcrumbs')
    {!! App\Helpers\Breadcrumbs::generate([
        'Users' => ['backend.users.index'],
        'Create' => null,
    ]); !!}
@stop

@section('content')
    {!! Form::open(['route' => 'backend.users.create.post']) !!}

        @include('backend.users.partials.form')
        <div class="row">
            @include('backend.users.partials.password_and_confirmation')
        </div>

        {!! Form::submit('Create User', ['class' => 'btn btn-primary']) !!}

    {!! Form::close() !!}
@stop