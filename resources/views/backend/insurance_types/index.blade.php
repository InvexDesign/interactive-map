@extends('templates.master')

@section('page-title', isset($page_title) ? $page_title : 'Insurance Types')

@section('breadcrumbs')
    {!! App\Helpers\Breadcrumbs::generate([
        'Insurance Types' => null,
    ]); !!}
@stop

@section('content')

    {!! link_to_route('backend.insurance_types.create.get', 'Add Insurance Type', [], ['class' => 'btn btn-primary']) !!}
    <br />
    <br />

    @include('backend.insurance_types.partials.table')

@stop