@extends('templates.master')

@section('page-title', isset($page_title) ? $page_title : 'Add Insurance Type')

@section('breadcrumbs')
    {!! App\Helpers\Breadcrumbs::generate([
            'Insurance Types' => ['backend.insurance_types.index'],
            'Add'             =>  null
    ]); !!}
@stop

@section('content')
    {!! Form::open(['route' => 'backend.insurance_types.create.post']) !!}

        @include('backend.insurance_types.partials.form')

        <div class="form-group">
            {!! Form::submit('Add Insurance Type', ['class' => 'btn btn-primary']) !!}
        </div>

    {!! Form::close() !!}
@stop