@extends('templates.master')

@section('page-title', isset($page_title) ? $page_title : 'Edit Insurance Type')

@section('breadcrumbs')
    {!! App\Helpers\Breadcrumbs::generate([
        'Insurance Types'      => ['backend.insurance_types.index'],
        '#' . $insurance_type->id    => ['backend.insurance_types.show', $insurance_type->id],
        'Edit'                 =>  null
    ]); !!}
@stop

@section('content')

    {!! Form::model($insurance_type, ['route' => ['backend.insurance_types.edit.post', $insurance_type->id]]) !!}

        @include('backend.insurance_types.partials.form')

        <div class="form-group">
            {!! Form::submit('Update Insurance Type', ['class' => 'updatable btn btn-primary']) !!}
        </div>

    {!! Form::close() !!}

@stop