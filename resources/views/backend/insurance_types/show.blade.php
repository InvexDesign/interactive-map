@extends('templates.master')

@section('page-title', isset($page_title) ? $page_title : 'View Insurance Type')

@section('breadcrumbs')
    {!! App\Helpers\Breadcrumbs::generate([
        'Insurance Types'      => ['backend.insurance_types.index'],
        $insurance_type->display()   =>  null
    ]); !!}
@stop

@section('content')
    <ul class="nav nav-tabs">
        <li class="active"><a data-toggle="tab" href="#insurance_type">Insurance Type</a></li>
        <li><a data-toggle="tab" href="#facilities">Facilities</a></li>
    </ul>

    <div class="tab-content">
        <div id="insurance_type" class="tab-pane fade in active">
            {!! link_to_route('backend.insurance_types.edit.get', 'Edit Insurance Type', ['id' => $insurance_type->id], ['class' => 'btn btn-primary']) !!}
            {!! link_to_route('backend.insurance_types.destroy', 'Delete Insurance Type', ['id' => $insurance_type->id], ['class' => 'btn btn-danger']) !!}
            <br />
            <br />
            @include('backend.insurance_types.partials.snapshot')
        </div>
        <div id="facilities" class="tab-pane fade">
            @include('backend.facilities.partials.table', ['facilities' => $insurance_type->facilities])
        </div>
    </div>
@stop