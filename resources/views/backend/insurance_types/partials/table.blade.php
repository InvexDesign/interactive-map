<?php $table_id = isset($table_id) ? $table_id : 'insurance_type_table'; ?>

<table id="{{ $table_id }}" class="datatable">
    <thead>
    <tr>
        <th>ID</th>
        <th>Name</th>
        <th>Actions</th>
    </tr>
    </thead>

    <tbody>
    @foreach($insurance_types as $insurance_type)
        <tr>
            <td>{!! link_to_route('backend.insurance_types.show', $insurance_type->id, [$insurance_type->id]) !!}</td>
            <td>{!! link_to_route('backend.insurance_types.show', $insurance_type->name, [$insurance_type->id]) !!}</td>
            <td class="icon-td">
                {!! Helper::icon_to_route('backend.insurance_types.show', 'fa-eye', [$insurance_type->id], ['title' => 'View Insurance Type']) !!}
                {!! Helper::icon_to_route('backend.insurance_types.edit.get', 'fa-pencil', [$insurance_type->id], ['title' => 'Edit Insurance Type']) !!}
                {!! Helper::icon_to_route('backend.insurance_types.destroy', 'fa-ban', [$insurance_type->id], ['title' => 'Delete Insurance Type']) !!}
            </td>
        </tr>
    @endforeach
    </tbody>

</table>

<script>
    $(document).ready(function()
    {
        var hidden_columns;
        @include('includes.js.hidden_columns', ['default_hidden_columns' => ['ID' => 1]])

        var table_id = '{{ $table_id }}';
        datatables[table_id] = init_datatable_i(table_id, {hasToggles: false, hiddenColumns: hidden_columns, sortOrder: [[ 1, 'asc' ]]});

        @if(isset($has_filtering) && $has_filtering)
        $('.filter').change(function()
        {
            datatables[table_id].draw();
        });
        @endif
    });
</script>