@extends('templates.master')

@section('page-title', isset($page_title) ? $page_title : 'Age Groups')

@section('breadcrumbs')
    {!! App\Helpers\Breadcrumbs::generate([
        'Age Groups' => null,
    ]); !!}
@stop

@section('content')

    {!! link_to_route('backend.age_groups.create.get', 'Add Age Group', [], ['class' => 'btn btn-primary']) !!}
    <br />
    <br />

    @include('backend.age_groups.partials.table')

@stop