@extends('templates.master')

@section('page-title', isset($page_title) ? $page_title : 'Edit Age Group')

@section('breadcrumbs')
    {!! App\Helpers\Breadcrumbs::generate([
        'Age Groups'            => ['backend.age_groups.index'],
        '#' . $age_group->id    => ['backend.age_groups.show', $age_group->id],
        'Edit'                  =>  null
    ]); !!}
@stop

@section('content')

    {!! Form::model($age_group, ['route' => ['backend.age_groups.edit.post', $age_group->id]]) !!}

        @include('backend.age_groups.partials.form')

        <div class="form-group">
            {!! Form::submit('Update Age Group', ['class' => 'updatable btn btn-primary']) !!}
        </div>

    {!! Form::close() !!}

@stop