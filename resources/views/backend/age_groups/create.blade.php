@extends('templates.master')

@section('page-title', isset($page_title) ? $page_title : 'Add Age Group')

@section('breadcrumbs')
    {!! App\Helpers\Breadcrumbs::generate([
            'Age Groups'    => ['backend.age_groups.index'],
            'Add'           =>  null
    ]); !!}
@stop

@section('content')
    {!! Form::open(['route' => 'backend.age_groups.create.post']) !!}

        @include('backend.age_groups.partials.form')

        <div class="form-group">
            {!! Form::submit('Add Age Group', ['class' => 'btn btn-primary']) !!}
        </div>

    {!! Form::close() !!}
@stop