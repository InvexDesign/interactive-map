@extends('templates.master')

@section('page-title', isset($page_title) ? $page_title : 'View Age Group')

@section('breadcrumbs')
    {!! App\Helpers\Breadcrumbs::generate([
        'Age Groups'            => ['backend.age_groups.index'],
        $age_group->display()   =>  null
    ]); !!}
@stop

@section('content')
    <ul class="nav nav-tabs">
        <li class="active"><a data-toggle="tab" href="#age_group">Age Group</a></li>
        <li><a data-toggle="tab" href="#facilities">Facilities</a></li>
    </ul>

    <div class="tab-content">
        <div id="age_group" class="tab-pane fade in active">
            {!! link_to_route('backend.age_groups.edit.get', 'Edit Age Group', ['id' => $age_group->id], ['class' => 'btn btn-primary']) !!}
            {!! link_to_route('backend.age_groups.destroy', 'Delete Age Group', ['id' => $age_group->id], ['class' => 'btn btn-danger']) !!}
            <br />
            <br />
            @include('backend.age_groups.partials.snapshot')
        </div>
        <div id="facilities" class="tab-pane fade">
            @include('backend.facilities.partials.table', ['facilities' => $age_group->facilities])
        </div>
    </div>
@stop