<?php $table_id = isset($table_id) ? $table_id : 'age_group_table'; ?>

<table id="{{ $table_id }}" class="datatable">
    <thead>
    <tr>
        <th>ID</th>
        <th>Name</th>
        <th>Actions</th>
    </tr>
    </thead>

    <tbody>
    @foreach($age_groups as $age_group)
        <tr>
            <td>{!! link_to_route('backend.age_groups.show', $age_group->id, [$age_group->id]) !!}</td>
            <td>{!! link_to_route('backend.age_groups.show', $age_group->name, [$age_group->id]) !!}</td>
            <td class="icon-td">
                {!! Helper::icon_to_route('backend.age_groups.show', 'fa-eye', [$age_group->id], ['title' => 'View Age Group']) !!}
                {!! Helper::icon_to_route('backend.age_groups.edit.get', 'fa-pencil', [$age_group->id], ['title' => 'Edit Age Group']) !!}
                {!! Helper::icon_to_route('backend.age_groups.destroy', 'fa-ban', [$age_group->id], ['title' => 'Delete Age Group']) !!}
            </td>
        </tr>
    @endforeach
    </tbody>

</table>

<script>
    $(document).ready(function()
    {
        var hidden_columns;
        @include('includes.js.hidden_columns', ['default_hidden_columns' => ['ID' => 1]])

        var table_id = '{{ $table_id }}';
        datatables[table_id] = init_datatable_i(table_id, {hasToggles: false, hiddenColumns: hidden_columns, sortOrder: [[ 1, 'asc' ]]});

        @if(isset($has_filtering) && $has_filtering)
        $('.filter').change(function()
        {
            datatables[table_id].draw();
        });
        @endif
    });
</script>