@extends('templates.master')

@section('page-title', isset($page_title) ? $page_title : 'Create Language')

@section('breadcrumbs')
    {!! App\Helpers\Breadcrumbs::generate([
            'Languages' => ['backend.languages.index'],
            'Add'       =>  null
    ]); !!}
@stop

@section('content')
    {!! Form::open(['route' => 'backend.languages.create.post']) !!}

        @include('backend.languages.partials.form')

        <div class="form-group">
            {!! Form::submit('Add Language', ['class' => 'btn btn-primary']) !!}
        </div>

    {!! Form::close() !!}
@stop