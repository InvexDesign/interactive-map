@extends('templates.master')

@section('page-title', isset($page_title) ? $page_title : 'Languages')

@section('breadcrumbs')
    {!! App\Helpers\Breadcrumbs::generate([
        'Languages' => null,
    ]); !!}
@stop

@section('content')

    {!! link_to_route('backend.languages.create.get', 'Add Language', [], ['class' => 'btn btn-primary']) !!}
    <br />
    <br />

    @include('backend.languages.partials.table')

@stop