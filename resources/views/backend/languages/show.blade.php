@extends('templates.master')

@section('page-title', isset($page_title) ? $page_title : 'View Language')

@section('breadcrumbs')
    {!! App\Helpers\Breadcrumbs::generate([
        'Languages'            => ['backend.languages.index'],
        $language->display()   =>  null
    ]); !!}
@stop

@section('content')
    <ul class="nav nav-tabs">
        <li class="active"><a data-toggle="tab" href="#language">Language</a></li>
        <li><a data-toggle="tab" href="#facilities">Facilities</a></li>
    </ul>

    <div class="tab-content">
        <div id="language" class="tab-pane fade in active">
            {!! link_to_route('backend.languages.edit.get', 'Edit Language', ['id' => $language->id], ['class' => 'btn btn-primary']) !!}
            {!! link_to_route('backend.languages.destroy', 'Delete Language', ['id' => $language->id], ['class' => 'btn btn-danger']) !!}
            <br />
            <br />
            @include('backend.languages.partials.snapshot')
        </div>
        <div id="facilities" class="tab-pane fade">
            @include('backend.facilities.partials.table', ['facilities' => $language->facilities])
        </div>
    </div>
@stop