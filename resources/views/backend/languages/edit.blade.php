@extends('templates.master')

@section('page-title', isset($page_title) ? $page_title : 'Edit Language')

@section('breadcrumbs')
    {!! App\Helpers\Breadcrumbs::generate([
        'Languages'            => ['backend.languages.index'],
        '#' . $language->id    => ['backend.languages.show', $language->id],
        'Edit'                 =>  null
    ]); !!}
@stop

@section('content')

    {!! Form::model($language, ['route' => ['backend.languages.edit.post', $language->id]]) !!}

        @include('backend.languages.partials.form')

        <div class="form-group">
            {!! Form::submit('Update Language', ['class' => 'updatable btn btn-primary']) !!}
        </div>

    {!! Form::close() !!}

@stop