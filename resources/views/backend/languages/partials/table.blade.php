<?php $table_id = isset($table_id) ? $table_id : 'language_table'; ?>

<table id="{{ $table_id }}" class="datatable">
    <thead>
    <tr>
        <th>ID</th>
        <th>Name</th>
        <th>Actions</th>
    </tr>
    </thead>

    <tbody>
    @foreach($languages as $language)
        <tr>
            <td>{!! link_to_route('backend.languages.show', $language->id, [$language->id]) !!}</td>
            <td>{!! link_to_route('backend.languages.show', $language->name, [$language->id]) !!}</td>
            <td class="icon-td">
                {!! Helper::icon_to_route('backend.languages.show', 'fa-eye', [$language->id], ['title' => 'View Language']) !!}
                {!! Helper::icon_to_route('backend.languages.edit.get', 'fa-pencil', [$language->id], ['title' => 'Edit Language']) !!}
                {!! Helper::icon_to_route('backend.languages.destroy', 'fa-ban', [$language->id], ['title' => 'Delete Language']) !!}
            </td>
        </tr>
    @endforeach
    </tbody>

</table>

<script>
    $(document).ready(function()
    {
        var hidden_columns;
        @include('includes.js.hidden_columns', ['default_hidden_columns' => ['ID' => 1]])

        var table_id = '{{ $table_id }}';
        datatables[table_id] = init_datatable_i(table_id, {hasToggles: false, hiddenColumns: hidden_columns, sortOrder: [[ 1, 'asc' ]]});

        @if(isset($has_filtering) && $has_filtering)
        $('.filter').change(function()
        {
            datatables[table_id].draw();
        });
        @endif
    });
</script>