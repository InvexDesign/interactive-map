@extends('templates.master')

@section('page-title', isset($page_title) ? $page_title : 'Dashboard')

@section('content')

    <p>Welcome to the {{ Setting::get('site-title') }} dashboard!</p>

@stop