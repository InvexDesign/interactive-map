@extends('templates.master')

@section('page-title', isset($page_title) ? $page_title : 'View Facility')

@section('breadcrumbs')
    {!! App\Helpers\Breadcrumbs::generate([
        'Facilities'         => ['backend.facilities.index'],
        '#' . $facility->id  => null,
    ]); !!}
@stop

@section('content')
    <ul class="nav nav-tabs">
        <li class="active"><a data-toggle="tab" href="#facility">Facility</a></li>
        <li><a data-toggle="tab" href="#age_groups">Age Groups</a></li>
        <li><a data-toggle="tab" href="#insurance_types">Insurance Types</a></li>
        <li><a data-toggle="tab" href="#languages">Languages</a></li>
        <li><a data-toggle="tab" href="#services">Services</a></li>
        <li><a data-toggle="tab" href="#transit_options">Parking/Transport</a></li>
    </ul>

    <div class="tab-content">
        <div id="facility" class="tab-pane fade in active">
            {!! link_to_route('backend.facilities.edit.get', 'Edit Facility', ['id' => $facility->id], ['class' => 'btn btn-primary']) !!}
            {!! link_to_route('backend.facilities.duplicate.get', 'Duplicate Facility', ['id' => $facility->id], ['class' => 'btn btn-success']) !!}
            {!! link_to_route('backend.facilities.destroy', 'Delete Facility', ['id' => $facility->id], ['class' => 'btn btn-danger']) !!}
            <br />
            <br />

            @include('backend.facilities.partials.snapshot')
        </div>
        <div id="age_groups" class="tab-pane fade">
            @include('backend.age_groups.partials.table', ['age_groups' => $facility->ageGroups])
        </div>
        <div id="insurance_types" class="tab-pane fade">
            @include('backend.insurance_types.partials.table', ['insurance_types' => $facility->insuranceTypes])
        </div>
        <div id="languages" class="tab-pane fade">
            @include('backend.languages.partials.table', ['languages' => $facility->languages])
        </div>
        <div id="services" class="tab-pane fade">
            @include('backend.services.partials.table', ['services' => $facility->services])
        </div>
        <div id="transit_options" class="tab-pane fade">
            @include('backend.transit_options.partials.table', ['transit_options' => $facility->transitOptions])
        </div>
    </div>
@stop