@extends('templates.master')

@section('page-title', isset($page_title) ? $page_title : 'Edit Facility')

@section('breadcrumbs')
    {!! App\Helpers\Breadcrumbs::generate([
        'Facilities'         => ['backend.facilities.index'],
        '#' . $facility->id  => ['backend.facilities.show', $facility->id],
        'Edit'               => null,
    ]); !!}
@stop

@section('content')

    {!! Form::model($facility, ['route' => ['backend.facilities.edit.post', $facility->id]]) !!}

        @include('backend.facilities.partials.form')

        <div class="form-group">
            {!! Form::submit('Update Facility', ['class' => 'btn btn-primary']) !!}
        </div>

    {!! Form::close() !!}

@stop