@extends('templates.master')

@section('page-title', isset($page_title) ? $page_title : 'Duplicate Facility')

@section('breadcrumbs')
    {!! App\Helpers\Breadcrumbs::generate([
        'Facilities'         => ['backend.facilities.index'],
        '#' . $facility->id  => ['backend.facilities.show', $facility->id],
        'Duplicate'          => null,
    ]); !!}
@stop

@section('content')

    {!! Form::model($facility, ['route' => 'backend.facilities.create.post']) !!}

        @include('backend.facilities.partials.form')

        <div class="form-group">
            {!! Form::submit('Duplicate Facility', ['class' => 'btn btn-primary']) !!}
        </div>

    {!! Form::close() !!}

@stop