<div class="field-container col-3">
    <div class="field form-group">
        {!! Form::label('name', 'Name *') !!}
        {!! Form::text('name', null, ['class'=>'form-control']) !!}
    </div>
    <div class="field form-group">
        {!! Form::label('tagline', 'Tagline') !!}
        {!! Form::text('tagline', null, ['class'=>'form-control']) !!}
    </div>
    <div class="field form-group">
        {!! Form::label('organization', 'Organization') !!}
        {!! Form::text('organization', null, ['class'=>'form-control']) !!}
    </div>
    <div class="field full-width form-group">
        {!! Form::label('description', 'Description') !!}
        {!! Form::textarea('description', null, ['class'=>'cazary form-control']) !!}
    </div>
    <div class="field half-width form-group">
        {!! Form::label('address1', 'Address Line 1 *') !!}
        {!! Form::text('address1', null, ['class'=>'form-control']) !!}
    </div>
    <div class="field half-width form-group">
        {!! Form::label('address2', 'Address Line 2') !!}
        {!! Form::text('address2', null, ['class'=>'form-control']) !!}
    </div>
    <div class="field form-group">
        {!! Form::label('city', 'City *') !!}
        {!! Form::text('city', null, ['class'=>'form-control']) !!}
    </div>
    <div class="field form-group">
        {!! Form::label('state_id', 'State *') !!}
        {!! Form::select('state_id', $state_options, null, ['class'=>'select-two form-control']) !!}
    </div>
    <div class="field form-group">
        {!! Form::label('zipcode', 'Zipcode *') !!}
        {!! Form::text('zipcode', isset($zipcode) ? $zipcode : null, [isset($lock_zipcode) && $lock_zipcode ? 'readonly' : '', 'class'=>'form-control']) !!}
    </div>
    <div class="field full-width form-group">
        {!! Form::label('auto_gps', 'Auto GPS') !!}
        {!! Form::checkbox('auto_gps', 1, isset($auto_gps) ? $auto_gps : null, ['class'=>'form-control']) !!}
    </div>
    <div class="field half-width form-group lat-long">
        {!! Form::label('latitude', 'Latitude *') !!}
        {!! Form::text('latitude', null, ['class'=>'form-control']) !!}
    </div>
    <div class="field half-width form-group lat-long">
        {!! Form::label('longitude', 'Longitude *') !!}
        {!! Form::text('longitude', null, ['class'=>'form-control']) !!}
    </div>
    <div class="field full-width form-group">
        {!! Form::label('service_ids[]', 'Services *') !!}
        {!! Form::select('service_ids[]', $service_options, isset($initial_service_ids) ? $initial_service_ids : null, ['multiple' => true, 'class'=>'select-two updatable form-control']) !!}
    </div>
    <div class="field full-width form-group">
        {!! Form::label('language_ids[]', 'Languages Spoken') !!}
        {!! Form::select('language_ids[]', $language_options, isset($initial_language_ids) ? $initial_language_ids : null, ['multiple' => true, 'class'=>'select-two updatable form-control']) !!}
    </div>
    <div class="field full-width form-group">
        {!! Form::label('insurance_type_ids[]', 'Insurance Types') !!}
        {!! Form::select('insurance_type_ids[]', $insurance_type_options, isset($initial_insurance_type_ids) ? $initial_insurance_type_ids : null, ['multiple' => true, 'class'=>'select-two updatable form-control']) !!}
    </div>
    <div class="field full-width form-group">
        {!! Form::label('transit_option_ids[]', 'Parking / Transport') !!}
        {!! Form::select('transit_option_ids[]', $transit_options, isset($initial_transit_option_ids) ? $initial_transit_option_ids : null, ['multiple' => true, 'class'=>'select-two updatable form-control']) !!}
    </div>
    <div class="field full-width form-group">
        {!! Form::label('age_group_ids[]', 'Age Groups') !!}
        {!! Form::select('age_group_ids[]', $age_group_options, isset($initial_age_group_ids) ? $initial_age_group_ids : null, ['multiple' => true, 'class'=>'select-two updatable form-control']) !!}
    </div>
    <div class="field form-group">
        {!! Form::label('url', 'URL') !!}
        {!! Form::text('url', null, ['class'=>'form-control']) !!}
    </div>
    <div class="field form-group">
        {!! Form::label('email', 'Email') !!}
        {!! Form::text('email', null, ['class'=>'form-control']) !!}
    </div>
    <div class="field form-group">
        {!! Form::label('website', 'Website') !!}
        {!! Form::text('website', null, ['class'=>'form-control']) !!}
    </div>
    <div class="field form-group">
        {!! Form::label('phone', 'Phone') !!}
        {!! Form::text('phone', null, ['class'=>'phone-mask form-control']) !!}
    </div>
    <div class="field form-group">
        {!! Form::label('247_hotline', '24/7 Hotline') !!}
        {!! Form::text('247_hotline', null, ['class'=>'phone-mask form-control']) !!}
    </div>
    <div class="field form-group">
        {!! Form::label('text_line', 'Text Line') !!}
        {!! Form::text('text_line', null, ['class'=>'form-control']) !!}
    </div>
    <div class="field form-group">
        {!! Form::label('live_chat', 'Live Chat') !!}
        {!! Form::text('live_chat', null, ['class'=>'form-control']) !!}
    </div>
    <div class="field form-group">
        {!! Form::label('youth_approved', 'Youth Approved') !!}
        {!! Form::checkbox('youth_approved', 1, null, ['class'=>'form-control']) !!}
    </div>
    <div class="field form-group">
        {!! Form::label('hours', 'Hours') !!}
        {!! Form::textarea('hours', null, ['class'=>'cazary form-control']) !!}
    </div>
    <div class="field form-group">
        {!! Form::label('minor_access', 'Minor Access') !!}
        {!! Form::textarea('minor_access', null, ['class'=>'cazary form-control']) !!}
    </div>
    <div class="field form-group">
        {!! Form::label('appointments', 'Appointments') !!}
        {!! Form::textarea('appointments', null, ['class'=>'cazary form-control']) !!}
    </div>
    <div class="field full-width form-group">
        {!! Form::label('notes', 'Notes') !!}
        {!! Form::textarea('notes', null, ['class'=>'form-control']) !!}
    </div>
</div>
<script>
    $(document).ready(function()
    {
        $('#auto_gps').change(function()
        {
            $('.lat-long').toggle(!$(this).is(":checked"));
        }).trigger('change');
    });
</script>