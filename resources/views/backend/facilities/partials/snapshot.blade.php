<p>
    <strong>Name:</strong> {{ $facility->name }}<br />
    <strong>Tagline:</strong> {{ $facility->tagline }}<br />
    <strong>Organization:</strong> {{ $facility->organization }}<br />
    <strong>Address 1:</strong> {{ $facility->address1 }}<br />
@if($facility->address2)
    <strong>Address 2:</strong> {{ $facility->address2 }}<br />
@endif
    <strong>City:</strong> {{ $facility->city }}<br />
    <strong>State:</strong> {{ $facility->state->display() }}<br />
    <strong>Zip Code:</strong> {{ $facility->zipcode }}<br />
    <strong>Auto GPS:</strong> {{ $facility->auto_gps }}<br />
    <strong>Latitude:</strong> {{ $facility->latitude }}<br />
    <strong>Longitude:</strong> {{ $facility->longitude }}<br />
    <strong>URL:</strong> {{ $facility->url }}<br />
    <strong>Email:</strong> {{ $facility->email }}<br />
    <strong>Website:</strong> {{ $facility->website }}<br />
    <strong>Phone:</strong> {{ $facility->displayPhone('phone') }}<br />
    <strong>24/7 Hotline:</strong> {{ $facility->displayPhone('247_hotline') }}<br />
    <strong>Text Line:</strong> {{ $facility->displayPhone('text_line') }}<br />
    <strong>Live Chat:</strong> {{ $facility->live_chat }}<br />
    <strong>Youth Approved:</strong> {{ $facility->youth_approved ? 'Yes' : 'No' }}<br />
    <strong>Hours:</strong> {!! $facility->hours !!}<br />
    <strong>Image:</strong> {{ $facility->image }}<br />
    <strong>Description:</strong> {!! $facility->description !!}<br />
    <strong>Minor Access:</strong> {!! $facility->minor_access !!}<br />
    <strong>Appointments:</strong> {!! $facility->appointments !!}<br />
    <strong>Order:</strong> {{ $facility->order }}<br />
    <strong>Notes:</strong> {{ $facility->notes }}<br />
</p>