<?php $table_id = isset($table_id) ? $table_id : 'facility_table'; ?>

<table id="{{ $table_id }}" class="datatable">
    <thead>
    <tr>
        <th>ID</th>
        <th>UID</th>
        <th>Name</th>
        <th>Tagline</th>
        <th>Organization</th>
        <th>Address</th>
        <th>Latitude</th>
        <th>Longitude</th>
        <th>URL</th>
        <th>Email</th>
        <th>Website</th>
        <th>Phone</th>
        <th>Hours</th>
        <th>Description</th>
        <th>Actions</th>
    </tr>
    </thead>

    <tbody>
    @foreach($facilities as $facility)
        <tr>
            <td>{!! link_to_route('backend.facilities.show', $facility->id, ['id' => $facility->id]) !!}</td>
            <td>{!! link_to_route('backend.facilities.show', $facility->uid, ['id' => $facility->id]) !!}</td>
            <td>{!! link_to_route('backend.facilities.show', $facility->name, ['id' => $facility->id]) !!}</td>
            <td>{{ $facility->tagline }}</td>
            <td>{{ $facility->organization }}</td>
            <td>{{ $facility->displayAddress(true) }}</td>
            <td>{{ $facility->latitude }}</td>
            <td>{{ $facility->longitude }}</td>
            <td>{{ $facility->url }}</td>
            <td>{{ $facility->email }}</td>
            <td>{{ $facility->website }}</td>
            <td>{{ $facility->displayPhone('phone') }}</td>
            <td>{{ $facility->hours }}</td>
            <td>{{ $facility->description }}</td>
            <td class="icon-td">
                {!! Helper::icon_to_route('backend.facilities.show', 'fa-eye', [$facility->id], ['title' => 'View Facility']) !!}
                {!! Helper::icon_to_route('backend.facilities.duplicate.get', 'fa-cubes', [$facility->id], ['title' => 'Duplicate Facility']) !!}
                {!! Helper::icon_to_route('backend.facilities.edit.get', 'fa-pencil', [$facility->id], ['title' => 'Edit Facility']) !!}
                {!! Helper::icon_to_route('backend.facilities.destroy', 'fa-ban', [$facility->id], ['title' => 'Delete Facility']) !!}
            </td>
        </tr>
    @endforeach
    </tbody>
</table>

<script>
    $(document).ready(function()
    {

        var hidden_columns;
        @include('includes.js.hidden_columns', ['default_hidden_columns' => ['ID' => 1,'UID' => 1,'Tagline' => 1,'URL' => 1,'Email' => 1,'Website' => 1,'Hours' => 1,'Organization' => 1,'Latitude' => 1,'Longitude' => 1,'Description' => 1,'Notes' => 1]])

        var table_id = '{{ $table_id }}';
        datatables[table_id] = init_datatable_i(table_id, {hiddenColumns: hidden_columns, sortOrder: [[ 2, 'asc' ]]});
    });
</script>