@extends('templates.master')

@section('page-title', isset($page_title) ? $page_title : 'Facilities')

@section('breadcrumbs')
    {!! App\Helpers\Breadcrumbs::generate([
        'Facilities' => null,
    ]); !!}
@stop

@section('content')

    {!! link_to_route('backend.facilities.create.get', '+ Create Facility', [], ['class' => 'btn btn-primary']) !!}
    <br />
    <br />

    @include('backend.facilities.partials.table', ['facilities' => $facilities])

@stop