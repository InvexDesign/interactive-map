@extends('templates.master')

@section('page-title', isset($page_title) ? $page_title : 'Create Facility')

@section('breadcrumbs')
    {!! App\Helpers\Breadcrumbs::generate([
        'Facilities' => ['backend.facilities.index'],
        'Create'    => null
    ]); !!}
@stop

@section('content')

    {!! Form::open(['route' => 'backend.facilities.create.post']) !!}

        @include('backend.facilities.partials.form')

        <div class="form-group">
            {!! Form::submit('Create Facility', ['class' => 'btn btn-primary']) !!}
        </div>

    {!! Form::close() !!}

@stop