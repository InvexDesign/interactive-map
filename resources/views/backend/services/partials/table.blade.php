<?php $table_id = isset($table_id) ? $table_id : 'service_table'; ?>

<table id="{{ $table_id }}" class="datatable">
    <thead>
    <tr>
        <th>ID</th>
        <th>UID</th>
        <th>Name</th>
        <th>Tagline</th>
        <th>Description</th>
        <th>Notes</th>
        <th>Actions</th>
    </tr>
    </thead>

    <tbody>
    @foreach($services as $service)
        <tr>
            <td>{!! link_to_route('backend.services.show', $service->id, [$service->id]) !!}</td>
            <td>{!! link_to_route('backend.services.show', $service->uid, [$service->id]) !!}</td>
            <td>{!! link_to_route('backend.services.show', $service->display(), [$service->id]) !!}</td>
            <td>{{ $service->tagline }}</td>
            <td>{{ $service->description }}</td>
            <td>{{ $service->notes }}</td>
            <td class="icon-td">
                {!! Helper::icon_to_route('backend.services.show', 'fa-eye', [$service->id], ['title' => 'View Service']) !!}
                {!! Helper::icon_to_route('backend.services.edit.get', 'fa-pencil', [$service->id], ['title' => 'Edit Service']) !!}
                {!! Helper::icon_to_route('backend.services.destroy', 'fa-ban', [$service->id], ['title' => 'Delete Service']) !!}
            </td>
        </tr>
    @endforeach
    </tbody>

</table>

<script>
    $(document).ready(function()
    {
        var hidden_columns;
        @include('includes.js.hidden_columns', ['default_hidden_columns' => ['ID' => 1,'UID' => 1,'Notes' => 1]])

        var table_id = '{{ $table_id }}';
        datatables[table_id] = init_datatable_i(table_id, {hiddenColumns: hidden_columns, sortOrder: [[ 2, 'asc' ]]});

        @if(isset($has_filtering) && $has_filtering)
        $('.filter').change(function()
        {
            datatables[table_id].draw();
        });
        @endif
    });
</script>