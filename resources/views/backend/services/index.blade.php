@extends('templates.master')

@section('page-title', isset($page_title) ? $page_title : 'Services')

@section('breadcrumbs')
    {!! App\Helpers\Breadcrumbs::generate([
        'Services' => null,
    ]); !!}
@stop

@section('content')

    {!! link_to_route('backend.services.create.get', 'Create Service', [], ['class' => 'btn btn-primary']) !!}
    <br />
    <br />

    @include('backend.services.partials.table')

@stop