@extends('templates.master')

@section('page-title', isset($page_title) ? $page_title : 'Edit Service')

@section('breadcrumbs')
    {!! App\Helpers\Breadcrumbs::generate([
        'Services'            => ['backend.services.index'],
        '#' . $service->id    => ['backend.services.show', $service->id],
        'Edit'                =>  null
    ]); !!}
@stop

@section('content')

    {!! Form::model($service, ['route' => ['backend.services.edit.post', $service->id]]) !!}

        @include('backend.services.partials.form')

        <div class="form-group">
            {!! Form::submit('Update Service', ['class' => 'updatable btn btn-primary']) !!}
        </div>

    {!! Form::close() !!}

@stop