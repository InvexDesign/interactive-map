@extends('templates.master')

@section('page-title', isset($page_title) ? $page_title : 'Create Service')

@section('breadcrumbs')
    {!! App\Helpers\Breadcrumbs::generate([
            'Services' => ['backend.services.index'],
            'Create'   =>  null
    ]); !!}
@stop

@section('content')
    {!! Form::open(['route' => 'backend.services.create.post']) !!}

        @include('backend.services.partials.form')

        <div class="form-group">
            {!! Form::submit('Create Service', ['class' => 'btn btn-primary']) !!}
        </div>

    {!! Form::close() !!}
@stop