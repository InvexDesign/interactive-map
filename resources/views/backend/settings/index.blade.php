@extends('templates.master')

@section('page-title', isset($page_title) ? $page_title : 'Settings')

@section('breadcrumbs')
    {!! App\Helpers\Breadcrumbs::generate([
        'Settings' => null,
    ]); !!}
@stop

@section('content')
{{--    {!! link_to_route('backend.settings.create', '+ Create Setting', [], ['class' => 'btn btn-primary']) !!}--}}

    @include('backend.settings.partials.table')
@stop