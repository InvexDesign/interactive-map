<?php $table_id = isset($table_id) ? $table_id : 'login_attempt_table'; ?>

<table id="{{ $table_id }}" class="datatable">
    <thead>
    <tr>
        <th>ID</th>
        <th>Username</th>
        <th>IP</th>
        <th>Timestamp</th>
        <th>Success</th>
        <th>Location</th>
        <th>Lat / Lng</th>
        <th>Organization</th>
        <th>Hostname</th>
        <th>Browser</th>
        <th>Referrer</th>
        <th>User</th>
        <th>Actions</th>
    </tr>
    </thead>

    <tbody>
    @foreach($login_attempts as $login_attempt)
        <tr>
            <td>{!! link_to_route('backend.login_attempts.show', $login_attempt->id, [$login_attempt->id]) !!}</td>
            <td>{{ $login_attempt->username }}</td>
            <td>{{ $login_attempt->ip_address }}</td>
            <td>{{ $login_attempt->timestamp->format('m/d/Y') }}</td>
            <td>{{ $login_attempt->success ? 'Y' : 'N' }}</td>
            <td>{{ $login_attempt->country }} / {{ $login_attempt->region }} / {{ $login_attempt->city }} / {{ $login_attempt->postal }}</td>
            <td>{{ $login_attempt->latitude }} / {{ $login_attempt->longitude }}</td>
            <td>{{ $login_attempt->organization }}</td>
            <td>{{ $login_attempt->hostname }}</td>
            <td>{{ $login_attempt->browser }}</td>
            <td>{{ $login_attempt->referrer }}</td>
            @if($login_attempt->user)
                <td>{!! link_to_route('backend.users.show', $login_attempt->user->display(), [$login_attempt->user_id]) !!}</td>
            @else
                <td></td>
            @endif
            <td class="icon-td">
                {!! Helper::icon_to_route('backend.login_attempts.show', 'fa-eye', [$login_attempt->id], ['title' => 'View Login Attempt']) !!}
            </td>
        </tr>
    @endforeach
    </tbody>

</table>

<script>
    $(document).ready(function()
    {
        var hidden_columns;
        @include('includes.js.hidden_columns', ['default_hidden_columns' => ['ID' => 1,'Lat / Lng' => 1,'Organization' => 1,'Hostname' => 1,'Browser' => 1,'Referrer' => 1,'User' => 1,]])

        var table_id = '{{ $table_id }}';
        datatables[table_id] = init_datatable_i(table_id, {sortOrder: [[ 3, "desc" ]], displayLength: {{ $page_size }}, hiddenColumns: hidden_columns});

        @if(isset($has_filtering) && $has_filtering)
        $('.filter').change(function()
        {
            datatables[table_id].draw();
        });
        @endif
    });
</script>