@extends('templates.master')

@section('page-title', isset($page_title) ? $page_title : 'Login Attempts')

@section('breadcrumbs')
    {!! App\Helpers\Breadcrumbs::generate([
        'Login Attempts' => null,
    ]); !!}
@stop

@section('content')

    {!! $login_attempts->links() !!}
    @include('backend.login_attempts.partials.table')
    {!! $login_attempts->links() !!}

@stop