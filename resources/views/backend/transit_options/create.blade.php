@extends('templates.master')

@section('page-title', isset($page_title) ? $page_title : 'Add Transit Option')

@section('breadcrumbs')
    {!! App\Helpers\Breadcrumbs::generate([
            'Transit Options' => ['backend.transit_options.index'],
            'Add'       =>  null
    ]); !!}
@stop

@section('content')
    {!! Form::open(['route' => 'backend.transit_options.create.post']) !!}

        @include('backend.transit_options.partials.form')

        <div class="form-group">
            {!! Form::submit('Add Transit Option', ['class' => 'btn btn-primary']) !!}
        </div>

    {!! Form::close() !!}
@stop