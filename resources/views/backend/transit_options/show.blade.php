@extends('templates.master')

@section('page-title', isset($page_title) ? $page_title : 'View Transit Option')

@section('breadcrumbs')
    {!! App\Helpers\Breadcrumbs::generate([
        'Transit Options'            => ['backend.transit_options.index'],
        $transit_option->display()   => null
    ]); !!}
@stop

@section('content')
    <ul class="nav nav-tabs">
        <li class="active"><a data-toggle="tab" href="#transit_option">Transit Option</a></li>
        <li><a data-toggle="tab" href="#facilities">Facilities</a></li>
    </ul>

    <div class="tab-content">
        <div id="transit_option" class="tab-pane fade in active">
            {!! link_to_route('backend.transit_options.edit.get', 'Edit Transit Option', ['id' => $transit_option->id], ['class' => 'btn btn-primary']) !!}
            {!! link_to_route('backend.transit_options.destroy', 'Delete Transit Option', ['id' => $transit_option->id], ['class' => 'btn btn-danger']) !!}
            <br />
            <br />
            @include('backend.transit_options.partials.snapshot')
        </div>
        <div id="facilities" class="tab-pane fade">
            @include('backend.facilities.partials.table', ['facilities' => $transit_option->facilities])
        </div>
    </div>
@stop