@extends('templates.master')

@section('page-title', isset($page_title) ? $page_title : 'Transit Options')

@section('breadcrumbs')
    {!! App\Helpers\Breadcrumbs::generate([
        'Transit Options' => null,
    ]); !!}
@stop

@section('content')

    {!! link_to_route('backend.transit_options.create.get', 'Add Transit Option', [], ['class' => 'btn btn-primary']) !!}
    <br />
    <br />

    @include('backend.transit_options.partials.table')

@stop