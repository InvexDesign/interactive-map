@extends('templates.master')

@section('page-title', isset($page_title) ? $page_title : 'Edit Transit Option')

@section('breadcrumbs')
    {!! App\Helpers\Breadcrumbs::generate([
        'Transit Options'            => ['backend.transit_options.index'],
        '#' . $transit_option->id    => ['backend.transit_options.show', $transit_option->id],
        'Edit'                 =>  null
    ]); !!}
@stop

@section('content')

    {!! Form::model($transit_option, ['route' => ['backend.transit_options.edit.post', $transit_option->id]]) !!}

        @include('backend.transit_options.partials.form')

        <div class="form-group">
            {!! Form::submit('Update Transit Option', ['class' => 'updatable btn btn-primary']) !!}
        </div>

    {!! Form::close() !!}

@stop