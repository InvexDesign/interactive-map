<?php $table_id = isset($table_id) ? $table_id : 'transit_option_table'; ?>

<table id="{{ $table_id }}" class="datatable">
    <thead>
    <tr>
        <th>ID</th>
        <th>Name</th>
        <th>Actions</th>
    </tr>
    </thead>

    <tbody>
    @foreach($transit_options as $transit_option)
        <tr>
            <td>{!! link_to_route('backend.transit_options.show', $transit_option->id, [$transit_option->id]) !!}</td>
            <td>{!! link_to_route('backend.transit_options.show', $transit_option->name, [$transit_option->id]) !!}</td>
            <td class="icon-td">
                {!! Helper::icon_to_route('backend.transit_options.show', 'fa-eye', [$transit_option->id], ['title' => 'View Transit Option']) !!}
                {!! Helper::icon_to_route('backend.transit_options.edit.get', 'fa-pencil', [$transit_option->id], ['title' => 'Edit Transit Option']) !!}
                {!! Helper::icon_to_route('backend.transit_options.destroy', 'fa-ban', [$transit_option->id], ['title' => 'Delete Transit Option']) !!}
            </td>
        </tr>
    @endforeach
    </tbody>

</table>

<script>
    $(document).ready(function()
    {
        var hidden_columns;
        @include('includes.js.hidden_columns', ['default_hidden_columns' => ['ID' => 1]])

        var table_id = '{{ $table_id }}';
        datatables[table_id] = init_datatable_i(table_id, {hasToggles: false, hiddenColumns: hidden_columns, sortOrder: [[ 1, 'asc' ]]});

        @if(isset($has_filtering) && $has_filtering)
        $('.filter').change(function()
        {
            datatables[table_id].draw();
        });
        @endif
    });
</script>