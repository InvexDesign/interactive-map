<!doctype html>
<html>
<head>
    <meta charset="utf-8">

    @yield('meta')

    <link href="https://fonts.googleapis.com/css?family=Open+Sans:400,700" rel="stylesheet" type="text/css">
    <link href="{{ asset('css/external.css') }}" rel="stylesheet">

    <script src="{{ asset('js/jquery.min.js') }}"></script>

    @yield('includes')

    @yield('style')
</head>
<body>
    <div class="external-form">
        @if(Setting::get('display-logo-on-login-page', true))
            <div class="logo-container">
                <img src="{{ asset(Setting::get('login-page-logo', 'images/logos/invex-logo.png')) }}" style="width: 100%;" />
            </div>
        @endif

        @if(Session::has('errors') && Session::get('errors') != null && count(Session::get('errors')) > 0)
            <div class="alert alert-danger">
                <ul class="errors">
                    @if( is_array(Session::get('errors')) )
                        @foreach(Session::get('errors') as $error)
                            <li>{{ $error }}</li>
                        @endforeach
                    @else
                        @foreach(Session::get('errors')->getBag('default')->all() as $error)
                            <li>{{ $error }}</li>
                        @endforeach
                    @endif
                </ul>
            </div>
        @elseif(Session::has('status') && Session::get('status') != null)
            <div class="alert alert-info">
                <ul>
                    <li>{{ Session::get('status') }}</li>
                </ul>
            </div>
        @endif

        @yield('content')
    </div>

    <div class="flex-container copyright-container">
        <span><?php echo date("Y") ?> &copy; {{ Setting::get('copyright') }} | All Rights Reserved | Powered By <a target="_blank" href="{{ Setting::get('powered-by-url', 'http://www.invexdesign.com') }}">{{ Setting::get('powered-by-name', 'Invex Design') }}</a></span>
    </div>

    @yield('javascript')
</body>
</html>