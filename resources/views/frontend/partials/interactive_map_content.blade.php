<div class="search-pane">
    <div class="search-parameter">
		<?php echo Form::text('search', null, ['id' => 'search_filter', 'placeholder' => 'Search By Keyword', 'class' => 'filter form-control']); ?>
    </div>
    <div class="search-parameter">
		<?php echo Form::select('service_ids[]', $service_options, null, ['id' => 'services_filter', 'multiple' => true, 'class' => 'filter select-two-custom form-control']); ?>
    </div>
    <div class="search-parameter">
		<?php echo Form::select('age_group_ids[]', $age_group_options, null, ['id' => 'age_group_filter', 'multiple' => true, 'class' => 'filter select-two-custom form-control']); ?>
    </div>
</div>
<div class="results-pane">
    <div id="map_view" class="map-view">
        @include('frontend.partials.map', compact('facilities', 'process_facility_click_url'))
    </div>
    <?php $facility_count = count($facilities); ?>
    <div class="result-counts">Showing <span id="active_results_count"><?php echo $facility_count; ?></span> of <span id="total_results_count"><?php echo $facility_count; ?></span> Resources</div>
</div>
<div class="lightboxes">
    @foreach($facilities as $facility)
        @include('frontend.partials.lightbox')
    @endforeach
</div>