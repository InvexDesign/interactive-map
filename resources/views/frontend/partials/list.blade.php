<ul>
    @foreach($facilities as $facility)
        <li id="facility_list_{{ $facility->id }}">{{ $facility->name }} ({{ $facility->type }}): {{ $facility->description }}</li>
    @endforeach
</ul>