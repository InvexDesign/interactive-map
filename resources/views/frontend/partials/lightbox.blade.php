<div id="lightbox_{{ $facility->id }}" class="lightbox">
    <div class="top">
        <h4 class="name">{{ $facility->name }}</h4>
        @if($facility->tagline)
            <span class="tagline">{{ $facility->tagline }}</span>
        @endif
    </div>
    <div class="middle">
        <div class="content">
            <div class="description">
                {!! $facility->description !!}
            </div>
            <div class="other">
                <ul class="fields">
                    @if($facility->organization)
                    <li>
                        <i class="fa fa-2x fa-building" aria-hidden="true"></i>
                        <strong>Organization</strong>
                        <span>{{ $facility->organization }}</span>
                    </li>
                    @endif
                    @if($facility->url)
                    <li>
                        <i class="fa fa-2x fa-globe" aria-hidden="true"></i>
                        <strong>URL</strong>
                        <a href="{{ $facility->url }}">{{ $facility->url }}</a>
                    </li>
                    @endif
                    @if($facility->website)
                    <li>
                        <i class="fa fa-2x fa-globe" aria-hidden="true"></i>
                        <strong>Website</strong>
                        <a href="{{ $facility->website }}">{{ $facility->website }}</a>
                    </li>
                    @endif
                    @if($facility->email)
                    <li>
                        <i class="fa fa-2x fa-envelope" aria-hidden="true"></i>
                        <strong>Email</strong>
                        <a href="mailto:{{ $facility->email }}">{{ $facility->email }}</a>
                    </li>
                    @endif
                    @if($facility->phone)
                    <li>
                        <i class="fa fa-2x fa-phone-square" aria-hidden="true"></i>
                        <strong>Phone</strong>
                        <a href="tel:{{ $facility->displayTel('phone') }}">{{ $facility->displayPhone('phone') }}</a>
                    </li>
                    @endif
                    @if($facility->getAttributeValue('247_hotline'))
                    <li>
                        <i class="fa fa-2x fa-life-ring" aria-hidden="true"></i>
                        <strong>24/7 Hotline</strong>
                        <a href="tel:{{ $facility->displayTel('247_hotline') }}">{{ $facility->displayPhone('247_hotline') }}</a>
                    </li>
                    @endif
                    @if($facility->text_line)
                    <li>
                        <i class="fa fa-2x fa-mobile" aria-hidden="true"></i>
                        <strong>Text Line</strong>
                        <span>{{ $facility->text_line }}</span>
                    </li>
                    @endif
                    @if($facility->live_chat)
                    <li>
                        <i class="fa fa-2x fa-commenting" aria-hidden="true"></i>
                        <strong>Live Chat</strong>
                        <span>{{ $facility->live_chat }}</span>
                    </li>
                    @endif
                    @if($facility->lgbtq_services)
                    <li>
                        <i class="fa fa-2x fa-life-ring" aria-hidden="true"></i>
                        <strong>LGBTQ+ Services</strong>
                        <span>{{ $facility->lgbtq_services }}</span>
                    </li>
                    @endif
                    @if($facility->contact)
                    <li>
                        <i class="fa fa-2x fa-user" aria-hidden="true"></i>
                        <strong>Contact</strong>
                        <span>{{ $facility->contact }}</span>
                    </li>
                    @endif
                </ul>
                @if($facility->youth_approved)
                    <img class="youth-approved-icon" src="{{ defined('LARAVEL_URL') ? LARAVEL_URL . '/images/youth-approved.png' : asset('images/youth-approved.png') }}" />
                @endif
            </div>
        </div>
        <div class="map">
            <iframe id="facility_map_{{ $facility->id }}"
                    width="600"
                    height="450"
                    frameborder="0" style="border: 2px solid white"
                    src="https://www.google.com/maps/embed/v1/place?key={{ Setting::get('google-maps-api-key') }}&q={{ $facility->displayGoogleMapsAddress() }}" allowfullscreen>
            </iframe>
        </div>
    </div>
    <div class="bottom">
        @if($facility->services()->count() > 0)
        <div class="section">
            <span class="title"><span>Services Offered</span></span>
            <div class="cloud">
                @foreach($facility->services as $service)
                    <span class="element">{{ $service->display() }}</span>
                @endforeach
            </div>
        </div>
        @endif
        @if($facility->ageGroups()->count() > 0)
        <div class="section">
            <span class="title"><span>Age Groups</span></span>
            <div class="cloud">
                @foreach($facility->ageGroups as $age_group)
                    <span class="element">{{ $age_group->display() }}</span>
                @endforeach
            </div>
        </div>
        @endif
        @if($facility->transitOptions()->count() > 0)
        <div class="section">
            <span class="title"><span>Parking / Transport</span></span>
            <div class="cloud">
                @foreach($facility->transitOptions as $transit_option)
                    <span class="element">{{ $transit_option->display() }}</span>
                @endforeach
            </div>
        </div>
        @endif
        @if($facility->insuranceTypes()->count() > 0)
        <div class="section">
            <span class="title"><span>Accepted Insurance</span></span>
            <div class="cloud">
                @foreach($facility->insuranceTypes as $insurance_type)
                    <span class="element">{{ $insurance_type->display() }}</span>
                @endforeach
            </div>
        </div>
        @endif
        @if($facility->appointments)
        <div class="section">
            <span class="title"><span>Appointments</span></span>
            <div class="content">
                {!! $facility->appointments !!}
            </div>
        </div>
        @endif
        @if($facility->minor_access)
        <div class="section">
            <span class="title"><span>Minor Access</span></span>
            <div class="content">
                {!! $facility->minor_access !!}
            </div>
        </div>
        @endif
        @if($facility->hours)
        <div class="section">
            <span class="title"><span>Hours</span></span>
            <div class="content">
                {!! $facility->hours !!}
            </div>
        </div>
        @endif
        @if($facility->languages()->count() > 0)
        <div class="section">
            <span class="title"><span>Languages Spoken</span></span>
            <div class="cloud">
                @foreach($facility->languages as $language)
                    <span class="element">{{ $language->display() }}</span>
                @endforeach
            </div>
        </div>
        @endif
    </div>
</div>