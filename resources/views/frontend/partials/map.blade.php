<div id="google_map" style="height: 400px; width: 100%;"></div>
<script>
    var FACILITIES = {};

    function closeOpenInfoWindow()
    {

    }

    function openInfoWindow(map, facility_id)
    {
        $.featherlight('#lightbox_' + facility_id, {});
    }

    function registerFacilityClick(facility_id)
    {
        var data = { facility_id: facility_id };
        var jqxhr = $.post("{{ $process_facility_click_url }}", data)
            .done(function(response)
            {

            }).fail(function(response)
            {
                console.log("FAILED to logged facility click!");
                console.error(response);
            });
    }

    function initializeMap() {
        var center = { lat: {{ Setting::get('initial-map-latitude') }}, lng: {{ Setting::get('initial-map-longitude') }} };
        var map = new google.maps.Map(document.getElementById('google_map'), {
            zoom: {{ Setting::get('initial-map-zoom') }},
            center: center,
            scrollwheel: false
        });

@foreach($facilities as $facility)
    @if($facility->latitude && $facility->longitude)
        FACILITIES[{{ $facility->id }}] = {};
        FACILITIES[{{ $facility->id }}].infowindow = new google.maps.InfoWindow({
            content: '<h4>{{ $facility->name }}</h4><p>{{ $facility->description }}</p>'
        });
        FACILITIES[{{ $facility->id }}].marker = new google.maps.Marker({
            position: {lat: {{ $facility->latitude }}, lng: {{ $facility->longitude }}},
            <?php $map_marker_icon = Setting::get('map-marker-icon', false); ?>
            @if($map_marker_icon)
            <?php $map_marker_icon = (substr($map_marker_icon, 0, 1) == '/') ? env('APP_URL', '') . $map_marker_icon : $map_marker_icon; ?>
            icon: '{{ $map_marker_icon }}',
            @endif
            title: '{{ $facility->name }}',
            map: map
        });

        FACILITIES[{{ $facility->id }}].marker.addListener('click', function() {
            registerFacilityClick({{ $facility->id }});
            openInfoWindow(map, {{ $facility->id }});
        });
    @else
        console.error("Facility #{{ $facility->id }} is missing lat/lng and will not be displayed on map!");
    @endif
@endforeach
        window.FACILITIES = FACILITIES;
    }
</script>
<script async defer src="https://maps.googleapis.com/maps/api/js?key={{ Setting::get('google-maps-api-key') }}&callback=initializeMap"></script>