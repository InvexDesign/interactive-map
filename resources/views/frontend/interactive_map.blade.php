@extends('templates.frontend')

@section('content')

    <script>
        function filterFacilities()
        {
            closeOpenInfoWindow();

            $('div.loader').show();

            var data = {
                search: $('#search_filter').val(),
                service_ids: $('#services_filter').val(),
                age_group_ids: $('#age_group_filter').val()
            };
            var jqxhr = $.post("{{ $filter_url }}", data)
                .done(function(response)
                {
                    var active_results_count = 0;
                    var total_results_count = 0;
                    var json = JSON.parse(response);
                    $.each(json.facilities, function(id, is_visible)
                    {
                        if(FACILITIES[id] && FACILITIES[id].marker)
                        {
                            FACILITIES[id].marker.setVisible(is_visible);
                            $('#facility_list_' + id).toggle(is_visible);

                            if(is_visible)
                            {
                                active_results_count++;
                            }

                            total_results_count++
                        }
                        else
                        {
                            console.error('No Marker Found!');
                            console.error('Facility #' + id);
                        }
                    });
                    $('#active_results_count').html(active_results_count);
                    $('#total_results_count').html(total_results_count);
                }).fail(function(response)
                {
                    console.error("ERROR!");
                    console.error(response);
                }).always(function()
                {
                    $('div.loader').hide();
                });
        }

        $(document).ready(function()
        {
            $('#services_filter').select2({
                placeholder: 'All Offered Services'
            });
            $('#age_group_filter').select2({
                placeholder: 'All Ages (Adult & Teen)'
            });

            $('.filter').change(function ()
            {
                filterFacilities();
            });
        });
    </script>

    @include('frontend.partials.interactive_map_content')

@stop