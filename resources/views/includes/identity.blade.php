@if(Session::has('absorb_customer_identity') && Session::get('absorb_customer_identity.customer_id') != null)
    <?php $customer_id = Session::get('absorb_customer_identity.customer_id'); ?>
    <?php $customer = \App\Models\Customer::find($customer_id); ?>
    <div class="alert alert-info" style="display: block; margin: 0; max-width: none;">
        <p style="text-align: center;">Current Customer Identity: {{ $customer ? $customer->display() : 'MALFORMED IDENTITY' }} - {!! link_to_route('backend.identity.customer.surrender', 'Click here') !!} to return to your original account.</p>
    </div>
@endif