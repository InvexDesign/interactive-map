<?php $label = isset($label) ? $label : 'Image'; ?>
<?php $name = isset($name) ? $name : 'image'; ?>
<?php $preview_id = isset($preview_id) ? $preview_id : 'image_preview'; ?>
<div class="col-xs-6">
    {!! Form::label($name, $label) !!}
    <div class="input-group">
            <span class="input-group-btn">
                <span class="btn btn-primary btn-file">
                    Browse… {!! Form::file($name, ['onchange' => 'readURL(this, "' . $preview_id .'");']) !!}
                </span>
            </span>
        <input id="for_{{ $name }}" type="text" class="form-control" readonly="">
    </div>
    {!! $errors->first($name, '<span class="error">:message</span>') !!}
</div>

@if(isset($image_resource_path))
<div class="col-xs-3">
    <h4>Current Image</h4>
    <img src="{{ asset($image_resource_path) }}" width="250" />
</div>
@endif

<div class="col-xs-3">
    <h4>Preview</h4>
    <img id="{{ $preview_id }}" src="" width="250" />
</div>
<div class="clearer"></div>