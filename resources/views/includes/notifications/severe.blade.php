<?php $severe_notifications = \App\Models\Notifications\Notification::active()->severe()->get(); ?>
@if(count($severe_notifications) > 0)
    <div class="alert alert-danger">
        <h4>Severe Notifications</h4>
        <ul class="errors">
            @foreach($severe_notifications as $severe_notification)
                <li>{!! $severe_notification->display() !!} {!! link_to_route('notifications.dismiss', 'Dismiss', ['id' => $severe_notification->id]) !!}</li>
            @endforeach
        </ul>
    </div>
@endif