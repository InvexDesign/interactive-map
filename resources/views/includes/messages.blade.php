@if(Session::has('messages') && Session::get('messages') != null && count(Session::get('messages')) > 0)
    <div class="alert alert-info session">
        <h3>Hark! Success!</h3>
        <ul class="messages">
            @foreach(Session::get('messages') as $message)
                <li>{{ $message }}</li>
            @endforeach
        </ul>
    </div>
@elseif(isset($messages) && $messages != null && count($messages) > 0)
    <div class="alert alert-info variable">
        <h3>Hark! Success!</h3>
        <ul class="messages">
            @foreach($messages as $message)
                <li>{{ $message }}</li>
            @endforeach
        </ul>
    </div>
@endif

@if(Session::has('errors') && Session::get('errors') != null && count(Session::get('errors')) > 0)
    <div class="alert alert-danger session">
        <h3>Hark! An Error!</h3>
        <ul class="errors">
            @if( is_array(Session::get('errors')) )
                @foreach(Session::get('errors') as $error)
                    <li>{{ $error }}</li>
                @endforeach
            @else
                @foreach(Session::get('errors')->getBag('default')->all() as $error)
                    <li>{{ $error }}</li>
                @endforeach
            @endif
        </ul>
    </div>
@elseif(isset($errors) && $errors != null && count($errors) > 0)
    <div class="alert alert-danger variable">
        <h3>Hark! An Error!</h3>
        <ul class="errors">
            @foreach($errors as $error)
                <li>{{ $error }}</li>
            @endforeach
        </ul>
    </div>
@endif