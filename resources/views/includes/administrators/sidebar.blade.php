{{--<div class="shutter"></div>--}}
<div class="head">
    <div class="logo">
        <a class="large" href="{{ route('backend.dashboard.index') }}">
            {{--            <img style="width: 176px; height: 75px;" src="{{ asset('images/logos/sidebar.svg') }}" />--}}
            <img style="width: 176px; height: 75px;" src="{{ asset('images/logos/invex-v.svg') }}" />
            <span class="title">{{ App\Models\Setting::get('site-title', 'Admin Center') }}</span>
        </a>
        <a class="small" href="{{ route('backend.dashboard.index') }}">
            <img src="{{ asset('images/logos/sidebar-small.png') }}" />
            <span class="title">{{ App\Models\Setting::get('site-title', 'Admin Center') }}</span>
        </a>
    </div>
    <div class="user">
        <img class="icon" src="{{ Auth::user()->getGravatarImageUrl() }}" />
        <div class="details">
            <span class="username">{{ Auth::user()->username }}</span>
            <span class="role">{{ Auth::user()->getRole()->display() }}</span>
        </div>
    </div>
</div>
<ul class="navigation sidebar-nav">
    <li class="heading">Manage</li>
    <li><a href="{{ route('backend.dashboard.index') }}"><i class="fa fa-dashboard icon"></i><span>Dashboard</span></a>
    </li>
    <li class="dropdown">
        <a href="#" data-toggle="dropdown" class="dropdown-toggle"><i class="fa fa-language icon"></i><span>Languages<b
                        class="caret"></b></span></a>
        <ul class="dropdown-menu">
            <li class="heading">Languages</li>
            <li><a href="{{ route('backend.languages.index') }}">View All</a></li>
            <li><a href="{{ route('backend.languages.create.get') }}">Create New</a></li>
        </ul>
    </li>
    <li class="dropdown">
        <a href="#" data-toggle="dropdown" class="dropdown-toggle"><i class="fa fa-cubes icon"></i><span>Services<b
                        class="caret"></b></span></a>
        <ul class="dropdown-menu">
            <li class="heading">Services</li>
            <li><a href="{{ route('backend.services.index') }}">View All</a></li>
            <li><a href="{{ route('backend.services.create.get') }}">Create New</a></li>
        </ul>
    </li>
    <li class="dropdown">
        <a href="#" data-toggle="dropdown" class="dropdown-toggle"><i class="fa fa-bank icon"></i><span>Facilities<b
                        class="caret"></b></span></a>
        <ul class="dropdown-menu">
            <li class="heading">Facilities</li>
            <li><a href="{{ route('backend.facilities.index') }}">View All</a></li>
            <li><a href="{{ route('backend.facilities.create.get') }}">Create New</a></li>
            <li class="has-top-border"><a href="{{ route('backend.facility_types.index') }}">Facility Types</a></li>
            <li><a href="{{ route('backend.insurance_types.index') }}">Insurance Types</a></li>
            <li><a href="{{ route('backend.transit_options.index') }}">Transit Options</a></li>
        </ul>
    </li>
    <li class="heading">Analytics</li>
    <li class="dropdown">
        <a href="#" data-toggle="dropdown" class="dropdown-toggle"><i class="fa fa-pie-chart icon"></i><span>Service Searches<b
                        class="caret"></b></span></a>
        <ul class="dropdown-menu">
            <li class="heading">Service Searches</li>
            <li><a href="{{ route('backend.analytics.services.timeline') }}">Timeline</a></li>
            <li><a href="{{ route('backend.analytics.services.monthly') }}">Monthly</a></li>
        </ul>
    </li>
    <li class="dropdown">
        <a href="#" data-toggle="dropdown" class="dropdown-toggle"><i class="fa fa-bar-chart icon"></i><span>Facility Clicks<b
                        class="caret"></b></span></a>
        <ul class="dropdown-menu">
            <li class="heading">Facility Clicks</li>
            <li><a href="{{ route('backend.analytics.facilities.timeline') }}">Timeline</a></li>
            <li><a href="{{ route('backend.analytics.facilities.monthly') }}">Monthly</a></li>
        </ul>
    </li>
    <li class="heading">System</li>
    <li class="dropdown">
        <a href="#" data-toggle="dropdown" class="dropdown-toggle"><i class="fa fa-lock icon"></i><span>Login Attempts<b
                        class="caret"></b></span></a>
        <ul class="dropdown-menu">
            <li class="heading">Analytics</li>
            <li><a href="{{ route('backend.login_attempts.index') }}">View All</a></li>
            <li><a href="{{ route('backend.login_attempts.timeline') }}">Timeline</a></li>
        </ul>
    </li>
</ul>