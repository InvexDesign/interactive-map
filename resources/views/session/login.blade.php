@extends('templates.external')

@section('content')

    @if(isset($title))
        <h3>{{ $title }}</h3>
    @endif

    {!! Form::open(['route' => $form_route]) !!}

        <div class="form-group has-input-icon">
            {!! Form::label('username', 'Username') !!}
            {!! Form::text('username', null, ['autofocus', 'class'=>'form-control has-input-icon']) !!}
            <span class="input-icon"><img src="{{ asset('images/user.png') }}" /></span>
        </div>
        <br />
        <div class="form-group has-input-icon">
            {!! Form::label('password', 'Password') !!}
            {!! Form::password('password', ['class'=>'form-control has-input-icon']) !!}
            <span class="input-icon has-action" onclick="togglePasswordVisibility();" title="Click to show/hide password"><img id="eye_image" src="{{ asset('images/eye.png') }}" /></span>
        </div>
        <br />
        <div class="flex-container form-group">
            <label style="flex: 1;"><input type="checkbox" name="remember"> Remember Me</label>
            {!! Form::submit('Log In', ['class' => 'btn btn-primary', 'style' => 'flex: 1;']) !!}
        </div>

        <div class="forgot-password-container">
            {!! link_to_route('password.reset.email.get', 'Forgot your password? No worries.') !!}
        </div>

    {!! Form::close() !!}

<script>
    function togglePasswordVisibility()
    {
        var image = document.getElementById('eye_image');
        var password = document.getElementById('password');
        switch(password.type)
        {
            case 'password':
                password.type = 'text';
                image.style.opacity = 0.6;
                break;
            case 'text':
                password.type = 'password';
                image.style.opacity = 1;
                break;
        }
    }
</script>
@stop