<?php namespace App\Helpers;


use App\Models\Billing\Services\BraintreeBillingService;
use App\Models\Billing\Services\StripeBillingService;
use App\Models\DeliveryOptions\DeliveryOption;
use App\Models\Kitchen;
use Illuminate\Validation\Validator;

class CustomValidator extends Validator
{
	public function validateExistsOrZero($attribute, $value, $parameters)
	{
		if($value == 0)
		{
			return true;
		}
		else
		{
			return $this->validateExists($attribute, $value, $parameters);
		}
	}

	public function validateStringIntBetween($attribute, $value, $parameters)
	{
		$this->requireParameterCount(2, $parameters, 'string_int_between');

		$int_value = intval($value);
		$min = intval($parameters[0]);
		$max = intval($parameters[1]);

		return $min <= $int_value && $int_value <= $max;
	}

	public function validateExistingStripeCustomerId($attribute, $value, $parameters)
	{
		$result = StripeBillingService::getCustomer($value);

		return $result->success === true;
	}

	public function validateExistingBraintreeCustomerId($attribute, $value, $parameters)
	{
		$result = BraintreeBillingService::getCustomer($value);

		return $result->success === true;
	}

	public function validateIsServiceable($attribute, $value, $parameters)
	{
		$delivery_option = DeliveryOption::assignFromZipcode($value);

		return $delivery_option !== null;
	}
	
	public function validateUrlArray($attribute, $value, $parameters)
	{
		foreach($value as $url)
		{
			if(!$this->validateUrl($attribute, $url))
			{
				return false;
			}
		}

		return true;
	}
}