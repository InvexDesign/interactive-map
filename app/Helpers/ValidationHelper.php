<?php
/**
 * Created by PhpStorm.
 * User: asittwo
 * Date: 7/15/2015
 * Time: 8:35 AM
 */

namespace App\Helpers;


class ValidationHelper
{
	public static function generateArrayInputRules($rules, $count)
	{
		$all_rules = [];
		for($i = 0; $i < $count; $i++)
		{
			foreach($rules as $key => $rule)
			{
				$all_rules['items.' . $i . '.' . $key] = $rule;
			}
		}

		return $all_rules;
	}
}