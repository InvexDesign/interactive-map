<?php
/**
 * Created by PhpStorm.
 * User: asittwo
 * Date: 7/15/2015
 * Time: 8:35 AM
 */

namespace App\Helpers;

use Fetcher;
use FetchedDataURL;

class ViewFetcher extends Fetcher
{
	var $_content;

	function ViewFetcher($html)
	{
		$this->_content = $html;
	}

	function get_data($dummy1)
	{
		return new FetchedDataURL($this->_content, [], "");
	}

	function get_base_url()
	{
		return "";
	}
}