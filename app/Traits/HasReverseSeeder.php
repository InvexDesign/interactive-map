<?php namespace App\Traits;


use Helper;
use DB;

trait HasReverseSeeder
{
	public function getAllAttributes()
	{
		return array_merge(['id'], $this->fillable);
	}

	public function getReverseSeedAttributeAssignment($attribute, $exclusions = [])
	{
		if(in_array($attribute, $exclusions))
		{
			return '';
		}

		$type = DB::connection()->getDoctrineColumn($this->table, $attribute)->getType()->getName();
		$value = $this->getOriginal($attribute);

//		if($attribute == 'phone')
//		{
//			$value = preg_replace("/([^\d])/",'', $value);
//		}
		$output = "'$attribute' => ";
		switch($type)
		{
			case 'integer':
			case 'decimal':
				$output .= (($value != null) ? $value : 'null') . ",<br />";
				break;
			case 'boolean':
				$output .= (($value != null) ? ($value ? 'true' : 'false') : 'null') . ",<br />";
				break;
			case 'string':
			case 'text':
				$output .= (($value != null) ? "'" . Helper::escapeNonAlphaNum(htmlentities($value)) . "'" : 'null') . ",<br />";
				break;
			default:
				$output .= (($value != null) ? "'" . $value . "'" : 'null') . ",<br />";
				break;
		}

		return $output;
	}

	public function getReverseSeedPivot($attribute)
	{
		$output = "Facility::find(\$id)->$attribute()->sync([";
		foreach($this->$attribute as $model)
		{
			$output .= $model->id . ",";
		}
		$output .= "]);<br />";

		return $output;
	}

	public function getReverseSeedModel($parameters = [])
	{
		$defaults = [
			'pivots'    => [],
			'exclusions'    => [],
		];
		$parameters = $parameters + $defaults;

		$attributes = $this->getAllAttributes();
		$output = "\$id = DB::table('" . $this->table . "')->insertGetId([<br />";
		foreach($attributes as $attribute)
		{
			$output .= $this->getReverseSeedAttributeAssignment($attribute, $parameters['exclusions']);
		}
		$output .= "]);<br />";

		foreach($parameters['pivots'] as $pivot)
		{
			$output .= $this->getReverseSeedPivot($pivot);
		}

		return $output;
	}
}