<?php namespace App\Providers;

use App\Helpers\CustomFormBuilder;
use Collective\Html\HtmlServiceProvider;

class CustomFormBuilderServiceProvider extends HtmlServiceProvider
{
	/**
	 * Indicates if loading of the provider is deferred.
	 *
	 * @var bool
	 */
	protected $defer = true;

	/**
	 * Register the service provider.
	 *
	 * @return void
	 */
	public function registerFormBuilder()
	{
		$this->app->singleton('form', function($app)
		{
			$form = new CustomFormBuilder($app['html'], $app['url'], $app['view'], $app['session.store']->getToken());

			return $form->setSessionStore($app['session.store']);
		});
	}
}
