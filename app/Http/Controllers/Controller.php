<?php

namespace App\Http\Controllers;

use Carbon;
use Exception;
use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use Input;
use Log;

class Controller extends BaseController
{
    use AuthorizesRequests, DispatchesJobs, ValidatesRequests;

	protected function getDate($default = null)
	{
		if($string = Input::get('date'))
		{
			try
			{
				$date = Carbon::createFromFormat('m-d-Y', $string);
			}
			catch(Exception $e)
			{
				Log::error($e);
				Log::error('Controller: Error parsing date');

				return null;
			}
		}
		else
		{
			$date = $default ?: Carbon::today();
		}

		return $date;
	}

	protected function getDateRange($start_default = null, $end_default = null)
	{
		$_start = Input::get('start', false);
		$_end = Input::get('end', false);

		if($_start && $_end)
		{
			$end_date_is_inclusive = true;

			try
			{
				$start_at = Carbon::createFromFormat('m-d-Y', $_start);
				$end_at = Carbon::createFromFormat('m-d-Y', $_end);

				if($end_date_is_inclusive)
				{
					$end_at = $end_at->addHours(23)->addMinutes(59)->addSeconds(59);
				}
			}
			catch(Exception $e)
			{
				Log::error($e);
				Log::error('Controller: Error parsing start/end dates');

				return null;
			}
		}
		else
		{
			$start_at = $start_default ?: Carbon::today()->firstOfMonth();
			$end_at = $end_default ?: Carbon::today();
		}

		return [$start_at, $end_at];
	}
}
