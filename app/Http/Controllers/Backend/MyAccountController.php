<?php namespace App\Http\Controllers\Backend;

use App\Events\PasswordWasUpdated;
use App\Http\Controllers\Controller;
use App\Http\Requests\Backend\MyAccount\PostChangePasswordRequest;
use App\Http\Requests\Backend\MyAccount\PostEditMyAccountRequest;
use App\Http\Requests\ChangePasswordRequest;
use App\Http\Requests\UpdateUserRequest;
use Carbon\Carbon;
use Event;
use Illuminate\Support\Facades\Redirect;

class MyAccountController extends Controller
{
	function __construct()
	{
		$this->middleware('auth');
		$this->middleware('require-role:' . ADMINISTRATORS);
	}

	public static function composeViews()
	{

	}

	public function show()
	{
		$user = \Auth::user();
		$vars = compact('user');

		return view('backend.my_account.show', $vars);
	}

	public function getEdit()
	{
		$user = \Auth::user();
		$vars = compact('user');

		return view('backend.my_account.edit', $vars);
	}

	public function postEdit(PostEditMyAccountRequest $request)
	{
		$user = $request->user;
		$user->fill($request->only(['username', 'email', 'first_name', 'last_name']));
		$user->save();

		return Redirect::route('backend.my_account.edit.get')->with('messages', ['Your account was successfully updated!']);
	}
	
	public function getChangePassword()
	{
		$vars = ['check_current_password' => \Auth::user()->requiresCurrentPasswordToUpdate()];

		return view('backend.my_account.change_password', $vars);
	}

	public function postChangePassword(PostChangePasswordRequest $request)
	{
		$user = \Auth::user();
		$user->password = $request->password;
		$user->password_updated_at = Carbon::now();
		$user->save();

		return Redirect::route('backend.my_account.show')->with('messages', ['Your password was successfully updated!']);
	}

}