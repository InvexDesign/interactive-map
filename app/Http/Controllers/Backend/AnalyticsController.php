<?php namespace App\Http\Controllers\Backend;

use App\Http\Controllers\Controller;
use App\Models\MonthlyFacilityClickRecord;
use App\Models\MonthlyServiceSearchRecord;
use Carbon;

class AnalyticsController extends Controller
{
	function __construct()
	{
		$this->middleware('auth');
		$this->middleware('require-role:' . ADMINISTRATORS);
	}

	public function facilityClickTimeline()
	{
		$faker = \Faker\Factory::create();
		$facility_ids = \Input::get('ids', false);
		$facility_ids = $facility_ids ? explode(',', $facility_ids) : false;
		list($start_at, $end_at) = $this->getDateRange(Carbon::today()->startOfMonth()->subMonths(6), Carbon::today()->startOfMonth());

		$data = [
			'labels'   => [],
			'datasets' => []
		];

		$records = MonthlyFacilityClickRecord::query()->whereBetween('month', [
			$start_at,
			$end_at
		])->orderBy('month', 'asc')->get();

		foreach($records as $record)
		{
			$formatted_month = $record->month->format('m/Y');
			if(!isset($data['labels'][$formatted_month]))
			{
				$data['labels'][$formatted_month] = $formatted_month;
			}

			$facility = $record->facility;

			if(!isset($data['datasets'][$facility->id]))
			{
				$data['datasets'][$facility->id] = [
					'label'       => $facility->name,
					'borderColor' => $faker->hexColor,
					'fill'        => false,
					'data'        => [],
					'hidden'      => $facility_ids ? !in_array($facility->id, $facility_ids) : false
				];
			}

			$data['datasets'][$facility->id]['data'][] = $record->hits;
		}

		$title = 'Facility Clicks Timeline';
		$vars = compact(
			'start_at',
			'end_at',
			'data',
			'title'
		);

		return view('backend.analytics.facility_clicks_timeline', $vars);
	}

	public function facilityClickMonthly()
	{
		$faker = \Faker\Factory::create();
		$date = $this->getDate(Carbon::today());
		$start_at = $date->copy()->startOfMonth();
		$end_at = $start_at->copy()->endOfMonth();

		$data = [
			'labels'   => [],
			'datasets' => [
				'label'           => "Number of Clicks",
				'backgroundColor' => [],
				'data'            => []
			]
		];

		$records = MonthlyFacilityClickRecord::query()->whereBetween('month', [
			$start_at,
			$end_at
		])->orderBy('facility_id', 'asc')->get();

		foreach($records as $record)
		{
			$facility = $record->facility;

			$name = $facility->name;
			if(!isset($data['labels'][$name]))
			{
				$data['labels'][$name] = $name;
			}

			$data['datasets']['backgroundColor'][] = $faker->hexColor;
			$data['datasets']['data'][] = $record->hits;
		}

		$title = 'Facility Clicks Monthly';
		$vars = compact(
			'date',
			'start_at',
			'end_at',
			'data',
			'title'
		);

		return view('backend.analytics.facility_clicks_monthly', $vars);
	}

	public function serviceSearchTimeline()
	{
		$faker = \Faker\Factory::create();
		$service_ids = \Input::get('ids', false);
		$service_ids = $service_ids ? explode(',', $service_ids) : false;
		list($start_at, $end_at) = $this->getDateRange(Carbon::today()->startOfMonth()->subMonths(6), Carbon::today()->startOfMonth());

		$data = [
			'labels'   => [],
			'datasets' => []
		];

		$records = MonthlyServiceSearchRecord::query()->whereBetween('month', [
			$start_at,
			$end_at
		])->orderBy('month', 'asc')->get();

		foreach($records as $record)
		{
			$formatted_month = $record->month->format('m/Y');
			if(!isset($data['labels'][$formatted_month]))
			{
				$data['labels'][$formatted_month] = $formatted_month;
			}

			$service = $record->service;

			if(!isset($data['datasets'][$service->id]))
			{
				$data['datasets'][$service->id] = [
					'label'       => $service->name,
					'borderColor' => $faker->hexColor,
					'fill'        => false,
					'data'        => [],
					'hidden'      => $service_ids ? !in_array($service->id, $service_ids) : false
				];
			}

			$data['datasets'][$service->id]['data'][] = $record->hits;
		}

		$title = 'Service Searches Timeline';
		$vars = compact(
			'start_at',
			'end_at',
			'data',
			'title'
		);

		return view('backend.analytics.service_searches_timeline', $vars);
	}

	public function serviceSearchMonthly()
	{
		$faker = \Faker\Factory::create();
		$date = $this->getDate(Carbon::today());
		$start_at = $date->copy()->startOfMonth();
		$end_at = $start_at->copy()->endOfMonth();

		$data = [
			'labels'   => [],
			'datasets' => [
				'label'           => "Number of Searches",
				'backgroundColor' => [],
				'data'            => []
			]
		];

		$records = MonthlyServiceSearchRecord::query()->whereBetween('month', [
			$start_at,
			$end_at
		])->orderBy('service_id', 'asc')->get();

		foreach($records as $record)
		{
			$service = $record->service;

			$name = $service->name;
			if(!isset($data['labels'][$name]))
			{
				$data['labels'][$name] = $name;
			}

			$data['datasets']['backgroundColor'][] = $faker->hexColor;
			$data['datasets']['data'][] = $record->hits;
		}

		$title = 'Service Searches Monthly';
		$vars = compact(
			'date',
			'start_at',
			'end_at',
			'data',
			'title'
		);

		return view('backend.analytics.facility_clicks_monthly', $vars);
	}
}