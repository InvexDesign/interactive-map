<?php namespace App\Http\Controllers\Backend;

use App\Http\Controllers\Controller;
use App\Http\Requests\Backend\DestroyLanguageRequest;
use App\Http\Requests\Backend\PostCreateLanguageRequest;
use App\Http\Requests\Backend\PostEditLanguageRequest;
use App\Models\Language;
use Illuminate\Support\Facades\Redirect;

class LanguageController extends Controller
{
	function __construct()
	{
		$this->middleware('auth');
		$this->middleware('require-role:' . ADMINISTRATORS);
	}

	public function index()
	{
		$languages = Language::query()->get();
		$vars = compact('languages');

		return view('backend.languages.index', $vars);
	}

	public function getCreate()
	{
		$vars = [];

		return view('backend.languages.create', $vars);
	}

	public function postCreate(PostCreateLanguageRequest $request)
	{
		$language = Language::create($request->all());

		return Redirect::route('backend.languages.show', $language->id)->with('messages', ['Language was successfully added!']);
	}

	public function show($id)
	{
		$language = Language::findOrFail($id);
		$vars = compact('language');

		return view('backend.languages.show', $vars);
	}

	public function getEdit($id)
	{
	    $language = Language::findOrFail($id);
	    $vars = compact('language');
	    
	    return view('backend.languages.edit', $vars);
	}

	public function postEdit(PostEditLanguageRequest $request, $id)
	{
		$language = $request->language;
		$language->fill($request->all());
		$language->save();

		return Redirect::route('backend.languages.edit.get', $id)->with('messages', ['Language was successfully updated!']);
	}

	public function destroy(DestroyLanguageRequest $request, $id)
	{
		$language = $request->language;

		if($language->facilities()->count() > 0)
		{
			return Redirect::back()->with('errors', ['Language cannot be deleted because it is assigned to one or more Facility!']);
		}

		$language->delete();

		$message = "Language #$id successfully deleted!";

		return Redirect::route('backend.languages.index')->with('messages', [$message]);
	}
}