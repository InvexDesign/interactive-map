<?php namespace App\Http\Controllers\Backend;

use App\Http\Controllers\Controller;
use App\Http\Requests\Backend\DestroyServiceRequest;
use App\Http\Requests\Backend\PostCreateServiceRequest;
use App\Http\Requests\Backend\PostEditServiceRequest;
use App\Models\Service;
use Illuminate\Support\Facades\Redirect;

class ServiceController extends Controller
{
	function __construct()
	{
		$this->middleware('auth');
		$this->middleware('require-role:' . ADMINISTRATORS);
	}

	public static function composeViews()
	{
		view()->composer('backend.services.partials.form', function ($view)
		{
//			if(isset($view->getData()['service']))
//			{
//				$service = $view->getData()['service'];
//			}
		});
	}

	public function index()
	{
		$services = Service::query()->get();
		$vars = compact('services');

		return view('backend.services.index', $vars);
	}

	public function getCreate()
	{
		$vars = [];

		return view('backend.services.create', $vars);
	}

	public function postCreate(PostCreateServiceRequest $request)
	{
		$service = Service::create($request->all());

		return Redirect::route('backend.services.show', $service->id)->with('messages', ['Service was successfully created!']);
	}

	public function show($id)
	{
		$service = Service::findOrFail($id);
		$vars = compact('service');

		return view('backend.services.show', $vars);
	}

	public function getEdit($id)
	{
	    $service = Service::findOrFail($id);
	    $vars = compact('service');
	    
	    return view('backend.services.edit', $vars);
	}

	public function postEdit(PostEditServiceRequest $request, $id)
	{
		$service = $request->service;
		$service->fill($request->all());
		$service->save();

		return Redirect::route('backend.services.edit.get', $id)->with('messages', ['Service was successfully updated!']);
	}

	public function destroy(DestroyServiceRequest $request, $id)
	{
		$service = $request->service;
		$service->delete();

		$message = "Service #$id successfully deleted!";

		return Redirect::route('backend.services.index')->with('messages', [$message]);
	}

	public function export()
	{
		foreach(Service::all() as $service)
		{
			echo $service->getReverseSeedModel();
			echo "<br />";
		}
	}
}