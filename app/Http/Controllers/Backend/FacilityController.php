<?php namespace App\Http\Controllers\Backend;

use App\Helpers\Helper;
use App\Http\Controllers\Controller;
use App\Http\Requests\Backend\Facility\DestroyFacilityRequest;
use App\Http\Requests\Backend\Facility\PostCreateFacilityRequest;
use App\Http\Requests\Backend\Facility\PostEditFacilityRequest;
use App\Models\Facility;
use App\Models\Language;
use App\Models\Options\AgeGroup;
use App\Models\Options\FacilityType;
use App\Models\Options\InsuranceType;
use App\Models\Options\TransitOption;
use App\Models\Service;
use App\Models\State;
use Illuminate\Support\Facades\Redirect;

class FacilityController extends Controller
{
	function __construct()
	{
		$this->middleware('auth');
		$this->middleware('require-role:' . ADMINISTRATORS);
	}

	public static function composeViews()
	{
		view()->composer('backend.facilities.partials.form', function ($view)
		{
			if(isset($view->getData()['facility']))
			{
				$facility = $view->getData()['facility'];
				$view->with('initial_service_ids', $facility->services->pluck('id')->all());
				$view->with('initial_language_ids', $facility->languages->pluck('id')->all());
				$view->with('initial_insurance_type_ids', $facility->insuranceTypes->pluck('id')->all());
				$view->with('initial_transit_option_ids', $facility->transitOptions->pluck('id')->all());
				$view->with('initial_age_group_ids', $facility->ageGroups->pluck('id')->all());
			}

			$view->with('service_options', Service::orderBy('name', 'asc')->get()->pluck('name', 'id')->all());
			$view->with('language_options', Language::orderBy('name', 'asc')->get()->pluck('name', 'id')->all());
			$view->with('insurance_type_options', InsuranceType::orderBy('name', 'asc')->get()->pluck('name', 'id')->all());
			$view->with('transit_options', TransitOption::orderBy('name', 'asc')->get()->pluck('name', 'id')->all());
			$view->with('state_options', State::orderBy('name', 'asc')->get()->pluck('short_name', 'id')->all());
			$view->with('age_group_options', AgeGroup::orderBy('name', 'asc')->get()->pluck('name', 'id')->all());
		});
	}

	public function index()
	{
		$facilities = Facility::all();
		$vars = compact('facilities');

		return view('backend.facilities.index', $vars);
	}

	public function getCreate()
	{
		$vars = ['auto_gps' => true];

		return view('backend.facilities.create', $vars);
	}

	public function postCreate(PostCreateFacilityRequest $request)
	{
		$facility = Facility::create($request->all());

		$facility->services()->sync($request->get('service_ids', []));
		$facility->languages()->sync($request->get('language_ids', []));
		$facility->insuranceTypes()->sync($request->get('insurance_type_ids', []));
		$facility->transitOptions()->sync($request->get('transit_option_ids', []));
		$facility->ageGroups()->sync($request->get('age_group_ids', []));

		return Redirect::route('backend.facilities.show', $facility->id)
					   ->with('messages', ['Facility was successfully created!']);
	}

	public function show($id)
	{
		$facility = Facility::findOrFail($id);
		$vars = compact('facility');

		return view('backend.facilities.show', $vars);
	}

	public function getDuplicate($id)
	{
	    $facility = Facility::findOrFail($id);
	    $vars = compact('facility');
	    
	    return view('backend.facilities.duplicate', $vars);
	}

	public function getEdit($id)
	{
	    $facility = Facility::findOrFail($id);
	    $vars = compact('facility');

	    return view('backend.facilities.edit', $vars);
	}

	public function postEdit(PostEditFacilityRequest $request, $id)
	{
		$facility = $request->facility;
		$facility->fill($request->all());
		$facility->save();

		$facility->services()->sync($request->get('service_ids', []));
		$facility->languages()->sync($request->get('language_ids', []));
		$facility->insuranceTypes()->sync($request->get('insurance_type_ids', []));
		$facility->transitOptions()->sync($request->get('transit_option_ids', []));
		$facility->ageGroups()->sync($request->get('age_group_ids', []));

		$message = 'Facility was successfully updated!';

		return Redirect::route('backend.facilities.edit.get', $id)->with('messages', [$message]);
	}

	public function destroy(DestroyFacilityRequest $request, $id)
	{
		$facility = $request->facility;

		$facility->services()->sync([]);
		$facility->languages()->sync([]);
		$facility->insuranceTypes()->sync([]);
		$facility->transitOptions()->sync([]);
		$facility->ageGroups()->sync([]);

		$facility->delete();

		$message = 'Facility (ID: ' . $id . ') successfully deleted!';

		return Redirect::route('backend.facilities.index')->with('messages', [$message]);
	}

	public function export()
	{
		foreach(Facility::all() as $facility)
		{
			echo $facility->getReverseSeedModel(['pivots' => ['services', 'languages', 'insuranceTypes', 'transitOptions', 'ageGroups']]);
			echo "<br />";
		}
	}
}