<?php namespace App\Http\Controllers\Backend;

use App\Http\Controllers\Controller;
use App\Http\Requests\Backend\DestroyInsuranceTypeRequest;
use App\Http\Requests\Backend\PostCreateInsuranceTypeRequest;
use App\Http\Requests\Backend\PostEditInsuranceTypeRequest;
use App\Models\Options\InsuranceType;
use Illuminate\Support\Facades\Redirect;

class InsuranceTypeController extends Controller
{
	function __construct()
	{
		$this->middleware('auth');
		$this->middleware('require-role:' . ADMINISTRATORS);
	}

	public function index()
	{
		$insurance_types = InsuranceType::query()->get();
		$vars = compact('insurance_types');

		return view('backend.insurance_types.index', $vars);
	}

	public function getCreate()
	{
		$vars = [];

		return view('backend.insurance_types.create', $vars);
	}

	public function postCreate(PostCreateInsuranceTypeRequest $request)
	{
		$insurance_type = InsuranceType::create($request->all());

		return Redirect::route('backend.insurance_types.show', $insurance_type->id)->with('messages', ['InsuranceType was successfully added!']);
	}

	public function show($id)
	{
		$insurance_type = InsuranceType::findOrFail($id);
		$vars = compact('insurance_type');

		return view('backend.insurance_types.show', $vars);
	}

	public function getEdit($id)
	{
	    $insurance_type = InsuranceType::findOrFail($id);
	    $vars = compact('insurance_type');
	    
	    return view('backend.insurance_types.edit', $vars);
	}

	public function postEdit(PostEditInsuranceTypeRequest $request, $id)
	{
		$insurance_type = $request->insurance_type;
		$insurance_type->fill($request->all());
		$insurance_type->save();

		return Redirect::route('backend.insurance_types.edit.get', $id)->with('messages', ['InsuranceType was successfully updated!']);
	}

	public function destroy(DestroyInsuranceTypeRequest $request, $id)
	{
		$insurance_type = $request->insurance_type;

		if($insurance_type->facilities()->count() > 0)
		{
			return Redirect::back()->with('errors', ['InsuranceType cannot be deleted because it is assigned to one or more Facility!']);
		}

		$insurance_type->delete();

		$message = "InsuranceType #$id successfully deleted!";

		return Redirect::route('backend.insurance_types.index')->with('messages', [$message]);
	}
}