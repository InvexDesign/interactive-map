<?php namespace App\Http\Controllers\Backend;

use App\Http\Controllers\Controller;
use App\Http\Requests\Backend\DestroyFacilityTypeRequest;
use App\Http\Requests\Backend\PostCreateFacilityTypeRequest;
use App\Http\Requests\Backend\PostEditFacilityTypeRequest;
use App\Models\Options\FacilityType;
use Illuminate\Support\Facades\Redirect;

class FacilityTypeController extends Controller
{
	function __construct()
	{
		$this->middleware('auth');
		$this->middleware('require-role:' . ADMINISTRATORS);
	}

	public function index()
	{
		$facility_types = FacilityType::query()->get();
		$vars = compact('facility_types');

		return view('backend.facility_types.index', $vars);
	}

	public function getCreate()
	{
		$vars = [];

		return view('backend.facility_types.create', $vars);
	}

	public function postCreate(PostCreateFacilityTypeRequest $request)
	{
		$facility_type = FacilityType::create($request->all());

		return Redirect::route('backend.facility_types.show', $facility_type->id)->with('messages', ['FacilityType was successfully added!']);
	}

	public function show($id)
	{
		$facility_type = FacilityType::findOrFail($id);
		$vars = compact('facility_type');

		return view('backend.facility_types.show', $vars);
	}

	public function getEdit($id)
	{
	    $facility_type = FacilityType::findOrFail($id);
	    $vars = compact('facility_type');
	    
	    return view('backend.facility_types.edit', $vars);
	}

	public function postEdit(PostEditFacilityTypeRequest $request, $id)
	{
		$facility_type = $request->facility_type;
		$facility_type->fill($request->all());
		$facility_type->save();

		return Redirect::route('backend.facility_types.edit.get', $id)->with('messages', ['FacilityType was successfully updated!']);
	}

	public function destroy(DestroyFacilityTypeRequest $request, $id)
	{
		$facility_type = $request->facility_type;
		$facility_type->delete();

		$message = "FacilityType #$id successfully deleted!";

		return Redirect::route('backend.facility_types.index')->with('messages', [$message]);
	}
}