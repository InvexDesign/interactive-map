<?php namespace App\Http\Controllers\Backend;

use App\Http\Controllers\Controller;
use App\Http\Requests\Backend\DestroyTransitOptionRequest;
use App\Http\Requests\Backend\PostCreateTransitOptionRequest;
use App\Http\Requests\Backend\PostEditTransitOptionRequest;
use App\Models\Options\TransitOption;
use Illuminate\Support\Facades\Redirect;

class TransitOptionController extends Controller
{
	function __construct()
	{
		$this->middleware('auth');
		$this->middleware('require-role:' . ADMINISTRATORS);
	}

	public function index()
	{
		$transit_options = TransitOption::query()->get();
		$vars = compact('transit_options');

		return view('backend.transit_options.index', $vars);
	}

	public function getCreate()
	{
		$vars = [];

		return view('backend.transit_options.create', $vars);
	}

	public function postCreate(PostCreateTransitOptionRequest $request)
	{
		$transit_option = TransitOption::create($request->all());

		return Redirect::route('backend.transit_options.show', $transit_option->id)->with('messages', ['TransitOption was successfully added!']);
	}

	public function show($id)
	{
		$transit_option = TransitOption::findOrFail($id);
		$vars = compact('transit_option');

		return view('backend.transit_options.show', $vars);
	}

	public function getEdit($id)
	{
	    $transit_option = TransitOption::findOrFail($id);
	    $vars = compact('transit_option');
	    
	    return view('backend.transit_options.edit', $vars);
	}

	public function postEdit(PostEditTransitOptionRequest $request, $id)
	{
		$transit_option = $request->transit_option;
		$transit_option->fill($request->all());
		$transit_option->save();

		return Redirect::route('backend.transit_options.edit.get', $id)->with('messages', ['TransitOption was successfully updated!']);
	}

	public function destroy(DestroyTransitOptionRequest $request, $id)
	{
		$transit_option = $request->transit_option;

		if($transit_option->facilities()->count() > 0)
		{
			return Redirect::back()->with('errors', ['TransitOption cannot be deleted because it is assigned to one or more Facility!']);
		}

		$transit_option->delete();

		$message = "TransitOption #$id successfully deleted!";

		return Redirect::route('backend.transit_options.index')->with('messages', [$message]);
	}
}