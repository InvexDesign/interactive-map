<?php namespace App\Http\Controllers\Backend;

use App\Http\Controllers\Controller;
use App\Http\Requests\Backend\DestroyAgeGroupRequest;
use App\Http\Requests\Backend\PostCreateAgeGroupRequest;
use App\Http\Requests\Backend\PostEditAgeGroupRequest;
use App\Models\Options\AgeGroup;
use Illuminate\Support\Facades\Redirect;

class AgeGroupController extends Controller
{
	function __construct()
	{
		$this->middleware('auth');
		$this->middleware('require-role:' . ADMINISTRATORS);
	}

	public function index()
	{
		$age_groups = AgeGroup::query()->get();
		$vars = compact('age_groups');

		return view('backend.age_groups.index', $vars);
	}

	public function getCreate()
	{
		$vars = [];

		return view('backend.age_groups.create', $vars);
	}

	public function postCreate(PostCreateAgeGroupRequest $request)
	{
		$age_group = AgeGroup::create($request->all());

		return Redirect::route('backend.age_groups.show', $age_group->id)->with('messages', ['AgeGroup was successfully added!']);
	}

	public function show($id)
	{
		$age_group = AgeGroup::findOrFail($id);
		$vars = compact('age_group');

		return view('backend.age_groups.show', $vars);
	}

	public function getEdit($id)
	{
	    $age_group = AgeGroup::findOrFail($id);
	    $vars = compact('age_group');
	    
	    return view('backend.age_groups.edit', $vars);
	}

	public function postEdit(PostEditAgeGroupRequest $request, $id)
	{
		$age_group = $request->age_group;
		$age_group->fill($request->all());
		$age_group->save();

		return Redirect::route('backend.age_groups.edit.get', $id)->with('messages', ['AgeGroup was successfully updated!']);
	}

	public function destroy(DestroyAgeGroupRequest $request, $id)
	{
		$age_group = $request->age_group;

		if($age_group->facilities()->count() > 0)
		{
			return Redirect::back()->with('errors', ['AgeGroup cannot be deleted because it is assigned to one or more Facility!']);
		}

		$age_group->delete();

		$message = "AgeGroup #$id successfully deleted!";

		return Redirect::route('backend.age_groups.index')->with('messages', [$message]);
	}
}