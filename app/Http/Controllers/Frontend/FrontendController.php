<?php namespace App\Http\Controllers;

use App\Models\Facility;
use App\Models\MonthlyFacilityClickRecord;
use App\Models\Options\AgeGroup;
use App\Models\Service;
use Exception;
use Input;

class FrontendController extends Controller
{
	public function interactiveMap()
	{
		$filter_url = route('frontend.filter');
		$process_facility_click_url = route('frontend.process_facility_click');
		$services = Service::all();
		$service_options = Service::all()->pluck('name', 'id')->all();
		$facilities = Facility::all();
		$age_group_options = AgeGroup::all()->pluck('name', 'id')->all();

		$vars = compact(
			'filter_url',
			'process_facility_click_url',
			'services',
			'service_options',
			'facilities',
			'age_group_options'
		);

		return view('frontend.interactive_map', $vars);
	}

	public function filter()
	{
		try
		{
			$search = Input::get('search', false);
			$service_ids = Input::get('service_ids', []);
			$age_group_ids = Input::get('age_group_ids', []);

			$facilities_query = Facility::query();

			if($search !== false && $search !== '')
			{
				$facilities_query = $facilities_query->where(function($query) use ($search)
				{
					$query->where('name', 'like', '%' . $search . '%')
						  ->orWhere('description', 'like', '%' . $search . '%');
				});
			}

			if(count($service_ids) > 0)
			{
				$facility_ids = Facility::query()->pluck('id')->all();
				$services = Service::whereIn('id', $service_ids)->get();
				foreach($services as $service)
				{
					$service->incrementSearchAnalytics();
					$facility_ids = array_intersect($facility_ids, $service->facilities()->pluck('facility_id')->all());
				}

				$facilities_query = $facilities_query->whereIn('id', $facility_ids);
			}

			if(count($age_group_ids) > 0)
			{
				$facility_ids = Facility::query()->pluck('id')->all();
				$age_groups = AgeGroup::whereIn('id', $age_group_ids)->get();
				foreach($age_groups as $age_group)
				{
					$age_group->incrementSearchAnalytics();
					$facility_ids = array_intersect($facility_ids, $age_group->facilities()->pluck('facility_id')->all());
				}

				$facilities_query = $facilities_query->whereIn('id', $facility_ids);
			}

			$success = true;
			$visible_facility_ids = $facilities_query->pluck('id')->all();
			$hidden_facility_ids = Facility::query()->whereNotIn('id', $visible_facility_ids)->pluck('id')->all();

			$facilities = [];
			foreach($visible_facility_ids as $id)
			{
				$facilities[$id] = true;
			}

			foreach($hidden_facility_ids as $id)
			{
				$facilities[$id] = false;
			}

			$json = compact('success','facilities');
		}
		catch(Exception $e)
		{
			$json = [
				'success' => false,
				'message' => $e->getMessage(),
				'line'    => $e->getLine(),
				'file'    => $e->getFile(),
				'trace'   => $e->getTraceAsString(),
			];
		}

		return json_encode($json);
	}

	public function processFacilityClick()
	{
//		try
//		{
			$facility_id = Input::get('facility_id', null);
			if($facility_id)
			{
				MonthlyFacilityClickRecord::incrementCurrentMonthsRecord($facility_id);
			}
//		}
//		catch(Exception $e)
//		{
//			\Log::error('processFacilityClick FAILED');
//			\Log::error($e->getMessage());
//		}
	}
}