<?php namespace App\Http\Controllers;

class ErrorController extends Controller
{
	public function __construct()
	{
		$this->middleware('auth');
	}

	public function unauthorized()
	{
		return view('errors.unauthorized');
	}
}
