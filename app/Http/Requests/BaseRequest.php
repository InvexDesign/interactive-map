<?php namespace App\Http\Requests;

use Auth;
use Illuminate\Http\Exceptions\HttpResponseException;
use Redirect;
use Validator;

class BaseRequest extends Request
{
	public $error;

	public function forbiddenResponse()
	{
		$message = isset($this->error) && $this->error && $this->error != '' ? $this->error : 'You do not have permission to perform that action!';

		if(Auth::guest())
		{
			return Redirect::route('session.login')->withErrors([$message]);
		}
		else
		{
			return Redirect::route('errors.unauthorized')->withErrors([$message]);
		}
	}

	public function __validate()
	{
		if(!$this->passesAuthorization())
		{
			$this->failedAuthorization();
		}
	}

	public function _validate(array $data, array $rules, array $messages = [], array $custom_attributes = [])
	{
		$validator = Validator::make($data, $rules, $messages, $custom_attributes);
		if($validator->fails())
		{
			$redirect = Redirect::back()
								->withInput($this->input())
								->withErrors($validator->errors()->getMessages(), 'default');

			throw new HttpResponseException($redirect);
		}
	}
}
