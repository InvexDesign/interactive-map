<?php namespace App\Http\Requests\Backend;

use App\Http\Requests\BaseRequest;
use App\Models\Options\AgeGroup;
use HttpResponseException;
use Redirect;

class DestroyAgeGroupRequest extends BaseRequest
{
	public function validate()
	{
		$this->__validate();

		$id = $this->route()->parameters()['id'];
		$age_group = AgeGroup::find($id);
		if(!$age_group)
		{
			$redirect = Redirect::route('backend.age_groups.index')->with('errors', ["AgeGroup #$id does not exist!"]);
			throw new HttpResponseException($redirect);
		}

		$this->merge(['age_group' => $age_group]);
	}

	public function authorize()
	{
		return true;
	}
}
