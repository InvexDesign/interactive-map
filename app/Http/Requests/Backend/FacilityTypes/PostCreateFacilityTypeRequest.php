<?php namespace App\Http\Requests\Backend;

use App\Http\Requests\BaseRequest;

class PostCreateFacilityTypeRequest extends BaseRequest
{
	public function validate()
	{
		$this->__validate();

		$this->_validate($this->all(), [
			'name' => 'required|default_string_length',
		]);
	}

	public function authorize()
	{
		return true;
	}
}
