<?php namespace App\Http\Requests\Backend;

use App\Http\Requests\BaseRequest;
use App\Models\Options\FacilityType;
use HttpResponseException;
use Redirect;

class DestroyFacilityTypeRequest extends BaseRequest
{
	public function validate()
	{
		$this->__validate();

		$id = $this->route()->parameters()['id'];
		$facility_type = FacilityType::find($id);
		if(!$facility_type)
		{
			$redirect = Redirect::route('backend.facility_types.index')->with('errors', ["FacilityType #$id does not exist!"]);
			throw new HttpResponseException($redirect);
		}

		$this->merge(['facility_type' => $facility_type]);
	}

	public function authorize()
	{
		return true;
	}
}
