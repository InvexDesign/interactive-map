<?php namespace App\Http\Requests\Backend;

use App\Http\Requests\BaseRequest;
use App\Models\Language;
use HttpResponseException;
use Redirect;

class DestroyLanguageRequest extends BaseRequest
{
	public function validate()
	{
		$this->__validate();

		$id = $this->route()->parameters()['id'];
		$language = Language::find($id);
		if(!$language)
		{
			$redirect = Redirect::route('backend.languages.index')->with('errors', ["Language #$id does not exist!"]);
			throw new HttpResponseException($redirect);
		}

		$this->merge(['language' => $language]);
	}

	public function authorize()
	{
		return true;
	}
}
