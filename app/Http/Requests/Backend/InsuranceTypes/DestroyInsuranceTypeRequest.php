<?php namespace App\Http\Requests\Backend;

use App\Http\Requests\BaseRequest;
use App\Models\Options\InsuranceType;
use HttpResponseException;
use Redirect;

class DestroyInsuranceTypeRequest extends BaseRequest
{
	public function validate()
	{
		$this->__validate();

		$id = $this->route()->parameters()['id'];
		$insurance_type = InsuranceType::find($id);
		if(!$insurance_type)
		{
			$redirect = Redirect::route('backend.insurance_types.index')->with('errors', ["InsuranceType #$id does not exist!"]);
			throw new HttpResponseException($redirect);
		}

		$this->merge(['insurance_type' => $insurance_type]);
	}

	public function authorize()
	{
		return true;
	}
}
