<?php namespace App\Http\Requests\Backend;

use App\Http\Requests\BaseRequest;
use App\Models\Options\TransitOption;
use HttpResponseException;
use Redirect;

class PostEditTransitOptionRequest extends BaseRequest
{
	public function validate()
	{
		$this->__validate();

		$id = $this->route()->parameters()['id'];
		$transit_option = TransitOption::find($id);
		if(!$transit_option)
		{
			$redirect = Redirect::route('backend.transit_options.index')->with('errors', ["TransitOption #$id does not exist!"]);
			throw new HttpResponseException($redirect);
		}

		$this->_validate($this->all(), [
			'name' => 'required|default_string_length',
		]);

		$this->merge(['transit_option' => $transit_option]);
	}

	public function authorize()
	{
		return true;
	}
}
