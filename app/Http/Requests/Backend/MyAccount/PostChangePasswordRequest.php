<?php namespace App\Http\Requests\Backend\MyAccount;

use App\Http\Requests\BaseRequest;
use Illuminate\Http\Exceptions\HttpResponseException;
use Redirect;
use Validator;

class PostChangePasswordRequest extends BaseRequest
{
	public function validate()
	{
		$this->__validate();

		$user = \Auth::user();

		if(\Auth::user()->requiresCurrentPasswordToUpdate())
		{
			$current_password_matches = \Auth::validate([
				'username' => $user->username,
				'password' => $this->current_password
			]);
			if(!$current_password_matches)
			{
				$redirect = Redirect::back()->with('errors', ["Your current password is incorrect!"]);
				throw new HttpResponseException($redirect);
			}
		}

		$this->_validate($this->all(), [
			'password' => 'required|min:5|confirmed',
		]);

		$this->merge(['user' => $user]);
	}

	public function __validate()
	{
		if(!$this->passesAuthorization())
		{
			$this->failedAuthorization();
		}
	}

	public function _validate(array $data, array $rules, array $messages = [], array $custom_attributes = [])
	{
		$validator = Validator::make($data, $rules, $messages, $custom_attributes);
		if($validator->fails())
		{
			$redirect = Redirect::back()
								->withInput($this->input())
								->withErrors($validator->errors()->getMessages(), 'default');

			throw new HttpResponseException($redirect);
		}
	}

	public function authorize()
	{
		return true;
	}
}
