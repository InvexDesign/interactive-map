<?php namespace App\Http\Requests\Backend\Facility;

use App\Http\Requests\BaseRequest;
use App\Models\Facility;
use App\Models\State;
use Helper;
use HttpResponseException;
use Redirect;

class PostEditFacilityRequest extends BaseRequest
{
	public function validate()
	{
		$this->__validate();

		$id = $this->route()->parameters()['id'];
		$facility = Facility::find($id);
		if(!$facility)
		{
			$redirect = Redirect::route('backend.facilities.index')->with('errors', ["Facility #$id does not exist!"]);
			throw new HttpResponseException($redirect);
		}

		//TODO: Add service_ids
		$this->_validate($this->all(), [
			'uid'                => '',
			'name'               => 'required',
			'tagline'            => 'default_string_length',
			'organization'       => 'default_string_length',
			'address1'           => 'required|default_string_length',
			'address2'           => 'default_string_length',
			'city'               => 'required|default_string_length',
			'state_id'           => 'required|exists:states,id',
			'zipcode'            => 'required',
			'latitude'           => 'required_without:auto_gps',
			'longitude'          => 'required_without:auto_gps',
			'url'                => 'default_string_length',
			'email'              => 'default_string_length',
			'website'            => 'default_string_length',
			'phone'              => 'default_string_length',
			'247_hotline'        => 'default_string_length',
			'text_line'          => 'default_string_length',
			'live_chat'          => 'default_string_length',
			'youth_approved'     => '',
			'hours'              => '',
			'image'              => 'default_string_length',
			'description'        => '',
			'minor_access'       => '',
			'appointments'       => '',
			'notes'              => '',
			'insurance_type_ids' => '',
			'language_ids'       => '',
			'service_ids'        => 'required',
			'transit_option_ids' => '',
		]);

		if($this->get('auto_gps', false))
		{
			$formatted_address = str_replace([".", ","], ["", ""], $this->get('address1'));
			$formatted_address = str_replace(" ", "+", $formatted_address);

			$formatted_city = str_replace([".", ","], ["", ""], $this->get('city'));
			$formatted_city = str_replace(" ", "+", $formatted_city);

			$state = State::find($this->get('state_id'));
			$formatted_state = $state->short_name;

			$zipcode = $this->get('zipcode');

			$geocode_url = "http://maps.google.com/maps/api/geocode/json?address={$formatted_address}+{$formatted_city}+{$formatted_state}+{$zipcode}&sensor=false";
			\Log::debug("AUTO GPS: ATTEMPT - ($geocode_url)");

			$tries = 5;
			while($tries > 0)
			{
				$response = file_get_contents($geocode_url);
				$json = json_decode($response);

				if($json->status == "OK")
				{
					$this->merge([
						'latitude'  => $json->results[0]->geometry->location->lat,
						'longitude' => $json->results[0]->geometry->location->lng
					]);
					break;
				}

				\Log::error("AUTO GPS: FAILED - ($geocode_url)");
				$tries--;
			}
		}

		$this->merge([
			'facility' => $facility,
			'url'      => Helper::addHttp($this->url),
			'website'  => Helper::addHttp($this->website),
		]);
	}

	public function authorize()
	{
		return true;
	}
}
