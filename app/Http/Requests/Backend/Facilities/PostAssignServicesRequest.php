<?php namespace App\Http\Requests\Backend\Facility;

use App\Http\Requests\BaseRequest;
use App\Models\Facility;
use HttpResponseException;
use Redirect;

class PostAssignServicesRequest extends BaseRequest
{
	public function validate()
	{
		$this->__validate();

		$id = $this->route()->parameters()['id'];
		$facility = Facility::find($id);
		if(!$facility)
		{
			$redirect = Redirect::route('backend.facilities.index')->with('errors', ["Facility #$id does not exist!"]);
			throw new HttpResponseException($redirect);
		}

		$this->_validate($this->all(), [
			'service_'               => '',
			'name'              => 'required',
			'tagline'           => '',
			'organization'      => '',
			'address1'          => 'required',
			'address2'          => '',
			'city'              => 'required',
			'state_id'          => 'required|exists:states,id',
			'zipcode'           => 'required',
			'latitude'          => '',
			'longitude'         => '',
			'url'               => '',
			'email'             => '',
			'website'           => '',
			'phone'             => '',
			'hours'             => '',
			'image'             => '',
			'description'       => '',
			'parking_transport' => '',
			'lgbtq_services'    => '',
			'minor_access'      => '',
			'appointments'      => '',
			'contact'           => '',
			'notes'             => '',
		]);

		$this->merge(['facility' => $facility]);
	}

	public function authorize()
	{
		return true;
	}
}
