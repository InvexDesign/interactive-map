<?php namespace App\Http\Requests\Backend\Facility;

use App\Http\Requests\BaseRequest;
use App\Models\Facility;
use HttpResponseException;
use Redirect;

class DestroyFacilityRequest extends BaseRequest
{
	public function validate()
	{
		$this->__validate();

		$id = $this->route()->parameters()['id'];
		$facility = Facility::find($id);
		if(!$facility)
		{
			$redirect = Redirect::route('backend.facilities.index')->with('errors', ["Facility #$id does not exist!"]);
			throw new HttpResponseException($redirect);
		}

		$this->merge(['facility' => $facility]);
	}

	public function authorize()
	{
		return true;
	}
}
