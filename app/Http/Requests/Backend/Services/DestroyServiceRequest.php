<?php namespace App\Http\Requests\Backend;

use App\Http\Requests\BaseRequest;
use App\Models\Service;
use HttpResponseException;
use Redirect;

class DestroyServiceRequest extends BaseRequest
{
	public function validate()
	{
		$this->__validate();

		$id = $this->route()->parameters()['id'];
		$service = Service::find($id);
		if(!$service)
		{
			$redirect = Redirect::route('backend.services.index')->with('errors', ["Service #$id does not exist!"]);
			throw new HttpResponseException($redirect);
		}

		$this->merge(['service' => $service]);
	}

	public function authorize()
	{
		return true;
	}
}
