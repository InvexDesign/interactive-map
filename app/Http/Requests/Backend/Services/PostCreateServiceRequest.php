<?php namespace App\Http\Requests\Backend;

use App\Http\Requests\BaseRequest;
use Redirect;

class PostCreateServiceRequest extends BaseRequest
{
	public function validate()
	{
		$this->__validate();

		$this->_validate($this->all(), [
			'uid'         => 'default_string_length',
			'name'        => 'required|default_string_length',
			'tagline'     => 'default_string_length',
			'description' => '',
			'image'       => 'default_string_length',
			'order'       => '',
			'notes'       => '',
		]);
	}

	public function authorize()
	{
		return true;
	}
}
