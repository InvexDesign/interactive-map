<?php

namespace App\Http\Requests;

use App\Models\Setting;
use Auth;

class CreateSettingRequest extends BaseRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
		return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $rules = Setting::getRules();
        $rules['key'] .=  '|unique:settings,key';

        return $rules;
    }
}
