<?php

namespace App\Http\Requests;

use App\Models\Setting;
use App\Models\User;
use Auth;

class UserLoginRequest extends BaseRequest
{
	/**
	 * Determine if the user is authorized to make this request.
	 *
	 * @return bool
	 */
	public function authorize()
	{
		if(!Setting::get('is-user-login-allowed', false))
		{
			$this->error = 'User logins are currently disabled, please try again later.';

			return false;
		}

		return true;
	}

	/**
	 * Get the validation rules that apply to the request.
	 *
	 * @return array
	 */
	public function rules()
	{
		//TODO: Should this retrieved from User model? ie User::getLoginRules()
		return [
			'username' => 'required',
			'password' => 'required',
		];
	}
}
