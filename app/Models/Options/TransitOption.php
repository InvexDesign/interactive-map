<?php namespace App\Models\Options;

use App\Models\BaseModel;
use App\Models\Facility;

class TransitOption extends BaseModel
{
	protected $table    = 'transit_options';
	protected $fillable = [
		'name',
	];


	/* ELOQUENT */
	public function facilities()
	{
		return $this->belongsToMany(Facility::class);
	}


	/* QUERIES */


	/* PRESENTATION */
	public function display()
	{
		return $this->name;
	}
}
