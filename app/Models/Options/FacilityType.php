<?php namespace App\Models\Options;

use App\Models\BaseModel;
use App\Models\Facility;

class FacilityType extends BaseModel
{
	protected $table    = 'facility_types';
	protected $fillable = [
		'name',
	];


	/* ELOQUENT */
	public function facilities()
	{
		return $this->hasMany(Facility::class, 'type_id');
	}


	/* QUERIES */


	/* PRESENTATION */
	public function display()
	{
		return $this->name;
	}
}
