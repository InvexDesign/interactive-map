<?php namespace App\Models\Options;

use App\Models\BaseModel;
use App\Models\Facility;

class InsuranceType extends BaseModel
{
	protected $table    = 'insurance_types';
	protected $fillable = [
		'name',
	];


	/* ELOQUENT */
	public function facilities()
	{
		return $this->belongsToMany(Facility::class);
	}


	/* QUERIES */


	/* PRESENTATION */
	public function display()
	{
		return $this->name;
	}
}
