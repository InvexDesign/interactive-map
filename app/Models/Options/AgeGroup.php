<?php namespace App\Models\Options;

use App\Models\BaseModel;
use App\Models\Facility;
use App\Models\MonthlyAgeGroupSearchRecord;

class AgeGroup extends BaseModel
{
	protected $table    = 'age_groups';
	protected $fillable = [
		'name',
	];


	/* ELOQUENT */
	public function facilities()
	{
		return $this->belongsToMany(Facility::class);
	}


	/* QUERIES */


	/* PRESENTATION */
	public function display()
	{
		return $this->name;
	}


	/* ANALYTICS*/
	public function incrementSearchAnalytics()
	{
		MonthlyAgeGroupSearchRecord::incrementCurrentMonthsRecord($this);
	}
}
