<?php namespace App\Models;

class Language extends BaseModel
{
	protected $table    = 'languages';
	protected $fillable = [
		'name',
	];


	/* ELOQUENT */
	public function facilities()
	{
		return $this->belongsToMany(Facility::class);
	}


	/* QUERIES */


	/* PRESENTATION */
	public function display()
	{
		return $this->name;
	}
}
