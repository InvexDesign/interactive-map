<?php namespace App\Models;

use App\Interfaces\Displayable;
use App\Interfaces\HasDisplayRoutes;
use App\Models\Events\Event;
use App\Traits\HasReverseSeeder;

class Service extends BaseModel implements Displayable
{
	use HasReverseSeeder;

	protected $table    = 'services';
	protected $fillable = [
		'uid',
		'name',
		'tagline',
		'description',
		'image',
		'order',
		'notes',
	];

	public function getAllAttributes()
	{
		return array_merge(['id'], $this->fillable);
	}

	public static function getAttributeMap()
	{
		return [
			'name'        => 'string-escaped',
			'tagline'     => 'string-escaped-nullable',
			'description' => 'text-escaped-nullable',
			'order'       => 'integer',
			'notes'       => 'text-escaped-nullable',
		];
	}


	/* ELOQUENT */
	public function facilities()
	{
	    return $this->belongsToMany(Facility::class);
	}


	public function getDisplayAttribute()
	{
		return $this->display();
	}


	/* PRESENTATION */
	public function display()
	{
		return $this->name;
	}


	/* ANALYTICS*/
	public function incrementSearchAnalytics()
	{
		MonthlyServiceSearchRecord::incrementCurrentMonthsRecord($this);
	}
}