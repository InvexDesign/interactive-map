<?php namespace App\Models;

use App\Interfaces\Displayable;

class MonthlyServiceSearchRecord extends BaseModel implements Displayable
{
	protected $table    = 'analytics__monthly_service_searches';
	protected $fillable = [
		'service_id',
		'month',
		'hits',
	];

	public static function getAttributeMap()
	{
		return [
			'month' => 'datetime',
			'hits'  => 'integer',
		];
	}

	public static function incrementCurrentMonthsRecord($service)
	{
		$month = (new \Carbon('first day of this month'))->startOfDay();
		$record = self::query()
					  ->where('service_id', $service->id)
					  ->where('month', $month)
					  ->get()
					  ->first();

		if(!$record)
		{
			self::create([
				'service_id' => $service->id,
				'month'       => $month,
				'hits'        => 1,
			]);
		}
		else
		{
			$record->hits = $record->hits + 1;
			$record->save();
		}
	}


	/* ELOQUENT */
	public function service()
	{
		return $this->belongsTo(Service::class, 'service_id');
	}

	public function getDisplayAttribute()
	{
		return $this->display();
	}


	/* PRESENTATION */
	public function display()
	{
		return $this->month->format('m/d/Y') . ' | ' . $this->service->display() . ': ' . $this->hits;
	}
}