<?php namespace App\Models;

use App\Interfaces\Displayable;
use App\Models\Options\AgeGroup;

class MonthlyAgeGroupSearchRecord extends BaseModel implements Displayable
{
	protected $table    = 'analytics__monthly_age_group_searches';
	protected $fillable = [
		'age_group_id',
		'month',
		'hits',
	];

	public static function getAttributeMap()
	{
		return [
			'month' => 'datetime',
			'hits'  => 'integer',
		];
	}

	public static function incrementCurrentMonthsRecord($age_group)
	{
		$month = (new \Carbon('first day of this month'))->startOfDay();
		$record = self::query()
					  ->where('age_group_id', $age_group->id)
					  ->where('month', $month)
					  ->get()
					  ->first();

		if(!$record)
		{
			self::create([
				'age_group_id' => $age_group->id,
				'month'        => $month,
				'hits'         => 1,
			]);
		}
		else
		{
			$record->hits = $record->hits + 1;
			$record->save();
		}
	}


	/* ELOQUENT */
	public function ageGroup()
	{
		return $this->belongsTo(AgeGroup::class, 'age_group_id');
	}

	public function getDisplayAttribute()
	{
		return $this->display();
	}


	/* PRESENTATION */
	public function display()
	{
		return $this->month->format('m/d/Y') . ' | ' . $this->ageGroup->display() . ': ' . $this->hits;
	}
}