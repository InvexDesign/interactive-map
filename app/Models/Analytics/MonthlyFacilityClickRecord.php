<?php namespace App\Models;

use App\Interfaces\Displayable;

class MonthlyFacilityClickRecord extends BaseModel implements Displayable
{
	protected $table    = 'analytics__monthly_facility_clicks';
	protected $fillable = [
		'facility_id',
		'month',
		'hits',
	];

	public static function getAttributeMap()
	{
		return [
			'month' => 'datetime',
			'hits'  => 'integer',
		];
	}

	public static function incrementCurrentMonthsRecord($facility_id)
	{
		$month = (new \Carbon('first day of this month'))->startOfDay();
		$record = self::query()
					  ->where('facility_id', $facility_id)
					  ->where('month', $month)
					  ->get()
					  ->first();

		if(!$record)
		{
			self::create([
				'facility_id' => $facility_id,
				'month'       => $month,
				'hits'        => 1,
			]);
		}
		else
		{
			$record->hits = $record->hits + 1;
			$record->save();
		}
	}


	/* ELOQUENT */
	public function facility()
	{
		return $this->belongsTo(Facility::class, 'facility_id');
	}

	public function getDisplayAttribute()
	{
		return $this->display();
	}


	/* PRESENTATION */
	public function display()
	{
		return $this->month->format('m/d/Y') . ' | ' . $this->facility->display() . ': ' . $this->hits;
	}
}