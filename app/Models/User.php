<?php namespace App\Models;

use App\Interfaces\Displayable;
use App\Models\Notifications\UserNotification;
use Hash;
use Illuminate\Auth\Authenticatable;
use Illuminate\Auth\Passwords\CanResetPassword;
use Illuminate\Contracts\Auth\Access\Authorizable as AuthorizableContract;
use Illuminate\Contracts\Auth\Authenticatable as AuthenticatableContract;
use Illuminate\Contracts\Auth\CanResetPassword as CanResetPasswordContract;
use Illuminate\Foundation\Auth\Access\Authorizable;
use Zizaco\Entrust\Traits\EntrustUserTrait;

class User extends BaseModel implements AuthenticatableContract, AuthorizableContract, CanResetPasswordContract, Displayable
{
	use Authenticatable, Authorizable, CanResetPassword;
	use EntrustUserTrait
	{
		EntrustUserTrait::can insteadof Authorizable;
	}

	protected $table    = 'users';
	protected $fillable = [
		'username',
		'email',
		'password',
		'first_name',
		'last_name',
		'password_updated_at',
	];
	protected $hidden   = [
		'password',
		'remember_token'
	];

	public static function getAttributeMap()
	{
		return [
			'first_name' => 'string-escaped',
			'last_name'  => 'string-escaped',
		];
	}


	/* GETTERS / SETTERS */
	public function isExecutive()
	{
		return $this->hasRole('overlord') || $this->hasRole('master');
	}

	public function isAdministrator()
	{
		return $this->isExecutive() || $this->hasRole('administrator');
	}

	public function getTemplate()
	{
		if($this->isExecutive())
		{
			return 'executives';
		}
		elseif($this->isAdministrator())
		{
			return 'administrators';
		}

		return 'none';
	}

	public function getRole()
	{
		$role = $this->roles()->first();
		if(!$role)
		{
			$model_id = $this->id;
			$description = 'User is not assigned a role! please contact site administrator!';
			$notification_has_not_already_been_made = (UserNotification::where('model_id', $model_id)
																	   ->where('description', BaseModel::escapeNonAlphaNum($description))
																	   ->where('is_active', true)->count() == 0);
			if($notification_has_not_already_been_made)
			{
				UserNotification::create([
					'model_id'    => $model_id,
					'description' => $description,
					'is_severe'   => true,
				]);
			}
		}

		return $role;
	}

	public function setPasswordAttribute($password)
	{
		$this->attributes['password'] = Hash::make($password);
	}

	public function getLastCommaFirstAttribute()
	{
		return $this->display_helper(true);
	}

	public function getDisplayAttribute()
	{
		return $this->display();
	}

	public function getFirstLastAttribute()
	{
		return $this->display_helper(false);
	}

	public function getDisplayEmailAttribute()
	{
		return $this->display_helper(false) . ($this->email && $this->email != '' ? ' <' . $this->email . '>' : '');
	}

	public function getUsernameEmailAttribute()
	{
		return $this->username . ' <' . $this->email . '>';
	}

	public function requiresCurrentPasswordToUpdate()
	{
		return $this->password_updated_at != null;
	}

	/* ELOQUENT */


	/* PRESENTATION */
	public function display_helper($is_last_comma_first = false)
	{
		$display = '';

		if($this->last_name . $this->first_name == '')
		{
			if($this->username)
			{
				$display .= $this->username;
			}
		}
		else
		{
			if($is_last_comma_first)
			{
				$display .= $this->last_name . ', ' . $this->first_name;
			}
			else
			{
				$display .= $this->first_name . ' ' . $this->last_name;
			}
		}

		return $display;
	}

	public function display()
	{
		return $this->display_helper(false);
	}

	public function getGravatarImageUrl()
	{
		return "https://www.gravatar.com/avatar/" . md5(strtolower(trim($this->email))) . "?s=40&d=mm";
	}
}
