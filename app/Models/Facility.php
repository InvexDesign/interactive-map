<?php namespace App\Models;

use App\Interfaces\Displayable;
use App\Models\Options\AgeGroup;
use App\Models\Options\FacilityType;
use App\Models\Options\InsuranceType;
use App\Models\Options\TransitOption;
use App\Traits\DisplaysPhoneNumber;
use App\Traits\HasReverseSeeder;

class Facility extends BaseModel implements Displayable
{
	use HasReverseSeeder, DisplaysPhoneNumber;

	protected $table    = 'facilities';
	protected $fillable = [
		'uid',
		'name',
		'tagline',
		'organization',
		'address1',
		'address2',
		'city',
		'state_id',
		'zipcode',
		'auto_gps',
		'latitude',
		'longitude',
		'url',
		'email',
		'website',
		'phone',
		'247_hotline',
		'text_line',
		'live_chat',
		'youth_approved',
		'hours',
		'image',
		'description',
		'minor_access',
		'appointments',
		'order',
		'notes',
	];

	public static function getAttributeMap()
	{
		return [
			'name'              => 'string-escaped',
			'tagline'           => 'string-escaped-nullable',
			'organization'      => 'string-escaped-nullable',
			'address1'          => 'string-escaped',
			'address2'          => 'string-escaped-nullable',
			'city'              => 'string-escaped',
			'zipcode'           => 'string-escaped',
			'auto_gps'          => 'boolean',
			'latitude'          => 'float',
			'longitude'         => 'float',
			'url'               => 'string-escaped',
			'email'             => 'string-escaped',
			'website'           => 'string-escaped',
			'phone'             => 'phone',
			'247_hotline'       => 'phone',
			'text_line'         => 'phone',
			'live_chat'         => 'string-escaped',
			'youth_approved'    => 'boolean',
			'hours'             => 'string-escaped',
			'description'       => 'text-escaped-nullable',
			'minor_access'      => 'text-escaped-nullable',
			'appointments'      => 'text-escaped-nullable',
			'order'             => 'integer',
			'notes'             => 'text-escaped-nullable',
		];
	}


	/* ELOQUENT */
	public function state()
	{
		return $this->belongsTo(State::class, 'state_id');
	}

	public function ageGroups()
	{
	    return $this->belongsToMany(AgeGroup::class);
	}

	public function services()
	{
		return $this->belongsToMany(Service::class);
	}

	public function languages()
	{
		return $this->belongsToMany(Language::class);
	}

	public function insuranceTypes()
	{
		return $this->belongsToMany(InsuranceType::class);
	}

	public function transitOptions()
	{
		return $this->belongsToMany(TransitOption::class);
	}

	public function getDisplayAttribute()
	{
		return $this->display();
	}


	/* PRESENTATION */
	public function display()
	{
		return $this->name;
	}

	public function displayAddress($inline = false)
	{
		$separator = "<br />";
		if($inline)
		{
			$separator = " ";
		}

		$display = $this->address1 . $separator;

		if($this->address2)
		{
			$display .= $this->address2 . $separator;
		}

		$display .= $this->city . ", " . $this->state->display() . " " . $this->zipcode;

		return $display;
	}

	public function displayGoogleMapsAddress()
	{
		$display = $this->address1 . ',';

		if($this->address2)
		{
			$display .= $this->address2 . ',';
		}

		$display .= $this->city . " " . $this->state->display() . " " . $this->zipcode;

//		return urlencode(str_replace(' ', '+', $display));
		return str_replace(' ', '+', $display);
	}


	/* ANALYTICS*/
	public function incrementClickAnalytics()
	{
		MonthlyFacilityClickRecord::incrementCurrentMonthsRecord($this);
	}
}