<?php namespace App\Models\Notifications;

use App\Interfaces\Displayable;
use App\Models\User;

class UserNotification extends Notification implements Displayable
{
	protected static $singleTableType = 'UserNotification';

	public static function getRules()
	{
		return [
			'description' => 'required',
			'model_id'    => 'required|exists:users,id',
			'remind_at'   => 'date',
		];
	}


	/* GETTERS / SETTERS */
	public function getModelShowRoute()
	{
		return 'users.show';
	}

	public function getColor()
	{
		return 'red';
	}


	/* PRESENTATION */
	public function displayModel()
	{
		return 'USER-' . $this->model_id;
	}


	/* ELOQUENT */
	public function getModel()
	{
		if(!$this->model_id)
		{
			return null;
		}

		return User::find($this->model_id);
	}
}