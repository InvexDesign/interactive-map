<?php

use App\Models\Facility;
use App\Models\Language;
use App\Models\Options\FacilityType;
use App\Models\Options\InsuranceType;
use App\Models\Options\TransitOption;
use App\Models\Service;
use App\Models\State;
use Illuminate\Database\Seeder;

class FakeFacilitySeeder extends Seeder
{
	public function run()
	{
		$faker = \Faker\Factory::create();
		$names = [
			'Backalley Burgers',
			'Burger Town',
			'Ketchup Jazz Fest',
			'Lifehouse Special',
			'Barn Salad Stew',
			'Togepi Surprise',
			'Slimers Den',
			'Poppy Seed Field',
			'Time Travel Spaceship',
			'Long Time Friend Zone',
			'Piecemeal Jazz Session',
		];
		$line2_options = [
			'Basement',
			'Building A',
			'Building B',
			'Building C',
			'Building D',
			'Classroom A',
			'Classroom B',
			'Classroom C',
			'Classroom D',
			'Main Gym',
		];
		$state_ids = State::query()->pluck('id')->all();
		$all_service_ids = Service::all()->pluck('id')->all();
		$all_language_ids = Language::all()->pluck('id')->all();
		$all_transit_option_ids = TransitOption::all()->pluck('id')->all();
		$all_insurance_type_ids = InsuranceType::all()->pluck('id')->all();
		$services_count = count($all_service_ids);
		$languages_count = count($all_language_ids);
		$transit_option_count = count($all_transit_option_ids);
		$insurance_type_count = count($all_insurance_type_ids);
		$facility_type_ids = FacilityType::all()->pluck('id')->all();

		$initial_latitude = Setting::get('initial-map-latitude', 41.8858001);
		$initial_longitude = Setting::get('initial-map-longitude', -88.326725);

		foreach($names as $i => $name)
		{
			$facility = Facility::create([
				'uid'               => 'FAC-' . ($i + 1),
				'name'              => $name,
				'tagline'           => $faker->sentence(10),
				'organization'      => $faker->sentence(2),
				'address1'          => $faker->streetAddress,
				'address2'          => $faker->boolean() ? null : $faker->randomElement($line2_options),
				'city'              => $faker->city,
				'state_id'          => $faker->randomElement($state_ids),
				'zipcode'           => $faker->numberBetween(10000, 99999),
				'auto_gps'          => true,
				'latitude'          => $initial_latitude + ($faker->numberBetween(100, 99999) / 1000000),
				'longitude'         => $initial_longitude + ($faker->numberBetween(100, 99999) / 1000000),
				'type_id'           => $faker->randomElement($facility_type_ids),
				'url'               => $faker->url,
				'email'             => $faker->email,
				'website'           => $faker->url,
				'phone'             => self::generateRandomPhoneNumber($faker),
				'247_hotline'       => self::generateRandomPhoneNumber($faker),
				'text_line'         => self::generateRandomPhoneNumber($faker),
				'live_chat'         => $faker->phoneNumber,
				'youth_approved'    => $faker->boolean(),
				'hours'             => $faker->sentence(50),
				'image'             => null,
				'description'       => $faker->sentence(100),
//				'parking_transport' => null,
				'minor_access'      => $faker->sentence(50),
				'appointments'      => $faker->sentence(50),
				'order'             => null,
				'notes'             => null,
			]);

			$service_ids = $faker->randomElements($all_service_ids, $faker->numberBetween(3, $services_count));
			$facility->services()->sync($service_ids);

			$language_ids = $faker->randomElements($all_language_ids, $faker->numberBetween(3, $languages_count));
			$facility->languages()->sync($language_ids);

			$insurance_type_ids = $faker->randomElements($all_insurance_type_ids, $faker->numberBetween(3, $insurance_type_count));
			$facility->insuranceTypes()->sync($insurance_type_ids);

			$transit_option_ids = $faker->randomElements($all_transit_option_ids, $faker->numberBetween(3, $transit_option_count));
			$facility->transitOptions()->sync($transit_option_ids);
		}
	}

	private static function generateRandomPhoneNumber($faker)
	{
		if($faker->boolean())
		{
			return $faker->numberBetween(200, 999) . $faker->numberBetween(200, 999) . $faker->numberBetween(2000, 9999);
		}

		return $faker->numberBetween(200, 999) . $faker->numberBetween(200, 999) . $faker->numberBetween(2000, 9999) . $faker->numberBetween(10, 999);
	}
}
