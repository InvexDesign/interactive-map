<?php

use App\Models\Service;
use Illuminate\Database\Seeder;

class FakeServiceSeeder extends Seeder
{
	public function run()
	{
		$services = [
			'Abortion Information',
			'Adoption Information',
			'Housing Referrals',
			'LGBTQ+ Services',
			'Maternity & Infant Supplies',
			'Medical Referrals',
			'Parenting Education',
			'Post-Abortion Support',
			'Pregnancy Options Information',
			'Pregnancy Tests',
			'STD/STI Information',
			'Ultrasound (Onsite)',
			'Ultrasound Referrals',
		];

		$faker = \Faker\Factory::create();
		foreach($services as $index => $service)
		{
			Service::create([
				'uid'         => 'SVC-' . ($index + 1),
				'name'        => $service,
				'tagline'     => $faker->sentence(),
				'description' => $faker->sentence(),
				'image'       => null,
				'order'       => null,
				'notes'       => $faker->sentence(),
			]);
		}
	}

	private static function generateRandomPhoneNumber($faker)
	{
		if($faker->boolean())
		{
			return $faker->numberBetween(200, 999) . $faker->numberBetween(200, 999) . $faker->numberBetween(2000, 9999);
		}

		return $faker->numberBetween(200, 999) . $faker->numberBetween(200, 999) . $faker->numberBetween(2000, 9999) . $faker->numberBetween(10, 999);
	}
}
