<?php

use App\Models\Addresses\CustomerAddress;
use App\Models\Kitchen;
use App\Models\User;
use App\Role;
use Illuminate\Database\Seeder;

class BaseUserSeeder extends Seeder
{
	public function run()
	{
		$faker = \Faker\Factory::create();

		$overlord_role = Role::get('overlord', true);
		$master_role = Role::get('master', true);
		$administrator_role = Role::get('administrator', true);

		$is_local_environment = config('app.env') == 'local';
		$password = $is_local_environment ? 'password' : 'r287gfuewihoiej0gh9ubt';

		//Create Overlord
		$user = User::create([
			'username'   => 'dev',
			'password'   => $password,
			'first_name' => 'Babay',
			'last_name'  => 'Lettuce',
			'email'      => 'andrew@invexdesign.com'
		]);
		$user->attachRole($overlord_role);

		$user = User::create([
			'username'   => 'admin',
			'password'   => $password,
			'first_name' => 'Admin',
			'last_name'  => '',
			'email'      => 'brian@invexdesign.com'
		]);
		$user->attachRole($administrator_role);

		$user = User::create([
			'username'   => 'kyc',
			'password'   => $password,
			'first_name' => 'KYC',
			'last_name'  => '',
			'email'      => 'krystalw@kennethyoung.org'
		]);
		$user->attachRole($administrator_role);
	}
}
