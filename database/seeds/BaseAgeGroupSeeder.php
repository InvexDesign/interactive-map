<?php

use App\Models\Options\AgeGroup;
use Illuminate\Database\Seeder;

class BaseAgeGroupSeeder extends Seeder
{
	public function run()
	{
		$names = [
			'Adult',
			'Teen',
		];

		foreach($names as $name)
		{
			AgeGroup::create([
				'name' => $name,
			]);
		}
	}
}
