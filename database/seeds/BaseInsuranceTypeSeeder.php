<?php

use App\Models\Options\InsuranceType;
use Illuminate\Database\Seeder;

class BaseInsuranceTypeSeeder extends Seeder
{
	public function run()
	{
		$names = [
			'Private Insurance',
			'Medicare',
			'Medicaid',
		];

		foreach($names as $name)
		{
			InsuranceType::create([
				'name' => $name,
			]);
		}
	}
}
