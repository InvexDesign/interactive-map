<?php

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
		Model::unguard();
		$is_clean_seed = config('system.is_clean_seed');

		$this->call('BaseRoleSeeder');
		$BASE_USER_SEEDER = env('BASE_USER_SEEDER', 'BaseUserSeeder');
		$this->call($BASE_USER_SEEDER);
		$this->call('BaseStateSeeder');
		$this->call('BaseLanguageSeeder');

		$BASE_SETTING_SEEDER = env('BASE_SETTING_SEEDER', 'BaseSettingSeeder');
		$this->call($BASE_SETTING_SEEDER);

		if(!$is_clean_seed)
		{
			$this->call('BaseAgeGroupSeeder');
//			$this->call('BaseFacilityTypeSeeder');
			$this->call('BaseInsuranceTypeSeeder');
			$this->call('BaseTransitOptionSeeder');
			$BASE_SERVICE_SEEDER = env('BASE_SERVICE_SEEDER', 'FakeServiceSeeder');
			$this->call($BASE_SERVICE_SEEDER);
			$BASE_FACILITY_SEEDER = env('BASE_FACILITY_SEEDER', 'FakeFacilitySeeder');
			$this->call($BASE_FACILITY_SEEDER);
		}

		$FAKE_ANALYTICS_SEEDER = env('FAKE_ANALYTICS_SEEDER', false);
		if($FAKE_ANALYTICS_SEEDER)
		{
			$this->call($FAKE_ANALYTICS_SEEDER);
		}
    }
}
