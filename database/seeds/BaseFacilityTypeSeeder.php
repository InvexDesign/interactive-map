<?php

use App\Models\Options\FacilityType;
use Illuminate\Database\Seeder;

class BaseFacilityTypeSeeder extends Seeder
{
	public function run()
	{
		$names = [
			'Adult',
			'Teen',
		];

		foreach($names as $name)
		{
			FacilityType::create([
				'name' => $name,
			]);
		}
	}
}
