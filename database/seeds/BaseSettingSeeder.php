<?php

use App\Models\Setting;
use Illuminate\Database\Seeder;

class BaseSettingSeeder extends Seeder
{
	public function run()
	{
		/* BEGIN SITE SPECIFIC SETTINGS */
		Setting::create([
			'key'         => 'copyright',
			'value'       => 'Invex Design',
			'type'        => 'string',
			'description' => 'The copyright owner.',
			'user_id'     => null,
		]);
		Setting::create([
			'key'         => 'powered-by-url',
			'value'       => 'http://www.invexdesign.com',
			'type'        => 'string',
			'description' => 'The link to the powered-by company.',
			'user_id'     => null,
		]);
		Setting::create([
			'key'         => 'powered-by-name',
			'value'       => 'Invex Design',
			'type'        => 'string',
			'description' => 'The name of the powered-by company.',
			'user_id'     => null,
		]);
		Setting::create([
			'key'         => 'site-title',
			'value'       => 'Interactive Map',
			'type'        => 'string',
			'description' => 'The site title.',
			'user_id'     => null,
		]);
		Setting::create([
			'key'         => 'timepicker-default',
			'value'       => '8:00AM',
			'type'        => 'string',
			'description' => 'The timepicker time selected by default.',
			'user_id'     => null,
		]);
		Setting::create([
			'key'         => 'timepicker-minimum',
			'value'       => '4:00AM',
			'type'        => 'string',
			'description' => 'The earliest timepicker time available.',
			'user_id'     => null,
		]);
		Setting::create([
			'key'         => 'timepicker-maximum',
			'value'       => '11:30PM',
			'type'        => 'string',
			'description' => 'The latest timepicker time available.',
			'user_id'     => null,
		]);
		Setting::create([
			'key'         => 'timezone',
			'value'       => 'America/Chicago',
			'type'        => 'string',
			'description' => 'The timezone to be used within this application.',
			'user_id'     => null,
		]);
		Setting::create([
			'key'         => 'display-logo-on-login-page',
			'value'       => 'true',
			'type'        => 'boolean',
			'description' => 'Enables or disables logo display on the login page.',
			'user_id'     => null,
		]);
		Setting::create([
			'key'         => 'login-page-logo',
			'value'       => 'images/logos/invex-logo.png',
			'type'        => 'string',
			'description' => 'URL of the logo that will display on the login page.',
			'user_id'     => null,
		]);
		Setting::create([
			'key'         => 'google-maps-api-key',
//			'value'       => 'AIzaSyDUZbJTD2cg2ofIVT5vWXtjEvh7EkkQwS8',
//			'value'       => 'AIzaSyAbJb43smfp1HMjHydgs_yiwJ2sXUGIUsM',//asittwo@gmail.com
			'value'       => 'AIzaSyCdFbNJ6o6E58pJjgQFbNAOMkDBZ1wxFZM',//asittwo@gmail.com MAPS
			'type'        => 'string',
			'description' => 'Your Google Maps API v3 key. Must enabled Google Maps JavaScript API and Google Maps Embed API.',
			'user_id'     => null,
		]);
		Setting::create([
			'key'         => 'initial-map-latitude',
			'value'       => '41.938391',
			'type'        => 'float',
			'description' => 'The initial latitude of your Google Maps.',
			'user_id'     => null,
		]);
		Setting::create([
			'key'         => 'initial-map-longitude',
			'value'       => '-88.021454',
			'type'        => 'float',
			'description' => 'The initial latitude of your Google Maps.',
			'user_id'     => null,
		]);
		Setting::create([
			'key'         => 'initial-map-zoom',
			'value'       => '9',
			'type'        => 'float',
			'description' => 'The initial zoom of your Google Maps.',
			'user_id'     => null,
		]);
		Setting::create([
			'key'         => 'map-marker-icon',
			'value'       => '/images/map-marker-icon.png',//'https://maps.google.com/mapfiles/kml/shapes/library_maps.png',
			'type'        => 'string',
			'description' => 'The image to be used for the Google Map marker icons.',
			'user_id'     => null,
		]);
		/* END SITE SPECIFIC SETTINGS */

		Setting::create([
			'key'         => 'is-user-login-allowed',
			'value'       => 'true',
			'type'        => 'boolean',
			'description' => 'Enables or disables all user logins.',
			'user_id'     => null,
		]);

		/* BEGIN FONT AWESOME ICONS */
		Setting::create([
			'key'         => 'user-fa-icon',
			'value'       => 'user',
			'type'        => 'string',
			'description' => 'The Font Awesome icon to be used for Users.',
			'user_id'     => null,
		]);
		Setting::create([
			'key'         => 'setting-fa-icon',
			'value'       => 'gears',
			'type'        => 'string',
			'description' => 'The Font Awesome icon to be used for Settings.',
			'user_id'     => null,
		]);
		Setting::create([
			'key'         => 'address-fa-icon',
			'value'       => 'address-book',
			'type'        => 'string',
			'description' => 'The Font Awesome icon to be used for Addresses.',
			'user_id'     => null,
		]);
		Setting::create([
			'key'         => 'category-fa-icon',
			'value'       => 'folder',
			'type'        => 'string',
			'description' => 'The Font Awesome icon to be used for Categories.',
			'user_id'     => null,
		]);
		Setting::create([
			'key'         => 'course-fa-icon',
			'value'       => 'graduation-cap',
			'type'        => 'string',
			'description' => 'The Font Awesome icon to be used for Courses.',
			'user_id'     => null,
		]);
		Setting::create([
			'key'         => 'event-group-fa-icon',
			'value'       => 'object-group',
			'type'        => 'string',
			'description' => 'The Font Awesome icon to be used for EventGroups.',
			'user_id'     => null,
		]);
		Setting::create([
			'key'         => 'event-fa-icon',
			'value'       => 'calendar',
			'type'        => 'string',
			'description' => 'The Font Awesome icon to be used for Events.',
			'user_id'     => null,
		]);
		Setting::create([
			'key'         => 'instructor-fa-icon',
			'value'       => 'users',
			'type'        => 'string',
			'description' => 'The Font Awesome icon to be used for Instructors.',
			'user_id'     => null,
		]);
		Setting::create([
			'key'         => 'calendar-fa-icon',
			'value'       => 'calendar',
			'type'        => 'string',
			'description' => 'The Font Awesome icon to be used for Calendars.',
			'user_id'     => null,
		]);
		/* END FONT AWESOME ICONS */
	}
}
