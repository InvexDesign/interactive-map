<?php

use App\Models\Facility;
use App\Models\LoginAttempt;
use App\Models\MonthlyFacilityClickRecord;
use App\Models\MonthlyServiceSearchRecord;
use App\Models\Service;
use Illuminate\Database\Seeder;

class FakeAnalyticsSeeder extends Seeder
{
	public function run()
	{
		$faker = \Faker\Factory::create();

		$min_clicks = 50;
		$max_clicks = 500;
		$min_searches = 5;
		$max_searches = 100;

		$now = Carbon::now();
		$range_in_months = 14;
		$month = (new Carbon('first day of this month'))->startOfDay()->subMonths($range_in_months);
		$facilities = Facility::all();
		$services = Service::all();

		while($month->lt($now))
		{
			foreach($facilities as $facility)
			{
				MonthlyFacilityClickRecord::create([
					'facility_id' => $facility->id,
					'month'       => $month,
					'hits'        => $faker->numberBetween($min_clicks, $max_clicks)
				]);
			}

			foreach($services as $service)
			{
				MonthlyServiceSearchRecord::create([
					'service_id' => $service->id,
					'month'      => $month,
					'hits'       => $faker->numberBetween($min_searches, $max_searches)
				]);
			}

			$month->addMonth();
		}

		for($i = 0; $i < 1000; $i++)
		{
			LoginAttempt::create([
				'username'     => null,
				'ip_address'   => $faker->ipv4,
				'timestamp'    => $faker->dateTimeBetween('-6 months', 'today'),
				'success'      => $faker->boolean(),
				'country'      => $faker->country,
				'region'       => $faker->word,
				'city'         => $faker->city,
				'postal'       => $faker->postcode,
				'latitude'     => $faker->latitude,
				'longitude'    => $faker->longitude,
				'organization' => $faker->domainName,
				'hostname'     => $faker->word,
				'browser'      => $faker->word,
				'referrer'     => $faker->word,
				'user_id'      => null,
			]);
		}
	}
}
