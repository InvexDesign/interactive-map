<?php

use Illuminate\Database\Seeder;

class BaseFacilitySeeder extends Seeder
{
	public function run()
	{
		$this->seedFacilities();
		$this->seedFacilityLanguagePivot();
		$this->seedFacilityServicePivot();
		$this->seedFacilityInsuranceTypePivot();
		$this->seedFacilityTransitOptionPivot();
		$this->seedFacilityAgeGroupPivot();
	}

	private function seedFacilities()
	{
		\DB::table('facilities')->delete();

		\DB::table('facilities')->insert([
			0  =>
				[
					'id'             => 1,
					'uid'            => null,
					'name'           => 'Schaumburg Township',
					'tagline'        => null,
					'organization'   => null,
					'address1'       => '1 Illinois Blvd\\.',
					'address2'       => null,
					'city'           => 'Hoffman Estates',
					'state_id'       => 13,
					'zipcode'        => '60160',
					'auto_gps'       => 1,
					'latitude'       => '42.0277687',
					'longitude'      => '-88.0919772',
					'url'            => '',
					'email'          => '',
					'website'        => 'http\\:\\/\\/www\\.schaumburgtownship\\.org',
					'phone'          => '8478840030',
					'247_hotline'    => null,
					'text_line'      => null,
					'live_chat'      => '',
					'youth_approved' => 0,
					'hours'          => '\\<font face\\=\\"Arial\\"\\>Mon\\-Fri\\: 8\\:30AM \\- 5PM\\<\\/font\\>',
					'image'          => null,
					'description'    => '\\<span style\\=\\"white\\-space\\: pre\\-wrap\\; background\\-color\\: rgb\\(255\\, 255\\, 255\\)\\;\\"\\>\\<font face\\=\\"Arial\\"\\>Schaumburg Township offers an array of services\\: General Assistance \\(GA\\)\\, Emergency Assistance \\(EA\\)\\, Supplemental Nutrition Assistance Program \\(SNAP\\)\\, Access to Care Healthcare Program\\, Food Pantry\\, Residential Special Hardship Program \\(ComEd\\)\\.\\<\\/font\\>\\<\\/span\\>\\<div\\>\\<span style\\=\\"font\\-family\\: Arial\\; white\\-space\\: pre\\-wrap\\; background\\-color\\: rgb\\(255\\, 255\\, 255\\)\\;\\"\\>The township serves the \\<\\/span\\>\\<span style\\=\\"font\\-family\\: Arial\\; white\\-space\\: pre\\-wrap\\; background\\-color\\: rgb\\(255\\, 255\\, 255\\)\\;\\"\\>Schaumburg Township area \\(parts of Elk Grove\\, Hanover Park\\, Hoffman Estates\\, Rolling Meadows\\, Roselle\\, Schaumburg\\, Streamwood and unincorporated areas of Cook County\\)\\.\\<\\/span\\>\\<\\/div\\>',
					'minor_access'   => null,
					'appointments'   => '\\<font face\\=\\"Arial\\"\\>Walk\\-ins acceptable\\.\\<\\/font\\>',
					'order'          => null,
					'notes'          => null,
					'created_at'     => '2017-08-14 10:21:26',
					'updated_at'     => '2017-08-29 11:15:27',
				],
			1  =>
				[
					'id'             => 2,
					'uid'            => null,
					'name'           => 'Children\\\'s Gender and Sex Development Program at Lurie Children\\\'s Hospital',
					'tagline'        => null,
					'organization'   => 'Lurie Children\\\'s Hospital',
					'address1'       => '225 E\\. Chicago Avenue',
					'address2'       => null,
					'city'           => 'Chicago',
					'state_id'       => 13,
					'zipcode'        => '60611',
					'auto_gps'       => 1,
					'latitude'       => '41.8962514',
					'longitude'      => '-87.6219521',
					'url'            => '',
					'email'          => 'Jleininger\\@luriechildrens\\.org',
					'website'        => 'http\\:\\/\\/www\\.luriechildrens\\.org',
					'phone'          => '7733036056',
					'247_hotline'    => null,
					'text_line'      => null,
					'live_chat'      => '',
					'youth_approved' => 0,
					'hours'          => '\\<font face\\=\\"Arial\\"\\>By appointment\\<\\/font\\>',
					'image'          => null,
					'description'    => '\\<span style\\=\\"white\\-space\\: pre\\-wrap\\; background\\-color\\: rgb\\(255\\, 255\\, 255\\)\\;\\"\\>\\<font face\\=\\"Arial\\"\\>Comprehensive medical and psychological consultation for families with gender\\-nonconforming\\, gender\\-questioning and transgender youth\\.\\<\\/font\\>\\<\\/span\\>\\<span style\\=\\"font\\-family\\: Arial\\; white\\-space\\: pre\\-wrap\\; background\\-color\\: rgb\\(255\\, 255\\, 255\\)\\;\\"\\>Routine consultation\\, administration and monitoring of medical treatments to reversibly pause pubertal development among gender\\-questioning youth entering puberty\\. \\<\\/span\\>\\<span style\\=\\"font\\-family\\: Arial\\; white\\-space\\: pre\\-wrap\\; background\\-color\\: rgb\\(255\\, 255\\, 255\\)\\;\\"\\>Connection to legal consultation services \\(e\\.g\\.\\, legal name changing\\, etc\\.\\)\\. \\<\\/span\\>\\<span style\\=\\"font\\-family\\: Arial\\; white\\-space\\: pre\\-wrap\\; background\\-color\\: rgb\\(255\\, 255\\, 255\\)\\;\\"\\>Socializing and support groups\\. \\<\\/span\\>\\<span style\\=\\"font\\-family\\: Arial\\; white\\-space\\: pre\\-wrap\\; background\\-color\\: rgb\\(255\\, 255\\, 255\\)\\;\\"\\>Consultation for community providers and clinicians to enhance gender\\-affirming care\\.\\<\\/span\\>\\<div\\>\\<div\\>\\<span style\\=\\"white\\-space\\: pre\\-wrap\\; background\\-color\\: rgb\\(255\\, 255\\, 255\\)\\;\\"\\>\\<font face\\=\\"Arial\\"\\>\\<br\\>\\<\\/font\\>\\<\\/span\\>\\<\\/div\\>\\<\\/div\\>\\<div\\>\\<span style\\=\\"font\\-family\\: Arial\\; white\\-space\\: pre\\-wrap\\; background\\-color\\: rgb\\(255\\, 255\\, 255\\)\\;\\"\\>Sliding scale for those who qualify\\.\\<\\/span\\>\\<br\\>\\<\\/div\\>',
					'minor_access'   => null,
					'appointments'   => '\\<font face\\=\\"Arial\\"\\>Patients are advised to call ahead\\<\\/font\\>',
					'order'          => null,
					'notes'          => null,
					'created_at'     => '2017-08-14 10:28:33',
					'updated_at'     => '2017-08-28 16:32:12',
				],
			2  =>
				[
					'id'             => 3,
					'uid'            => null,
					'name'           => 'Hanover Park Police Department',
					'tagline'        => null,
					'organization'   => null,
					'address1'       => '2011 W\\. Lake Street',
					'address2'       => null,
					'city'           => 'Hanover Park',
					'state_id'       => 13,
					'zipcode'        => '60133',
					'auto_gps'       => 1,
					'latitude'       => '41.9894734',
					'longitude'      => '-88.1562249',
					'url'            => '',
					'email'          => '',
					'website'        => 'http\\:\\/\\/www\\.hanoverparkillinois\\.org\\/Services\\/Police\\.aspx',
					'phone'          => '6303724400',
					'247_hotline'    => null,
					'text_line'      => null,
					'live_chat'      => '',
					'youth_approved' => 0,
					'hours'          => '\\<font face\\=\\"Arial\\"\\>24\\/7\\<\\/font\\>',
					'image'          => null,
					'description'    => '\\<span style\\=\\"white\\-space\\: pre\\-wrap\\; background\\-color\\: rgb\\(255\\, 255\\, 255\\)\\;\\"\\>\\<font face\\=\\"Arial\\"\\>Relevant services include\\: child car seat installation by appointment only Monday\\- Friday from 8am to 4pm \\(resident \\- free\\, non\\-resident \\- \\$35\\)\\, Law enforcement\\, Traffic\\, Crime prevention\\, Neighborhood watch\\.\\<\\/font\\>\\<\\/span\\>\\<div\\>\\<span style\\=\\"white\\-space\\: pre\\-wrap\\; background\\-color\\: rgb\\(255\\, 255\\, 255\\)\\;\\"\\>\\<font face\\=\\"Arial\\"\\>\\<br\\>\\<\\/font\\>\\<\\/span\\>\\<\\/div\\>\\<div\\>\\<span style\\=\\"white\\-space\\: pre\\-wrap\\; background\\-color\\: rgb\\(255\\, 255\\, 255\\)\\;\\"\\>\\<font face\\=\\"Arial\\"\\>Call 9\\-1\\-1 for emergencies\\.\\<\\/font\\>\\<\\/span\\>\\<\\/div\\>',
					'minor_access'   => null,
					'appointments'   => '\\<font face\\=\\"Arial\\"\\>Walk\\-ins accepted\\<\\/font\\>',
					'order'          => null,
					'notes'          => null,
					'created_at'     => '2017-08-14 10:36:32',
					'updated_at'     => '2017-08-29 10:12:39',
				],
			3  =>
				[
					'id'             => 5,
					'uid'            => null,
					'name'           => 'Tranformative Justice Law Project',
					'tagline'        => null,
					'organization'   => null,
					'address1'       => '203 N\\. Lasalle',
					'address2'       => 'Suite 2100',
					'city'           => 'Chicago',
					'state_id'       => 13,
					'zipcode'        => '60601',
					'auto_gps'       => 1,
					'latitude'       => '41.8860480',
					'longitude'      => '-87.6316725',
					'url'            => '',
					'email'          => 'info\\@tjlp\\.org',
					'website'        => 'http\\:\\/\\/www\\.tjlp\\.org',
					'phone'          => '3125581472',
					'247_hotline'    => null,
					'text_line'      => null,
					'live_chat'      => '',
					'youth_approved' => 0,
					'hours'          => '\\<font face\\=\\"Arial\\"\\>By appointment\\<\\/font\\>',
					'image'          => null,
					'description'    => '\\<span style\\=\\"white\\-space\\: pre\\-wrap\\; background\\-color\\: rgb\\(255\\, 255\\, 255\\)\\;\\"\\>\\<font face\\=\\"Arial\\"\\>Support\\, advocacy\\, and criminal legal services to low\\-income transgender individuals\\.\\<\\/font\\>\\<\\/span\\>\\<div\\>\\<font face\\=\\"Arial\\"\\>\\<span style\\=\\"white\\-space\\: pre\\-wrap\\; background\\-color\\: rgb\\(255\\, 255\\, 255\\)\\;\\"\\>\\<br\\>\\<\\/span\\>\\<\\/font\\>\\<div\\>\\<font face\\=\\"Arial\\"\\>Clients will not be turned away due to inability to pay\\.\\<\\/font\\>\\<span style\\=\\"font\\-family\\: arial\\, sans\\, sans\\-serif\\; font\\-size\\: 13px\\; white\\-space\\: pre\\-wrap\\; background\\-color\\: rgb\\(255\\, 255\\, 255\\)\\;\\"\\>\\<br\\>\\<\\/span\\>\\<\\/div\\>\\<\\/div\\>',
					'minor_access'   => null,
					'appointments'   => '\\<font face\\=\\"Arial\\"\\>Patients are advised to call ahead\\.\\<\\/font\\>',
					'order'          => null,
					'notes'          => null,
					'created_at'     => '2017-08-14 11:12:07',
					'updated_at'     => '2017-08-29 11:28:13',
				],
			4  =>
				[
					'id'             => 6,
					'uid'            => null,
					'name'           => 'AMITA Health',
					'tagline'        => null,
					'organization'   => 'Women\\\'s Health First',
					'address1'       => '600 W\\. Lake Cook Road',
					'address2'       => 'Suite 120',
					'city'           => 'Buffalo Grove',
					'state_id'       => 13,
					'zipcode'        => '60089',
					'auto_gps'       => 1,
					'latitude'       => '42.1522262',
					'longitude'      => '-87.9728888',
					'url'            => '',
					'email'          => '',
					'website'        => 'http\\:\\/\\/www\\.womenfirst\\.net',
					'phone'          => '8478088884',
					'247_hotline'    => null,
					'text_line'      => null,
					'live_chat'      => '',
					'youth_approved' => 0,
					'hours'          => '\\<div\\>\\<font face\\=\\"Arial\\"\\>Mon\\-Thu\\: 8AM \\- 7PM\\<\\/font\\>\\<\\/div\\>\\<div\\>\\<font face\\=\\"Arial\\"\\>Fri\\: 8AM \\- 5PM\\<\\/font\\>\\<\\/div\\>\\<div\\>\\<font face\\=\\"Arial\\"\\>Sat\\: By appointment\\<\\/font\\>\\<\\/div\\>',
					'image'          => null,
					'description'    => '\\<span style\\=\\"white\\-space\\: pre\\-wrap\\; background\\-color\\: rgb\\(255\\, 255\\, 255\\)\\;\\"\\>\\<font face\\=\\"Arial\\"\\>Parent education\\, informational handouts\\, and support groups\\. \\<\\/font\\>\\<\\/span\\>\\<div\\>\\<font face\\=\\"Arial\\"\\>\\<br\\>\\<\\/font\\>\\<\\/div\\>\\<div\\>\\<font face\\=\\"Arial\\"\\>Checks\\, cash and credit cards accepted\\. \\*\\<\\/font\\>\\<span style\\=\\"font\\-family\\: Arial\\;\\"\\>Must have insurance and pay on same day\\.\\<\\/span\\>\\<\\/div\\>\\<div\\>\\<br\\>\\<\\/div\\>',
					'minor_access'   => '\\<span style\\=\\"font\\-family\\: Arial\\;\\"\\>If they are pregnant\\, they may get discounted services or be referred to another \\(cheaper\\) facility\\.\\<\\/span\\>',
					'appointments'   => '\\<font face\\=\\"Arial\\"\\>Patients are advised to call ahead\\<\\/font\\>',
					'order'          => null,
					'notes'          => null,
					'created_at'     => '2017-08-14 11:20:42',
					'updated_at'     => '2017-08-28 16:30:07',
				],
			6  =>
				[
					'id'             => 8,
					'uid'            => null,
					'name'           => 'ACCESS \\- Northwest Family Health Center',
					'tagline'        => null,
					'organization'   => null,
					'address1'       => '675 W\\. Central Road',
					'address2'       => null,
					'city'           => 'Arlington Heights',
					'state_id'       => 13,
					'zipcode'        => '60005',
					'auto_gps'       => 1,
					'latitude'       => '42.0661640',
					'longitude'      => '-87.9908700',
					'url'            => '',
					'email'          => '',
					'website'        => 'http\\:\\/\\/www\\.accesscommunityhealth\\.net',
					'phone'          => '8473421554',
					'247_hotline'    => null,
					'text_line'      => null,
					'live_chat'      => '',
					'youth_approved' => 0,
					'hours'          => '\\<div\\>\\<font face\\=\\"Arial\\"\\>Mon \\- Thu\\: 8AM \\- 7PM\\<\\/font\\>\\<\\/div\\>\\<div\\>\\<font face\\=\\"Arial\\"\\>Fri \\- Sat\\: 8AM \\- 5PM\\<\\/font\\>\\<\\/div\\>\\<div\\>\\<font face\\=\\"Arial\\"\\>Sun\\: Closed\\<\\/font\\>\\<\\/div\\>',
					'image'          => null,
					'description'    => '\\<span style\\=\\"background\\-color\\: rgb\\(255\\, 255\\, 255\\)\\;\\"\\>\\<span style\\=\\"white\\-space\\: pre\\-wrap\\;\\"\\>\\<font face\\=\\"Arial\\"\\>Relevant services include\\: Pediatrics\\, Behavioral Health\\, Ophthalmology\\, Family Medicine\\, Obstetrics and Gynecology\\, Internal Medicine\\, School\\/Sports Physicals\\, Management of Chronic Diseases\\, On\\-site Language Interpretation \\(Spoken\\/ASL\\) serving the northwest Chicago suburbs\\.\\<\\/font\\>\\<\\/span\\>\\<\\/span\\>\\<div\\>\\<font face\\=\\"Arial\\"\\>\\<span style\\=\\"white\\-space\\: pre\\-wrap\\; background\\-color\\: rgb\\(255\\, 255\\, 255\\)\\;\\"\\>\\<br\\>\\<\\/span\\>\\<\\/font\\>\\<\\/div\\>\\<div\\>\\<font face\\=\\"Arial\\"\\>Private Insurance\\, medicaid\\, and medicare accepted\\.\\<\\/font\\>\\<\\/div\\>\\<div\\>\\<font face\\=\\"Arial\\"\\>Sliding scale for those who qualify\\.\\<span style\\=\\"white\\-space\\: pre\\-wrap\\; background\\-color\\: rgb\\(255\\, 255\\, 255\\)\\;\\"\\>\\<br\\>\\<\\/span\\>\\<\\/font\\>\\<div\\>\\<div\\>\\<br\\>\\<\\/div\\>\\<div\\>\\<span style\\=\\"background\\-color\\: rgb\\(255\\, 255\\, 255\\)\\;\\"\\>\\<font face\\=\\"Calibri\\"\\>\\<span style\\=\\"white\\-space\\: pre\\-wrap\\;\\"\\>\\<br\\>\\<\\/span\\>\\<\\/font\\>\\<\\/span\\>\\<\\/div\\>\\<\\/div\\>\\<\\/div\\>',
					'minor_access'   => null,
					'appointments'   => '\\<font face\\=\\"Arial\\"\\>Patients are advised to call ahead\\.\\<\\/font\\>',
					'order'          => null,
					'notes'          => null,
					'created_at'     => '2017-08-14 11:28:39',
					'updated_at'     => '2017-08-28 16:05:27',
				],
			7  =>
				[
					'id'             => 9,
					'uid'            => null,
					'name'           => 'Willow Creek Care Center',
					'tagline'        => null,
					'organization'   => null,
					'address1'       => '67 E\\. Algonquin Road',
					'address2'       => null,
					'city'           => 'South Barrington',
					'state_id'       => 13,
					'zipcode'        => '60010',
					'auto_gps'       => 1,
					'latitude'       => '42.1403305',
					'longitude'      => '-88.2441772',
					'url'            => '',
					'email'          => 'carecenter\\@willowcreek\\.org',
					'website'        => 'http\\:\\/\\/www\\.willowcreekcarecenter\\.org',
					'phone'          => '2245122600',
					'247_hotline'    => null,
					'text_line'      => null,
					'live_chat'      => '',
					'youth_approved' => 0,
					'hours'          => '\\<div\\>\\<font face\\=\\"Arial\\"\\>Mon\\-Thur\\: 10AM \\- 12\\:30PM\\; 6\\:30 \\- 7\\:30PM\\<\\/font\\>\\<\\/div\\>\\<div\\>\\<font face\\=\\"Arial\\"\\>Fri\\: Closed\\<\\/font\\>\\<\\/div\\>\\<div\\>\\<font face\\=\\"Arial\\"\\>Sat\\: 8\\:30AM \\- 10\\:30AM\\<\\/font\\>\\<\\/div\\>\\<div\\>\\<font face\\=\\"Arial\\"\\>Sun\\: 1\\:30PM \\- 3PM\\<\\/font\\>\\<\\/div\\>',
					'image'          => null,
					'description'    => '\\<span style\\=\\"white\\-space\\: pre\\-wrap\\; background\\-color\\: rgb\\(255\\, 255\\, 255\\)\\;\\"\\>\\<font face\\=\\"Arial\\"\\>Emergency shelter for individuals or families who are homeless\\, Public Action to Deliver Shelter \\(PADS\\)\\, Health services\\, Legal services\\, Financial services\\.\\<\\/font\\>\\<\\/span\\>',
					'minor_access'   => null,
					'appointments'   => '\\<font face\\=\\"Arial\\"\\>Walk\\-ins accepted\\<\\/font\\>',
					'order'          => null,
					'notes'          => null,
					'created_at'     => '2017-08-14 11:31:14',
					'updated_at'     => '2017-08-29 11:34:05',
				],
			8  =>
				[
					'id'             => 10,
					'uid'            => null,
					'name'           => 'The Society for the Prevention of Teen Suicide',
					'tagline'        => null,
					'organization'   => null,
					'address1'       => '110 W\\. Main Street',
					'address2'       => null,
					'city'           => 'Freehold',
					'state_id'       => 30,
					'zipcode'        => '07728',
					'auto_gps'       => 1,
					'latitude'       => '40.2556469',
					'longitude'      => '-74.2804190',
					'url'            => '',
					'email'          => '',
					'website'        => 'http\\:\\/\\/www\\.sptsusa\\.org',
					'phone'          => '7324107900',
					'247_hotline'    => '8002738255',
					'text_line'      => null,
					'live_chat'      => '',
					'youth_approved' => 0,
					'hours'          => '',
					'image'          => null,
					'description'    => '\\<span style\\=\\"white\\-space\\: pre\\-wrap\\; background\\-color\\: rgb\\(255\\, 255\\, 255\\)\\;\\"\\>\\<font face\\=\\"Arial\\"\\>Teen suicide prevention resources for parents\\.\\<\\/font\\>\\<\\/span\\>',
					'minor_access'   => null,
					'appointments'   => null,
					'order'          => null,
					'notes'          => null,
					'created_at'     => '2017-08-14 11:34:01',
					'updated_at'     => '2017-08-29 11:22:57',
				],
			9  =>
				[
					'id'             => 63,
					'uid'            => null,
					'name'           => 'WINGS Program\\, Inc\\.',
					'tagline'        => null,
					'organization'   => null,
					'address1'       => 'P\\.O\\. Box 95615',
					'address2'       => null,
					'city'           => 'Palatine',
					'state_id'       => 13,
					'zipcode'        => '60095',
					'auto_gps'       => 1,
					'latitude'       => '42.0993621',
					'longitude'      => '-88.0130707',
					'url'            => '',
					'email'          => '',
					'website'        => 'http\\:\\/\\/wingsprogram\\.com',
					'phone'          => '8475197820',
					'247_hotline'    => '8472215680',
					'text_line'      => null,
					'live_chat'      => '',
					'youth_approved' => 0,
					'hours'          => '\\<style type\\=\\"text\\/css\\"\\>\\<\\!\\-\\-td \\{border\\: 1px solid \\#ccc\\;\\}br \\{mso\\-data\\-placement\\:same\\-cell\\;\\}\\-\\-\\>\\<\\/style\\>\\<span style\\=\\"font\\-family\\: Arial\\;\\" data\\-sheets\\-value\\=\\"\\{\\" 1\\"\\:2\\,\\"2\\"\\:\\"business\\=\\"\\" hours\\:\\=\\"\\" 8\\:30am\\=\\"\\" \\-\\=\\"\\" 4\\:30pm\\"\\}\\"\\=\\"\\" data\\-sheets\\-userformat\\=\\"\\{\\" 2\\"\\:13057\\,\\"3\\"\\:\\[null\\,0\\]\\,\\"11\\"\\:4\\,\\"12\\"\\:0\\,\\"15\\"\\:\\"arial\\"\\,\\"16\\"\\:11\\}\\"\\=\\"\\"\\>Mon\\-Fri\\: 8\\:30AM \\- 4\\:30PM\\<\\/span\\>\\<div\\>\\<span style\\=\\"font\\-size\\: 11pt\\; font\\-family\\: Arial\\;\\" data\\-sheets\\-value\\=\\"\\{\\" 1\\"\\:2\\,\\"2\\"\\:\\"business\\=\\"\\" hours\\:\\=\\"\\" 8\\:30am\\=\\"\\" \\-\\=\\"\\" 4\\:30pm\\"\\}\\"\\=\\"\\" data\\-sheets\\-userformat\\=\\"\\{\\" 2\\"\\:13057\\,\\"3\\"\\:\\[null\\,0\\]\\,\\"11\\"\\:4\\,\\"12\\"\\:0\\,\\"15\\"\\:\\"arial\\"\\,\\"16\\"\\:11\\}\\"\\=\\"\\"\\>\\<br\\>\\<\\/span\\>\\<\\/div\\>',
					'image'          => null,
					'description'    => '\\<style type\\=\\"text\\/css\\"\\>\\<\\!\\-\\-td \\{border\\: 1px solid \\#ccc\\;\\}br \\{mso\\-data\\-placement\\:same\\-cell\\;\\}\\-\\-\\>\\<\\/style\\>\\<span style\\=\\"font\\-family\\: Arial\\;\\" data\\-sheets\\-value\\=\\"\\{\\" 1\\"\\:2\\,\\"2\\"\\:\\"emergency\\=\\"\\" safe\\=\\"\\" housing\\,\\=\\"\\" transitional\\=\\"\\" behavior\\=\\"\\" assessment\\,\\=\\"\\" individual\\=\\"\\" and\\=\\"\\" group\\=\\"\\" counseling\\,\\=\\"\\" art\\=\\"\\" play\\=\\"\\" therapy\\,\\=\\"\\" recreational\\=\\"\\" activities\\,\\=\\"\\" goal\\=\\"\\" setting\\,\\=\\"\\" relationship\\=\\"\\" building\\,\\=\\"\\" teen\\=\\"\\" dating\\=\\"\\" violence\\=\\"\\" information\\,\\=\\"\\" mentoring\\=\\"\\" services\\"\\}\\"\\=\\"\\" data\\-sheets\\-userformat\\=\\"\\{\\" 2\\"\\:13187\\,\\"3\\"\\:\\[null\\,0\\]\\,\\"4\\"\\:\\[null\\,2\\,16573901\\]\\,\\"10\\"\\:2\\,\\"11\\"\\:4\\,\\"12\\"\\:0\\,\\"15\\"\\:\\"arial\\"\\,\\"16\\"\\:11\\}\\"\\=\\"\\"\\>Emergency Safe Housing\\, Transitional Housing\\, Behavior Assessment\\, Individual and Group Counseling\\, Art and Play Therapy\\, Recreational Activities\\, Goal Setting\\, Relationship Building\\, Teen Dating Violence Information\\, Mentoring Services\\.\\<\\/span\\>\\<div\\>\\<span style\\=\\"font\\-size\\: 11pt\\; font\\-family\\: Arial\\;\\" data\\-sheets\\-value\\=\\"\\{\\" 1\\"\\:2\\,\\"2\\"\\:\\"emergency\\=\\"\\" safe\\=\\"\\" housing\\,\\=\\"\\" transitional\\=\\"\\" behavior\\=\\"\\" assessment\\,\\=\\"\\" individual\\=\\"\\" and\\=\\"\\" group\\=\\"\\" counseling\\,\\=\\"\\" art\\=\\"\\" play\\=\\"\\" therapy\\,\\=\\"\\" recreational\\=\\"\\" activities\\,\\=\\"\\" goal\\=\\"\\" setting\\,\\=\\"\\" relationship\\=\\"\\" building\\,\\=\\"\\" teen\\=\\"\\" dating\\=\\"\\" violence\\=\\"\\" information\\,\\=\\"\\" mentoring\\=\\"\\" services\\"\\}\\"\\=\\"\\" data\\-sheets\\-userformat\\=\\"\\{\\" 2\\"\\:13187\\,\\"3\\"\\:\\[null\\,0\\]\\,\\"4\\"\\:\\[null\\,2\\,16573901\\]\\,\\"10\\"\\:2\\,\\"11\\"\\:4\\,\\"12\\"\\:0\\,\\"15\\"\\:\\"arial\\"\\,\\"16\\"\\:11\\}\\"\\=\\"\\"\\>\\<br\\>\\<\\/span\\>\\<\\/div\\>\\<div\\>\\<span style\\=\\"font\\-size\\: 11pt\\; font\\-family\\: Arial\\;\\" data\\-sheets\\-value\\=\\"\\{\\" 1\\"\\:2\\,\\"2\\"\\:\\"emergency\\=\\"\\" safe\\=\\"\\" housing\\,\\=\\"\\" transitional\\=\\"\\" behavior\\=\\"\\" assessment\\,\\=\\"\\" individual\\=\\"\\" and\\=\\"\\" group\\=\\"\\" counseling\\,\\=\\"\\" art\\=\\"\\" play\\=\\"\\" therapy\\,\\=\\"\\" recreational\\=\\"\\" activities\\,\\=\\"\\" goal\\=\\"\\" setting\\,\\=\\"\\" relationship\\=\\"\\" building\\,\\=\\"\\" teen\\=\\"\\" dating\\=\\"\\" violence\\=\\"\\" information\\,\\=\\"\\" mentoring\\=\\"\\" services\\"\\}\\"\\=\\"\\" data\\-sheets\\-userformat\\=\\"\\{\\" 2\\"\\:13187\\,\\"3\\"\\:\\[null\\,0\\]\\,\\"4\\"\\:\\[null\\,2\\,16573901\\]\\,\\"10\\"\\:2\\,\\"11\\"\\:4\\,\\"12\\"\\:0\\,\\"15\\"\\:\\"arial\\"\\,\\"16\\"\\:11\\}\\"\\=\\"\\"\\>Hotline\\- 24 hour Emergency Shelter Assistance\\<\\/span\\>\\<\\/div\\>',
					'minor_access'   => '\\<style type\\=\\"text\\/css\\"\\>\\<\\!\\-\\-td \\{border\\: 1px solid \\#ccc\\;\\}br \\{mso\\-data\\-placement\\:same\\-cell\\;\\}\\-\\-\\>\\<\\/style\\>\\<span style\\=\\"font\\-family\\: Arial\\;\\" data\\-sheets\\-value\\=\\"\\{\\" 1\\"\\:2\\,\\"2\\"\\:\\"children\\=\\"\\" must\\=\\"\\" be\\=\\"\\" with\\=\\"\\" parent\\=\\"\\" guardian\\=\\"\\" to\\=\\"\\" access\\=\\"\\" emergency\\=\\"\\" shelter\\.\\=\\"\\" \\"\\}\\"\\=\\"\\" data\\-sheets\\-userformat\\=\\"\\{\\" 2\\"\\:13185\\,\\"3\\"\\:\\[null\\,0\\]\\,\\"10\\"\\:2\\,\\"11\\"\\:4\\,\\"12\\"\\:0\\,\\"15\\"\\:\\"arial\\"\\,\\"16\\"\\:11\\}\\"\\=\\"\\"\\>Parent permission required\\. Children must be with parent\\/guardian to access emergency shelter\\.\\<\\/span\\>',
					'appointments'   => '\\<style type\\=\\"text\\/css\\"\\>\\<\\!\\-\\-td \\{border\\: 1px solid \\#ccc\\;\\}br \\{mso\\-data\\-placement\\:same\\-cell\\;\\}\\-\\-\\>\\<\\/style\\>\\<span style\\=\\"font\\-family\\: Arial\\;\\" data\\-sheets\\-value\\=\\"\\{\\" 1\\"\\:2\\,\\"2\\"\\:\\"patients\\=\\"\\" advised\\=\\"\\" to\\=\\"\\" call\\=\\"\\" ahead\\"\\}\\"\\=\\"\\" data\\-sheets\\-userformat\\=\\"\\{\\" 2\\"\\:13185\\,\\"3\\"\\:\\[null\\,0\\]\\,\\"10\\"\\:2\\,\\"11\\"\\:4\\,\\"12\\"\\:0\\,\\"15\\"\\:\\"arial\\"\\,\\"16\\"\\:11\\}\\"\\=\\"\\"\\>Patients advised to call ahead and make an appointment\\.\\<\\/span\\>',
					'order'          => null,
					'notes'          => null,
					'created_at'     => '2017-08-29 11:23:33',
					'updated_at'     => '2017-08-29 11:53:25',
				],
			10 =>
				[
					'id'             => 11,
					'uid'            => null,
					'name'           => 'Open Door Clinic',
					'tagline'        => null,
					'organization'   => null,
					'address1'       => '1665 Larkin Avenue',
					'address2'       => null,
					'city'           => 'Elgin',
					'state_id'       => 13,
					'zipcode'        => '60123',
					'auto_gps'       => 1,
					'latitude'       => '42.0334437',
					'longitude'      => '-88.3190346',
					'url'            => '',
					'email'          => '',
					'website'        => 'https\\:\\/\\/odhcil\\.org\\/',
					'phone'          => '8476951093',
					'247_hotline'    => null,
					'text_line'      => null,
					'live_chat'      => '',
					'youth_approved' => 1,
					'hours'          => '\\<span style\\=\\"font\\-family\\: Arial\\; white\\-space\\: pre\\-wrap\\; background\\-color\\: rgb\\(255\\, 255\\, 255\\)\\;\\"\\>Mon\\-Fri\\: 8\\:30AM \\- 4\\:30PM \\<\\/span\\>',
					'image'          => null,
					'description'    => '\\<span style\\=\\"font\\-family\\: Arial\\; white\\-space\\: pre\\-wrap\\; background\\-color\\: rgb\\(255\\, 255\\, 255\\)\\;\\"\\>Routine medical care including yearly exams\\, vaccinations and referrals to specialists\\, STI testing\\, Substance Abuse services\\, Diagnostic evaluation\\, Behavioral health discharge planning\\, Chronic disease and chronic pain management\\, Health related anxiety and trauma\\, Depression\\/mood disorders\\, Learn To live a fulfilling life with HIV\\, LGBTQI concerns\\, Cognitive function screening \\&amp\\; referral\\.\\<\\/span\\>\\<div\\>\\<br\\>\\<br\\>\\<\\/div\\>\\<div\\>\\<span style\\=\\"background\\-color\\: rgb\\(255\\, 255\\, 255\\)\\;\\"\\>\\<span style\\=\\"font\\-family\\: Arial\\; white\\-space\\: pre\\-wrap\\;\\"\\>\\$100 per STI test\\. Some patients may quality for free HIV and STD\\/STI testing\\.  \\<\\/span\\>\\<span style\\=\\"font\\-family\\: Arial\\; font\\-size\\: 15px\\; white\\-space\\: pre\\-wrap\\;\\"\\>\\<br\\>\\<\\/span\\>\\<\\/span\\>\\<\\/div\\>\\<div\\>\\<span style\\=\\"font\\-family\\: Arial\\; white\\-space\\: pre\\-wrap\\; background\\-color\\: rgb\\(255\\, 255\\, 255\\)\\;\\"\\>Cash and Credit Cards are accepted\\.\\&nbsp\\;\\<\\/span\\>\\<br\\>\\<\\/div\\>\\<div\\>\\<span style\\=\\"font\\-family\\: Arial\\; font\\-size\\: 15px\\; white\\-space\\: pre\\-wrap\\; background\\-color\\: rgb\\(255\\, 255\\, 255\\)\\;\\"\\>\\<br\\>\\<\\/span\\>\\<\\/div\\>',
					'minor_access'   => '\\<font face\\=\\"Arial\\"\\>Parent permission required\\.\\<\\/font\\>',
					'appointments'   => '\\<font face\\=\\"Arial\\"\\>Patients advised to call ahead\\.\\<\\/font\\>',
					'order'          => null,
					'notes'          => null,
					'created_at'     => '2017-08-21 09:53:52',
					'updated_at'     => '2017-08-29 11:02:33',
				],
			11 =>
				[
					'id'             => 12,
					'uid'            => null,
					'name'           => 'CareNet Pregnancy Services',
					'tagline'        => null,
					'organization'   => null,
					'address1'       => '132 E\\. Irving Park Road',
					'address2'       => null,
					'city'           => 'Wood Dale',
					'state_id'       => 13,
					'zipcode'        => '60191',
					'auto_gps'       => 1,
					'latitude'       => '41.9628463',
					'longitude'      => '-87.9776560',
					'url'            => '',
					'email'          => '',
					'website'        => 'http\\:\\/\\/www\\.pregnanthelp\\.com',
					'phone'          => '6306861850',
					'247_hotline'    => '6304550300',
					'text_line'      => '313131',
					'live_chat'      => '',
					'youth_approved' => 1,
					'hours'          => '\\<span style\\=\\"white\\-space\\: pre\\-wrap\\; background\\-color\\: rgb\\(255\\, 255\\, 255\\)\\;\\"\\>\\<font face\\=\\"Arial\\"\\>Sun\\-Mon\\: Closed\\<\\/font\\>\\<\\/span\\>\\<div\\>\\<span style\\=\\"white\\-space\\: pre\\-wrap\\; background\\-color\\: rgb\\(255\\, 255\\, 255\\)\\;\\"\\>\\<font face\\=\\"Arial\\"\\>Tue\\-Fri\\: 9AM \\- 5PM\\<\\/font\\>\\<\\/span\\>\\<\\/div\\>\\<div\\>\\<span style\\=\\"white\\-space\\: pre\\-wrap\\; background\\-color\\: rgb\\(255\\, 255\\, 255\\)\\;\\"\\>\\<font face\\=\\"Arial\\"\\>Sat\\: 8AM \\- 12PM\\<\\/font\\>\\<\\/span\\>\\<\\/div\\>',
					'image'          => null,
					'description'    => '\\<span style\\=\\"font\\-family\\: Arial\\; white\\-space\\: pre\\-wrap\\; background\\-color\\: rgb\\(255\\, 255\\, 255\\)\\;\\"\\>Abortion Information\\, Adoption Information\\, Housing Referrals\\, Maternity \\& Infant Supplies\\, Medical Referrals\\, Parenting Education\\, Post\\-Abortion Support\\, Pregnancy Options Information\\, Pregnancy Tests\\, STD\\/STI Information\\, Limited Ultrasound \\(Onsite\\)\\<\\/span\\>\\<br\\>',
					'minor_access'   => '\\<font face\\=\\"Arial\\"\\>All services provided are free and confidential\\<\\/font\\>',
					'appointments'   => '\\<font face\\=\\"Arial\\"\\>Same day appointments available\\<\\/font\\>',
					'order'          => null,
					'notes'          => null,
					'created_at'     => '2017-08-21 10:05:39',
					'updated_at'     => '2017-08-28 16:31:32',
				],
			12 =>
				[
					'id'             => 13,
					'uid'            => null,
					'name'           => 'Planned Parenthood',
					'tagline'        => null,
					'organization'   => null,
					'address1'       => '3051 E\\. New York Street',
					'address2'       => null,
					'city'           => 'Aurora',
					'state_id'       => 13,
					'zipcode'        => '60504',
					'auto_gps'       => 1,
					'latitude'       => '41.7581522',
					'longitude'      => '-88.2392652',
					'url'            => '',
					'email'          => '',
					'website'        => 'https\\:\\/\\/www\\.plannedparenthood\\.org',
					'phone'          => '6305855588',
					'247_hotline'    => null,
					'text_line'      => '774636',
					'live_chat'      => 'https\\:\\/\\/www\\.plannedparenthood\\.org\\/online\\-tools\\/chat',
					'youth_approved' => 1,
					'hours'          => '\\<font face\\=\\"Arial\\"\\>\\<span style\\=\\"white\\-space\\: pre\\-wrap\\;\\"\\>Mon\\-Wed\\: 8AM \\- 5PM\\<\\/span\\>\\<\\/font\\>\\<br\\>\\<div\\>\\<font face\\=\\"Arial\\"\\>\\<span style\\=\\"white\\-space\\: pre\\-wrap\\;\\"\\>Thu\\: 10AM \\- 6PM\\<\\/span\\>\\<\\/font\\>\\<br\\>\\<\\/div\\>\\<div\\>\\<span style\\=\\"font\\-family\\: Arial\\; white\\-space\\: pre\\-wrap\\; background\\-color\\: rgb\\(255\\, 255\\, 255\\)\\;\\"\\>Fri\\: 8AM \\- 3PM\\<\\/span\\>\\<\\/div\\>\\<div\\>\\<span style\\=\\"font\\-family\\: Arial\\; white\\-space\\: pre\\-wrap\\; background\\-color\\: rgb\\(255\\, 255\\, 255\\)\\;\\"\\>Sat\\: 7AM \\- 3PM \\<\\/span\\>\\<\\/div\\>',
					'image'          => null,
					'description'    => '\\<span style\\=\\"font\\-family\\: Arial\\; white\\-space\\: pre\\-wrap\\; background\\-color\\: rgb\\(255\\, 255\\, 255\\)\\;\\"\\>Abortion Services\\, Birth Control\\, HIV Testing\\, LGBTQ\\+ Services\\, Men\\\'s Health Care\\, Emergency Contraception\\, Pregnancy Testing\\, Pregnancy Services\\, STD\\/STI Testing\\, Treatment\\, Vaccinations\\, Women\\\'s Health Care\\, Checkups\\: Reproductive\\/Sexual Health Issue\\, Breast Exams\\, Cervical Cancer Screening\\, Colposcopy\\, Loop Electrosurgical Excision Procedure\\, Mammogram Referrals\\, Papanicolaou Test \\(PAP\\)\\, Sexual Response Education\\, Testing and Treatment for Urinary Tract and Vaginal Infections\\<\\/span\\>\\<br\\>\\<div\\>\\<span style\\=\\"font\\-family\\: Arial\\; white\\-space\\: pre\\-wrap\\; background\\-color\\: rgb\\(255\\, 255\\, 255\\)\\;\\"\\>\\<br\\>\\<\\/span\\>\\<\\/div\\>\\<div\\>\\<span style\\=\\"font\\-family\\: Arial\\; white\\-space\\: pre\\-wrap\\; background\\-color\\: rgb\\(255\\, 255\\, 255\\)\\;\\"\\>Sliding Fee Scale for those who qualify\\.\\<\\/span\\>\\<\\/div\\>\\<div\\>\\<span style\\=\\"font\\-family\\: Arial\\; white\\-space\\: pre\\-wrap\\; background\\-color\\: rgb\\(255\\, 255\\, 255\\)\\;\\"\\>Cash and credit card accepted\\.\\<\\/span\\>\\<\\/div\\>',
					'minor_access'   => '\\<font face\\=\\"Arial\\"\\>Parent permission not required\\.\\&nbsp\\;\\<span style\\=\\"white\\-space\\: pre\\-wrap\\; background\\-color\\: rgb\\(255\\, 255\\, 255\\)\\;\\"\\>Must be 12 or older to access STD\\/STI\\/HIV and pregnancy testing\\. Parental notification for abortion\\.\\<\\/span\\>\\<\\/font\\>',
					'appointments'   => '\\<span style\\=\\"white\\-space\\: pre\\-wrap\\; background\\-color\\: rgb\\(255\\, 255\\, 255\\)\\;\\"\\>\\<font face\\=\\"Arial\\"\\>Same day appointments available \\&amp\\; walk\\-ins accepted\\. Patients advised to call ahead\\.\\<\\/font\\>\\<\\/span\\>',
					'order'          => null,
					'notes'          => null,
					'created_at'     => '2017-08-21 13:13:44',
					'updated_at'     => '2017-08-29 11:06:06',
				],
			13 =>
				[
					'id'             => 14,
					'uid'            => null,
					'name'           => 'Village of Schaumburg Police Department',
					'tagline'        => 'police',
					'organization'   => null,
					'address1'       => '1000 Schaumburg Road',
					'address2'       => null,
					'city'           => 'Schaumburg',
					'state_id'       => 13,
					'zipcode'        => '60194',
					'auto_gps'       => 1,
					'latitude'       => '42.0283412',
					'longitude'      => '-88.0558025',
					'url'            => '',
					'email'          => '',
					'website'        => 'http\\:\\/\\/www\\.villageofschaumburg\\.com',
					'phone'          => '8478823586',
					'247_hotline'    => null,
					'text_line'      => null,
					'live_chat'      => '',
					'youth_approved' => 0,
					'hours'          => '\\<font face\\=\\"Arial\\"\\>24\\/7\\<\\/font\\>',
					'image'          => null,
					'description'    => '\\<span data\\-sheets\\-value\\=\\"\\{\\" 1\\"\\:2\\,\\"2\\"\\:\\"law\\=\\"\\" enforcement\\,\\=\\"\\" traffic\\,\\=\\"\\" crime\\=\\"\\" prevention\\,\\=\\"\\" neighborhood\\=\\"\\" watch\\"\\}\\"\\=\\"\\" data\\-sheets\\-userformat\\=\\"\\{\\" 2\\"\\:13187\\,\\"3\\"\\:\\[null\\,0\\]\\,\\"4\\"\\:\\[null\\,2\\,16573901\\]\\,\\"10\\"\\:2\\,\\"11\\"\\:4\\,\\"12\\"\\:0\\,\\"15\\"\\:\\"arial\\"\\,\\"16\\"\\:11\\}\\"\\=\\"\\"\\>\\<font face\\=\\"Arial\\"\\>Law Enforcement\\, Traffic\\, Crime Prevention\\, Neighborhood Watch\\<\\/font\\>\\<\\/span\\>',
					'minor_access'   => '\\<font face\\=\\"Arial\\"\\>Parent permission not required\\.\\<\\/font\\>',
					'appointments'   => '\\<font face\\=\\"Arial\\"\\>Walk\\-ins accepted\\.\\<\\/font\\>',
					'order'          => null,
					'notes'          => null,
					'created_at'     => '2017-08-21 14:22:24',
					'updated_at'     => '2017-08-29 11:28:54',
				],
			14 =>
				[
					'id'             => 15,
					'uid'            => null,
					'name'           => 'The Village of Palatine Police Department',
					'tagline'        => 'police',
					'organization'   => null,
					'address1'       => '595 N\\. Hicks Road',
					'address2'       => null,
					'city'           => 'Palatine',
					'state_id'       => 13,
					'zipcode'        => '60074',
					'auto_gps'       => 1,
					'latitude'       => '42.1211209',
					'longitude'      => '-88.0326811',
					'url'            => '',
					'email'          => '',
					'website'        => 'http\\:\\/\\/www\\.palatine\\.il\\.us',
					'phone'          => '8473599000',
					'247_hotline'    => null,
					'text_line'      => null,
					'live_chat'      => '',
					'youth_approved' => 0,
					'hours'          => '\\<font face\\=\\"Arial\\"\\>24\\/7\\<\\/font\\>',
					'image'          => null,
					'description'    => '\\<span data\\-sheets\\-value\\=\\"\\{\\" 1\\"\\:2\\,\\"2\\"\\:\\"law\\=\\"\\" enforcement\\,\\=\\"\\" traffic\\,\\=\\"\\" crime\\=\\"\\" prevention\\,\\=\\"\\" neighborhood\\=\\"\\" watch\\"\\}\\"\\=\\"\\" data\\-sheets\\-userformat\\=\\"\\{\\" 2\\"\\:13187\\,\\"3\\"\\:\\[null\\,0\\]\\,\\"4\\"\\:\\[null\\,2\\,16573901\\]\\,\\"10\\"\\:2\\,\\"11\\"\\:4\\,\\"12\\"\\:0\\,\\"15\\"\\:\\"arial\\"\\,\\"16\\"\\:11\\}\\"\\=\\"\\"\\>\\<font face\\=\\"Arial\\"\\>Law Enforcement\\, Traffic\\, Crime Prevention\\, Neighborhood Watch\\<\\/font\\>\\<\\/span\\>\\<div\\>\\<span data\\-sheets\\-value\\=\\"\\{\\" 1\\"\\:2\\,\\"2\\"\\:\\"law\\=\\"\\" enforcement\\,\\=\\"\\" traffic\\,\\=\\"\\" crime\\=\\"\\" prevention\\,\\=\\"\\" neighborhood\\=\\"\\" watch\\"\\}\\"\\=\\"\\" data\\-sheets\\-userformat\\=\\"\\{\\" 2\\"\\:13187\\,\\"3\\"\\:\\[null\\,0\\]\\,\\"4\\"\\:\\[null\\,2\\,16573901\\]\\,\\"10\\"\\:2\\,\\"11\\"\\:4\\,\\"12\\"\\:0\\,\\"15\\"\\:\\"arial\\"\\,\\"16\\"\\:11\\}\\"\\=\\"\\"\\>\\<font face\\=\\"Arial\\"\\>\\<br\\>\\<\\/font\\>\\<\\/span\\>\\<\\/div\\>\\<div\\>\\<span data\\-sheets\\-value\\=\\"\\{\\" 1\\"\\:2\\,\\"2\\"\\:\\"law\\=\\"\\" enforcement\\,\\=\\"\\" traffic\\,\\=\\"\\" crime\\=\\"\\" prevention\\,\\=\\"\\" neighborhood\\=\\"\\" watch\\"\\}\\"\\=\\"\\" data\\-sheets\\-userformat\\=\\"\\{\\" 2\\"\\:13187\\,\\"3\\"\\:\\[null\\,0\\]\\,\\"4\\"\\:\\[null\\,2\\,16573901\\]\\,\\"10\\"\\:2\\,\\"11\\"\\:4\\,\\"12\\"\\:0\\,\\"15\\"\\:\\"arial\\"\\,\\"16\\"\\:11\\}\\"\\=\\"\\"\\>\\<font face\\=\\"Arial\\"\\>Call 9\\-1\\-1 for emergencies\\.\\<\\/font\\>\\<\\/span\\>\\<\\/div\\>',
					'minor_access'   => '\\<font face\\=\\"Arial\\"\\>Parent permission not required\\.\\<\\/font\\>',
					'appointments'   => '\\<font face\\=\\"Arial\\"\\>Walk\\-ins accepted\\.\\<\\/font\\>',
					'order'          => null,
					'notes'          => null,
					'created_at'     => '2017-08-21 14:41:27',
					'updated_at'     => '2017-08-29 11:25:00',
				],
			15 =>
				[
					'id'             => 17,
					'uid'            => null,
					'name'           => 'Roselle Police Department',
					'tagline'        => 'police',
					'organization'   => null,
					'address1'       => '103 S\\. Prospect Street',
					'address2'       => null,
					'city'           => 'Roselle',
					'state_id'       => 13,
					'zipcode'        => '60172',
					'auto_gps'       => 1,
					'latitude'       => '41.9819607',
					'longitude'      => '-88.0770381',
					'url'            => '',
					'email'          => '',
					'website'        => 'http\\:\\/\\/www\\.roselle\\.il\\.us',
					'phone'          => '6309802025',
					'247_hotline'    => null,
					'text_line'      => null,
					'live_chat'      => '',
					'youth_approved' => 0,
					'hours'          => '\\<font face\\=\\"Arial\\"\\>24\\/7\\<\\/font\\>',
					'image'          => null,
					'description'    => '\\<span style\\=\\"font\\-family\\: Arial\\;\\" data\\-sheets\\-value\\=\\"\\{\\" 1\\"\\:2\\,\\"2\\"\\:\\"law\\=\\"\\" enforcement\\,\\=\\"\\" traffic\\,\\=\\"\\" crime\\=\\"\\" prevention\\,\\=\\"\\" neighborhood\\=\\"\\" watch\\"\\}\\"\\=\\"\\" data\\-sheets\\-userformat\\=\\"\\{\\" 2\\"\\:13187\\,\\"3\\"\\:\\[null\\,0\\]\\,\\"4\\"\\:\\[null\\,2\\,16573901\\]\\,\\"10\\"\\:2\\,\\"11\\"\\:4\\,\\"12\\"\\:0\\,\\"15\\"\\:\\"arial\\"\\,\\"16\\"\\:11\\}\\"\\=\\"\\"\\>Law Enforcement\\, Traffic\\, Crime Prevention\\, Neighborhood Watch\\.\\<\\/span\\>\\<div\\>\\<br\\>\\<\\/div\\>\\<div\\>\\<font face\\=\\"Arial\\"\\>Call 9\\-1\\-1 for emergencies\\.\\<\\/font\\>\\<\\/div\\>',
					'minor_access'   => '\\<font face\\=\\"Arial\\"\\>Parent permission not required\\.\\<\\/font\\>',
					'appointments'   => '\\<font face\\=\\"Arial\\"\\>Walk\\-ins accepted\\.\\<\\/font\\>',
					'order'          => null,
					'notes'          => null,
					'created_at'     => '2017-08-21 15:12:27',
					'updated_at'     => '2017-08-29 11:08:31',
				],
			16 =>
				[
					'id'             => 16,
					'uid'            => null,
					'name'           => 'Elk Grove Village Police Department',
					'tagline'        => 'police',
					'organization'   => null,
					'address1'       => '901 Wellington Avenue',
					'address2'       => null,
					'city'           => 'Elk Grove Village',
					'state_id'       => 13,
					'zipcode'        => '60007',
					'auto_gps'       => 1,
					'latitude'       => '42.0029050',
					'longitude'      => '-88.0078306',
					'url'            => '',
					'email'          => '',
					'website'        => 'http\\:\\/\\/www\\.elkgrove\\.org',
					'phone'          => '8473574100',
					'247_hotline'    => null,
					'text_line'      => null,
					'live_chat'      => '',
					'youth_approved' => 0,
					'hours'          => '\\<font face\\=\\"Arial\\"\\>24\\/7\\<\\/font\\>',
					'image'          => null,
					'description'    => '\\<span data\\-sheets\\-value\\=\\"\\{\\" 1\\"\\:2\\,\\"2\\"\\:\\"law\\=\\"\\" enforcement\\,\\=\\"\\" traffic\\,\\=\\"\\" crime\\=\\"\\" prevention\\,\\=\\"\\" neighborhood\\=\\"\\" watch\\"\\}\\"\\=\\"\\" data\\-sheets\\-userformat\\=\\"\\{\\" 2\\"\\:13187\\,\\"3\\"\\:\\[null\\,0\\]\\,\\"4\\"\\:\\[null\\,2\\,16573901\\]\\,\\"10\\"\\:2\\,\\"11\\"\\:4\\,\\"12\\"\\:0\\,\\"15\\"\\:\\"arial\\"\\,\\"16\\"\\:11\\}\\"\\=\\"\\"\\>\\<font face\\=\\"Arial\\"\\>Law Enforcement\\, Traffic\\, Crime Prevention\\, Neighborhood Watch\\.\\<\\/font\\>\\<\\/span\\>\\<div\\>\\<span data\\-sheets\\-value\\=\\"\\{\\" 1\\"\\:2\\,\\"2\\"\\:\\"law\\=\\"\\" enforcement\\,\\=\\"\\" traffic\\,\\=\\"\\" crime\\=\\"\\" prevention\\,\\=\\"\\" neighborhood\\=\\"\\" watch\\"\\}\\"\\=\\"\\" data\\-sheets\\-userformat\\=\\"\\{\\" 2\\"\\:13187\\,\\"3\\"\\:\\[null\\,0\\]\\,\\"4\\"\\:\\[null\\,2\\,16573901\\]\\,\\"10\\"\\:2\\,\\"11\\"\\:4\\,\\"12\\"\\:0\\,\\"15\\"\\:\\"arial\\"\\,\\"16\\"\\:11\\}\\"\\=\\"\\"\\>\\<font face\\=\\"Arial\\"\\>\\<br\\>\\<\\/font\\>\\<\\/span\\>\\<\\/div\\>\\<div\\>\\<span data\\-sheets\\-value\\=\\"\\{\\" 1\\"\\:2\\,\\"2\\"\\:\\"law\\=\\"\\" enforcement\\,\\=\\"\\" traffic\\,\\=\\"\\" crime\\=\\"\\" prevention\\,\\=\\"\\" neighborhood\\=\\"\\" watch\\"\\}\\"\\=\\"\\" data\\-sheets\\-userformat\\=\\"\\{\\" 2\\"\\:13187\\,\\"3\\"\\:\\[null\\,0\\]\\,\\"4\\"\\:\\[null\\,2\\,16573901\\]\\,\\"10\\"\\:2\\,\\"11\\"\\:4\\,\\"12\\"\\:0\\,\\"15\\"\\:\\"arial\\"\\,\\"16\\"\\:11\\}\\"\\=\\"\\"\\>\\<span style\\=\\"background\\-color\\: rgb\\(255\\, 255\\, 255\\)\\;\\"\\>\\<span style\\=\\"white\\-space\\: pre\\-wrap\\;\\"\\>\\<font face\\=\\"Arial\\"\\>Call 9\\-1\\-1 if there is an emergency\\.\\<\\/font\\>\\<\\/span\\>\\<\\/span\\>\\<br\\>\\<\\/span\\>\\<\\/div\\>',
					'minor_access'   => '\\<font face\\=\\"Arial\\"\\>Parent permission not required\\<\\/font\\>',
					'appointments'   => '\\<font face\\=\\"Arial\\"\\>Walk\\-ins accepted\\<\\/font\\>',
					'order'          => null,
					'notes'          => null,
					'created_at'     => '2017-08-21 15:04:43',
					'updated_at'     => '2017-08-29 09:15:11',
				],
			17 =>
				[
					'id'             => 18,
					'uid'            => null,
					'name'           => 'Aunt Martha\\\'s \\- Carpentersville Community Health Center',
					'tagline'        => null,
					'organization'   => null,
					'address1'       => '3003 Wakefield Drive',
					'address2'       => null,
					'city'           => 'Carpentersville',
					'state_id'       => 13,
					'zipcode'        => '60110',
					'auto_gps'       => 1,
					'latitude'       => '42.1060306',
					'longitude'      => '-88.2468352',
					'url'            => '',
					'email'          => '',
					'website'        => 'http\\:\\/\\/www\\.auntmarthas\\.org',
					'phone'          => '8776928686',
					'247_hotline'    => null,
					'text_line'      => null,
					'live_chat'      => '',
					'youth_approved' => 1,
					'hours'          => '\\<font face\\=\\"Arial\\"\\>Mon\\-Fri\\: 8\\:30AM \\- 4\\:30PM\\<\\/font\\>\\<div\\>\\<span style\\=\\"white\\-space\\: pre\\-wrap\\; background\\-color\\: rgb\\(255\\, 255\\, 255\\)\\;\\"\\>\\<font face\\=\\"Arial\\"\\>HOURS VARY\\. Advised to call ahead\\.\\<\\/font\\>\\<\\/span\\>\\<br\\>\\<\\/div\\>',
					'image'          => null,
					'description'    => '\\<span style\\=\\"font\\-family\\: Arial\\; white\\-space\\: pre\\-wrap\\; background\\-color\\: rgb\\(255\\, 255\\, 255\\)\\;\\"\\>Primary care for adults\\, Pediatrics\\, Mental Health \\(Psychiatry and counseling\\)\\, Dental\\, Family Planning\\, Womens Health \\(OB\\/GYN\\)\\. \\<\\/span\\>\\<br\\>\\<div\\>\\<span style\\=\\"font\\-family\\: Arial\\; white\\-space\\: pre\\-wrap\\; background\\-color\\: rgb\\(255\\, 255\\, 255\\)\\;\\"\\>\\<br\\>\\<\\/span\\>\\<\\/div\\>\\<div\\>\\<span style\\=\\"font\\-family\\: Arial\\; white\\-space\\: pre\\-wrap\\; background\\-color\\: rgb\\(255\\, 255\\, 255\\)\\;\\"\\>Patients can provide income to possibly reduce service costs\\.\\<\\/span\\>\\<span style\\=\\"font\\-family\\: Arial\\; font\\-size\\: 15px\\; white\\-space\\: pre\\-wrap\\; background\\-color\\: rgb\\(255\\, 255\\, 255\\)\\;\\"\\>\\<br\\>\\<\\/span\\>\\<\\/div\\>',
					'minor_access'   => '\\<span style\\=\\"font\\-family\\: Arial\\;\\" data\\-sheets\\-value\\=\\"\\{\\" 1\\"\\:2\\,\\"2\\"\\:\\"all\\=\\"\\" services\\=\\"\\" provided\\=\\"\\" are\\=\\"\\" free\\=\\"\\" and\\=\\"\\" confidential\\"\\}\\"\\=\\"\\" data\\-sheets\\-userformat\\=\\"\\{\\" 2\\"\\:13187\\,\\"3\\"\\:\\[null\\,0\\]\\,\\"4\\"\\:\\[null\\,2\\,16773836\\]\\,\\"10\\"\\:2\\,\\"11\\"\\:4\\,\\"12\\"\\:0\\,\\"15\\"\\:\\"arial\\,sans\\,sans\\-serif\\"\\,\\"16\\"\\:11\\}\\"\\=\\"\\"\\>All services provided are free and confidential\\<\\/span\\>\\<div\\>\\<span style\\=\\"font\\-family\\: Arial\\;\\" data\\-sheets\\-value\\=\\"\\{\\" 1\\"\\:2\\,\\"2\\"\\:\\"all\\=\\"\\" services\\=\\"\\" provided\\=\\"\\" are\\=\\"\\" free\\=\\"\\" and\\=\\"\\" confidential\\"\\}\\"\\=\\"\\" data\\-sheets\\-userformat\\=\\"\\{\\" 2\\"\\:13187\\,\\"3\\"\\:\\[null\\,0\\]\\,\\"4\\"\\:\\[null\\,2\\,16773836\\]\\,\\"10\\"\\:2\\,\\"11\\"\\:4\\,\\"12\\"\\:0\\,\\"15\\"\\:\\"arial\\,sans\\,sans\\-serif\\"\\,\\"16\\"\\:11\\}\\"\\=\\"\\"\\>\\<br\\>\\<\\/span\\>\\<\\/div\\>\\<div\\>\\<span style\\=\\"font\\-family\\: Arial\\;\\" data\\-sheets\\-value\\=\\"\\{\\" 1\\"\\:2\\,\\"2\\"\\:\\"all\\=\\"\\" services\\=\\"\\" provided\\=\\"\\" are\\=\\"\\" free\\=\\"\\" and\\=\\"\\" confidential\\"\\}\\"\\=\\"\\" data\\-sheets\\-userformat\\=\\"\\{\\" 2\\"\\:13187\\,\\"3\\"\\:\\[null\\,0\\]\\,\\"4\\"\\:\\[null\\,2\\,16773836\\]\\,\\"10\\"\\:2\\,\\"11\\"\\:4\\,\\"12\\"\\:0\\,\\"15\\"\\:\\"arial\\,sans\\,sans\\-serif\\"\\,\\"16\\"\\:11\\}\\"\\=\\"\\"\\>\\<span data\\-sheets\\-value\\=\\"\\{\\" 1\\"\\:2\\,\\"2\\"\\:\\"ages\\=\\"\\" 12\\-21\\=\\"\\" requires\\=\\"\\" no\\=\\"\\" parent\\=\\"\\" permission\\=\\"\\" and\\=\\"\\" confidential\\.\\=\\"\\" it\\=\\"\\" would\\=\\"\\" be\\=\\"\\" a\\=\\"\\" \\$20\\=\\"\\" copay\\,\\=\\"\\" but\\=\\"\\" advised\\=\\"\\" to\\=\\"\\" bring\\=\\"\\" what\\=\\"\\" they\\=\\"\\" have\\.\\"\\}\\"\\=\\"\\" data\\-sheets\\-userformat\\=\\"\\{\\" 2\\"\\:13187\\,\\"3\\"\\:\\[null\\,0\\]\\,\\"4\\"\\:\\[null\\,2\\,16773836\\]\\,\\"10\\"\\:2\\,\\"11\\"\\:4\\,\\"12\\"\\:0\\,\\"15\\"\\:\\"arial\\,sans\\,sans\\-serif\\"\\,\\"16\\"\\:11\\}\\"\\=\\"\\"\\>Ages 12\\-21 requires no parent permission and confidential\\. It would be a \\$20 copay\\, but advised to bring what they have\\.\\<\\/span\\>\\<br\\>\\<\\/span\\>\\<\\/div\\>',
					'appointments'   => '\\<span style\\=\\"font\\-family\\: Arial\\;\\" data\\-sheets\\-value\\=\\"\\{\\" 1\\"\\:2\\,\\"2\\"\\:\\"patients\\=\\"\\" advised\\=\\"\\" to\\=\\"\\" call\\=\\"\\" ahead\\"\\}\\"\\=\\"\\" data\\-sheets\\-userformat\\=\\"\\{\\" 2\\"\\:13187\\,\\"3\\"\\:\\[null\\,0\\]\\,\\"4\\"\\:\\[null\\,2\\,16773836\\]\\,\\"10\\"\\:2\\,\\"11\\"\\:4\\,\\"12\\"\\:0\\,\\"15\\"\\:\\"arial\\,sans\\,sans\\-serif\\"\\,\\"16\\"\\:11\\}\\"\\=\\"\\"\\>Patients advised to call ahead\\.\\<\\/span\\>',
					'order'          => null,
					'notes'          => null,
					'created_at'     => '2017-08-23 11:53:29',
					'updated_at'     => '2017-08-28 16:31:09',
				],
			18 =>
				[
					'id'             => 19,
					'uid'            => null,
					'name'           => 'F\\.A\\.I\\.R\\. Counseling',
					'tagline'        => null,
					'organization'   => null,
					'address1'       => '2010 E\\. Algonquin Road',
					'address2'       => 'Suites 203\\-206',
					'city'           => 'Schaumburg',
					'state_id'       => 13,
					'zipcode'        => '60173',
					'auto_gps'       => 1,
					'latitude'       => '42.0646385',
					'longitude'      => '-88.0319958',
					'url'            => '',
					'email'          => '',
					'website'        => 'http\\:\\/\\/www\\.faircounseling\\.com',
					'phone'          => '8473595192',
					'247_hotline'    => null,
					'text_line'      => null,
					'live_chat'      => '',
					'youth_approved' => 0,
					'hours'          => '\\<span style\\=\\"font\\-family\\: Arial\\; white\\-space\\: pre\\-wrap\\; background\\-color\\: rgb\\(255\\, 255\\, 255\\)\\;\\"\\>Tue\\-Sat\\: 1PM \\- 10PM\\<\\/span\\>',
					'image'          => null,
					'description'    => '\\<span style\\=\\"font\\-family\\: Arial\\; white\\-space\\: pre\\-wrap\\; background\\-color\\: rgb\\(255\\, 255\\, 255\\)\\;\\"\\>Adolescent Intensive Outpatient Program\\, Individual Counseling\\, Family Counseling\\, Drug Education\\, DUI Services\\<\\/span\\>\\<div\\>\\<span style\\=\\"font\\-family\\: Arial\\; white\\-space\\: pre\\-wrap\\; background\\-color\\: rgb\\(255\\, 255\\, 255\\)\\;\\"\\>\\<br\\>\\<\\/span\\>\\<\\/div\\>\\<div\\>\\<span style\\=\\"background\\-color\\: rgb\\(255\\, 255\\, 255\\)\\;\\"\\>\\<span style\\=\\"font\\-family\\: Arial\\; white\\-space\\: pre\\-wrap\\;\\"\\>\\<br\\>\\<\\/span\\>\\<\\/span\\>\\<\\/div\\>\\<div\\>\\<span style\\=\\"font\\-family\\: Arial\\; white\\-space\\: pre\\-wrap\\; background\\-color\\: rgb\\(255\\, 255\\, 255\\)\\;\\"\\>If you do not have insurance at evaluation\\, you will be provided with an estimated cost of treatment and a financial agreement\\.\\&nbsp\\;\\<\\/span\\>\\<\\/div\\>\\<div\\>\\<span style\\=\\"font\\-family\\: Arial\\; white\\-space\\: pre\\-wrap\\; background\\-color\\: rgb\\(255\\, 255\\, 255\\)\\;\\"\\>Credit card\\, debit card\\, personal check\\, and cashier\\\'s check accepted\\.\\<\\/span\\>\\<\\/div\\>',
					'minor_access'   => '\\<span style\\=\\"white\\-space\\: pre\\-wrap\\; background\\-color\\: rgb\\(255\\, 255\\, 255\\)\\;\\"\\>\\<font face\\=\\"Arial\\"\\>Parent permission not required\\.\\<\\/font\\>\\<\\/span\\>\\<div\\>\\<span style\\=\\"white\\-space\\: pre\\-wrap\\; background\\-color\\: rgb\\(255\\, 255\\, 255\\)\\;\\"\\>\\<font face\\=\\"Arial\\"\\>Parent permission is not necessary for the free assessment but recommended\\. An assessment is needed before counseling\\; an email can be sent through the website or call 847\\.359\\.5192\\.\\<\\/font\\>\\<\\/span\\>\\<span style\\=\\"font\\-family\\: Arial\\; font\\-size\\: 15px\\; white\\-space\\: pre\\-wrap\\; background\\-color\\: rgb\\(255\\, 255\\, 255\\)\\;\\"\\>\\<br\\>\\<\\/span\\>\\<\\/div\\>',
					'appointments'   => '\\<span style\\=\\"font\\-family\\: Arial\\; white\\-space\\: pre\\-wrap\\; background\\-color\\: rgb\\(255\\, 255\\, 255\\)\\;\\"\\>Same day appointments available\\.\\<\\/span\\>',
					'order'          => null,
					'notes'          => null,
					'created_at'     => '2017-08-23 14:10:05',
					'updated_at'     => '2017-08-29 09:21:05',
				],
			19 =>
				[
					'id'             => 20,
					'uid'            => null,
					'name'           => 'Streamwood Behavioral Healthcare System',
					'tagline'        => null,
					'organization'   => null,
					'address1'       => '1400 E\\. Irving Park Road',
					'address2'       => null,
					'city'           => 'Streamwood',
					'state_id'       => 13,
					'zipcode'        => '60107',
					'auto_gps'       => 1,
					'latitude'       => '42.0087888',
					'longitude'      => '-88.1511134',
					'url'            => '',
					'email'          => '',
					'website'        => 'http\\:\\/\\/www\\.streamwoodhospital\\.com',
					'phone'          => '8002727790',
					'247_hotline'    => '6308379000',
					'text_line'      => null,
					'live_chat'      => '',
					'youth_approved' => 0,
					'hours'          => '\\<span style\\=\\"font\\-family\\: Arial\\; white\\-space\\: pre\\-wrap\\; background\\-color\\: rgb\\(255\\, 255\\, 255\\)\\;\\"\\>24\\/7\\<\\/span\\>',
					'image'          => null,
					'description'    => '\\<span style\\=\\"font\\-family\\: Arial\\; white\\-space\\: pre\\-wrap\\; background\\-color\\: rgb\\(255\\, 255\\, 255\\)\\;\\"\\>Autism Spectrum Programming\\, Self\\-Injury Treatment\\, Inpatient Stabilization\\, Mental Illness and Substance Abuse Track\\, Partial Hospitalization Day Treatment\\, Outpatient Assessment and Treatment\\, Intensive Restoration Program\\<\\/span\\>\\<div\\>\\<span style\\=\\"font\\-family\\: Arial\\; white\\-space\\: pre\\-wrap\\; background\\-color\\: rgb\\(255\\, 255\\, 255\\)\\;\\"\\>\\<br\\>\\<\\/span\\>\\<\\/div\\>\\<div\\>\\<br\\>\\<\\/div\\>\\<div\\>\\<span style\\=\\"background\\-color\\: rgb\\(255\\, 255\\, 255\\)\\;\\"\\>\\<span style\\=\\"font\\-family\\: Arial\\; white\\-space\\: pre\\-wrap\\;\\"\\>Sliding scale for those who qualify\\<\\/span\\>\\<span style\\=\\"font\\-family\\: Arial\\; white\\-space\\: pre\\-wrap\\;\\"\\>\\<br\\>\\<\\/span\\>\\<\\/span\\>\\<\\/div\\>\\<div\\>\\<span style\\=\\"font\\-family\\: Arial\\; white\\-space\\: pre\\-wrap\\; background\\-color\\: rgb\\(255\\, 255\\, 255\\)\\;\\"\\>Cash\\, credit card\\, and debit card accepted\\.\\<\\/span\\>\\<\\/div\\>',
					'minor_access'   => '\\<span style\\=\\"font\\-family\\: Arial\\; white\\-space\\: pre\\-wrap\\; background\\-color\\: rgb\\(255\\, 255\\, 255\\)\\;\\"\\>Parent permission required\\.\\<\\/span\\>\\<div\\>\\<span style\\=\\"font\\-family\\: Arial\\; white\\-space\\: pre\\-wrap\\; background\\-color\\: rgb\\(255\\, 255\\, 255\\)\\;\\"\\>Need a parent unless they\\\'re a \\"ward\\.\\" Guardian needs to be contacted and give permission before assessment\\.\\<\\/span\\>\\<span style\\=\\"font\\-family\\: Arial\\; font\\-size\\: 15px\\; white\\-space\\: pre\\-wrap\\; background\\-color\\: rgb\\(255\\, 255\\, 255\\)\\;\\"\\>\\<br\\>\\<\\/span\\>\\<\\/div\\>',
					'appointments'   => '\\<span style\\=\\"font\\-family\\: Arial\\; white\\-space\\: pre\\-wrap\\; background\\-color\\: rgb\\(255\\, 255\\, 255\\)\\;\\"\\>Same day appointments available \\&amp\\; walk\\-ins accepted\\.\\<\\/span\\>',
					'order'          => null,
					'notes'          => null,
					'created_at'     => '2017-08-23 14:17:11',
					'updated_at'     => '2017-08-29 11:16:52',
				],
			20 =>
				[
					'id'             => 21,
					'uid'            => null,
					'name'           => 'Arlington Center for Recovery',
					'tagline'        => null,
					'organization'   => null,
					'address1'       => '1655 S\\. Arlington Heights Road',
					'address2'       => 'Suite 200',
					'city'           => 'Arlington Heights',
					'state_id'       => 13,
					'zipcode'        => '60005\\-3774',
					'auto_gps'       => 1,
					'latitude'       => '42.0539143',
					'longitude'      => '-87.9812567',
					'url'            => '',
					'email'          => '',
					'website'        => 'http\\:\\/\\/www\\.arlingtoncenterforrecovery\\.net',
					'phone'          => '8474279680',
					'247_hotline'    => null,
					'text_line'      => null,
					'live_chat'      => '',
					'youth_approved' => 1,
					'hours'          => '\\<span style\\=\\"white\\-space\\: pre\\-wrap\\; background\\-color\\: rgb\\(255\\, 255\\, 255\\)\\;\\"\\>\\<font face\\=\\"Arial\\"\\>Intensive Outpatient Program\\:\\<\\/font\\>\\<\\/span\\>\\<div\\>\\<font face\\=\\"Arial\\"\\>Mon\\, Wed\\, and Thu\\: 5\\:30PM \\- 8\\:30PM\\<\\/font\\>\\<div\\>\\<font face\\=\\"Arial\\"\\>Tue\\-Thu\\: 9\\:30AM \\- 12\\:30PM\\<\\/font\\>\\<div\\>\\<br\\>\\<\\/div\\>\\<\\/div\\>\\<\\/div\\>',
					'image'          => null,
					'description'    => '\\<span style\\=\\"font\\-family\\: Arial\\; white\\-space\\: pre\\-wrap\\; background\\-color\\: rgb\\(255\\, 255\\, 255\\)\\;\\"\\>Intensive Outpatient Program\\, Outpatient Substance Abuse Treatment\\, Court Mandated DUI Programs\\, Individual Counseling\\, Adolescent Education Programs\\, Drug Testing\\<\\/span\\>\\<div\\>\\<span style\\=\\"font\\-family\\: Arial\\; white\\-space\\: pre\\-wrap\\; background\\-color\\: rgb\\(255\\, 255\\, 255\\)\\;\\"\\>\\<br\\>\\<\\/span\\>\\<\\/div\\>\\<div\\>\\<span style\\=\\"background\\-color\\: rgb\\(255\\, 255\\, 255\\)\\;\\"\\>\\<span style\\=\\"font\\-family\\: Arial\\; white\\-space\\: pre\\-wrap\\;\\"\\>\\<br\\>\\<\\/span\\>\\<\\/span\\>\\<\\/div\\>\\<div\\>\\<span style\\=\\"background\\-color\\: rgb\\(255\\, 255\\, 255\\)\\;\\"\\>\\<span style\\=\\"font\\-family\\: Arial\\; white\\-space\\: pre\\-wrap\\;\\"\\>Sliding scale for those who qualify\\<\\/span\\>\\<span style\\=\\"font\\-family\\: Arial\\; white\\-space\\: pre\\-wrap\\;\\"\\>\\<br\\>\\<\\/span\\>\\<\\/span\\>\\<\\/div\\>\\<div\\>\\<span style\\=\\"font\\-family\\: Arial\\; white\\-space\\: pre\\-wrap\\; background\\-color\\: rgb\\(255\\, 255\\, 255\\)\\;\\"\\>Credit card and debit card accepted\\.\\<\\/span\\>\\<span style\\=\\"font\\-family\\: Arial\\; font\\-size\\: 15px\\; white\\-space\\: pre\\-wrap\\; background\\-color\\: rgb\\(252\\, 229\\, 205\\)\\;\\"\\>\\<br\\>\\<\\/span\\>\\<\\/div\\>',
					'minor_access'   => '\\<span style\\=\\"white\\-space\\: pre\\-wrap\\; background\\-color\\: rgb\\(255\\, 255\\, 255\\)\\;\\"\\>\\<font face\\=\\"Arial\\"\\>Parent permission required\\.\\<\\/font\\>\\<\\/span\\>\\<div\\>\\<span style\\=\\"font\\-family\\: Arial\\; font\\-size\\: 15px\\; white\\-space\\: pre\\-wrap\\; background\\-color\\: rgb\\(255\\, 255\\, 255\\)\\;\\"\\>\\<br\\>\\<\\/span\\>\\<\\/div\\>',
					'appointments'   => '\\<span style\\=\\"font\\-family\\: Arial\\; white\\-space\\: pre\\-wrap\\; background\\-color\\: rgb\\(255\\, 255\\, 255\\)\\;\\"\\>Patients advised to call ahead\\.\\<\\/span\\>',
					'order'          => null,
					'notes'          => null,
					'created_at'     => '2017-08-23 14:23:38',
					'updated_at'     => '2017-08-28 16:30:51',
				],
			21 =>
				[
					'id'             => 22,
					'uid'            => null,
					'name'           => 'Kenneth Young Center',
					'tagline'        => null,
					'organization'   => null,
					'address1'       => '1001 Rohlwing Road',
					'address2'       => null,
					'city'           => 'Elk Grove Village',
					'state_id'       => 13,
					'zipcode'        => '60007',
					'auto_gps'       => 1,
					'latitude'       => '41.9966892',
					'longitude'      => '-88.0306085',
					'url'            => '',
					'email'          => '',
					'website'        => 'http\\:\\/\\/www\\.kennethyoung\\.org',
					'phone'          => '8475248800',
					'247_hotline'    => '8003459049',
					'text_line'      => null,
					'live_chat'      => '',
					'youth_approved' => 0,
					'hours'          => '\\<font face\\=\\"Arial\\"\\>Mon\\-Thu\\: 10AM \\- 9PM\\<\\/font\\>\\<div\\>\\<font face\\=\\"Arial\\"\\>Fri\\: 10AM \\- 7PM\\<\\/font\\>\\<\\/div\\>\\<div\\>\\<font face\\=\\"Arial\\"\\>Sat\\: \\(Elk Grove Office\\)\\: 10AM \\- 2PM\\<\\/font\\>\\<\\/div\\>',
					'image'          => null,
					'description'    => '\\<span style\\=\\"white\\-space\\: pre\\-wrap\\; background\\-color\\: rgb\\(255\\, 255\\, 255\\)\\;\\"\\>\\<font face\\=\\"Arial\\"\\>Individual\\/Family Therapy\\, Transitioning Youth Services\\, Psychiatric Services\\, SASS Crisis Services\\, Individual Care Grant\\. Support Including\\: Case Management\\, Mentoring\\, Substance Abuse Counseling\\, Group Therapy\\.\\<\\/font\\>\\<\\/span\\>\\<div\\>\\<font face\\=\\"Arial\\"\\>\\<span style\\=\\"white\\-space\\: pre\\-wrap\\; background\\-color\\: rgb\\(255\\, 255\\, 255\\)\\;\\"\\>\\<br\\>\\<\\/span\\>\\<\\/font\\>\\<\\/div\\>\\<div\\>\\<font face\\=\\"Arial\\"\\>\\<span style\\=\\"white\\-space\\: pre\\-wrap\\; background\\-color\\: rgb\\(255\\, 255\\, 255\\)\\;\\"\\>Located at various locations\\, including\\: Schaumburg Township and Mount Prospect\\.\\<\\/span\\>\\<\\/font\\>\\<div\\>\\<span style\\=\\"background\\-color\\: rgb\\(255\\, 255\\, 255\\)\\;\\"\\>\\<font face\\=\\"Arial\\"\\>\\<span style\\=\\"white\\-space\\: pre\\-wrap\\;\\"\\>Sliding scale for those who qualify\\<\\/span\\>\\<span style\\=\\"white\\-space\\: pre\\-wrap\\;\\"\\>\\<br\\>\\<\\/span\\>\\<\\/font\\>\\<\\/span\\>\\<\\/div\\>\\<div\\>\\<font face\\=\\"Arial\\"\\>\\<span style\\=\\"white\\-space\\: pre\\-wrap\\; background\\-color\\: rgb\\(255\\, 255\\, 255\\)\\;\\"\\>Cash\\, credit card\\, debit card accepted\\.\\<\\/span\\>\\<span style\\=\\"white\\-space\\: pre\\-wrap\\; background\\-color\\: rgb\\(252\\, 229\\, 205\\)\\;\\"\\>\\<br\\>\\<\\/span\\>\\<\\/font\\>\\<\\/div\\>\\<div\\>\\<span style\\=\\"font\\-family\\: Arial\\; font\\-size\\: 15px\\; white\\-space\\: pre\\-wrap\\; background\\-color\\: rgb\\(252\\, 229\\, 205\\)\\;\\"\\>\\<br\\>\\<\\/span\\>\\<\\/div\\>\\<\\/div\\>',
					'minor_access'   => '\\<span style\\=\\"white\\-space\\: pre\\-wrap\\; background\\-color\\: rgb\\(255\\, 255\\, 255\\)\\;\\"\\>\\<font face\\=\\"Arial\\"\\>Parent permission required\\.\\<\\/font\\>\\<\\/span\\>\\<div\\>\\<span style\\=\\"font\\-family\\: Arial\\; font\\-size\\: 15px\\; white\\-space\\: pre\\-wrap\\; background\\-color\\: rgb\\(255\\, 255\\, 255\\)\\;\\"\\>\\<br\\>\\<\\/span\\>\\<\\/div\\>',
					'appointments'   => '\\<span style\\=\\"white\\-space\\: pre\\-wrap\\; background\\-color\\: rgb\\(255\\, 255\\, 255\\)\\;\\"\\>\\<font face\\=\\"Arial\\"\\>Patients advised to call ahead\\.\\<\\/font\\>\\<\\/span\\>',
					'order'          => null,
					'notes'          => null,
					'created_at'     => '2017-08-23 14:50:21',
					'updated_at'     => '2017-08-29 10:17:40',
				],
			22 =>
				[
					'id'             => 23,
					'uid'            => null,
					'name'           => 'ACCESS \\- Genesis Center for Health \\& Empowerment',
					'tagline'        => null,
					'organization'   => null,
					'address1'       => '1 N\\. Broadway Street',
					'address2'       => null,
					'city'           => 'Des Plaines',
					'state_id'       => 13,
					'zipcode'        => '60016',
					'auto_gps'       => 1,
					'latitude'       => '42.0517052',
					'longitude'      => '-87.9089714',
					'url'            => '',
					'email'          => '',
					'website'        => 'http\\:\\/\\/www\\.achn\\.net',
					'phone'          => '8472983150',
					'247_hotline'    => null,
					'text_line'      => null,
					'live_chat'      => '',
					'youth_approved' => 0,
					'hours'          => '\\<span style\\=\\"font\\-family\\: Arial\\; white\\-space\\: pre\\-wrap\\; background\\-color\\: rgb\\(255\\, 255\\, 255\\)\\;\\"\\>Mon\\: 8AM \\- 8PM\\<\\/span\\>\\<div\\>\\<span style\\=\\"font\\-family\\: Arial\\; white\\-space\\: pre\\-wrap\\; background\\-color\\: rgb\\(255\\, 255\\, 255\\)\\;\\"\\>Tue\\-Thu\\: 8AM \\- 7PM\\<\\/span\\>\\<\\/div\\>\\<div\\>\\<span style\\=\\"font\\-family\\: Arial\\; white\\-space\\: pre\\-wrap\\; background\\-color\\: rgb\\(255\\, 255\\, 255\\)\\;\\"\\>Fri\\: 8AM \\- 5PM\\<\\/span\\>\\<\\/div\\>\\<div\\>\\<span style\\=\\"font\\-family\\: Arial\\; white\\-space\\: pre\\-wrap\\; background\\-color\\: rgb\\(255\\, 255\\, 255\\)\\;\\"\\>Sat\\: 8AM \\- 12PM\\<\\/span\\>\\<\\/div\\>',
					'image'          => null,
					'description'    => '\\<span style\\=\\"white\\-space\\: pre\\-wrap\\; background\\-color\\: rgb\\(255\\, 255\\, 255\\)\\;\\"\\>\\<font face\\=\\"Arial\\"\\>Anger Management\\, Anxiety\\, Caregiver Issues\\, Depression\\, Smoking Cessation\\, Stress Management\\, Alcohol\\, Substance Use and Other Addictions\\, Trauma and Abuse\\, Other Behavioral Health Issues\\, Pediatrics\\, Family Medicine\\, Obstetrics and Gynecology\\, Family Practice\\, Management of Chronic Diseases\\<\\/font\\>\\<\\/span\\>\\<div\\>\\<span style\\=\\"white\\-space\\: pre\\-wrap\\; background\\-color\\: rgb\\(255\\, 255\\, 255\\)\\;\\"\\>\\<font face\\=\\"Arial\\"\\>\\<br\\>\\<\\/font\\>\\<\\/span\\>\\<\\/div\\>\\<div\\>\\<span style\\=\\"background\\-color\\: rgb\\(255\\, 255\\, 255\\)\\;\\"\\>\\<font face\\=\\"Arial\\"\\>\\<span style\\=\\"white\\-space\\: pre\\-wrap\\;\\"\\>Insurance accepted\\: Private Insurance\\, Medicaid\\, Medicare\\.\\<\\/span\\>\\<span style\\=\\"white\\-space\\: pre\\-wrap\\;\\"\\>\\<br\\>\\<\\/span\\>\\<\\/font\\>\\<\\/span\\>\\<\\/div\\>\\<div\\>\\<span style\\=\\"background\\-color\\: rgb\\(255\\, 255\\, 255\\)\\;\\"\\>\\<font face\\=\\"Arial\\"\\>\\<span style\\=\\"white\\-space\\: pre\\-wrap\\;\\"\\>Sliding scale for those who qualify\\.\\<\\/span\\>\\<span style\\=\\"white\\-space\\: pre\\-wrap\\;\\"\\>\\<br\\>\\<\\/span\\>\\<\\/font\\>\\<\\/span\\>\\<\\/div\\>\\<div\\>\\<span style\\=\\"white\\-space\\: pre\\-wrap\\; background\\-color\\: rgb\\(255\\, 255\\, 255\\)\\;\\"\\>\\<font face\\=\\"Arial\\"\\>Patients without the ability to pay will not be denied services\\.\\<\\/font\\>\\<\\/span\\>\\<span style\\=\\"font\\-family\\: Arial\\; font\\-size\\: 15px\\; white\\-space\\: pre\\-wrap\\; background\\-color\\: rgb\\(252\\, 229\\, 205\\)\\;\\"\\>\\<br\\>\\<\\/span\\>\\<\\/div\\>',
					'minor_access'   => '\\<span style\\=\\"font\\-family\\: Arial\\; white\\-space\\: pre\\-wrap\\; background\\-color\\: rgb\\(255\\, 255\\, 255\\)\\;\\"\\>Parent permission not required\\.\\<\\/span\\>\\<div\\>\\<span style\\=\\"font\\-family\\: Arial\\; white\\-space\\: pre\\-wrap\\; background\\-color\\: rgb\\(255\\, 255\\, 255\\)\\;\\"\\>Minors would have to pay 100\\% for their visit\\/services if they do not have their parents\\\' insurance or their own insurance\\.\\<\\/span\\>\\<span style\\=\\"font\\-family\\: Arial\\; font\\-size\\: 15px\\; white\\-space\\: pre\\-wrap\\; background\\-color\\: rgb\\(255\\, 255\\, 255\\)\\;\\"\\>\\<br\\>\\<\\/span\\>\\<\\/div\\>\\<div\\>\\<span style\\=\\"font\\-family\\: Arial\\; font\\-size\\: 15px\\; white\\-space\\: pre\\-wrap\\; background\\-color\\: rgb\\(255\\, 255\\, 255\\)\\;\\"\\>\\<br\\>\\<\\/span\\>\\<\\/div\\>',
					'appointments'   => '\\<span style\\=\\"font\\-family\\: Arial\\; white\\-space\\: pre\\-wrap\\; background\\-color\\: rgb\\(255\\, 255\\, 255\\)\\;\\"\\>Same day appointments available\\.\\<\\/span\\>',
					'order'          => null,
					'notes'          => null,
					'created_at'     => '2017-08-23 15:11:37',
					'updated_at'     => '2017-08-28 16:27:51',
				],
			23 =>
				[
					'id'             => 24,
					'uid'            => null,
					'name'           => 'The Bridge Youth and Family Services',
					'tagline'        => null,
					'organization'   => null,
					'address1'       => '721 S\\. Quentin Road',
					'address2'       => 'Suite 103',
					'city'           => 'Palatine',
					'state_id'       => 13,
					'zipcode'        => '60067',
					'auto_gps'       => 1,
					'latitude'       => '42.0965530',
					'longitude'      => '-88.0624176',
					'url'            => '',
					'email'          => '',
					'website'        => 'http\\:\\/\\/www\\.bridgeyouth\\.org',
					'phone'          => '8473597490',
					'247_hotline'    => '8477763720',
					'text_line'      => null,
					'live_chat'      => '',
					'youth_approved' => 0,
					'hours'          => '\\<font face\\=\\"Arial\\"\\>Mon\\-Thu\\: 9AM \\- 8PM\\<\\/font\\>\\<div\\>\\<font face\\=\\"Arial\\"\\>Fri\\: 9AM \\- 4PM\\<\\/font\\>\\<\\/div\\>',
					'image'          => null,
					'description'    => '\\<span style\\=\\"font\\-family\\: Arial\\; white\\-space\\: pre\\-wrap\\; background\\-color\\: rgb\\(255\\, 255\\, 255\\)\\;\\"\\>Counseling\\, Crisis Intervention\\, Mentoring and Youth Council\\, Group Therapy\\, Psychiatric Services\\, Trauma Recovery\\, Run\\-Away\\, Locked Out\\, Homeless\\, Suicide Prevention\\<\\/span\\>\\<div\\>\\<br\\>\\<\\/div\\>\\<div\\>\\<span style\\=\\"background\\-color\\: rgb\\(255\\, 255\\, 255\\)\\;\\"\\>\\<span style\\=\\"font\\-family\\: Arial\\; white\\-space\\: pre\\-wrap\\;\\"\\>Patients will not be turned away due to inability to pay\\.\\<\\/span\\>\\<span style\\=\\"font\\-family\\: Arial\\; white\\-space\\: pre\\-wrap\\;\\"\\>\\<br\\>\\<\\/span\\>\\<\\/span\\>\\<\\/div\\>\\<div\\>\\<span style\\=\\"font\\-family\\: Arial\\; white\\-space\\: pre\\-wrap\\; background\\-color\\: rgb\\(255\\, 255\\, 255\\)\\;\\"\\>Cash\\, credit card\\, and check accepted\\.\\<\\/span\\>\\<span style\\=\\"font\\-family\\: Arial\\; font\\-size\\: 15px\\; white\\-space\\: pre\\-wrap\\; background\\-color\\: rgb\\(252\\, 229\\, 205\\)\\;\\"\\>\\<br\\>\\<\\/span\\>\\<\\/div\\>',
					'minor_access'   => '\\<span style\\=\\"font\\-family\\: Arial\\; white\\-space\\: pre\\-wrap\\; background\\-color\\: rgb\\(255\\, 255\\, 255\\)\\;\\"\\>Parent permission required\\<\\/span\\>',
					'appointments'   => '\\<span style\\=\\"font\\-family\\: Arial\\; white\\-space\\: pre\\-wrap\\; background\\-color\\: rgb\\(255\\, 255\\, 255\\)\\;\\"\\>Patients advised to call ahead\\<\\/span\\>',
					'order'          => null,
					'notes'          => null,
					'created_at'     => '2017-08-24 16:17:57',
					'updated_at'     => '2017-08-29 11:19:58',
				],
			24 =>
				[
					'id'             => 25,
					'uid'            => null,
					'name'           => 'Northwest Compass',
					'tagline'        => null,
					'organization'   => null,
					'address1'       => '1300 N\\. Northwest Highway',
					'address2'       => null,
					'city'           => 'Mount Prospect',
					'state_id'       => 13,
					'zipcode'        => '60056',
					'auto_gps'       => 1,
					'latitude'       => '42.0721247',
					'longitude'      => '-87.9552693',
					'url'            => '',
					'email'          => 'info\\@northwestcompass\\.org',
					'website'        => 'http\\:\\/\\/www\\.northwestcompass\\.org',
					'phone'          => '8473922344',
					'247_hotline'    => null,
					'text_line'      => null,
					'live_chat'      => '',
					'youth_approved' => 0,
					'hours'          => '\\<style type\\=\\"text\\/css\\"\\>\\<\\!\\-\\-td \\{border\\: 1px solid \\#ccc\\;\\}br \\{mso\\-data\\-placement\\:same\\-cell\\;\\}\\-\\-\\>\\<\\/style\\>\\<span data\\-sheets\\-value\\=\\"\\{\\" 1\\"\\:2\\,\\"2\\"\\:\\"mon\\,\\=\\"\\" tues\\,\\=\\"\\" thurs\\,\\=\\"\\" fri\\:\\=\\"\\" 9\\=\\"\\" am\\=\\"\\" \\-\\=\\"\\" 4\\=\\"\\" pm\\=\\"\\" wed\\:\\=\\"\\" 7\\=\\"\\" \\(closed\\=\\"\\" daily\\=\\"\\" for\\=\\"\\" lunch\\=\\"\\" between\\=\\"\\" 12\\=\\"\\" 1\\=\\"\\" pm\\)\\"\\}\\"\\=\\"\\" data\\-sheets\\-userformat\\=\\"\\{\\" 2\\"\\:13249\\,\\"3\\"\\:\\[null\\,0\\]\\,\\"9\\"\\:0\\,\\"10\\"\\:1\\,\\"11\\"\\:4\\,\\"12\\"\\:0\\,\\"15\\"\\:\\"calibri\\"\\,\\"16\\"\\:12\\}\\"\\=\\"\\"\\>\\<font face\\=\\"Arial\\"\\>Mon\\, Tues\\, Thurs\\, Fri\\: 9AM \\- 4PM\\<\\/font\\>\\<\\/span\\>\\<div\\>\\<span data\\-sheets\\-value\\=\\"\\{\\" 1\\"\\:2\\,\\"2\\"\\:\\"mon\\,\\=\\"\\" tues\\,\\=\\"\\" thurs\\,\\=\\"\\" fri\\:\\=\\"\\" 9\\=\\"\\" am\\=\\"\\" \\-\\=\\"\\" 4\\=\\"\\" pm\\=\\"\\" wed\\:\\=\\"\\" 7\\=\\"\\" \\(closed\\=\\"\\" daily\\=\\"\\" for\\=\\"\\" lunch\\=\\"\\" between\\=\\"\\" 12\\=\\"\\" 1\\=\\"\\" pm\\)\\"\\}\\"\\=\\"\\" data\\-sheets\\-userformat\\=\\"\\{\\" 2\\"\\:13249\\,\\"3\\"\\:\\[null\\,0\\]\\,\\"9\\"\\:0\\,\\"10\\"\\:1\\,\\"11\\"\\:4\\,\\"12\\"\\:0\\,\\"15\\"\\:\\"calibri\\"\\,\\"16\\"\\:12\\}\\"\\=\\"\\"\\>\\<font face\\=\\"Arial\\"\\>Wed\\: 9AM \\- 7PM \\(Closed daily for lunch between 12PM \\- 1PM\\)\\<\\/font\\>\\<\\/span\\>\\<\\/div\\>',
					'image'          => null,
					'description'    => '\\<style type\\=\\"text\\/css\\"\\>\\<\\!\\-\\-td \\{border\\: 1px solid \\#ccc\\;\\}br \\{mso\\-data\\-placement\\:same\\-cell\\;\\}\\-\\-\\>\\<\\/style\\>\\<span style\\=\\"font\\-size\\: 12pt\\; font\\-family\\: Calibri\\, Arial\\;\\" data\\-sheets\\-value\\=\\"\\{\\" 1\\"\\:2\\,\\"2\\"\\:\\"free\\=\\"\\" individual\\=\\"\\" counseling\\,\\=\\"\\" limited\\=\\"\\" homeless\\=\\"\\" prevention\\=\\"\\" financial\\=\\"\\" assistance\\,\\=\\"\\" education\\=\\"\\" and\\=\\"\\" advocacy\\=\\"\\" for\\=\\"\\" residents\\=\\"\\" facing\\=\\"\\" eviction\\,\\=\\"\\" foreclosure\\,\\=\\"\\" or\\=\\"\\" homelessness\\,\\=\\"\\" prospective\\=\\"\\" homebuyers\\=\\"\\" on\\=\\"\\" the\\=\\"\\" different\\=\\"\\" aspects\\=\\"\\" of\\=\\"\\" home\\=\\"\\" ownership\\,\\=\\"\\" including\\=\\"\\" information\\=\\"\\" down\\=\\"\\" payment\\=\\"\\" assistance\\=\\"\\" programs\\,\\=\\"\\" money\\=\\"\\" management\\=\\"\\" education\\,\\=\\"\\" practical\\=\\"\\" tools\\=\\"\\" to\\=\\"\\" help\\=\\"\\" clients\\=\\"\\" understand\\=\\"\\" their\\=\\"\\" finances\\,\\=\\"\\" career\\=\\"\\" coaching\\=\\"\\" support\\"\\}\\"\\=\\"\\" data\\-sheets\\-userformat\\=\\"\\{\\" 2\\"\\:13249\\,\\"3\\"\\:\\[null\\,0\\]\\,\\"9\\"\\:0\\,\\"10\\"\\:1\\,\\"11\\"\\:4\\,\\"12\\"\\:0\\,\\"15\\"\\:\\"calibri\\"\\,\\"16\\"\\:12\\}\\"\\=\\"\\"\\>Free individual counseling\\, Limited homeless prevention financial assistance\\, Education and advocacy for residents facing eviction\\, foreclosure\\, or homelessness\\, Education for prospective homebuyers on the different aspects of home ownership\\, including information for down payment assistance programs\\, Money Management counseling\\, education\\, and practical information and tools to help clients understand their finances\\, Career Coaching and support\\<\\/span\\>\\<br\\>\\<div\\>\\<span style\\=\\"font\\-size\\: 12pt\\; font\\-family\\: Calibri\\, Arial\\;\\" data\\-sheets\\-value\\=\\"\\{\\" 1\\"\\:2\\,\\"2\\"\\:\\"free\\=\\"\\" individual\\=\\"\\" counseling\\,\\=\\"\\" limited\\=\\"\\" homeless\\=\\"\\" prevention\\=\\"\\" financial\\=\\"\\" assistance\\,\\=\\"\\" education\\=\\"\\" and\\=\\"\\" advocacy\\=\\"\\" for\\=\\"\\" residents\\=\\"\\" facing\\=\\"\\" eviction\\,\\=\\"\\" foreclosure\\,\\=\\"\\" or\\=\\"\\" homelessness\\,\\=\\"\\" prospective\\=\\"\\" homebuyers\\=\\"\\" on\\=\\"\\" the\\=\\"\\" different\\=\\"\\" aspects\\=\\"\\" of\\=\\"\\" home\\=\\"\\" ownership\\,\\=\\"\\" including\\=\\"\\" information\\=\\"\\" down\\=\\"\\" payment\\=\\"\\" assistance\\=\\"\\" programs\\,\\=\\"\\" money\\=\\"\\" management\\=\\"\\" education\\,\\=\\"\\" practical\\=\\"\\" tools\\=\\"\\" to\\=\\"\\" help\\=\\"\\" clients\\=\\"\\" understand\\=\\"\\" their\\=\\"\\" finances\\,\\=\\"\\" career\\=\\"\\" coaching\\=\\"\\" support\\"\\}\\"\\=\\"\\" data\\-sheets\\-userformat\\=\\"\\{\\" 2\\"\\:13249\\,\\"3\\"\\:\\[null\\,0\\]\\,\\"9\\"\\:0\\,\\"10\\"\\:1\\,\\"11\\"\\:4\\,\\"12\\"\\:0\\,\\"15\\"\\:\\"calibri\\"\\,\\"16\\"\\:12\\}\\"\\=\\"\\"\\>\\<br\\>\\<\\/span\\>\\<\\/div\\>\\<div\\>\\<span style\\=\\"font\\-size\\: 12pt\\; font\\-family\\: Calibri\\, Arial\\;\\" data\\-sheets\\-value\\=\\"\\{\\" 1\\"\\:2\\,\\"2\\"\\:\\"free\\=\\"\\" individual\\=\\"\\" counseling\\,\\=\\"\\" limited\\=\\"\\" homeless\\=\\"\\" prevention\\=\\"\\" financial\\=\\"\\" assistance\\,\\=\\"\\" education\\=\\"\\" and\\=\\"\\" advocacy\\=\\"\\" for\\=\\"\\" residents\\=\\"\\" facing\\=\\"\\" eviction\\,\\=\\"\\" foreclosure\\,\\=\\"\\" or\\=\\"\\" homelessness\\,\\=\\"\\" prospective\\=\\"\\" homebuyers\\=\\"\\" on\\=\\"\\" the\\=\\"\\" different\\=\\"\\" aspects\\=\\"\\" of\\=\\"\\" home\\=\\"\\" ownership\\,\\=\\"\\" including\\=\\"\\" information\\=\\"\\" down\\=\\"\\" payment\\=\\"\\" assistance\\=\\"\\" programs\\,\\=\\"\\" money\\=\\"\\" management\\=\\"\\" education\\,\\=\\"\\" practical\\=\\"\\" tools\\=\\"\\" to\\=\\"\\" help\\=\\"\\" clients\\=\\"\\" understand\\=\\"\\" their\\=\\"\\" finances\\,\\=\\"\\" career\\=\\"\\" coaching\\=\\"\\" support\\"\\}\\"\\=\\"\\" data\\-sheets\\-userformat\\=\\"\\{\\" 2\\"\\:13249\\,\\"3\\"\\:\\[null\\,0\\]\\,\\"9\\"\\:0\\,\\"10\\"\\:1\\,\\"11\\"\\:4\\,\\"12\\"\\:0\\,\\"15\\"\\:\\"calibri\\"\\,\\"16\\"\\:12\\}\\"\\=\\"\\"\\>\\<span style\\=\\"font\\-size\\: 12pt\\;\\" data\\-sheets\\-value\\=\\"\\{\\" 1\\"\\:2\\,\\"2\\"\\:\\"sliding\\=\\"\\" scale\\=\\"\\" for\\=\\"\\" those\\=\\"\\" who\\=\\"\\" qualify\\"\\}\\"\\=\\"\\" data\\-sheets\\-userformat\\=\\"\\{\\" 2\\"\\:13249\\,\\"3\\"\\:\\[null\\,0\\]\\,\\"9\\"\\:0\\,\\"10\\"\\:1\\,\\"11\\"\\:4\\,\\"12\\"\\:0\\,\\"15\\"\\:\\"calibri\\"\\,\\"16\\"\\:12\\}\\"\\=\\"\\"\\>\\<span style\\=\\"font\\-size\\: 12pt\\;\\" data\\-sheets\\-value\\=\\"\\{\\" 1\\"\\:2\\,\\"2\\"\\:\\"northwest\\=\\"\\" compass\\=\\"\\" services\\=\\"\\" are\\=\\"\\" provided\\=\\"\\" free\\=\\"\\" of\\=\\"\\" charge\\,\\=\\"\\" however\\,\\=\\"\\" income\\=\\"\\" eligibility\\=\\"\\" guidelines\\=\\"\\" apply\\=\\"\\" to\\=\\"\\" qualify\\=\\"\\" for\\=\\"\\" certain\\=\\"\\" programs\\.\\=\\"\\" a\\=\\"\\" program\\=\\"\\" specialist\\=\\"\\" will\\=\\"\\" be\\=\\"\\" able\\=\\"\\" provide\\=\\"\\" more\\=\\"\\" information\\=\\"\\" based\\=\\"\\" on\\=\\"\\" the\\=\\"\\" specifics\\=\\"\\" your\\=\\"\\" unique\\=\\"\\" situation\\=\\"\\" charge\\.\\"\\}\\"\\=\\"\\" data\\-sheets\\-userformat\\=\\"\\{\\" 2\\"\\:13249\\,\\"3\\"\\:\\[null\\,0\\]\\,\\"9\\"\\:0\\,\\"10\\"\\:1\\,\\"11\\"\\:4\\,\\"12\\"\\:0\\,\\"15\\"\\:\\"calibri\\"\\,\\"16\\"\\:12\\}\\"\\=\\"\\"\\>Northwest Compass services are provided free of charge\\, however\\, income eligibility guidelines apply to qualify for certain programs\\. A Program Specialist will be able to provide more information based on the specifics of your unique situation for free of charge\\.\\&nbsp\\;\\<\\/span\\>Sliding scale for those who qualify\\.\\<br\\>\\<\\/span\\>\\<\\/span\\>\\<\\/div\\>',
					'minor_access'   => null,
					'appointments'   => '\\<style type\\=\\"text\\/css\\"\\>\\<\\!\\-\\-td \\{border\\: 1px solid \\#ccc\\;\\}br \\{mso\\-data\\-placement\\:same\\-cell\\;\\}\\-\\-\\>\\<\\/style\\>\\<span data\\-sheets\\-value\\=\\"\\{\\" 1\\"\\:2\\,\\"2\\"\\:\\"walk\\-ins\\=\\"\\" accepted\\"\\}\\"\\=\\"\\" data\\-sheets\\-userformat\\=\\"\\{\\" 2\\"\\:13249\\,\\"3\\"\\:\\[null\\,0\\]\\,\\"9\\"\\:0\\,\\"10\\"\\:1\\,\\"11\\"\\:4\\,\\"12\\"\\:0\\,\\"15\\"\\:\\"calibri\\"\\,\\"16\\"\\:12\\}\\"\\=\\"\\"\\>\\<font face\\=\\"Arial\\"\\>Walk\\-ins accepted\\.\\<\\/font\\>\\<\\/span\\>',
					'order'          => null,
					'notes'          => null,
					'created_at'     => '2017-08-25 12:30:04',
					'updated_at'     => '2017-08-29 10:44:54',
				],
			25 =>
				[
					'id'             => 26,
					'uid'            => null,
					'name'           => 'Hamdard Center',
					'tagline'        => null,
					'organization'   => null,
					'address1'       => '228 E\\. Lake Street',
					'address2'       => null,
					'city'           => 'Addison',
					'state_id'       => 13,
					'zipcode'        => '60101',
					'auto_gps'       => 1,
					'latitude'       => '41.9307333',
					'longitude'      => '-87.9827851',
					'url'            => '',
					'email'          => 'main\\@hamdardcenter\\.org',
					'website'        => 'http\\:\\/\\/www\\.hamdardcenter\\.org',
					'phone'          => '6308351430',
					'247_hotline'    => '8663053933',
					'text_line'      => null,
					'live_chat'      => '',
					'youth_approved' => 0,
					'hours'          => '\\<style type\\=\\"text\\/css\\"\\>\\<\\!\\-\\-td \\{border\\: 1px solid \\#ccc\\;\\}br \\{mso\\-data\\-placement\\:same\\-cell\\;\\}\\-\\-\\>\\<\\/style\\>\\<span data\\-sheets\\-value\\=\\"\\{\\" 1\\"\\:2\\,\\"2\\"\\:\\"mon\\=\\"\\" \\-\\=\\"\\" fri\\:\\=\\"\\" 9\\=\\"\\" am\\=\\"\\" 5\\=\\"\\" pm\\"\\}\\"\\=\\"\\" data\\-sheets\\-userformat\\=\\"\\{\\" 2\\"\\:13249\\,\\"3\\"\\:\\[null\\,0\\]\\,\\"9\\"\\:0\\,\\"10\\"\\:1\\,\\"11\\"\\:4\\,\\"12\\"\\:0\\,\\"15\\"\\:\\"calibri\\"\\,\\"16\\"\\:12\\}\\"\\=\\"\\"\\>\\<font face\\=\\"Arial\\"\\>Mon \\- Fri\\: 9 AM  \\- 5 PM\\<\\/font\\>\\<\\/span\\>\\<div\\>\\<span data\\-sheets\\-value\\=\\"\\{\\" 1\\"\\:2\\,\\"2\\"\\:\\"mon\\=\\"\\" \\-\\=\\"\\" fri\\:\\=\\"\\" 9\\=\\"\\" am\\=\\"\\" 5\\=\\"\\" pm\\"\\}\\"\\=\\"\\" data\\-sheets\\-userformat\\=\\"\\{\\" 2\\"\\:13249\\,\\"3\\"\\:\\[null\\,0\\]\\,\\"9\\"\\:0\\,\\"10\\"\\:1\\,\\"11\\"\\:4\\,\\"12\\"\\:0\\,\\"15\\"\\:\\"calibri\\"\\,\\"16\\"\\:12\\}\\"\\=\\"\\"\\>\\<font face\\=\\"Arial\\"\\>Sat\\: By appointment\\<\\/font\\>\\<\\/span\\>\\<\\/div\\>',
					'image'          => null,
					'description'    => '\\<style type\\=\\"text\\/css\\"\\>\\<\\!\\-\\-td \\{border\\: 1px solid \\#ccc\\;\\}br \\{mso\\-data\\-placement\\:same\\-cell\\;\\}\\-\\-\\>\\<\\/style\\>\\<span data\\-sheets\\-value\\=\\"\\{\\" 1\\"\\:2\\,\\"2\\"\\:\\"general\\,\\=\\"\\" primary\\=\\"\\" care\\,\\=\\"\\" family\\=\\"\\" medicine\\,\\=\\"\\" ob\\-gyn\\,\\=\\"\\" health\\=\\"\\" education\\,\\=\\"\\" nutrition\\,\\=\\"\\" cardiology\\,\\=\\"\\" immunizations\\,\\=\\"\\" school\\=\\"\\" physicals\\,\\=\\"\\" immigration\\=\\"\\" pediatrics\\,\\=\\"\\" crisis\\=\\"\\" intervention\\,\\=\\"\\" psychiatric\\=\\"\\" evaluation\\,\\=\\"\\" medication\\=\\"\\" management\\=\\"\\" monitoring\\,\\=\\"\\" mental\\=\\"\\" assessment\\,\\=\\"\\" comprehensive\\=\\"\\" psychological\\=\\"\\" evaluation\\=\\"\\" for\\:\\=\\"\\" medical\\=\\"\\" certification\\=\\"\\" for\\=\\"\\" disability\\=\\"\\" exception\\=\\"\\" \\(n\\-648\\)\\=\\"\\" and\\=\\"\\" u\\-visa\\,\\=\\"\\" substance\\=\\"\\" abuse\\=\\"\\" counseling\\,\\=\\"\\" individual\\=\\"\\" psychotherapy\\,\\=\\"\\" group\\=\\"\\" therapy\\,\\=\\"\\" domestic\\=\\"\\" violence\\=\\"\\" line\\,\\=\\"\\" residential\\=\\"\\" shelter\\=\\"\\" facility\\,\\=\\"\\" partner\\=\\"\\" intervention\\=\\"\\" program\\,\\=\\"\\" case\\=\\"\\" medicare\\=\\"\\" medicaid\\,\\=\\"\\" supplemental\\=\\"\\" security\\=\\"\\" income\\=\\"\\" disability\\,\\=\\"\\" link\\=\\"\\" card\\,\\=\\"\\" women\\=\\"\\" infants\\=\\"\\" children\\=\\"\\" \\(wic\\)\\,\\=\\"\\" temporary\\=\\"\\" assistance\\=\\"\\" needy\\=\\"\\" families\\=\\"\\" \\(tanf\\)\\,\\=\\"\\" low\\-income\\=\\"\\" housing\\=\\"\\" referrals\\,\\=\\"\\" subsidized\\,\\=\\"\\" child\\=\\"\\" care\\=\\"\\" day\\=\\"\\" free\\=\\"\\" bus\\=\\"\\" passes\\=\\"\\" seniors\\,\\=\\"\\" legal\\=\\"\\" advocacy\\,\\=\\"\\" translation\\=\\"\\" interpretation\\,\\=\\"\\" community\\=\\"\\" resources\\,\\=\\"\\" insurance\\=\\"\\" enrollment\\,\\=\\"\\" outreach\\"\\}\\"\\=\\"\\" data\\-sheets\\-userformat\\=\\"\\{\\" 2\\"\\:13249\\,\\"3\\"\\:\\[null\\,0\\]\\,\\"9\\"\\:0\\,\\"10\\"\\:1\\,\\"11\\"\\:4\\,\\"12\\"\\:0\\,\\"15\\"\\:\\"calibri\\"\\,\\"16\\"\\:12\\}\\"\\=\\"\\"\\>\\<font face\\=\\"Arial\\"\\>General\\, Primary Care\\, Family Medicine\\, OB\\-GYN\\, Health Education\\, Nutrition\\, Cardiology\\, Immunizations\\, School Physicals\\, Immigration Physicals\\, Pediatrics\\, Crisis Intervention\\, Psychiatric Evaluation\\, Medication Management\\/Monitoring\\, Mental Health Assessment\\, Comprehensive Psychological Evaluation for\\: Medical Certification for Disability Exception \\(N\\-648\\) and U\\-Visa\\, Substance Abuse Counseling\\, Individual Psychotherapy\\, Group Therapy\\, Domestic Violence Crisis Line\\, Residential Shelter Facility\\, Partner Abuse Intervention Program\\, Case Management for\\: Medicare\\/Medicaid\\, Supplemental Security Income\\/Disability\\, LINK Card\\, Women Infants and Children \\(WIC\\)\\, Temporary Assistance for Needy Families \\(TANF\\)\\, Low\\-income housing referrals\\, Subsidized\\, child care\\/day care\\, free bus passes for seniors\\, Legal advocacy\\, Translation\\/interpretation\\, community resources\\, Health Insurance Enrollment\\, and Community Outreach\\<\\/font\\>\\<\\/span\\>\\<div\\>\\<span style\\=\\"font\\-size\\: 12pt\\; font\\-family\\: Calibri\\, Arial\\;\\" data\\-sheets\\-value\\=\\"\\{\\" 1\\"\\:2\\,\\"2\\"\\:\\"general\\,\\=\\"\\" primary\\=\\"\\" care\\,\\=\\"\\" family\\=\\"\\" medicine\\,\\=\\"\\" ob\\-gyn\\,\\=\\"\\" health\\=\\"\\" education\\,\\=\\"\\" nutrition\\,\\=\\"\\" cardiology\\,\\=\\"\\" immunizations\\,\\=\\"\\" school\\=\\"\\" physicals\\,\\=\\"\\" immigration\\=\\"\\" pediatrics\\,\\=\\"\\" crisis\\=\\"\\" intervention\\,\\=\\"\\" psychiatric\\=\\"\\" evaluation\\,\\=\\"\\" medication\\=\\"\\" management\\=\\"\\" monitoring\\,\\=\\"\\" mental\\=\\"\\" assessment\\,\\=\\"\\" comprehensive\\=\\"\\" psychological\\=\\"\\" evaluation\\=\\"\\" for\\:\\=\\"\\" medical\\=\\"\\" certification\\=\\"\\" for\\=\\"\\" disability\\=\\"\\" exception\\=\\"\\" \\(n\\-648\\)\\=\\"\\" and\\=\\"\\" u\\-visa\\,\\=\\"\\" substance\\=\\"\\" abuse\\=\\"\\" counseling\\,\\=\\"\\" individual\\=\\"\\" psychotherapy\\,\\=\\"\\" group\\=\\"\\" therapy\\,\\=\\"\\" domestic\\=\\"\\" violence\\=\\"\\" line\\,\\=\\"\\" residential\\=\\"\\" shelter\\=\\"\\" facility\\,\\=\\"\\" partner\\=\\"\\" intervention\\=\\"\\" program\\,\\=\\"\\" case\\=\\"\\" medicare\\=\\"\\" medicaid\\,\\=\\"\\" supplemental\\=\\"\\" security\\=\\"\\" income\\=\\"\\" disability\\,\\=\\"\\" link\\=\\"\\" card\\,\\=\\"\\" women\\=\\"\\" infants\\=\\"\\" children\\=\\"\\" \\(wic\\)\\,\\=\\"\\" temporary\\=\\"\\" assistance\\=\\"\\" needy\\=\\"\\" families\\=\\"\\" \\(tanf\\)\\,\\=\\"\\" low\\-income\\=\\"\\" housing\\=\\"\\" referrals\\,\\=\\"\\" subsidized\\,\\=\\"\\" child\\=\\"\\" care\\=\\"\\" day\\=\\"\\" free\\=\\"\\" bus\\=\\"\\" passes\\=\\"\\" seniors\\,\\=\\"\\" legal\\=\\"\\" advocacy\\,\\=\\"\\" translation\\=\\"\\" interpretation\\,\\=\\"\\" community\\=\\"\\" resources\\,\\=\\"\\" insurance\\=\\"\\" enrollment\\,\\=\\"\\" outreach\\"\\}\\"\\=\\"\\" data\\-sheets\\-userformat\\=\\"\\{\\" 2\\"\\:13249\\,\\"3\\"\\:\\[null\\,0\\]\\,\\"9\\"\\:0\\,\\"10\\"\\:1\\,\\"11\\"\\:4\\,\\"12\\"\\:0\\,\\"15\\"\\:\\"calibri\\"\\,\\"16\\"\\:12\\}\\"\\=\\"\\"\\>\\<br\\>\\<\\/span\\>\\<\\/div\\>\\<div\\>\\<span style\\=\\"font\\-size\\: 12pt\\; font\\-family\\: Calibri\\, Arial\\;\\" data\\-sheets\\-value\\=\\"\\{\\" 1\\"\\:2\\,\\"2\\"\\:\\"general\\,\\=\\"\\" primary\\=\\"\\" care\\,\\=\\"\\" family\\=\\"\\" medicine\\,\\=\\"\\" ob\\-gyn\\,\\=\\"\\" health\\=\\"\\" education\\,\\=\\"\\" nutrition\\,\\=\\"\\" cardiology\\,\\=\\"\\" immunizations\\,\\=\\"\\" school\\=\\"\\" physicals\\,\\=\\"\\" immigration\\=\\"\\" pediatrics\\,\\=\\"\\" crisis\\=\\"\\" intervention\\,\\=\\"\\" psychiatric\\=\\"\\" evaluation\\,\\=\\"\\" medication\\=\\"\\" management\\=\\"\\" monitoring\\,\\=\\"\\" mental\\=\\"\\" assessment\\,\\=\\"\\" comprehensive\\=\\"\\" psychological\\=\\"\\" evaluation\\=\\"\\" for\\:\\=\\"\\" medical\\=\\"\\" certification\\=\\"\\" for\\=\\"\\" disability\\=\\"\\" exception\\=\\"\\" \\(n\\-648\\)\\=\\"\\" and\\=\\"\\" u\\-visa\\,\\=\\"\\" substance\\=\\"\\" abuse\\=\\"\\" counseling\\,\\=\\"\\" individual\\=\\"\\" psychotherapy\\,\\=\\"\\" group\\=\\"\\" therapy\\,\\=\\"\\" domestic\\=\\"\\" violence\\=\\"\\" line\\,\\=\\"\\" residential\\=\\"\\" shelter\\=\\"\\" facility\\,\\=\\"\\" partner\\=\\"\\" intervention\\=\\"\\" program\\,\\=\\"\\" case\\=\\"\\" medicare\\=\\"\\" medicaid\\,\\=\\"\\" supplemental\\=\\"\\" security\\=\\"\\" income\\=\\"\\" disability\\,\\=\\"\\" link\\=\\"\\" card\\,\\=\\"\\" women\\=\\"\\" infants\\=\\"\\" children\\=\\"\\" \\(wic\\)\\,\\=\\"\\" temporary\\=\\"\\" assistance\\=\\"\\" needy\\=\\"\\" families\\=\\"\\" \\(tanf\\)\\,\\=\\"\\" low\\-income\\=\\"\\" housing\\=\\"\\" referrals\\,\\=\\"\\" subsidized\\,\\=\\"\\" child\\=\\"\\" care\\=\\"\\" day\\=\\"\\" free\\=\\"\\" bus\\=\\"\\" passes\\=\\"\\" seniors\\,\\=\\"\\" legal\\=\\"\\" advocacy\\,\\=\\"\\" translation\\=\\"\\" interpretation\\,\\=\\"\\" community\\=\\"\\" resources\\,\\=\\"\\" insurance\\=\\"\\" enrollment\\,\\=\\"\\" outreach\\"\\}\\"\\=\\"\\" data\\-sheets\\-userformat\\=\\"\\{\\" 2\\"\\:13249\\,\\"3\\"\\:\\[null\\,0\\]\\,\\"9\\"\\:0\\,\\"10\\"\\:1\\,\\"11\\"\\:4\\,\\"12\\"\\:0\\,\\"15\\"\\:\\"calibri\\"\\,\\"16\\"\\:12\\}\\"\\=\\"\\"\\>\\<br\\>\\<\\/span\\>\\<\\/div\\>',
					'minor_access'   => null,
					'appointments'   => '\\<style type\\=\\"text\\/css\\"\\>\\<\\!\\-\\-td \\{border\\: 1px solid \\#ccc\\;\\}br \\{mso\\-data\\-placement\\:same\\-cell\\;\\}\\-\\-\\>\\<\\/style\\>\\<span style\\=\\"font\\-size\\: 12pt\\; font\\-family\\: Calibri\\, Arial\\;\\" data\\-sheets\\-value\\=\\"\\{\\"1\\"\\:2\\,\\"2\\"\\:\\"Patients advised to call ahead\\"\\}\\" data\\-sheets\\-userformat\\=\\"\\{\\"2\\"\\:13249\\,\\"3\\"\\:\\[null\\,0\\]\\,\\"9\\"\\:0\\,\\"10\\"\\:1\\,\\"11\\"\\:4\\,\\"12\\"\\:0\\,\\"15\\"\\:\\"Calibri\\"\\,\\"16\\"\\:12\\}\\"\\>Patients advised to call ahead\\.\\<\\/span\\>',
					'order'          => null,
					'notes'          => null,
					'created_at'     => '2017-08-25 12:40:36',
					'updated_at'     => '2017-08-29 10:10:04',
				],
			26 =>
				[
					'id'             => 27,
					'uid'            => null,
					'name'           => 'DuPage County Community Services',
					'tagline'        => null,
					'organization'   => null,
					'address1'       => '421 N\\. County Farm Road',
					'address2'       => null,
					'city'           => 'Wheaton',
					'state_id'       => 13,
					'zipcode'        => '60187',
					'auto_gps'       => 1,
					'latitude'       => '41.8676859',
					'longitude'      => '-88.1427231',
					'url'            => '',
					'email'          => 'N\\/A',
					'website'        => 'http\\:\\/\\/www\\.dupagecris\\.org',
					'phone'          => '6304076500',
					'247_hotline'    => null,
					'text_line'      => null,
					'live_chat'      => '',
					'youth_approved' => 0,
					'hours'          => '\\<font face\\=\\"Arial\\"\\>Call for more information\\.\\<\\/font\\>',
					'image'          => null,
					'description'    => '\\<style type\\=\\"text\\/css\\"\\>\\<\\!\\-\\-td \\{border\\: 1px solid \\#ccc\\;\\}br \\{mso\\-data\\-placement\\:same\\-cell\\;\\}\\-\\-\\>\\<\\/style\\>\\<span style\\=\\"font\\-size\\: 12pt\\; font\\-family\\: Calibri\\, Arial\\;\\" data\\-sheets\\-value\\=\\"\\{\\"1\\"\\:2\\,\\"2\\"\\:\\"Confidential services and programs\\, Client advocacy\\, and Referrals to over 1\\,100 social service resources nationwide\\"\\}\\" data\\-sheets\\-userformat\\=\\"\\{\\"2\\"\\:13249\\,\\"3\\"\\:\\[null\\,0\\]\\,\\"9\\"\\:0\\,\\"10\\"\\:1\\,\\"11\\"\\:4\\,\\"12\\"\\:0\\,\\"15\\"\\:\\"Calibri\\"\\,\\"16\\"\\:12\\}\\"\\>Confidential services and programs\\, Client advocacy\\, and Referrals to over 1\\,100 social service resources nationwide\\.\\<\\/span\\>',
					'minor_access'   => null,
					'appointments'   => null,
					'order'          => null,
					'notes'          => null,
					'created_at'     => '2017-08-25 12:44:57',
					'updated_at'     => '2017-08-29 09:09:19',
				],
			27 =>
				[
					'id'             => 28,
					'uid'            => null,
					'name'           => 'Children\\\'s Home \\+ Aid',
					'tagline'        => null,
					'organization'   => null,
					'address1'       => '725 E\\. Schaumburg Road',
					'address2'       => null,
					'city'           => 'Schaumburg',
					'state_id'       => 13,
					'zipcode'        => '60194',
					'auto_gps'       => 1,
					'latitude'       => '42.0276210',
					'longitude'      => '-88.0614160',
					'url'            => '',
					'email'          => '',
					'website'        => 'http\\:\\/\\/www\\.childrenshomeandaid\\.org',
					'phone'          => '6306712400',
					'247_hotline'    => null,
					'text_line'      => null,
					'live_chat'      => '',
					'youth_approved' => 0,
					'hours'          => '\\<style type\\=\\"text\\/css\\"\\>\\<\\!\\-\\-td \\{border\\: 1px solid \\#ccc\\;\\}br \\{mso\\-data\\-placement\\:same\\-cell\\;\\}\\-\\-\\>\\<\\/style\\>\\<span style\\=\\"font\\-size\\: 12pt\\; font\\-family\\: Calibri\\, Arial\\;\\" data\\-sheets\\-value\\=\\"\\{\\" 1\\"\\:2\\,\\"2\\"\\:\\"mon\\=\\"\\" \\-\\=\\"\\" fri\\:\\=\\"\\" 7\\=\\"\\" am\\=\\"\\" 6\\=\\"\\" pm\\"\\}\\"\\=\\"\\" data\\-sheets\\-userformat\\=\\"\\{\\" 2\\"\\:13249\\,\\"3\\"\\:\\[null\\,0\\]\\,\\"9\\"\\:0\\,\\"10\\"\\:1\\,\\"11\\"\\:4\\,\\"12\\"\\:0\\,\\"15\\"\\:\\"calibri\\"\\,\\"16\\"\\:12\\}\\"\\=\\"\\"\\>Mon\\-Fri\\: 7AM \\- 6PM\\<\\/span\\>',
					'image'          => null,
					'description'    => '\\<style type\\=\\"text\\/css\\"\\>\\<\\!\\-\\-td \\{border\\: 1px solid \\#ccc\\;\\}br \\{mso\\-data\\-placement\\:same\\-cell\\;\\}\\-\\-\\>\\<\\/style\\>\\<span style\\=\\"font\\-size\\: 12pt\\;\\" data\\-sheets\\-value\\=\\"\\{\\" 1\\"\\:2\\,\\"2\\"\\:\\"subsidized\\=\\"\\" services\\=\\"\\" to\\=\\"\\" low\\-income\\,\\=\\"\\" high\\-risk\\=\\"\\" families\\,\\=\\"\\" child\\=\\"\\" care\\=\\"\\" children\\=\\"\\" of\\=\\"\\" working\\=\\"\\" parents\\=\\"\\" up\\=\\"\\" age\\=\\"\\" 6\\\\n\\"\\}\\"\\=\\"\\" data\\-sheets\\-userformat\\=\\"\\{\\" 2\\"\\:13249\\,\\"3\\"\\:\\[null\\,0\\]\\,\\"9\\"\\:0\\,\\"10\\"\\:1\\,\\"11\\"\\:4\\,\\"12\\"\\:0\\,\\"15\\"\\:\\"calibri\\"\\,\\"16\\"\\:12\\}\\"\\=\\"\\"\\>\\<font face\\=\\"Arial\\"\\>Subsidized services to low\\-income\\, high\\-risk families\\, Child Care services to children of working parents up to age 6\\.\\<br\\>\\<\\/font\\>\\<\\/span\\>\\<div\\>\\<span style\\=\\"font\\-size\\: 12pt\\;\\" data\\-sheets\\-value\\=\\"\\{\\" 1\\"\\:2\\,\\"2\\"\\:\\"subsidized\\=\\"\\" services\\=\\"\\" to\\=\\"\\" low\\-income\\,\\=\\"\\" high\\-risk\\=\\"\\" families\\,\\=\\"\\" child\\=\\"\\" care\\=\\"\\" children\\=\\"\\" of\\=\\"\\" working\\=\\"\\" parents\\=\\"\\" up\\=\\"\\" age\\=\\"\\" 6\\\\n\\"\\}\\"\\=\\"\\" data\\-sheets\\-userformat\\=\\"\\{\\" 2\\"\\:13249\\,\\"3\\"\\:\\[null\\,0\\]\\,\\"9\\"\\:0\\,\\"10\\"\\:1\\,\\"11\\"\\:4\\,\\"12\\"\\:0\\,\\"15\\"\\:\\"calibri\\"\\,\\"16\\"\\:12\\}\\"\\=\\"\\"\\>\\<font face\\=\\"Arial\\"\\>\\<br\\>\\<\\/font\\>\\<\\/span\\>\\<\\/div\\>\\<div\\>\\<span style\\=\\"font\\-size\\: 12pt\\;\\" data\\-sheets\\-value\\=\\"\\{\\" 1\\"\\:2\\,\\"2\\"\\:\\"subsidized\\=\\"\\" services\\=\\"\\" to\\=\\"\\" low\\-income\\,\\=\\"\\" high\\-risk\\=\\"\\" families\\,\\=\\"\\" child\\=\\"\\" care\\=\\"\\" children\\=\\"\\" of\\=\\"\\" working\\=\\"\\" parents\\=\\"\\" up\\=\\"\\" age\\=\\"\\" 6\\\\n\\"\\}\\"\\=\\"\\" data\\-sheets\\-userformat\\=\\"\\{\\" 2\\"\\:13249\\,\\"3\\"\\:\\[null\\,0\\]\\,\\"9\\"\\:0\\,\\"10\\"\\:1\\,\\"11\\"\\:4\\,\\"12\\"\\:0\\,\\"15\\"\\:\\"calibri\\"\\,\\"16\\"\\:12\\}\\"\\=\\"\\"\\>\\<style type\\=\\"text\\/css\\"\\>\\<\\!\\-\\-td \\{border\\: 1px solid \\#ccc\\;\\}br \\{mso\\-data\\-placement\\:same\\-cell\\;\\}\\-\\-\\>\\<\\/style\\>\\<span style\\=\\"font\\-size\\: 12pt\\;\\" data\\-sheets\\-value\\=\\"\\{\\" 1\\"\\:2\\,\\"2\\"\\:\\"sliding\\=\\"\\" scale\\=\\"\\" for\\=\\"\\" those\\=\\"\\" who\\=\\"\\" qualify\\"\\}\\"\\=\\"\\" data\\-sheets\\-userformat\\=\\"\\{\\" 2\\"\\:13249\\,\\"3\\"\\:\\[null\\,0\\]\\,\\"9\\"\\:0\\,\\"10\\"\\:1\\,\\"11\\"\\:4\\,\\"12\\"\\:0\\,\\"15\\"\\:\\"calibri\\"\\,\\"16\\"\\:12\\}\\"\\=\\"\\"\\>\\<font face\\=\\"Arial\\"\\>Sliding scale for those who qualify\\.\\<\\/font\\>\\<\\/span\\>\\<br\\>\\<\\/span\\>\\<\\/div\\>\\<div\\>\\<span style\\=\\"font\\-size\\: 12pt\\; font\\-family\\: Calibri\\, Arial\\;\\" data\\-sheets\\-value\\=\\"\\{\\" 1\\"\\:2\\,\\"2\\"\\:\\"subsidized\\=\\"\\" services\\=\\"\\" to\\=\\"\\" low\\-income\\,\\=\\"\\" high\\-risk\\=\\"\\" families\\,\\=\\"\\" child\\=\\"\\" care\\=\\"\\" children\\=\\"\\" of\\=\\"\\" working\\=\\"\\" parents\\=\\"\\" up\\=\\"\\" age\\=\\"\\" 6\\\\n\\"\\}\\"\\=\\"\\" data\\-sheets\\-userformat\\=\\"\\{\\" 2\\"\\:13249\\,\\"3\\"\\:\\[null\\,0\\]\\,\\"9\\"\\:0\\,\\"10\\"\\:1\\,\\"11\\"\\:4\\,\\"12\\"\\:0\\,\\"15\\"\\:\\"calibri\\"\\,\\"16\\"\\:12\\}\\"\\=\\"\\"\\>\\<span style\\=\\"font\\-size\\: 12pt\\;\\" data\\-sheets\\-value\\=\\"\\{\\" 1\\"\\:2\\,\\"2\\"\\:\\"sliding\\=\\"\\" scale\\=\\"\\" for\\=\\"\\" those\\=\\"\\" who\\=\\"\\" qualify\\"\\}\\"\\=\\"\\" data\\-sheets\\-userformat\\=\\"\\{\\" 2\\"\\:13249\\,\\"3\\"\\:\\[null\\,0\\]\\,\\"9\\"\\:0\\,\\"10\\"\\:1\\,\\"11\\"\\:4\\,\\"12\\"\\:0\\,\\"15\\"\\:\\"calibri\\"\\,\\"16\\"\\:12\\}\\"\\=\\"\\"\\>\\<br\\>\\<\\/span\\>\\<\\/span\\>\\<\\/div\\>\\<div\\>\\<span style\\=\\"font\\-size\\: 12pt\\; font\\-family\\: Calibri\\, Arial\\;\\" data\\-sheets\\-value\\=\\"\\{\\" 1\\"\\:2\\,\\"2\\"\\:\\"subsidized\\=\\"\\" services\\=\\"\\" to\\=\\"\\" low\\-income\\,\\=\\"\\" high\\-risk\\=\\"\\" families\\,\\=\\"\\" child\\=\\"\\" care\\=\\"\\" children\\=\\"\\" of\\=\\"\\" working\\=\\"\\" parents\\=\\"\\" up\\=\\"\\" age\\=\\"\\" 6\\\\n\\"\\}\\"\\=\\"\\" data\\-sheets\\-userformat\\=\\"\\{\\" 2\\"\\:13249\\,\\"3\\"\\:\\[null\\,0\\]\\,\\"9\\"\\:0\\,\\"10\\"\\:1\\,\\"11\\"\\:4\\,\\"12\\"\\:0\\,\\"15\\"\\:\\"calibri\\"\\,\\"16\\"\\:12\\}\\"\\=\\"\\"\\>\\<span style\\=\\"font\\-size\\: 12pt\\;\\" data\\-sheets\\-value\\=\\"\\{\\" 1\\"\\:2\\,\\"2\\"\\:\\"sliding\\=\\"\\" scale\\=\\"\\" for\\=\\"\\" those\\=\\"\\" who\\=\\"\\" qualify\\"\\}\\"\\=\\"\\" data\\-sheets\\-userformat\\=\\"\\{\\" 2\\"\\:13249\\,\\"3\\"\\:\\[null\\,0\\]\\,\\"9\\"\\:0\\,\\"10\\"\\:1\\,\\"11\\"\\:4\\,\\"12\\"\\:0\\,\\"15\\"\\:\\"calibri\\"\\,\\"16\\"\\:12\\}\\"\\=\\"\\"\\>\\<br\\>\\<\\/span\\>\\<\\/span\\>\\<\\/div\\>\\<div\\>\\<span style\\=\\"font\\-size\\: 12pt\\; font\\-family\\: Calibri\\, Arial\\;\\" data\\-sheets\\-value\\=\\"\\{\\" 1\\"\\:2\\,\\"2\\"\\:\\"subsidized\\=\\"\\" services\\=\\"\\" to\\=\\"\\" low\\-income\\,\\=\\"\\" high\\-risk\\=\\"\\" families\\,\\=\\"\\" child\\=\\"\\" care\\=\\"\\" children\\=\\"\\" of\\=\\"\\" working\\=\\"\\" parents\\=\\"\\" up\\=\\"\\" age\\=\\"\\" 6\\\\n\\"\\}\\"\\=\\"\\" data\\-sheets\\-userformat\\=\\"\\{\\" 2\\"\\:13249\\,\\"3\\"\\:\\[null\\,0\\]\\,\\"9\\"\\:0\\,\\"10\\"\\:1\\,\\"11\\"\\:4\\,\\"12\\"\\:0\\,\\"15\\"\\:\\"calibri\\"\\,\\"16\\"\\:12\\}\\"\\=\\"\\"\\>\\<span style\\=\\"font\\-size\\: 12pt\\;\\" data\\-sheets\\-value\\=\\"\\{\\" 1\\"\\:2\\,\\"2\\"\\:\\"sliding\\=\\"\\" scale\\=\\"\\" for\\=\\"\\" those\\=\\"\\" who\\=\\"\\" qualify\\"\\}\\"\\=\\"\\" data\\-sheets\\-userformat\\=\\"\\{\\" 2\\"\\:13249\\,\\"3\\"\\:\\[null\\,0\\]\\,\\"9\\"\\:0\\,\\"10\\"\\:1\\,\\"11\\"\\:4\\,\\"12\\"\\:0\\,\\"15\\"\\:\\"calibri\\"\\,\\"16\\"\\:12\\}\\"\\=\\"\\"\\>\\<br\\>\\<\\/span\\>\\<\\/span\\>\\<\\/div\\>',
					'minor_access'   => null,
					'appointments'   => '\\<style type\\=\\"text\\/css\\"\\>\\<\\!\\-\\-td \\{border\\: 1px solid \\#ccc\\;\\}br \\{mso\\-data\\-placement\\:same\\-cell\\;\\}\\-\\-\\>\\<\\/style\\>\\<span style\\=\\"font\\-size\\: 12pt\\; font\\-family\\: Calibri\\, Arial\\;\\" data\\-sheets\\-value\\=\\"\\{\\"1\\"\\:2\\,\\"2\\"\\:\\"Patients advised to call ahead\\"\\}\\" data\\-sheets\\-userformat\\=\\"\\{\\"2\\"\\:13249\\,\\"3\\"\\:\\[null\\,0\\]\\,\\"9\\"\\:0\\,\\"10\\"\\:1\\,\\"11\\"\\:4\\,\\"12\\"\\:0\\,\\"15\\"\\:\\"Calibri\\"\\,\\"16\\"\\:12\\}\\"\\>Patients advised to call ahead\\.\\<\\/span\\>',
					'order'          => null,
					'notes'          => null,
					'created_at'     => '2017-08-25 12:49:26',
					'updated_at'     => '2017-08-28 16:37:37',
				],
			28 =>
				[
					'id'             => 29,
					'uid'            => null,
					'name'           => 'Children\\\'s Home \\+ Aid',
					'tagline'        => null,
					'organization'   => null,
					'address1'       => '721 S\\. Quentin Road',
					'address2'       => null,
					'city'           => 'Palatine',
					'state_id'       => 13,
					'zipcode'        => '60067',
					'auto_gps'       => 1,
					'latitude'       => '42.0965530',
					'longitude'      => '-88.0624176',
					'url'            => '',
					'email'          => '',
					'website'        => 'http\\:\\/\\/www\\.childrenshomeandaid\\.org',
					'phone'          => '6306712400',
					'247_hotline'    => null,
					'text_line'      => null,
					'live_chat'      => '',
					'youth_approved' => 0,
					'hours'          => '\\<style type\\=\\"text\\/css\\"\\>\\<\\!\\-\\-td \\{border\\: 1px solid \\#ccc\\;\\}br \\{mso\\-data\\-placement\\:same\\-cell\\;\\}\\-\\-\\>\\<\\/style\\>\\<span style\\=\\"font\\-size\\: 12pt\\; font\\-family\\: Calibri\\, Arial\\;\\" data\\-sheets\\-value\\=\\"\\{\\" 1\\"\\:2\\,\\"2\\"\\:\\"mon\\=\\"\\" \\-\\=\\"\\" fri\\:\\=\\"\\" 7\\=\\"\\" am\\=\\"\\" 6\\=\\"\\" pm\\"\\}\\"\\=\\"\\" data\\-sheets\\-userformat\\=\\"\\{\\" 2\\"\\:13249\\,\\"3\\"\\:\\[null\\,0\\]\\,\\"9\\"\\:0\\,\\"10\\"\\:1\\,\\"11\\"\\:4\\,\\"12\\"\\:0\\,\\"15\\"\\:\\"calibri\\"\\,\\"16\\"\\:12\\}\\"\\=\\"\\"\\>Mon\\-Fri\\: 7AM \\- 6PM\\<\\/span\\>',
					'image'          => null,
					'description'    => '\\<style type\\=\\"text\\/css\\"\\>\\<\\!\\-\\-td \\{border\\: 1px solid \\#ccc\\;\\}br \\{mso\\-data\\-placement\\:same\\-cell\\;\\}\\-\\-\\>\\<\\/style\\>\\<span style\\=\\"font\\-size\\: 12pt\\; font\\-family\\: Calibri\\, Arial\\;\\" data\\-sheets\\-value\\=\\"\\{\\"1\\"\\:2\\,\\"2\\"\\:\\"Subsidized services to low\\-income\\, high\\-risk families\\, Child Care services to children of working parents up to age 6\\\\n\\"\\}\\" data\\-sheets\\-userformat\\=\\"\\{\\"2\\"\\:13249\\,\\"3\\"\\:\\[null\\,0\\]\\,\\"9\\"\\:0\\,\\"10\\"\\:1\\,\\"11\\"\\:4\\,\\"12\\"\\:0\\,\\"15\\"\\:\\"Calibri\\"\\,\\"16\\"\\:12\\}\\"\\>Subsidized services to low\\-income\\, high\\-risk families\\, Child Care services to children of working parents up to age 6\\.\\<br\\>\\<\\/span\\>\\<div\\>\\<span style\\=\\"font\\-size\\: 12pt\\; font\\-family\\: Calibri\\, Arial\\;\\" data\\-sheets\\-value\\=\\"\\{\\"1\\"\\:2\\,\\"2\\"\\:\\"Subsidized services to low\\-income\\, high\\-risk families\\, Child Care services to children of working parents up to age 6\\\\n\\"\\}\\" data\\-sheets\\-userformat\\=\\"\\{\\"2\\"\\:13249\\,\\"3\\"\\:\\[null\\,0\\]\\,\\"9\\"\\:0\\,\\"10\\"\\:1\\,\\"11\\"\\:4\\,\\"12\\"\\:0\\,\\"15\\"\\:\\"Calibri\\"\\,\\"16\\"\\:12\\}\\"\\>\\<br\\>\\<\\/span\\>\\<\\/div\\>\\<div\\>\\<span style\\=\\"font\\-size\\: 12pt\\; font\\-family\\: Calibri\\, Arial\\;\\" data\\-sheets\\-value\\=\\"\\{\\"1\\"\\:2\\,\\"2\\"\\:\\"Subsidized services to low\\-income\\, high\\-risk families\\, Child Care services to children of working parents up to age 6\\\\n\\"\\}\\" data\\-sheets\\-userformat\\=\\"\\{\\"2\\"\\:13249\\,\\"3\\"\\:\\[null\\,0\\]\\,\\"9\\"\\:0\\,\\"10\\"\\:1\\,\\"11\\"\\:4\\,\\"12\\"\\:0\\,\\"15\\"\\:\\"Calibri\\"\\,\\"16\\"\\:12\\}\\"\\>\\<style type\\=\\"text\\/css\\"\\>\\<\\!\\-\\-td \\{border\\: 1px solid \\#ccc\\;\\}br \\{mso\\-data\\-placement\\:same\\-cell\\;\\}\\-\\-\\>\\<\\/style\\>\\<span style\\=\\"font\\-size\\: 12pt\\;\\" data\\-sheets\\-value\\=\\"\\{\\"1\\"\\:2\\,\\"2\\"\\:\\"Sliding scale for those who qualify\\"\\}\\" data\\-sheets\\-userformat\\=\\"\\{\\"2\\"\\:13249\\,\\"3\\"\\:\\[null\\,0\\]\\,\\"9\\"\\:0\\,\\"10\\"\\:1\\,\\"11\\"\\:4\\,\\"12\\"\\:0\\,\\"15\\"\\:\\"Calibri\\"\\,\\"16\\"\\:12\\}\\"\\>Sliding scale for those who qualify\\<\\/span\\>\\<br\\>\\<\\/span\\>\\<\\/div\\>\\<div\\>\\<span style\\=\\"font\\-size\\: 12pt\\; font\\-family\\: Calibri\\, Arial\\;\\" data\\-sheets\\-value\\=\\"\\{\\"1\\"\\:2\\,\\"2\\"\\:\\"Subsidized services to low\\-income\\, high\\-risk families\\, Child Care services to children of working parents up to age 6\\\\n\\"\\}\\" data\\-sheets\\-userformat\\=\\"\\{\\"2\\"\\:13249\\,\\"3\\"\\:\\[null\\,0\\]\\,\\"9\\"\\:0\\,\\"10\\"\\:1\\,\\"11\\"\\:4\\,\\"12\\"\\:0\\,\\"15\\"\\:\\"Calibri\\"\\,\\"16\\"\\:12\\}\\"\\>\\<span style\\=\\"font\\-size\\: 12pt\\;\\" data\\-sheets\\-value\\=\\"\\{\\"1\\"\\:2\\,\\"2\\"\\:\\"Sliding scale for those who qualify\\"\\}\\" data\\-sheets\\-userformat\\=\\"\\{\\"2\\"\\:13249\\,\\"3\\"\\:\\[null\\,0\\]\\,\\"9\\"\\:0\\,\\"10\\"\\:1\\,\\"11\\"\\:4\\,\\"12\\"\\:0\\,\\"15\\"\\:\\"Calibri\\"\\,\\"16\\"\\:12\\}\\"\\>\\<br\\>\\<\\/span\\>\\<\\/span\\>\\<\\/div\\>\\<div\\>\\<span style\\=\\"font\\-size\\: 12pt\\; font\\-family\\: Calibri\\, Arial\\;\\" data\\-sheets\\-value\\=\\"\\{\\"1\\"\\:2\\,\\"2\\"\\:\\"Subsidized services to low\\-income\\, high\\-risk families\\, Child Care services to children of working parents up to age 6\\\\n\\"\\}\\" data\\-sheets\\-userformat\\=\\"\\{\\"2\\"\\:13249\\,\\"3\\"\\:\\[null\\,0\\]\\,\\"9\\"\\:0\\,\\"10\\"\\:1\\,\\"11\\"\\:4\\,\\"12\\"\\:0\\,\\"15\\"\\:\\"Calibri\\"\\,\\"16\\"\\:12\\}\\"\\>\\<span style\\=\\"font\\-size\\: 12pt\\;\\" data\\-sheets\\-value\\=\\"\\{\\"1\\"\\:2\\,\\"2\\"\\:\\"Sliding scale for those who qualify\\"\\}\\" data\\-sheets\\-userformat\\=\\"\\{\\"2\\"\\:13249\\,\\"3\\"\\:\\[null\\,0\\]\\,\\"9\\"\\:0\\,\\"10\\"\\:1\\,\\"11\\"\\:4\\,\\"12\\"\\:0\\,\\"15\\"\\:\\"Calibri\\"\\,\\"16\\"\\:12\\}\\"\\>\\<br\\>\\<\\/span\\>\\<\\/span\\>\\<\\/div\\>\\<div\\>\\<span style\\=\\"font\\-size\\: 12pt\\; font\\-family\\: Calibri\\, Arial\\;\\" data\\-sheets\\-value\\=\\"\\{\\"1\\"\\:2\\,\\"2\\"\\:\\"Subsidized services to low\\-income\\, high\\-risk families\\, Child Care services to children of working parents up to age 6\\\\n\\"\\}\\" data\\-sheets\\-userformat\\=\\"\\{\\"2\\"\\:13249\\,\\"3\\"\\:\\[null\\,0\\]\\,\\"9\\"\\:0\\,\\"10\\"\\:1\\,\\"11\\"\\:4\\,\\"12\\"\\:0\\,\\"15\\"\\:\\"Calibri\\"\\,\\"16\\"\\:12\\}\\"\\>\\<span style\\=\\"font\\-size\\: 12pt\\;\\" data\\-sheets\\-value\\=\\"\\{\\"1\\"\\:2\\,\\"2\\"\\:\\"Sliding scale for those who qualify\\"\\}\\" data\\-sheets\\-userformat\\=\\"\\{\\"2\\"\\:13249\\,\\"3\\"\\:\\[null\\,0\\]\\,\\"9\\"\\:0\\,\\"10\\"\\:1\\,\\"11\\"\\:4\\,\\"12\\"\\:0\\,\\"15\\"\\:\\"Calibri\\"\\,\\"16\\"\\:12\\}\\"\\>\\<br\\>\\<\\/span\\>\\<\\/span\\>\\<\\/div\\>',
					'minor_access'   => null,
					'appointments'   => '\\<style type\\=\\"text\\/css\\"\\>\\<\\!\\-\\-td \\{border\\: 1px solid \\#ccc\\;\\}br \\{mso\\-data\\-placement\\:same\\-cell\\;\\}\\-\\-\\>\\<\\/style\\>\\<span style\\=\\"font\\-size\\: 12pt\\; font\\-family\\: Calibri\\, Arial\\;\\" data\\-sheets\\-value\\=\\"\\{\\"1\\"\\:2\\,\\"2\\"\\:\\"Patients advised to call ahead\\"\\}\\" data\\-sheets\\-userformat\\=\\"\\{\\"2\\"\\:13249\\,\\"3\\"\\:\\[null\\,0\\]\\,\\"9\\"\\:0\\,\\"10\\"\\:1\\,\\"11\\"\\:4\\,\\"12\\"\\:0\\,\\"15\\"\\:\\"Calibri\\"\\,\\"16\\"\\:12\\}\\"\\>Patients advised to call ahead\\.\\<\\/span\\>',
					'order'          => null,
					'notes'          => null,
					'created_at'     => '2017-08-25 12:56:22',
					'updated_at'     => '2017-08-28 16:34:03',
				],
			29 =>
				[
					'id'             => 30,
					'uid'            => null,
					'name'           => 'The Children\\\'s Advocacy Center of North and Northwest Cook County',
					'tagline'        => null,
					'organization'   => null,
					'address1'       => '640 Illinois Blvd\\.',
					'address2'       => null,
					'city'           => 'Hoffman Estates',
					'state_id'       => 13,
					'zipcode'        => '60169',
					'auto_gps'       => 1,
					'latitude'       => '42.0388462',
					'longitude'      => '-88.0851176',
					'url'            => '',
					'email'          => 'info\\@cachelps\\.org',
					'website'        => 'http\\:\\/\\/www\\.cachelps\\.org',
					'phone'          => '8478850100',
					'247_hotline'    => null,
					'text_line'      => null,
					'live_chat'      => '',
					'youth_approved' => 0,
					'hours'          => '\\<style type\\=\\"text\\/css\\"\\>\\<\\!\\-\\-td \\{border\\: 1px solid \\#ccc\\;\\}br \\{mso\\-data\\-placement\\:same\\-cell\\;\\}\\-\\-\\>\\<\\/style\\>\\<span style\\=\\"font\\-size\\: 12pt\\; font\\-family\\: Calibri\\, Arial\\;\\" data\\-sheets\\-value\\=\\"\\{\\" 1\\"\\:2\\,\\"2\\"\\:\\"mon\\-\\=\\"\\" thurs\\:\\=\\"\\" 9\\=\\"\\" am\\=\\"\\" \\-\\=\\"\\" 5\\=\\"\\" pm\\=\\"\\" fri\\:\\=\\"\\" 4\\:30\\=\\"\\" pm\\"\\}\\"\\=\\"\\" data\\-sheets\\-userformat\\=\\"\\{\\" 2\\"\\:13249\\,\\"3\\"\\:\\[null\\,0\\]\\,\\"9\\"\\:0\\,\\"10\\"\\:1\\,\\"11\\"\\:4\\,\\"12\\"\\:0\\,\\"15\\"\\:\\"calibri\\"\\,\\"16\\"\\:12\\}\\"\\=\\"\\"\\>Mon\\- Thurs\\: 9AM \\- 5PM\\<\\/span\\>\\<div\\>\\<span style\\=\\"font\\-size\\: 12pt\\; font\\-family\\: Calibri\\, Arial\\;\\" data\\-sheets\\-value\\=\\"\\{\\" 1\\"\\:2\\,\\"2\\"\\:\\"mon\\-\\=\\"\\" thurs\\:\\=\\"\\" 9\\=\\"\\" am\\=\\"\\" \\-\\=\\"\\" 5\\=\\"\\" pm\\=\\"\\" fri\\:\\=\\"\\" 4\\:30\\=\\"\\" pm\\"\\}\\"\\=\\"\\" data\\-sheets\\-userformat\\=\\"\\{\\" 2\\"\\:13249\\,\\"3\\"\\:\\[null\\,0\\]\\,\\"9\\"\\:0\\,\\"10\\"\\:1\\,\\"11\\"\\:4\\,\\"12\\"\\:0\\,\\"15\\"\\:\\"calibri\\"\\,\\"16\\"\\:12\\}\\"\\=\\"\\"\\>Fri\\: 9AM \\- 4\\:30PM\\<\\/span\\>\\<\\/div\\>',
					'image'          => null,
					'description'    => '\\<style type\\=\\"text\\/css\\"\\>\\<\\!\\-\\-td \\{border\\: 1px solid \\#ccc\\;\\}br \\{mso\\-data\\-placement\\:same\\-cell\\;\\}\\-\\-\\>\\<\\/style\\>\\<span data\\-sheets\\-value\\=\\"\\{\\" 1\\"\\:2\\,\\"2\\"\\:\\"crisis\\=\\"\\" intervention\\,\\=\\"\\" short\\-term\\=\\"\\" counseling\\,\\=\\"\\" parent\\=\\"\\" support\\=\\"\\" group\\,\\=\\"\\" group\\=\\"\\" child\\=\\"\\" sexual\\=\\"\\" abuse\\=\\"\\" assessments\\,\\=\\"\\" court\\=\\"\\" advocacy\\,\\=\\"\\" case\\=\\"\\" management\\,\\=\\"\\" safe\\=\\"\\" from\\=\\"\\" the\\=\\"\\" start\\:\\=\\"\\" an\\=\\"\\" early\\=\\"\\" intervention\\=\\"\\" program\\=\\"\\" designed\\=\\"\\" to\\=\\"\\" prevent\\=\\"\\" exposure\\=\\"\\" violence\\=\\"\\" in\\=\\"\\" children\\=\\"\\" age\\=\\"\\" 0\\-5\\.\\"\\}\\"\\=\\"\\" data\\-sheets\\-userformat\\=\\"\\{\\" 2\\"\\:13251\\,\\"3\\"\\:\\[null\\,0\\]\\,\\"4\\"\\:\\[null\\,2\\,16777215\\]\\,\\"9\\"\\:0\\,\\"10\\"\\:1\\,\\"11\\"\\:4\\,\\"12\\"\\:0\\,\\"15\\"\\:\\"calibri\\"\\,\\"16\\"\\:12\\}\\"\\=\\"\\"\\>\\<font face\\=\\"Arial\\"\\>Crisis intervention\\, Short\\-term counseling\\, Parent support group\\, Group intervention\\, Child sexual abuse assessments\\, Court advocacy\\, Case management\\, Safe from the Start\\: an early intervention program designed to prevent exposure to violence in children age 0\\-5\\.\\<\\/font\\>\\<\\/span\\>\\<div\\>\\<span data\\-sheets\\-value\\=\\"\\{\\" 1\\"\\:2\\,\\"2\\"\\:\\"crisis\\=\\"\\" intervention\\,\\=\\"\\" short\\-term\\=\\"\\" counseling\\,\\=\\"\\" parent\\=\\"\\" support\\=\\"\\" group\\,\\=\\"\\" group\\=\\"\\" child\\=\\"\\" sexual\\=\\"\\" abuse\\=\\"\\" assessments\\,\\=\\"\\" court\\=\\"\\" advocacy\\,\\=\\"\\" case\\=\\"\\" management\\,\\=\\"\\" safe\\=\\"\\" from\\=\\"\\" the\\=\\"\\" start\\:\\=\\"\\" an\\=\\"\\" early\\=\\"\\" intervention\\=\\"\\" program\\=\\"\\" designed\\=\\"\\" to\\=\\"\\" prevent\\=\\"\\" exposure\\=\\"\\" violence\\=\\"\\" in\\=\\"\\" children\\=\\"\\" age\\=\\"\\" 0\\-5\\.\\"\\}\\"\\=\\"\\" data\\-sheets\\-userformat\\=\\"\\{\\" 2\\"\\:13251\\,\\"3\\"\\:\\[null\\,0\\]\\,\\"4\\"\\:\\[null\\,2\\,16777215\\]\\,\\"9\\"\\:0\\,\\"10\\"\\:1\\,\\"11\\"\\:4\\,\\"12\\"\\:0\\,\\"15\\"\\:\\"calibri\\"\\,\\"16\\"\\:12\\}\\"\\=\\"\\"\\>\\<font face\\=\\"Arial\\"\\>\\<br\\>\\<\\/font\\>\\<\\/span\\>\\<\\/div\\>\\<div\\>\\<span data\\-sheets\\-value\\=\\"\\{\\" 1\\"\\:2\\,\\"2\\"\\:\\"crisis\\=\\"\\" intervention\\,\\=\\"\\" short\\-term\\=\\"\\" counseling\\,\\=\\"\\" parent\\=\\"\\" support\\=\\"\\" group\\,\\=\\"\\" group\\=\\"\\" child\\=\\"\\" sexual\\=\\"\\" abuse\\=\\"\\" assessments\\,\\=\\"\\" court\\=\\"\\" advocacy\\,\\=\\"\\" case\\=\\"\\" management\\,\\=\\"\\" safe\\=\\"\\" from\\=\\"\\" the\\=\\"\\" start\\:\\=\\"\\" an\\=\\"\\" early\\=\\"\\" intervention\\=\\"\\" program\\=\\"\\" designed\\=\\"\\" to\\=\\"\\" prevent\\=\\"\\" exposure\\=\\"\\" violence\\=\\"\\" in\\=\\"\\" children\\=\\"\\" age\\=\\"\\" 0\\-5\\.\\"\\}\\"\\=\\"\\" data\\-sheets\\-userformat\\=\\"\\{\\" 2\\"\\:13251\\,\\"3\\"\\:\\[null\\,0\\]\\,\\"4\\"\\:\\[null\\,2\\,16777215\\]\\,\\"9\\"\\:0\\,\\"10\\"\\:1\\,\\"11\\"\\:4\\,\\"12\\"\\:0\\,\\"15\\"\\:\\"calibri\\"\\,\\"16\\"\\:12\\}\\"\\=\\"\\"\\>\\<style type\\=\\"text\\/css\\"\\>\\<\\!\\-\\-td \\{border\\: 1px solid \\#ccc\\;\\}br \\{mso\\-data\\-placement\\:same\\-cell\\;\\}\\-\\-\\>\\<\\/style\\>\\<span data\\-sheets\\-value\\=\\"\\{\\" 1\\"\\:2\\,\\"2\\"\\:\\"all\\=\\"\\" services\\=\\"\\" are\\=\\"\\" provided\\=\\"\\" free\\=\\"\\" of\\=\\"\\" charge\\"\\}\\"\\=\\"\\" data\\-sheets\\-userformat\\=\\"\\{\\" 2\\"\\:13251\\,\\"3\\"\\:\\[null\\,0\\]\\,\\"4\\"\\:\\[null\\,2\\,16777215\\]\\,\\"9\\"\\:0\\,\\"10\\"\\:1\\,\\"11\\"\\:4\\,\\"12\\"\\:0\\,\\"15\\"\\:\\"calibri\\"\\,\\"16\\"\\:12\\}\\"\\=\\"\\"\\>\\<font face\\=\\"Arial\\"\\>All services are provided free of charge\\.\\<\\/font\\>\\<\\/span\\>\\<br\\>\\<\\/span\\>\\<\\/div\\>',
					'minor_access'   => null,
					'appointments'   => '\\<style type\\=\\"text\\/css\\"\\>\\<\\!\\-\\-td \\{border\\: 1px solid \\#ccc\\;\\}br \\{mso\\-data\\-placement\\:same\\-cell\\;\\}\\-\\-\\>\\<\\/style\\>\\<span style\\=\\"font\\-size\\: 12pt\\; font\\-family\\: Calibri\\, Arial\\;\\" data\\-sheets\\-value\\=\\"\\{\\"1\\"\\:2\\,\\"2\\"\\:\\"Walk\\-ins accepted\\"\\}\\" data\\-sheets\\-userformat\\=\\"\\{\\"2\\"\\:13249\\,\\"3\\"\\:\\[null\\,0\\]\\,\\"9\\"\\:0\\,\\"10\\"\\:1\\,\\"11\\"\\:4\\,\\"12\\"\\:0\\,\\"15\\"\\:\\"Calibri\\"\\,\\"16\\"\\:12\\}\\"\\>Walk\\-ins accepted\\.\\<\\/span\\>',
					'order'          => null,
					'notes'          => null,
					'created_at'     => '2017-08-25 13:03:41',
					'updated_at'     => '2017-08-29 11:21:59',
				],
			30 =>
				[
					'id'             => 31,
					'uid'            => null,
					'name'           => 'Omni Youth Services',
					'tagline'        => null,
					'organization'   => null,
					'address1'       => '1616 N\\. Arlington Heights Road',
					'address2'       => null,
					'city'           => 'Arlington Heights',
					'state_id'       => 13,
					'zipcode'        => '60004',
					'auto_gps'       => 1,
					'latitude'       => '42.1062242',
					'longitude'      => '-87.9808555',
					'url'            => '',
					'email'          => '',
					'website'        => 'http\\:\\/\\/www\\.omniyouth\\.org',
					'phone'          => '8473531500',
					'247_hotline'    => '8473531500',
					'text_line'      => null,
					'live_chat'      => '',
					'youth_approved' => 0,
					'hours'          => '\\<font face\\=\\"Arial\\"\\>Mon\\-Thu\\: 9AM\\-9PM\\<\\/font\\>\\<div\\>\\<font face\\=\\"Arial\\"\\>Fri\\: 9AM\\-6PM\\<\\/font\\>\\<\\/div\\>\\<div\\>\\<font face\\=\\"Arial\\"\\>Sat\\: By appointment only\\<\\/font\\>\\<\\/div\\>',
					'image'          => null,
					'description'    => '\\<span style\\=\\"font\\-family\\: Arial\\; white\\-space\\: pre\\-wrap\\; background\\-color\\: rgb\\(255\\, 255\\, 255\\)\\;\\"\\>Drug Evaluation\\, Alcohol evaluation\\, Basic Outpatient Services\\, Intensive Outpatient Services\\, Drug Testing\\, Seven Challenges Program\\, Crisis Intervention Services\\, Critical Incident Services\\, Diagnostic Assessment Services\\, Suicide Prevention\\, Gang Prevention\\, Gang Intervention\\, Juvenile Justice Programming\\, Girls After School Program\\, Bullying Prevention\\, Bullying Intervention\\, Substance Use\\/Abuse\\, Parenting Adolescents\\, Social Skills\\, Transition to High School \\<\\/span\\>\\<div\\>\\<br\\>\\<\\/div\\>\\<div\\>\\<span style\\=\\"background\\-color\\: rgb\\(255\\, 255\\, 255\\)\\;\\"\\>\\<span style\\=\\"font\\-family\\: Arial\\; white\\-space\\: pre\\-wrap\\;\\"\\>Sliding scale for those who qualify\\<\\/span\\>\\<span style\\=\\"font\\-family\\: Arial\\; white\\-space\\: pre\\-wrap\\;\\"\\>\\<br\\>\\<\\/span\\>\\<\\/span\\>\\<\\/div\\>\\<div\\>\\<span style\\=\\"font\\-family\\: Arial\\; white\\-space\\: pre\\-wrap\\; background\\-color\\: rgb\\(255\\, 255\\, 255\\)\\;\\"\\>Cash\\, credit card\\, debit card\\, and check accepted\\.\\<\\/span\\>\\<span style\\=\\"font\\-family\\: Arial\\; font\\-size\\: 15px\\; white\\-space\\: pre\\-wrap\\; background\\-color\\: rgb\\(252\\, 229\\, 205\\)\\;\\"\\>\\<br\\>\\<\\/span\\>\\<\\/div\\>',
					'minor_access'   => '\\<span style\\=\\"font\\-family\\: Arial\\; white\\-space\\: pre\\-wrap\\; background\\-color\\: rgb\\(255\\, 255\\, 255\\)\\;\\"\\>Parent permission not required\\.\\<\\/span\\>',
					'appointments'   => '\\<span style\\=\\"font\\-family\\: Arial\\; white\\-space\\: pre\\-wrap\\; background\\-color\\: rgb\\(255\\, 255\\, 255\\)\\;\\"\\>Same day appointments available \\&amp\\; walk\\-ins accepted\\.\\<\\/span\\>',
					'order'          => null,
					'notes'          => null,
					'created_at'     => '2017-08-25 13:06:43',
					'updated_at'     => '2017-08-29 10:46:47',
				],
			31 =>
				[
					'id'             => 59,
					'uid'            => null,
					'name'           => 'Elk Grove Township\\- Youth Services',
					'tagline'        => null,
					'organization'   => null,
					'address1'       => '401 W\\. Golf Road',
					'address2'       => null,
					'city'           => 'Mt\\. Prospect',
					'state_id'       => 13,
					'zipcode'        => '60056',
					'auto_gps'       => 1,
					'latitude'       => '42.0471083',
					'longitude'      => '-87.9428304',
					'url'            => '',
					'email'          => '',
					'website'        => 'http\\:\\/\\/www\\.elkgrovetownship\\.com',
					'phone'          => '8479810373',
					'247_hotline'    => null,
					'text_line'      => null,
					'live_chat'      => '',
					'youth_approved' => 0,
					'hours'          => '\\<style type\\=\\"text\\/css\\"\\>\\<\\!\\-\\-td \\{border\\: 1px solid \\#ccc\\;\\}br \\{mso\\-data\\-placement\\:same\\-cell\\;\\}\\-\\-\\>\\<\\/style\\>\\<span style\\=\\"font\\-family\\: Arial\\;\\" data\\-sheets\\-value\\=\\"\\{\\" 1\\"\\:2\\,\\"2\\"\\:\\"mon\\-fri\\:\\=\\"\\" 9am\\-4pm\\,\\=\\"\\" evenings\\:\\=\\"\\" by\\=\\"\\" apt\\.\\=\\"\\" only\\"\\}\\"\\=\\"\\" data\\-sheets\\-userformat\\=\\"\\{\\" 2\\"\\:13185\\,\\"3\\"\\:\\[null\\,0\\]\\,\\"10\\"\\:2\\,\\"11\\"\\:4\\,\\"12\\"\\:0\\,\\"15\\"\\:\\"arial\\,sans\\,sans\\-serif\\"\\,\\"16\\"\\:11\\}\\"\\=\\"\\"\\>Mon\\-Fri\\: 9AM \\- 4PM\\<\\/span\\>\\<div\\>\\<span style\\=\\"font\\-family\\: Arial\\;\\" data\\-sheets\\-value\\=\\"\\{\\" 1\\"\\:2\\,\\"2\\"\\:\\"mon\\-fri\\:\\=\\"\\" 9am\\-4pm\\,\\=\\"\\" evenings\\:\\=\\"\\" by\\=\\"\\" apt\\.\\=\\"\\" only\\"\\}\\"\\=\\"\\" data\\-sheets\\-userformat\\=\\"\\{\\" 2\\"\\:13185\\,\\"3\\"\\:\\[null\\,0\\]\\,\\"10\\"\\:2\\,\\"11\\"\\:4\\,\\"12\\"\\:0\\,\\"15\\"\\:\\"arial\\,sans\\,sans\\-serif\\"\\,\\"16\\"\\:11\\}\\"\\=\\"\\"\\>Evenings\\: By appointment only\\<\\/span\\>\\<\\/div\\>',
					'image'          => null,
					'description'    => '\\<style type\\=\\"text\\/css\\"\\>\\<\\!\\-\\-td \\{border\\: 1px solid \\#ccc\\;\\}br \\{mso\\-data\\-placement\\:same\\-cell\\;\\}\\-\\-\\>\\<\\/style\\>\\<span style\\=\\"font\\-family\\: Arial\\;\\" data\\-sheets\\-value\\=\\"\\{\\" 1\\"\\:2\\,\\"2\\"\\:\\"bullying\\=\\"\\" prevention\\=\\"\\" in\\=\\"\\" schools\\,\\=\\"\\" social\\=\\"\\" skills\\=\\"\\" groups\\,\\=\\"\\" life\\=\\"\\" adhd\\=\\"\\" programs\\,\\=\\"\\" aspergers\\=\\"\\" girls\\=\\"\\" empowerment\\=\\"\\" gang\\=\\"\\" parenting\\=\\"\\" seminars\\,\\=\\"\\" leadership\\=\\"\\" development\\,\\=\\"\\" counseling\\=\\"\\" for\\:\\=\\"\\" child\\=\\"\\" misbehavior\\=\\"\\" or\\=\\"\\" defiance\\,\\=\\"\\" attention\\=\\"\\" problems\\,\\=\\"\\" family\\=\\"\\" conflict\\,\\=\\"\\" academic\\=\\"\\" underachievement\\,\\=\\"\\" depression\\,\\=\\"\\" challenge\\=\\"\\" group\\"\\}\\"\\=\\"\\" data\\-sheets\\-userformat\\=\\"\\{\\" 2\\"\\:13187\\,\\"3\\"\\:\\[null\\,0\\]\\,\\"4\\"\\:\\[null\\,2\\,16573901\\]\\,\\"10\\"\\:2\\,\\"11\\"\\:4\\,\\"12\\"\\:0\\,\\"15\\"\\:\\"arial\\"\\,\\"16\\"\\:11\\}\\"\\=\\"\\"\\>Bullying Prevention in Schools\\, Social Skills Groups\\, Life Skills Groups\\, ADHD Programs\\, Aspergers Groups\\, Girls Empowerment Groups\\, Gang Prevention in Schools\\, Parenting Seminars\\, Leadership Development\\, Counseling For\\: Child Misbehavior or Defiance\\, Attention Problems\\, Family Conflict\\, Academic Underachievement\\, Depression\\, Challenge Group\\<\\/span\\>\\<div\\>\\<span style\\=\\"font\\-family\\: Arial\\;\\" data\\-sheets\\-value\\=\\"\\{\\" 1\\"\\:2\\,\\"2\\"\\:\\"bullying\\=\\"\\" prevention\\=\\"\\" in\\=\\"\\" schools\\,\\=\\"\\" social\\=\\"\\" skills\\=\\"\\" groups\\,\\=\\"\\" life\\=\\"\\" adhd\\=\\"\\" programs\\,\\=\\"\\" aspergers\\=\\"\\" girls\\=\\"\\" empowerment\\=\\"\\" gang\\=\\"\\" parenting\\=\\"\\" seminars\\,\\=\\"\\" leadership\\=\\"\\" development\\,\\=\\"\\" counseling\\=\\"\\" for\\:\\=\\"\\" child\\=\\"\\" misbehavior\\=\\"\\" or\\=\\"\\" defiance\\,\\=\\"\\" attention\\=\\"\\" problems\\,\\=\\"\\" family\\=\\"\\" conflict\\,\\=\\"\\" academic\\=\\"\\" underachievement\\,\\=\\"\\" depression\\,\\=\\"\\" challenge\\=\\"\\" group\\"\\}\\"\\=\\"\\" data\\-sheets\\-userformat\\=\\"\\{\\" 2\\"\\:13187\\,\\"3\\"\\:\\[null\\,0\\]\\,\\"4\\"\\:\\[null\\,2\\,16573901\\]\\,\\"10\\"\\:2\\,\\"11\\"\\:4\\,\\"12\\"\\:0\\,\\"15\\"\\:\\"arial\\"\\,\\"16\\"\\:11\\}\\"\\=\\"\\"\\>\\<br\\>\\<\\/span\\>\\<\\/div\\>\\<div\\>\\<span style\\=\\"font\\-family\\: Arial\\;\\" data\\-sheets\\-value\\=\\"\\{\\" 1\\"\\:2\\,\\"2\\"\\:\\"bullying\\=\\"\\" prevention\\=\\"\\" in\\=\\"\\" schools\\,\\=\\"\\" social\\=\\"\\" skills\\=\\"\\" groups\\,\\=\\"\\" life\\=\\"\\" adhd\\=\\"\\" programs\\,\\=\\"\\" aspergers\\=\\"\\" girls\\=\\"\\" empowerment\\=\\"\\" gang\\=\\"\\" parenting\\=\\"\\" seminars\\,\\=\\"\\" leadership\\=\\"\\" development\\,\\=\\"\\" counseling\\=\\"\\" for\\:\\=\\"\\" child\\=\\"\\" misbehavior\\=\\"\\" or\\=\\"\\" defiance\\,\\=\\"\\" attention\\=\\"\\" problems\\,\\=\\"\\" family\\=\\"\\" conflict\\,\\=\\"\\" academic\\=\\"\\" underachievement\\,\\=\\"\\" depression\\,\\=\\"\\" challenge\\=\\"\\" group\\"\\}\\"\\=\\"\\" data\\-sheets\\-userformat\\=\\"\\{\\" 2\\"\\:13187\\,\\"3\\"\\:\\[null\\,0\\]\\,\\"4\\"\\:\\[null\\,2\\,16573901\\]\\,\\"10\\"\\:2\\,\\"11\\"\\:4\\,\\"12\\"\\:0\\,\\"15\\"\\:\\"arial\\"\\,\\"16\\"\\:11\\}\\"\\=\\"\\"\\>Sliding scale fee for those who qualify\\.\\<\\/span\\>\\<\\/div\\>',
					'minor_access'   => '\\<style type\\=\\"text\\/css\\"\\>\\<\\!\\-\\-td \\{border\\: 1px solid \\#ccc\\;\\}br \\{mso\\-data\\-placement\\:same\\-cell\\;\\}\\-\\-\\>\\<\\/style\\>\\<span style\\=\\"font\\-family\\: Arial\\;\\" data\\-sheets\\-value\\=\\"\\{\\" 1\\"\\:2\\,\\"2\\"\\:\\"parent\\=\\"\\" permission\\=\\"\\" required\\=\\"\\" after\\=\\"\\" 5\\=\\"\\" sessions\\=\\"\\" of\\=\\"\\" treatment\\.\\=\\"\\" social\\=\\"\\" skills\\=\\"\\" groups\\=\\"\\" offered\\=\\"\\" for\\=\\"\\" children\\=\\"\\" 7\\-16\\.\\"\\}\\"\\=\\"\\" data\\-sheets\\-userformat\\=\\"\\{\\" 2\\"\\:13185\\,\\"3\\"\\:\\[null\\,0\\]\\,\\"10\\"\\:2\\,\\"11\\"\\:4\\,\\"12\\"\\:0\\,\\"15\\"\\:\\"arial\\,sans\\,sans\\-serif\\"\\,\\"16\\"\\:11\\}\\"\\=\\"\\"\\>Parent permission required after 5 sessions of treatment\\. Social Skills Groups offered for children 7\\-16\\.\\<\\/span\\>',
					'appointments'   => '\\<style type\\=\\"text\\/css\\"\\>\\<\\!\\-\\-td \\{border\\: 1px solid \\#ccc\\;\\}br \\{mso\\-data\\-placement\\:same\\-cell\\;\\}\\-\\-\\>\\<\\/style\\>\\<span style\\=\\"font\\-family\\: Arial\\;\\" data\\-sheets\\-value\\=\\"\\{\\" 1\\"\\:2\\,\\"2\\"\\:\\"same\\=\\"\\" day\\=\\"\\" appointments\\=\\"\\" available\\=\\"\\" \\&\\=\\"\\" walk\\-ins\\=\\"\\" accepted\\"\\}\\"\\=\\"\\" data\\-sheets\\-userformat\\=\\"\\{\\" 2\\"\\:13185\\,\\"3\\"\\:\\[null\\,0\\]\\,\\"10\\"\\:2\\,\\"11\\"\\:4\\,\\"12\\"\\:0\\,\\"15\\"\\:\\"arial\\,sans\\,sans\\-serif\\"\\,\\"16\\"\\:11\\}\\"\\=\\"\\"\\>Same day appointments available \\&amp\\; walk\\-ins accepted\\.\\<\\/span\\>',
					'order'          => null,
					'notes'          => null,
					'created_at'     => '2017-08-29 10:56:55',
					'updated_at'     => '2017-08-29 11:50:17',
				],
			32 =>
				[
					'id'             => 33,
					'uid'            => null,
					'name'           => 'Leyden Family Service and Mental Health Center',
					'tagline'        => null,
					'organization'   => null,
					'address1'       => '1776 Moon Lake Boulevard',
					'address2'       => null,
					'city'           => 'Hoffman Estates',
					'state_id'       => 13,
					'zipcode'        => '60169',
					'auto_gps'       => 1,
					'latitude'       => '42.0549567',
					'longitude'      => '-88.1382338',
					'url'            => '',
					'email'          => '',
					'website'        => 'http\\:\\/\\/www\\.leydenfamilyservice\\.org',
					'phone'          => '8474510330',
					'247_hotline'    => '8474511100',
					'text_line'      => null,
					'live_chat'      => '',
					'youth_approved' => 0,
					'hours'          => '\\<style type\\=\\"text\\/css\\"\\>\\<\\!\\-\\-td \\{border\\: 1px solid \\#ccc\\;\\}br \\{mso\\-data\\-placement\\:same\\-cell\\;\\}\\-\\-\\>\\<\\/style\\>\\<span style\\=\\"font\\-size\\: 12pt\\;\\" data\\-sheets\\-value\\=\\"\\{\\" 1\\"\\:2\\,\\"2\\"\\:\\"24\\=\\"\\" 7\\"\\}\\"\\=\\"\\" data\\-sheets\\-userformat\\=\\"\\{\\" 2\\"\\:13249\\,\\"3\\"\\:\\[null\\,0\\]\\,\\"9\\"\\:0\\,\\"10\\"\\:1\\,\\"11\\"\\:4\\,\\"12\\"\\:0\\,\\"15\\"\\:\\"calibri\\"\\,\\"16\\"\\:12\\}\\"\\=\\"\\"\\>\\<font face\\=\\"Arial\\"\\>24\\/7\\<\\/font\\>\\<\\/span\\>',
					'image'          => null,
					'description'    => '\\<style type\\=\\"text\\/css\\"\\>\\<\\!\\-\\-td \\{border\\: 1px solid \\#ccc\\;\\}br \\{mso\\-data\\-placement\\:same\\-cell\\;\\}\\-\\-\\>\\<\\/style\\>\\<span data\\-sheets\\-value\\=\\"\\{\\" 1\\"\\:2\\,\\"2\\"\\:\\"share\\=\\"\\" \\(start\\=\\"\\" here\\=\\"\\" addiction\\=\\"\\" rehabilitation\\=\\"\\" \\&\\=\\"\\" education\\)\\,\\=\\"\\" alcohol\\,\\=\\"\\" drug\\=\\"\\" and\\=\\"\\" gambling\\=\\"\\" facility\\"\\}\\"\\=\\"\\" data\\-sheets\\-userformat\\=\\"\\{\\" 2\\"\\:13249\\,\\"3\\"\\:\\[null\\,0\\]\\,\\"9\\"\\:0\\,\\"10\\"\\:1\\,\\"11\\"\\:4\\,\\"12\\"\\:0\\,\\"15\\"\\:\\"calibri\\"\\,\\"16\\"\\:12\\}\\"\\=\\"\\"\\>\\<font face\\=\\"Arial\\"\\>SHARE \\(Start Here Addiction Rehabilitation \\&amp\\; Education\\)\\, Alcohol\\, drug and gambling rehabilitation facility\\.\\<\\/font\\>\\<\\/span\\>\\<div\\>\\<span data\\-sheets\\-value\\=\\"\\{\\" 1\\"\\:2\\,\\"2\\"\\:\\"share\\=\\"\\" \\(start\\=\\"\\" here\\=\\"\\" addiction\\=\\"\\" rehabilitation\\=\\"\\" \\&\\=\\"\\" education\\)\\,\\=\\"\\" alcohol\\,\\=\\"\\" drug\\=\\"\\" and\\=\\"\\" gambling\\=\\"\\" facility\\"\\}\\"\\=\\"\\" data\\-sheets\\-userformat\\=\\"\\{\\" 2\\"\\:13249\\,\\"3\\"\\:\\[null\\,0\\]\\,\\"9\\"\\:0\\,\\"10\\"\\:1\\,\\"11\\"\\:4\\,\\"12\\"\\:0\\,\\"15\\"\\:\\"calibri\\"\\,\\"16\\"\\:12\\}\\"\\=\\"\\"\\>\\<font face\\=\\"Arial\\"\\>\\<br\\>\\<\\/font\\>\\<\\/span\\>\\<\\/div\\>\\<div\\>\\<span data\\-sheets\\-value\\=\\"\\{\\" 1\\"\\:2\\,\\"2\\"\\:\\"share\\=\\"\\" \\(start\\=\\"\\" here\\=\\"\\" addiction\\=\\"\\" rehabilitation\\=\\"\\" \\&\\=\\"\\" education\\)\\,\\=\\"\\" alcohol\\,\\=\\"\\" drug\\=\\"\\" and\\=\\"\\" gambling\\=\\"\\" facility\\"\\}\\"\\=\\"\\" data\\-sheets\\-userformat\\=\\"\\{\\" 2\\"\\:13249\\,\\"3\\"\\:\\[null\\,0\\]\\,\\"9\\"\\:0\\,\\"10\\"\\:1\\,\\"11\\"\\:4\\,\\"12\\"\\:0\\,\\"15\\"\\:\\"calibri\\"\\,\\"16\\"\\:12\\}\\"\\=\\"\\"\\>\\<font face\\=\\"Arial\\"\\>\\<style type\\=\\"text\\/css\\"\\>\\<\\!\\-\\-td \\{border\\: 1px solid \\#ccc\\;\\}br \\{mso\\-data\\-placement\\:same\\-cell\\;\\}\\-\\-\\>\\<\\/style\\>\\<span data\\-sheets\\-value\\=\\"\\{\\" 1\\"\\:2\\,\\"2\\"\\:\\"patients\\=\\"\\" will\\=\\"\\" not\\=\\"\\" be\\=\\"\\" turned\\=\\"\\" away\\=\\"\\" due\\=\\"\\" to\\=\\"\\" inability\\=\\"\\" pay\\"\\}\\"\\=\\"\\" data\\-sheets\\-userformat\\=\\"\\{\\" 2\\"\\:13249\\,\\"3\\"\\:\\[null\\,0\\]\\,\\"9\\"\\:0\\,\\"10\\"\\:1\\,\\"11\\"\\:4\\,\\"12\\"\\:0\\,\\"15\\"\\:\\"calibri\\"\\,\\"16\\"\\:12\\}\\"\\=\\"\\"\\>Patients will not be turned away due to inability to pay\\.\\<\\/span\\>\\<br\\>\\<\\/font\\>\\<\\/span\\>\\<\\/div\\>\\<div\\>\\<span data\\-sheets\\-value\\=\\"\\{\\" 1\\"\\:2\\,\\"2\\"\\:\\"share\\=\\"\\" \\(start\\=\\"\\" here\\=\\"\\" addiction\\=\\"\\" rehabilitation\\=\\"\\" \\&\\=\\"\\" education\\)\\,\\=\\"\\" alcohol\\,\\=\\"\\" drug\\=\\"\\" and\\=\\"\\" gambling\\=\\"\\" facility\\"\\}\\"\\=\\"\\" data\\-sheets\\-userformat\\=\\"\\{\\" 2\\"\\:13249\\,\\"3\\"\\:\\[null\\,0\\]\\,\\"9\\"\\:0\\,\\"10\\"\\:1\\,\\"11\\"\\:4\\,\\"12\\"\\:0\\,\\"15\\"\\:\\"calibri\\"\\,\\"16\\"\\:12\\}\\"\\=\\"\\"\\>\\<span data\\-sheets\\-value\\=\\"\\{\\" 1\\"\\:2\\,\\"2\\"\\:\\"patients\\=\\"\\" will\\=\\"\\" not\\=\\"\\" be\\=\\"\\" turned\\=\\"\\" away\\=\\"\\" due\\=\\"\\" to\\=\\"\\" inability\\=\\"\\" pay\\"\\}\\"\\=\\"\\" data\\-sheets\\-userformat\\=\\"\\{\\" 2\\"\\:13249\\,\\"3\\"\\:\\[null\\,0\\]\\,\\"9\\"\\:0\\,\\"10\\"\\:1\\,\\"11\\"\\:4\\,\\"12\\"\\:0\\,\\"15\\"\\:\\"calibri\\"\\,\\"16\\"\\:12\\}\\"\\=\\"\\"\\>\\<span data\\-sheets\\-value\\=\\"\\{\\" 1\\"\\:2\\,\\"2\\"\\:\\"uninsured\\=\\"\\" and\\=\\"\\" underinsured\\=\\"\\" clients\\=\\"\\" are\\=\\"\\" eligible\\=\\"\\" to\\=\\"\\" be\\=\\"\\" assessed\\=\\"\\" for\\=\\"\\" services\\=\\"\\" on\\=\\"\\" a\\=\\"\\" discounted\\=\\"\\" fee\\=\\"\\" schedule\\"\\}\\"\\=\\"\\" data\\-sheets\\-userformat\\=\\"\\{\\" 2\\"\\:13249\\,\\"3\\"\\:\\[null\\,0\\]\\,\\"9\\"\\:0\\,\\"10\\"\\:1\\,\\"11\\"\\:4\\,\\"12\\"\\:0\\,\\"15\\"\\:\\"calibri\\"\\,\\"16\\"\\:12\\}\\"\\=\\"\\"\\>\\<font face\\=\\"Arial\\"\\>Uninsured and underinsured clients are eligible to be assessed for services on a discounted fee schedule\\.\\<\\/font\\>\\<\\/span\\>\\<br\\>\\<\\/span\\>\\<\\/span\\>\\<\\/div\\>',
					'minor_access'   => null,
					'appointments'   => '\\<style type\\=\\"text\\/css\\"\\>\\<\\!\\-\\-td \\{border\\: 1px solid \\#ccc\\;\\}br \\{mso\\-data\\-placement\\:same\\-cell\\;\\}\\-\\-\\>\\<\\/style\\>\\<span style\\=\\"font\\-size\\: 12pt\\;\\" data\\-sheets\\-value\\=\\"\\{\\" 1\\"\\:2\\,\\"2\\"\\:\\"patients\\=\\"\\" advised\\=\\"\\" to\\=\\"\\" call\\=\\"\\" ahead\\"\\}\\"\\=\\"\\" data\\-sheets\\-userformat\\=\\"\\{\\" 2\\"\\:13249\\,\\"3\\"\\:\\[null\\,0\\]\\,\\"9\\"\\:0\\,\\"10\\"\\:1\\,\\"11\\"\\:4\\,\\"12\\"\\:0\\,\\"15\\"\\:\\"calibri\\"\\,\\"16\\"\\:12\\}\\"\\=\\"\\"\\>\\<font face\\=\\"Arial\\"\\>Patients advised to call ahead\\.\\<\\/font\\>\\<\\/span\\>',
					'order'          => null,
					'notes'          => null,
					'created_at'     => '2017-08-25 13:14:35',
					'updated_at'     => '2017-08-29 10:19:24',
				],
			33 =>
				[
					'id'             => 32,
					'uid'            => null,
					'name'           => 'Family Behavioral Health Clinic',
					'tagline'        => null,
					'organization'   => null,
					'address1'       => '1455 Golf Road',
					'address2'       => 'Suite 105',
					'city'           => 'Des Plaines',
					'state_id'       => 13,
					'zipcode'        => '60016',
					'auto_gps'       => 1,
					'latitude'       => '42.0544611',
					'longitude'      => '-87.8898508',
					'url'            => '',
					'email'          => '',
					'website'        => 'http\\:\\/\\/www\\.familybehavioralhealthclinic\\.com',
					'phone'          => '8473903004',
					'247_hotline'    => null,
					'text_line'      => null,
					'live_chat'      => '',
					'youth_approved' => 0,
					'hours'          => '\\<font face\\=\\"Arial\\"\\>Mon\\-Fri\\: 9AM \\- 9PM\\&nbsp\\;\\<\\/font\\>\\<div\\>\\<font face\\=\\"Arial\\"\\>Sat\\: 9AM \\- 1PM\\<\\/font\\>\\<\\/div\\>',
					'image'          => null,
					'description'    => '\\<span style\\=\\"font\\-family\\: Arial\\; white\\-space\\: pre\\-wrap\\; background\\-color\\: rgb\\(255\\, 255\\, 255\\)\\;\\"\\>DUI Risk Education Classes and Counseling\\, Drug and Alcohol Abuse Assessments and Referrals\\, Outpatient Therapy\\, Intensive Outpatient Therapy\\, Community Intervention Treatment\\, Early Intervention Treatment\\, Recovery Support and Maintenance\\<\\/span\\>\\<div\\>\\<span style\\=\\"font\\-family\\: Arial\\; white\\-space\\: pre\\-wrap\\; background\\-color\\: rgb\\(255\\, 255\\, 255\\)\\;\\"\\>\\<br\\>\\<\\/span\\>\\<\\/div\\>\\<div\\>\\<span style\\=\\"background\\-color\\: rgb\\(255\\, 255\\, 255\\)\\;\\"\\>\\<span style\\=\\"font\\-family\\: Arial\\; white\\-space\\: pre\\-wrap\\;\\"\\>\\<br\\>\\<\\/span\\>\\<\\/span\\>\\<\\/div\\>\\<div\\>\\<span style\\=\\"background\\-color\\: rgb\\(255\\, 255\\, 255\\)\\;\\"\\>\\<span style\\=\\"font\\-family\\: Arial\\; white\\-space\\: pre\\-wrap\\;\\"\\>Sliding scale for those who qualify\\<\\/span\\>\\<span style\\=\\"font\\-family\\: Arial\\; white\\-space\\: pre\\-wrap\\;\\"\\>\\<br\\>\\<\\/span\\>\\<\\/span\\>\\<\\/div\\>\\<div\\>\\<span style\\=\\"font\\-family\\: Arial\\; white\\-space\\: pre\\-wrap\\; background\\-color\\: rgb\\(255\\, 255\\, 255\\)\\;\\"\\>Cash\\, credit card\\, and debit card accepted\\.\\<\\/span\\>\\<span style\\=\\"font\\-family\\: Arial\\; font\\-size\\: 15px\\; white\\-space\\: pre\\-wrap\\; background\\-color\\: rgb\\(252\\, 229\\, 205\\)\\;\\"\\>\\<br\\>\\<\\/span\\>\\<\\/div\\>',
					'minor_access'   => '\\<span style\\=\\"font\\-family\\: Arial\\; white\\-space\\: pre\\-wrap\\; background\\-color\\: rgb\\(255\\, 255\\, 255\\)\\;\\"\\>Parent permission not required\\.\\<\\/span\\>',
					'appointments'   => '\\<span style\\=\\"font\\-family\\: Arial\\; white\\-space\\: pre\\-wrap\\; background\\-color\\: rgb\\(255\\, 255\\, 255\\)\\;\\"\\>Same day appointments available\\.\\<\\/span\\>',
					'order'          => null,
					'notes'          => null,
					'created_at'     => '2017-08-25 13:11:36',
					'updated_at'     => '2017-08-29 09:23:19',
				],
			34 =>
				[
					'id'             => 34,
					'uid'            => null,
					'name'           => 'Arbor Counseling Center',
					'tagline'        => null,
					'organization'   => null,
					'address1'       => '2300 N\\. Barrington Road',
					'address2'       => 'Suite 105',
					'city'           => 'Hoffman Estates',
					'state_id'       => 13,
					'zipcode'        => '60169',
					'auto_gps'       => 1,
					'latitude'       => '42.0637970',
					'longitude'      => '-88.1457025',
					'url'            => '',
					'email'          => '',
					'website'        => 'http\\:\\/\\/www\\.arborcounselingcenter\\.com',
					'phone'          => '8478847502',
					'247_hotline'    => null,
					'text_line'      => null,
					'live_chat'      => '',
					'youth_approved' => 0,
					'hours'          => '\\<font face\\=\\"Arial\\"\\>Mon\\-Sat\\: 8AM \\- 10PM\\&nbsp\\;\\<\\/font\\>',
					'image'          => null,
					'description'    => '\\<span style\\=\\"font\\-family\\: Arial\\; white\\-space\\: pre\\-wrap\\; background\\-color\\: rgb\\(255\\, 255\\, 255\\)\\;\\"\\>Alcohol Use Disorder\\, Caffeine Intoxication\\, Cannabis Use Disorder\\, Opioid Use Disorder\\, Tobacco Use Disorder\\, Suicide Prevention\\, Major Depressive Disorder\\, Dysthymic Disorder\\, Bipolar Disorder\\, Adjustment Disorder\\, Acute Stress Disorder\\, Agoraphobia\\, Panic Disorder\\, Social Anxiety Disorder\\, Phobias\\, Obsessive Compulsive Disorder\\, Post\\-Traumatic Stress Disorder\\, Generalized Anxiety Disorder\\, Body Dysmorphic Disorder\\, Adjustment Disorder\\, ADHD\\, ADD\\, Impulse Disorder\\, Learning Disorders\\, Oppositional\\-Defiant Disorders\\, Autism\\, Aspergers\\, Trauma\\, Anorexia Nervosa\\, Bulimia Nervosa\\, Obesity\\, Adoption Issues\\, Anger Management\\, Chronic\\/Terminal Illness\\, Custody Issues\\, Dissociative Identity Disorder\\, Family Violence\\, Hoarding Disorder\\, LGBTQ\\+ Supportive Services\\, Personality Disorders\\, Schizophrenia\\<\\/span\\>\\<div\\>\\<font face\\=\\"Arial\\"\\>\\<span style\\=\\"white\\-space\\: pre\\-wrap\\; background\\-color\\: rgb\\(255\\, 255\\, 255\\)\\;\\"\\>\\<br\\>\\<\\/span\\>\\<\\/font\\>\\<\\/div\\>\\<div\\>\\<br\\>\\<\\/div\\>\\<div\\>\\<span style\\=\\"font\\-family\\: Arial\\; white\\-space\\: pre\\-wrap\\; background\\-color\\: rgb\\(255\\, 255\\, 255\\)\\;\\"\\>Sliding scale for those who qualify\\<\\/span\\>\\<\\/div\\>\\<div\\>\\<span style\\=\\"font\\-family\\: Arial\\; white\\-space\\: pre\\-wrap\\; background\\-color\\: rgb\\(255\\, 255\\, 255\\)\\;\\"\\>Cash\\, credit card\\, debit card\\, and check accepted\\.\\<\\/span\\>\\<font face\\=\\"Arial\\"\\>\\<span style\\=\\"font\\-size\\: 15px\\; white\\-space\\: pre\\-wrap\\; background\\-color\\: rgb\\(252\\, 229\\, 205\\)\\;\\"\\>\\<br\\>\\<\\/span\\>\\<\\/font\\>\\<div\\>\\<span style\\=\\"font\\-family\\: Arial\\; font\\-size\\: 15px\\; white\\-space\\: pre\\-wrap\\; background\\-color\\: rgb\\(252\\, 229\\, 205\\)\\;\\"\\>\\<br\\>\\<\\/span\\>\\<\\/div\\>\\<div\\>\\<span style\\=\\"font\\-family\\: Arial\\; font\\-size\\: 15px\\; white\\-space\\: pre\\-wrap\\; background\\-color\\: rgb\\(252\\, 229\\, 205\\)\\;\\"\\>\\<br\\>\\<\\/span\\>\\<\\/div\\>\\<\\/div\\>',
					'minor_access'   => '\\<span style\\=\\"font\\-family\\: Arial\\; white\\-space\\: pre\\-wrap\\; background\\-color\\: rgb\\(255\\, 255\\, 255\\)\\;\\"\\>Parent permission required\\.\\<\\/span\\>',
					'appointments'   => '\\<span style\\=\\"white\\-space\\: pre\\-wrap\\; background\\-color\\: rgb\\(255\\, 255\\, 255\\)\\;\\"\\>\\<font face\\=\\"Arial\\"\\>Patients advised to call ahead\\.\\<\\/font\\>\\<\\/span\\>',
					'order'          => null,
					'notes'          => null,
					'created_at'     => '2017-08-25 13:16:53',
					'updated_at'     => '2017-08-28 16:30:31',
				],
			35 =>
				[
					'id'             => 35,
					'uid'            => null,
					'name'           => 'Alfred Campanelli YMCA',
					'tagline'        => null,
					'organization'   => null,
					'address1'       => '300 W\\. Wise Road',
					'address2'       => null,
					'city'           => 'Schaumburg',
					'state_id'       => 13,
					'zipcode'        => '60193',
					'auto_gps'       => 1,
					'latitude'       => '42.0056813',
					'longitude'      => '-88.0858608',
					'url'            => '',
					'email'          => 'membership\\@campanelliymca\\.org',
					'website'        => 'http\\:\\/\\/www\\.campanelliymca\\.org',
					'phone'          => '8478919622',
					'247_hotline'    => null,
					'text_line'      => null,
					'live_chat'      => '',
					'youth_approved' => 0,
					'hours'          => '\\<style type\\=\\"text\\/css\\"\\>\\<\\!\\-\\-td \\{border\\: 1px solid \\#ccc\\;\\}br \\{mso\\-data\\-placement\\:same\\-cell\\;\\}\\-\\-\\>\\<\\/style\\>\\<span style\\=\\"font\\-size\\: 12pt\\; font\\-family\\: Calibri\\, Arial\\;\\" data\\-sheets\\-value\\=\\"\\{\\" 1\\"\\:2\\,\\"2\\"\\:\\"mon\\=\\"\\" \\-\\=\\"\\" fri\\:\\=\\"\\" 5\\=\\"\\" am\\=\\"\\" 10\\=\\"\\" pm\\=\\"\\" sat\\:\\=\\"\\" 7\\=\\"\\" sun\\:\\=\\"\\" 4\\=\\"\\" pm\\"\\}\\"\\=\\"\\" data\\-sheets\\-userformat\\=\\"\\{\\" 2\\"\\:13249\\,\\"3\\"\\:\\[null\\,0\\]\\,\\"9\\"\\:0\\,\\"10\\"\\:1\\,\\"11\\"\\:4\\,\\"12\\"\\:0\\,\\"15\\"\\:\\"calibri\\"\\,\\"16\\"\\:12\\}\\"\\=\\"\\"\\>Mon \\- Fri\\:  5AM \\- 10PM\\&nbsp\\;\\<\\/span\\>\\<div\\>\\<span style\\=\\"font\\-size\\: 12pt\\; font\\-family\\: Calibri\\, Arial\\;\\" data\\-sheets\\-value\\=\\"\\{\\" 1\\"\\:2\\,\\"2\\"\\:\\"mon\\=\\"\\" \\-\\=\\"\\" fri\\:\\=\\"\\" 5\\=\\"\\" am\\=\\"\\" 10\\=\\"\\" pm\\=\\"\\" sat\\:\\=\\"\\" 7\\=\\"\\" sun\\:\\=\\"\\" 4\\=\\"\\" pm\\"\\}\\"\\=\\"\\" data\\-sheets\\-userformat\\=\\"\\{\\" 2\\"\\:13249\\,\\"3\\"\\:\\[null\\,0\\]\\,\\"9\\"\\:0\\,\\"10\\"\\:1\\,\\"11\\"\\:4\\,\\"12\\"\\:0\\,\\"15\\"\\:\\"calibri\\"\\,\\"16\\"\\:12\\}\\"\\=\\"\\"\\>Sat\\: 7AM \\- 7PM\\&nbsp\\;\\<\\/span\\>\\<\\/div\\>\\<div\\>\\<span style\\=\\"font\\-size\\: 12pt\\; font\\-family\\: Calibri\\, Arial\\;\\" data\\-sheets\\-value\\=\\"\\{\\" 1\\"\\:2\\,\\"2\\"\\:\\"mon\\=\\"\\" \\-\\=\\"\\" fri\\:\\=\\"\\" 5\\=\\"\\" am\\=\\"\\" 10\\=\\"\\" pm\\=\\"\\" sat\\:\\=\\"\\" 7\\=\\"\\" sun\\:\\=\\"\\" 4\\=\\"\\" pm\\"\\}\\"\\=\\"\\" data\\-sheets\\-userformat\\=\\"\\{\\" 2\\"\\:13249\\,\\"3\\"\\:\\[null\\,0\\]\\,\\"9\\"\\:0\\,\\"10\\"\\:1\\,\\"11\\"\\:4\\,\\"12\\"\\:0\\,\\"15\\"\\:\\"calibri\\"\\,\\"16\\"\\:12\\}\\"\\=\\"\\"\\>Sun\\: 7AM \\- 4PM\\<\\/span\\>\\<\\/div\\>',
					'image'          => null,
					'description'    => '\\<style type\\=\\"text\\/css\\"\\>\\<\\!\\-\\-td \\{border\\: 1px solid \\#ccc\\;\\}br \\{mso\\-data\\-placement\\:same\\-cell\\;\\}\\-\\-\\>\\<\\/style\\>\\<span style\\=\\"font\\-size\\: 12pt\\;\\" data\\-sheets\\-value\\=\\"\\{\\" 1\\"\\:2\\,\\"2\\"\\:\\"health\\,\\=\\"\\" fitness\\=\\"\\" and\\=\\"\\" wellness\\=\\"\\" programs\\,\\=\\"\\" youth\\=\\"\\" camps\\,\\=\\"\\" family\\=\\"\\" activities\\,\\=\\"\\" swimming\\=\\"\\" aquatics\\"\\}\\"\\=\\"\\" data\\-sheets\\-userformat\\=\\"\\{\\" 2\\"\\:13249\\,\\"3\\"\\:\\[null\\,0\\]\\,\\"9\\"\\:0\\,\\"10\\"\\:1\\,\\"11\\"\\:4\\,\\"12\\"\\:0\\,\\"15\\"\\:\\"calibri\\"\\,\\"16\\"\\:12\\}\\"\\=\\"\\"\\>\\<font face\\=\\"Arial\\"\\>Health\\, Fitness and wellness programs\\, Youth programs\\, Camps\\, Family activities\\, Swimming and aquatics\\.\\<\\/font\\>\\<\\/span\\>\\<div\\>\\<span style\\=\\"font\\-size\\: 12pt\\;\\" data\\-sheets\\-value\\=\\"\\{\\" 1\\"\\:2\\,\\"2\\"\\:\\"health\\,\\=\\"\\" fitness\\=\\"\\" and\\=\\"\\" wellness\\=\\"\\" programs\\,\\=\\"\\" youth\\=\\"\\" camps\\,\\=\\"\\" family\\=\\"\\" activities\\,\\=\\"\\" swimming\\=\\"\\" aquatics\\"\\}\\"\\=\\"\\" data\\-sheets\\-userformat\\=\\"\\{\\" 2\\"\\:13249\\,\\"3\\"\\:\\[null\\,0\\]\\,\\"9\\"\\:0\\,\\"10\\"\\:1\\,\\"11\\"\\:4\\,\\"12\\"\\:0\\,\\"15\\"\\:\\"calibri\\"\\,\\"16\\"\\:12\\}\\"\\=\\"\\"\\>\\<font face\\=\\"Arial\\"\\>\\<br\\>\\<\\/font\\>\\<\\/span\\>\\<\\/div\\>\\<div\\>\\<span style\\=\\"font\\-size\\: 12pt\\;\\" data\\-sheets\\-value\\=\\"\\{\\" 1\\"\\:2\\,\\"2\\"\\:\\"health\\,\\=\\"\\" fitness\\=\\"\\" and\\=\\"\\" wellness\\=\\"\\" programs\\,\\=\\"\\" youth\\=\\"\\" camps\\,\\=\\"\\" family\\=\\"\\" activities\\,\\=\\"\\" swimming\\=\\"\\" aquatics\\"\\}\\"\\=\\"\\" data\\-sheets\\-userformat\\=\\"\\{\\" 2\\"\\:13249\\,\\"3\\"\\:\\[null\\,0\\]\\,\\"9\\"\\:0\\,\\"10\\"\\:1\\,\\"11\\"\\:4\\,\\"12\\"\\:0\\,\\"15\\"\\:\\"calibri\\"\\,\\"16\\"\\:12\\}\\"\\=\\"\\"\\>\\<style type\\=\\"text\\/css\\"\\>\\<\\!\\-\\-td \\{border\\: 1px solid \\#ccc\\;\\}br \\{mso\\-data\\-placement\\:same\\-cell\\;\\}\\-\\-\\>\\<\\/style\\>\\<span style\\=\\"font\\-size\\: 12pt\\;\\" data\\-sheets\\-value\\=\\"\\{\\" 1\\"\\:2\\,\\"2\\"\\:\\"sliding\\=\\"\\" scale\\=\\"\\" for\\=\\"\\" those\\=\\"\\" who\\=\\"\\" qualify\\"\\}\\"\\=\\"\\" data\\-sheets\\-userformat\\=\\"\\{\\" 2\\"\\:13249\\,\\"3\\"\\:\\[null\\,0\\]\\,\\"9\\"\\:0\\,\\"10\\"\\:1\\,\\"11\\"\\:4\\,\\"12\\"\\:0\\,\\"15\\"\\:\\"calibri\\"\\,\\"16\\"\\:12\\}\\"\\=\\"\\"\\>\\<font face\\=\\"Arial\\"\\>Sliding scale for those who qualify\\.\\<\\/font\\>\\<\\/span\\>\\<br\\>\\<\\/span\\>\\<\\/div\\>',
					'minor_access'   => null,
					'appointments'   => '\\<style type\\=\\"text\\/css\\"\\>\\<\\!\\-\\-td \\{border\\: 1px solid \\#ccc\\;\\}br \\{mso\\-data\\-placement\\:same\\-cell\\;\\}\\-\\-\\>\\<\\/style\\>\\<span style\\=\\"font\\-size\\: 12pt\\;\\" data\\-sheets\\-value\\=\\"\\{\\" 1\\"\\:2\\,\\"2\\"\\:\\"walk\\-ins\\=\\"\\" accepted\\"\\}\\"\\=\\"\\" data\\-sheets\\-userformat\\=\\"\\{\\" 2\\"\\:13249\\,\\"3\\"\\:\\[null\\,0\\]\\,\\"9\\"\\:0\\,\\"10\\"\\:1\\,\\"11\\"\\:4\\,\\"12\\"\\:0\\,\\"15\\"\\:\\"calibri\\"\\,\\"16\\"\\:12\\}\\"\\=\\"\\"\\>\\<font face\\=\\"Arial\\"\\>Walk\\-ins accepted\\.\\<\\/font\\>\\<\\/span\\>',
					'order'          => null,
					'notes'          => null,
					'created_at'     => '2017-08-25 13:18:56',
					'updated_at'     => '2017-08-28 16:29:30',
				],
			36 =>
				[
					'id'             => 37,
					'uid'            => null,
					'name'           => 'Lutheran Social Services of Illinois',
					'tagline'        => null,
					'organization'   => null,
					'address1'       => '1001 E\\. Touhy Avenue',
					'address2'       => 'Suite 50',
					'city'           => 'Des Plaines',
					'state_id'       => 13,
					'zipcode'        => '60018',
					'auto_gps'       => 1,
					'latitude'       => '42.0082509',
					'longitude'      => '-87.8973714',
					'url'            => '',
					'email'          => 'info\\@lssi\\.org',
					'website'        => 'http\\:\\/\\/www\\.lssi\\.org',
					'phone'          => '8476354600',
					'247_hotline'    => null,
					'text_line'      => null,
					'live_chat'      => '',
					'youth_approved' => 0,
					'hours'          => '\\<style type\\=\\"text\\/css\\"\\>\\<\\!\\-\\-td \\{border\\: 1px solid \\#ccc\\;\\}br \\{mso\\-data\\-placement\\:same\\-cell\\;\\}\\-\\-\\>\\<\\/style\\>\\<span style\\=\\"font\\-size\\: 12pt\\; font\\-family\\: Calibri\\, Arial\\;\\" data\\-sheets\\-value\\=\\"\\{\\" 1\\"\\:2\\,\\"2\\"\\:\\"mon\\-\\=\\"\\" fri\\:\\=\\"\\" 8\\:30\\=\\"\\" am\\=\\"\\" \\-\\=\\"\\" 4\\:30\\=\\"\\" pm\\"\\}\\"\\=\\"\\" data\\-sheets\\-userformat\\=\\"\\{\\" 2\\"\\:13251\\,\\"3\\"\\:\\[null\\,0\\]\\,\\"4\\"\\:\\[null\\,2\\,16777215\\]\\,\\"9\\"\\:0\\,\\"10\\"\\:1\\,\\"11\\"\\:4\\,\\"12\\"\\:0\\,\\"15\\"\\:\\"calibri\\"\\,\\"16\\"\\:12\\}\\"\\=\\"\\"\\>Mon\\-Fri\\: 8\\:30AM \\- 4\\:30PM\\<\\/span\\>',
					'image'          => null,
					'description'    => '\\<style type\\=\\"text\\/css\\"\\>\\<\\!\\-\\-td \\{border\\: 1px solid \\#ccc\\;\\}br \\{mso\\-data\\-placement\\:same\\-cell\\;\\}\\-\\-\\>\\<\\/style\\>\\<span style\\=\\"font\\-size\\: 12pt\\; font\\-family\\: Calibri\\, Arial\\;\\" data\\-sheets\\-value\\=\\"\\{\\"1\\"\\:2\\,\\"2\\"\\:\\"Post\\-Adoption Services\\, Community Child Care\\, Foster Care\\, Therapeutic Foster Care\\, Family Support Services\\, Residential Treatment Services for Youth\\, Adult Mental Health Treatment\\, Alcohol\\/Drug Treatment\\, Services for Adults with Developmental Disabilities\\, Senior Home Care Services\\, Retired Senior Volunteer Program\\, Affordable Senior Housing\\, Prisoner and Family Ministry\\: Visits to Mom\\, Storybook Project\\, and Reentry Services for Returning Citizens\\"\\}\\" data\\-sheets\\-userformat\\=\\"\\{\\"2\\"\\:13251\\,\\"3\\"\\:\\[null\\,0\\]\\,\\"4\\"\\:\\[null\\,2\\,16777215\\]\\,\\"9\\"\\:0\\,\\"10\\"\\:1\\,\\"11\\"\\:4\\,\\"12\\"\\:0\\,\\"15\\"\\:\\"Calibri\\"\\,\\"16\\"\\:12\\}\\"\\>Post\\-Adoption Services\\, Community Child Care\\, Foster Care\\, Therapeutic Foster Care\\, Family Support Services\\, Residential Treatment Services for Youth\\, Adult Mental Health Treatment\\, Alcohol\\/Drug Treatment\\, Services for Adults with Developmental Disabilities\\, Senior Home Care Services\\, Retired Senior Volunteer Program\\, Affordable Senior Housing\\, Prisoner and Family Ministry\\: Visits to Mom\\, Storybook Project\\, and Reentry Services for Returning Citizens\\.\\<\\/span\\>\\<div\\>\\<span style\\=\\"font\\-size\\: 12pt\\; font\\-family\\: Calibri\\, Arial\\;\\" data\\-sheets\\-value\\=\\"\\{\\"1\\"\\:2\\,\\"2\\"\\:\\"Post\\-Adoption Services\\, Community Child Care\\, Foster Care\\, Therapeutic Foster Care\\, Family Support Services\\, Residential Treatment Services for Youth\\, Adult Mental Health Treatment\\, Alcohol\\/Drug Treatment\\, Services for Adults with Developmental Disabilities\\, Senior Home Care Services\\, Retired Senior Volunteer Program\\, Affordable Senior Housing\\, Prisoner and Family Ministry\\: Visits to Mom\\, Storybook Project\\, and Reentry Services for Returning Citizens\\"\\}\\" data\\-sheets\\-userformat\\=\\"\\{\\"2\\"\\:13251\\,\\"3\\"\\:\\[null\\,0\\]\\,\\"4\\"\\:\\[null\\,2\\,16777215\\]\\,\\"9\\"\\:0\\,\\"10\\"\\:1\\,\\"11\\"\\:4\\,\\"12\\"\\:0\\,\\"15\\"\\:\\"Calibri\\"\\,\\"16\\"\\:12\\}\\"\\>\\<br\\>\\<\\/span\\>\\<\\/div\\>\\<div\\>\\<span style\\=\\"font\\-size\\: 12pt\\; font\\-family\\: Calibri\\, Arial\\;\\" data\\-sheets\\-value\\=\\"\\{\\"1\\"\\:2\\,\\"2\\"\\:\\"Post\\-Adoption Services\\, Community Child Care\\, Foster Care\\, Therapeutic Foster Care\\, Family Support Services\\, Residential Treatment Services for Youth\\, Adult Mental Health Treatment\\, Alcohol\\/Drug Treatment\\, Services for Adults with Developmental Disabilities\\, Senior Home Care Services\\, Retired Senior Volunteer Program\\, Affordable Senior Housing\\, Prisoner and Family Ministry\\: Visits to Mom\\, Storybook Project\\, and Reentry Services for Returning Citizens\\"\\}\\" data\\-sheets\\-userformat\\=\\"\\{\\"2\\"\\:13251\\,\\"3\\"\\:\\[null\\,0\\]\\,\\"4\\"\\:\\[null\\,2\\,16777215\\]\\,\\"9\\"\\:0\\,\\"10\\"\\:1\\,\\"11\\"\\:4\\,\\"12\\"\\:0\\,\\"15\\"\\:\\"Calibri\\"\\,\\"16\\"\\:12\\}\\"\\>\\<style type\\=\\"text\\/css\\"\\>\\<\\!\\-\\-td \\{border\\: 1px solid \\#ccc\\;\\}br \\{mso\\-data\\-placement\\:same\\-cell\\;\\}\\-\\-\\>\\<\\/style\\>\\<span style\\=\\"font\\-size\\: 12pt\\;\\" data\\-sheets\\-value\\=\\"\\{\\"1\\"\\:2\\,\\"2\\"\\:\\"Sliding scale for those who qualify\\"\\}\\" data\\-sheets\\-userformat\\=\\"\\{\\"2\\"\\:13251\\,\\"3\\"\\:\\[null\\,0\\]\\,\\"4\\"\\:\\[null\\,2\\,16777215\\]\\,\\"9\\"\\:0\\,\\"10\\"\\:1\\,\\"11\\"\\:4\\,\\"12\\"\\:0\\,\\"15\\"\\:\\"Calibri\\"\\,\\"16\\"\\:12\\}\\"\\>Sliding scale for those who qualify\\.\\<\\/span\\>\\<br\\>\\<\\/span\\>\\<\\/div\\>',
					'minor_access'   => null,
					'appointments'   => '\\<style type\\=\\"text\\/css\\"\\>\\<\\!\\-\\-td \\{border\\: 1px solid \\#ccc\\;\\}br \\{mso\\-data\\-placement\\:same\\-cell\\;\\}\\-\\-\\>\\<\\/style\\>\\<span style\\=\\"font\\-size\\: 12pt\\; font\\-family\\: Calibri\\, Arial\\;\\" data\\-sheets\\-value\\=\\"\\{\\"1\\"\\:2\\,\\"2\\"\\:\\"Patients advised to call ahead\\"\\}\\" data\\-sheets\\-userformat\\=\\"\\{\\"2\\"\\:13251\\,\\"3\\"\\:\\[null\\,0\\]\\,\\"4\\"\\:\\[null\\,2\\,16777215\\]\\,\\"9\\"\\:0\\,\\"10\\"\\:1\\,\\"11\\"\\:4\\,\\"12\\"\\:0\\,\\"15\\"\\:\\"Calibri\\"\\,\\"16\\"\\:12\\}\\"\\>Patients advised to call ahead\\.\\<\\/span\\>',
					'order'          => null,
					'notes'          => null,
					'created_at'     => '2017-08-25 13:24:56',
					'updated_at'     => '2017-08-29 10:21:09',
				],
			37 =>
				[
					'id'             => 38,
					'uid'            => null,
					'name'           => 'Fellowship Housing',
					'tagline'        => null,
					'organization'   => null,
					'address1'       => '2200 W\\. Higgins Road',
					'address2'       => 'Suite 130',
					'city'           => 'Hoffman Estates',
					'state_id'       => 13,
					'zipcode'        => '60169',
					'auto_gps'       => 1,
					'latitude'       => '42.0568650',
					'longitude'      => '-88.1345495',
					'url'            => '',
					'email'          => 'contact\\@fhcmoms\\.org',
					'website'        => 'http\\:\\/\\/www\\.fhcmoms\\.org',
					'phone'          => '8478822511',
					'247_hotline'    => null,
					'text_line'      => null,
					'live_chat'      => '',
					'youth_approved' => 0,
					'hours'          => '\\<style type\\=\\"text\\/css\\"\\>\\<\\!\\-\\-td \\{border\\: 1px solid \\#ccc\\;\\}br \\{mso\\-data\\-placement\\:same\\-cell\\;\\}\\-\\-\\>\\<\\/style\\>\\<span style\\=\\"font\\-size\\: 12pt\\; font\\-family\\: Calibri\\, Arial\\;\\" data\\-sheets\\-value\\=\\"\\{\\" 1\\"\\:2\\,\\"2\\"\\:\\"mon\\=\\"\\" \\-\\=\\"\\" fri\\:\\=\\"\\" 9\\=\\"\\" am\\=\\"\\" 5\\=\\"\\" pm\\"\\}\\"\\=\\"\\" data\\-sheets\\-userformat\\=\\"\\{\\" 2\\"\\:13249\\,\\"3\\"\\:\\[null\\,0\\]\\,\\"9\\"\\:0\\,\\"10\\"\\:1\\,\\"11\\"\\:4\\,\\"12\\"\\:0\\,\\"15\\"\\:\\"calibri\\"\\,\\"16\\"\\:12\\}\\"\\=\\"\\"\\>Mon\\-Fri\\: 9AM \\- 5PM\\<\\/span\\>',
					'image'          => null,
					'description'    => '\\<style type\\=\\"text\\/css\\"\\>\\<\\!\\-\\-td \\{border\\: 1px solid \\#ccc\\;\\}br \\{mso\\-data\\-placement\\:same\\-cell\\;\\}\\-\\-\\>\\<\\/style\\>\\<span style\\=\\"font\\-size\\: 12pt\\; font\\-family\\: Calibri\\, Arial\\;\\" data\\-sheets\\-value\\=\\"\\{\\"1\\"\\:2\\,\\"2\\"\\:\\"Housing for homeless mothers and their children \\"\\}\\" data\\-sheets\\-userformat\\=\\"\\{\\"2\\"\\:13249\\,\\"3\\"\\:\\[null\\,0\\]\\,\\"9\\"\\:0\\,\\"10\\"\\:1\\,\\"11\\"\\:4\\,\\"12\\"\\:0\\,\\"15\\"\\:\\"Calibri\\"\\,\\"16\\"\\:12\\}\\"\\>Housing for homeless mothers and their children\\.\\<\\/span\\>\\<div\\>\\<font face\\=\\"Calibri\\, Arial\\"\\>\\<br\\>\\<\\/font\\>\\<\\/div\\>\\<div\\>\\<style type\\=\\"text\\/css\\"\\>\\<\\!\\-\\-td \\{border\\: 1px solid \\#ccc\\;\\}br \\{mso\\-data\\-placement\\:same\\-cell\\;\\}\\-\\-\\>\\<\\/style\\>\\<span style\\=\\"font\\-size\\: 12pt\\; font\\-family\\: Calibri\\, Arial\\;\\" data\\-sheets\\-value\\=\\"\\{\\"1\\"\\:2\\,\\"2\\"\\:\\"Sliding scale for those who qualify\\"\\}\\" data\\-sheets\\-userformat\\=\\"\\{\\"2\\"\\:13249\\,\\"3\\"\\:\\[null\\,0\\]\\,\\"9\\"\\:0\\,\\"10\\"\\:1\\,\\"11\\"\\:4\\,\\"12\\"\\:0\\,\\"15\\"\\:\\"Calibri\\"\\,\\"16\\"\\:12\\}\\"\\>Sliding scale for those who qualify\\.\\<\\/span\\>\\<font face\\=\\"Calibri\\, Arial\\"\\>\\<br\\>\\<\\/font\\>\\<div\\>\\<span style\\=\\"font\\-size\\: 12pt\\; font\\-family\\: Calibri\\, Arial\\;\\" data\\-sheets\\-value\\=\\"\\{\\"1\\"\\:2\\,\\"2\\"\\:\\"Housing for homeless mothers and their children \\"\\}\\" data\\-sheets\\-userformat\\=\\"\\{\\"2\\"\\:13249\\,\\"3\\"\\:\\[null\\,0\\]\\,\\"9\\"\\:0\\,\\"10\\"\\:1\\,\\"11\\"\\:4\\,\\"12\\"\\:0\\,\\"15\\"\\:\\"Calibri\\"\\,\\"16\\"\\:12\\}\\"\\>\\<br\\>\\<\\/span\\>\\<\\/div\\>\\<div\\>\\<span style\\=\\"font\\-size\\: 12pt\\; font\\-family\\: Calibri\\, Arial\\;\\" data\\-sheets\\-value\\=\\"\\{\\"1\\"\\:2\\,\\"2\\"\\:\\"Housing for homeless mothers and their children \\"\\}\\" data\\-sheets\\-userformat\\=\\"\\{\\"2\\"\\:13249\\,\\"3\\"\\:\\[null\\,0\\]\\,\\"9\\"\\:0\\,\\"10\\"\\:1\\,\\"11\\"\\:4\\,\\"12\\"\\:0\\,\\"15\\"\\:\\"Calibri\\"\\,\\"16\\"\\:12\\}\\"\\>\\<\\/span\\>\\<\\/div\\>\\<\\/div\\>',
					'minor_access'   => null,
					'appointments'   => '\\<style type\\=\\"text\\/css\\"\\>\\<\\!\\-\\-td \\{border\\: 1px solid \\#ccc\\;\\}br \\{mso\\-data\\-placement\\:same\\-cell\\;\\}\\-\\-\\>\\<\\/style\\>\\<span style\\=\\"font\\-size\\: 12pt\\; font\\-family\\: Calibri\\, Arial\\;\\" data\\-sheets\\-value\\=\\"\\{\\"1\\"\\:2\\,\\"2\\"\\:\\"Patients advised to call ahead\\"\\}\\" data\\-sheets\\-userformat\\=\\"\\{\\"2\\"\\:13249\\,\\"3\\"\\:\\[null\\,0\\]\\,\\"9\\"\\:0\\,\\"10\\"\\:1\\,\\"11\\"\\:4\\,\\"12\\"\\:0\\,\\"15\\"\\:\\"Calibri\\"\\,\\"16\\"\\:12\\}\\"\\>Patients advised to call ahead\\.\\<\\/span\\>',
					'order'          => null,
					'notes'          => null,
					'created_at'     => '2017-08-25 13:34:03',
					'updated_at'     => '2017-08-29 09:28:15',
				],
			38 =>
				[
					'id'             => 39,
					'uid'            => null,
					'name'           => 'National Center for Missing and Exploited Children\\: Team HOPE',
					'tagline'        => null,
					'organization'   => null,
					'address1'       => '699 Prince Street',
					'address2'       => null,
					'city'           => 'Alexandria',
					'state_id'       => 46,
					'zipcode'        => '22314',
					'auto_gps'       => 1,
					'latitude'       => '38.8040069',
					'longitude'      => '-77.0467670',
					'url'            => '',
					'email'          => '',
					'website'        => 'http\\:\\/\\/www\\.missingkids\\.com\\/teamhope',
					'phone'          => '7032242150',
					'247_hotline'    => '8008435678',
					'text_line'      => null,
					'live_chat'      => '',
					'youth_approved' => 0,
					'hours'          => '\\<font face\\=\\"Arial\\"\\>24\\/7\\<\\/font\\>',
					'image'          => null,
					'description'    => '\\<div\\>\\<font face\\=\\"Arial\\"\\>\\<span style\\=\\"background\\-color\\: rgb\\(255\\, 255\\, 255\\)\\;\\"\\>Help Families with missing children services \\&amp\\; child sexual exploitation\\, child safety \\&amp\\; prevention services\\, law enforcement training\\, victim \\&amp\\; family support\\, c\\<\\/span\\>\\<span style\\=\\"white\\-space\\: pre\\-wrap\\; background\\-color\\: rgb\\(255\\, 255\\, 255\\)\\;\\"\\>risis management\\, peer support\\, emotional support\\, coping tools\\, and resources for parents\\.\\<\\/span\\>\\<\\/font\\>\\<\\/div\\>\\<div\\>\\<font face\\=\\"Arial\\"\\>\\<span style\\=\\"white\\-space\\: pre\\-wrap\\; background\\-color\\: rgb\\(255\\, 255\\, 255\\)\\;\\"\\>\\<br\\>\\<\\/span\\>\\<\\/font\\>\\<\\/div\\>\\<div\\>\\<font face\\=\\"Arial\\"\\>Use their Cyber Tip Line to report suspected child sexual exploitation at\\: https\\:\\/\\/report\\.cybertip\\.org\\.\\<span style\\=\\"white\\-space\\: pre\\-wrap\\; background\\-color\\: rgb\\(255\\, 255\\, 255\\)\\;\\"\\>\\<br\\>\\<\\/span\\>\\<\\/font\\>\\<\\/div\\>\\<div\\>\\<font face\\=\\"Arimo\\, sans\\-serif\\" color\\=\\"\\#ffffff\\"\\>\\<span style\\=\\"font\\-size\\: 15px\\;\\"\\>\\<br\\>\\<\\/span\\>\\<\\/font\\>\\<\\/div\\>\\<div\\>\\<font color\\=\\"\\#0c2c43\\" face\\=\\"Arimo\\, sans\\-serif\\"\\>\\<span style\\=\\"font\\-size\\: 15px\\;\\"\\>\\<br\\>\\<\\/span\\>\\<\\/font\\>\\<\\/div\\>\\<ul style\\=\\"margin\\: 0px 0px 0px 10px\\; padding\\: 0px\\; border\\: 0px\\; outline\\: 0px\\; font\\-size\\: 1\\.5rem\\; vertical\\-align\\: baseline\\; font\\-family\\: Arimo\\, sans\\-serif\\; color\\: rgb\\(12\\, 44\\, 67\\)\\; line\\-height\\: 1\\.5em\\; list\\-style\\: none\\;\\"\\>\\<li style\\=\\"margin\\: 10px 0px\\; padding\\: 0px\\; border\\: 0px\\; outline\\: 0px\\; font\\-size\\: 15px\\; vertical\\-align\\: baseline\\; list\\-style\\-type\\: none\\; line\\-height\\: 1\\;\\"\\>\\<\\/li\\>\\<\\/ul\\>',
					'minor_access'   => null,
					'appointments'   => null,
					'order'          => null,
					'notes'          => null,
					'created_at'     => '2017-08-25 13:35:50',
					'updated_at'     => '2017-08-29 10:36:17',
				],
			39 =>
				[
					'id'             => 40,
					'uid'            => null,
					'name'           => 'Salvation Army Family Services \\- Des Plaines Corps Community Center',
					'tagline'        => null,
					'organization'   => null,
					'address1'       => '609 W\\. Dempster Street',
					'address2'       => null,
					'city'           => 'Des Plaines',
					'state_id'       => 13,
					'zipcode'        => '60016',
					'auto_gps'       => 1,
					'latitude'       => '42.0369818',
					'longitude'      => '-87.9353516',
					'url'            => '',
					'email'          => 'david\\_martinez\\@usc\\.salvationarmy\\.org\\; shannon\\_martinez\\@usc\\.salvationarmy\\.org',
					'website'        => 'http\\:\\/\\/www\\.salarmydesplaines\\.org',
					'phone'          => '8479819111',
					'247_hotline'    => null,
					'text_line'      => null,
					'live_chat'      => '',
					'youth_approved' => 0,
					'hours'          => '\\<style type\\=\\"text\\/css\\"\\>\\<\\!\\-\\-td \\{border\\: 1px solid \\#ccc\\;\\}br \\{mso\\-data\\-placement\\:same\\-cell\\;\\}\\-\\-\\>\\<\\/style\\>\\<span data\\-sheets\\-value\\=\\"\\{\\" 1\\"\\:2\\,\\"2\\"\\:\\"mon\\=\\"\\" \\-\\=\\"\\" thu\\:\\=\\"\\" 10\\=\\"\\" am\\=\\"\\" 6\\=\\"\\" pm\\=\\"\\" fri\\=\\"\\" sat\\:\\=\\"\\" 9\\=\\"\\" sun\\:\\=\\"\\" closed\\"\\}\\"\\=\\"\\" data\\-sheets\\-userformat\\=\\"\\{\\" 2\\"\\:13249\\,\\"3\\"\\:\\[null\\,0\\]\\,\\"9\\"\\:0\\,\\"10\\"\\:1\\,\\"11\\"\\:4\\,\\"12\\"\\:0\\,\\"15\\"\\:\\"calibri\\"\\,\\"16\\"\\:12\\}\\"\\=\\"\\"\\>\\<font face\\=\\"Arial\\"\\>Mon\\-Thu\\: 10AM \\- 6PM\\<\\/font\\>\\<\\/span\\>\\<div\\>\\<span data\\-sheets\\-value\\=\\"\\{\\" 1\\"\\:2\\,\\"2\\"\\:\\"mon\\=\\"\\" \\-\\=\\"\\" thu\\:\\=\\"\\" 10\\=\\"\\" am\\=\\"\\" 6\\=\\"\\" pm\\=\\"\\" fri\\=\\"\\" sat\\:\\=\\"\\" 9\\=\\"\\" sun\\:\\=\\"\\" closed\\"\\}\\"\\=\\"\\" data\\-sheets\\-userformat\\=\\"\\{\\" 2\\"\\:13249\\,\\"3\\"\\:\\[null\\,0\\]\\,\\"9\\"\\:0\\,\\"10\\"\\:1\\,\\"11\\"\\:4\\,\\"12\\"\\:0\\,\\"15\\"\\:\\"calibri\\"\\,\\"16\\"\\:12\\}\\"\\=\\"\\"\\>\\<font face\\=\\"Arial\\"\\>Fri \\- Sat\\: 10AM \\- 9PM\\<\\/font\\>\\<\\/span\\>\\<\\/div\\>\\<div\\>\\<span data\\-sheets\\-value\\=\\"\\{\\" 1\\"\\:2\\,\\"2\\"\\:\\"mon\\=\\"\\" \\-\\=\\"\\" thu\\:\\=\\"\\" 10\\=\\"\\" am\\=\\"\\" 6\\=\\"\\" pm\\=\\"\\" fri\\=\\"\\" sat\\:\\=\\"\\" 9\\=\\"\\" sun\\:\\=\\"\\" closed\\"\\}\\"\\=\\"\\" data\\-sheets\\-userformat\\=\\"\\{\\" 2\\"\\:13249\\,\\"3\\"\\:\\[null\\,0\\]\\,\\"9\\"\\:0\\,\\"10\\"\\:1\\,\\"11\\"\\:4\\,\\"12\\"\\:0\\,\\"15\\"\\:\\"calibri\\"\\,\\"16\\"\\:12\\}\\"\\=\\"\\"\\>\\<font face\\=\\"Arial\\"\\>Sun\\: Closed\\<\\/font\\>\\<\\/span\\>\\<\\/div\\>',
					'image'          => null,
					'description'    => '\\<style type\\=\\"text\\/css\\"\\>\\<\\!\\-\\-td \\{border\\: 1px solid \\#ccc\\;\\}br \\{mso\\-data\\-placement\\:same\\-cell\\;\\}\\-\\-\\>\\<\\/style\\>\\<span style\\=\\"font\\-size\\: 12pt\\; font\\-family\\: Calibri\\, Arial\\;\\" data\\-sheets\\-value\\=\\"\\{\\"1\\"\\:2\\,\\"2\\"\\:\\"Community Recreation \\& Education Programs\\, Domestic Violence Services\\, Emergency Financial Assistance\\, Food \\& Nutrition Programs\\, Casework Services\\"\\}\\" data\\-sheets\\-userformat\\=\\"\\{\\"2\\"\\:13249\\,\\"3\\"\\:\\[null\\,0\\]\\,\\"9\\"\\:0\\,\\"10\\"\\:1\\,\\"11\\"\\:4\\,\\"12\\"\\:0\\,\\"15\\"\\:\\"Calibri\\"\\,\\"16\\"\\:12\\}\\"\\>Community Recreation \\& Education Programs\\, Domestic Violence Services\\, Emergency Financial Assistance\\, Food \\& Nutrition Programs\\, Casework Services\\.\\<\\/span\\>',
					'minor_access'   => null,
					'appointments'   => '\\<style type\\=\\"text\\/css\\"\\>\\<\\!\\-\\-td \\{border\\: 1px solid \\#ccc\\;\\}br \\{mso\\-data\\-placement\\:same\\-cell\\;\\}\\-\\-\\>\\<\\/style\\>\\<span data\\-sheets\\-value\\=\\"\\{\\" 1\\"\\:2\\,\\"2\\"\\:\\"patients\\=\\"\\" advised\\=\\"\\" to\\=\\"\\" call\\=\\"\\" ahead\\"\\}\\"\\=\\"\\" data\\-sheets\\-userformat\\=\\"\\{\\" 2\\"\\:13249\\,\\"3\\"\\:\\[null\\,0\\]\\,\\"9\\"\\:0\\,\\"10\\"\\:1\\,\\"11\\"\\:4\\,\\"12\\"\\:0\\,\\"15\\"\\:\\"calibri\\"\\,\\"16\\"\\:12\\}\\"\\=\\"\\"\\>\\<font face\\=\\"Arial\\"\\>Patients advised to call ahead\\.\\<\\/font\\>\\<\\/span\\>',
					'order'          => null,
					'notes'          => null,
					'created_at'     => '2017-08-25 13:41:52',
					'updated_at'     => '2017-08-29 11:13:45',
				],
			41 =>
				[
					'id'             => 52,
					'uid'            => null,
					'name'           => 'Waterleaf Women\\\'s Center',
					'tagline'        => null,
					'organization'   => null,
					'address1'       => '2849 E\\. New York Street',
					'address2'       => null,
					'city'           => 'Aurora',
					'state_id'       => 13,
					'zipcode'        => '60502',
					'auto_gps'       => 1,
					'latitude'       => '41.7586071',
					'longitude'      => '-88.2450268',
					'url'            => '',
					'email'          => '',
					'website'        => 'http\\:\\/\\/www\\.waterleafwc\\.org',
					'phone'          => '6307016270',
					'247_hotline'    => '8889893435',
					'text_line'      => '6303602256',
					'live_chat'      => '',
					'youth_approved' => 0,
					'hours'          => '\\<span style\\=\\"font\\-family\\: Arial\\; white\\-space\\: pre\\-wrap\\; background\\-color\\: rgb\\(255\\, 255\\, 255\\)\\;\\"\\>Mon\\: 8AM \\- 4PM\\<\\/span\\>\\<div\\>\\<span style\\=\\"font\\-family\\: Arial\\; white\\-space\\: pre\\-wrap\\; background\\-color\\: rgb\\(255\\, 255\\, 255\\)\\;\\"\\>Tue\\: 9AM \\- 4PM\\<\\/span\\>\\<\\/div\\>\\<div\\>\\<span style\\=\\"font\\-family\\: Arial\\; white\\-space\\: pre\\-wrap\\; background\\-color\\: rgb\\(255\\, 255\\, 255\\)\\;\\"\\>Wed\\: 8AM \\- 4PM\\<\\/span\\>\\<\\/div\\>\\<div\\>\\<span style\\=\\"font\\-family\\: Arial\\; white\\-space\\: pre\\-wrap\\; background\\-color\\: rgb\\(255\\, 255\\, 255\\)\\;\\"\\>Thu\\: 10AM \\- 7PM\\<\\/span\\>\\<\\/div\\>\\<div\\>\\<span style\\=\\"font\\-family\\: Arial\\; white\\-space\\: pre\\-wrap\\; background\\-color\\: rgb\\(255\\, 255\\, 255\\)\\;\\"\\>Fri\\: 7AM \\- 3PM\\<\\/span\\>\\<\\/div\\>\\<div\\>\\<span style\\=\\"font\\-family\\: Arial\\; white\\-space\\: pre\\-wrap\\; background\\-color\\: rgb\\(255\\, 255\\, 255\\)\\;\\"\\>Sat\\: 7AM \\- 12PM\\<\\/span\\>\\<\\/div\\>',
					'image'          => null,
					'description'    => '\\<span style\\=\\"font\\-family\\: Arial\\; white\\-space\\: pre\\-wrap\\; background\\-color\\: rgb\\(255\\, 255\\, 255\\)\\;\\"\\>Pregnancy Testing\\, Ultrasound\\, Medical Consultation\\, Pregnancy Consultation\\, Abortion\\, Adoption \\&amp\\; Parenting Information\\, STD\\/STI Education\\, STD\\/STI Testing\\, STD\\/STI Treatment\\, Community Resources\\, Community Referrals\\, Furthering Education Assistance\\, Employment Assistance\\, Mentor Support\\, Group Classes\\<\\/span\\>\\<div\\>\\<span style\\=\\"font\\-family\\: Arial\\; white\\-space\\: pre\\-wrap\\; background\\-color\\: rgb\\(255\\, 255\\, 255\\)\\;\\"\\>\\<br\\>\\<\\/span\\>\\<\\/div\\>\\<div\\>\\<span style\\=\\"font\\-family\\: Arial\\; white\\-space\\: pre\\-wrap\\; background\\-color\\: rgb\\(255\\, 255\\, 255\\)\\;\\"\\>All services are free\\.\\<\\/span\\>\\<\\/div\\>',
					'minor_access'   => '\\<span style\\=\\"font\\-family\\: Arial\\; white\\-space\\: pre\\-wrap\\; background\\-color\\: rgb\\(255\\, 255\\, 255\\)\\;\\"\\>All services provided are free and confidential\\<\\/span\\>\\<br\\>',
					'appointments'   => '\\<span style\\=\\"font\\-family\\: Arial\\; white\\-space\\: pre\\-wrap\\; background\\-color\\: rgb\\(255\\, 255\\, 255\\)\\;\\"\\>Same day appointments available \\&amp\\; walk\\-ins accepted \\- Patients advised to call ahead\\<\\/span\\>\\<br\\>',
					'order'          => null,
					'notes'          => null,
					'created_at'     => '2017-08-25 14:35:11',
					'updated_at'     => '2017-08-29 11:31:43',
				],
			43 =>
				[
					'id'             => 44,
					'uid'            => null,
					'name'           => 'Centro de Informacion',
					'tagline'        => null,
					'organization'   => null,
					'address1'       => '2380 Glendale Terrace',
					'address2'       => 'Apt\\. 8',
					'city'           => 'Hanover Park',
					'state_id'       => 13,
					'zipcode'        => '60133',
					'auto_gps'       => 1,
					'latitude'       => '41.9943476',
					'longitude'      => '-88.1637335',
					'url'            => '',
					'email'          => '',
					'website'        => 'http\\:\\/\\/www\\.centrodeinformacion\\.org',
					'phone'          => '6305505131',
					'247_hotline'    => null,
					'text_line'      => null,
					'live_chat'      => '',
					'youth_approved' => 0,
					'hours'          => '\\<style type\\=\\"text\\/css\\"\\>\\<\\!\\-\\-td \\{border\\: 1px solid \\#ccc\\;\\}br \\{mso\\-data\\-placement\\:same\\-cell\\;\\}\\-\\-\\>\\<\\/style\\>\\<span style\\=\\"font\\-family\\: Arial\\;\\" data\\-sheets\\-value\\=\\"\\{\\" 1\\"\\:2\\,\\"2\\"\\:\\"tue\\,\\=\\"\\" wed\\,\\=\\"\\" thu\\:\\=\\"\\" 10am\\-6pm\\,\\=\\"\\" fri\\:\\=\\"\\" 9am\\-5pm\\"\\}\\"\\=\\"\\" data\\-sheets\\-userformat\\=\\"\\{\\" 2\\"\\:13185\\,\\"3\\"\\:\\[null\\,0\\]\\,\\"10\\"\\:2\\,\\"11\\"\\:4\\,\\"12\\"\\:0\\,\\"15\\"\\:\\"arial\\,sans\\,sans\\-serif\\"\\,\\"16\\"\\:11\\}\\"\\=\\"\\"\\>Tue\\, Wed\\, Thu\\: 10AM \\- 6PM\\<\\/span\\>\\<div\\>\\<span style\\=\\"font\\-family\\: Arial\\;\\" data\\-sheets\\-value\\=\\"\\{\\" 1\\"\\:2\\,\\"2\\"\\:\\"tue\\,\\=\\"\\" wed\\,\\=\\"\\" thu\\:\\=\\"\\" 10am\\-6pm\\,\\=\\"\\" fri\\:\\=\\"\\" 9am\\-5pm\\"\\}\\"\\=\\"\\" data\\-sheets\\-userformat\\=\\"\\{\\" 2\\"\\:13185\\,\\"3\\"\\:\\[null\\,0\\]\\,\\"10\\"\\:2\\,\\"11\\"\\:4\\,\\"12\\"\\:0\\,\\"15\\"\\:\\"arial\\,sans\\,sans\\-serif\\"\\,\\"16\\"\\:11\\}\\"\\=\\"\\"\\>Fri\\: 9AM \\- 5PM\\<\\/span\\>\\<\\/div\\>',
					'image'          => null,
					'description'    => '\\<style type\\=\\"text\\/css\\"\\>\\<\\!\\-\\-td \\{border\\: 1px solid \\#ccc\\;\\}br \\{mso\\-data\\-placement\\:same\\-cell\\;\\}\\-\\-\\>\\<\\/style\\>\\<span style\\=\\"font\\-family\\: Arial\\;\\" data\\-sheets\\-value\\=\\"\\{\\" 1\\"\\:2\\,\\"2\\"\\:\\"emergency\\=\\"\\" food\\=\\"\\" pantry\\,\\=\\"\\" computer\\=\\"\\" access\\,\\=\\"\\" baby\\=\\"\\" clothes\\,\\=\\"\\" general\\=\\"\\" information\\,\\=\\"\\" referrals\\=\\"\\" to\\=\\"\\" community\\=\\"\\" resources\\"\\}\\"\\=\\"\\" data\\-sheets\\-userformat\\=\\"\\{\\" 2\\"\\:13187\\,\\"3\\"\\:\\[null\\,0\\]\\,\\"4\\"\\:\\[null\\,2\\,16573901\\]\\,\\"10\\"\\:2\\,\\"11\\"\\:4\\,\\"12\\"\\:0\\,\\"15\\"\\:\\"arial\\"\\,\\"16\\"\\:11\\}\\"\\=\\"\\"\\>Emergency Food Pantry\\, Computer Access\\, Baby Clothes\\, General Information\\, Referrals to Community Resources\\.\\<\\/span\\>\\<div\\>\\<span style\\=\\"font\\-family\\: Arial\\;\\" data\\-sheets\\-value\\=\\"\\{\\" 1\\"\\:2\\,\\"2\\"\\:\\"emergency\\=\\"\\" food\\=\\"\\" pantry\\,\\=\\"\\" computer\\=\\"\\" access\\,\\=\\"\\" baby\\=\\"\\" clothes\\,\\=\\"\\" general\\=\\"\\" information\\,\\=\\"\\" referrals\\=\\"\\" to\\=\\"\\" community\\=\\"\\" resources\\"\\}\\"\\=\\"\\" data\\-sheets\\-userformat\\=\\"\\{\\" 2\\"\\:13187\\,\\"3\\"\\:\\[null\\,0\\]\\,\\"4\\"\\:\\[null\\,2\\,16573901\\]\\,\\"10\\"\\:2\\,\\"11\\"\\:4\\,\\"12\\"\\:0\\,\\"15\\"\\:\\"arial\\"\\,\\"16\\"\\:11\\}\\"\\=\\"\\"\\>\\<br\\>\\<\\/span\\>\\<\\/div\\>\\<div\\>\\<span style\\=\\"font\\-family\\: Arial\\;\\" data\\-sheets\\-value\\=\\"\\{\\" 1\\"\\:2\\,\\"2\\"\\:\\"emergency\\=\\"\\" food\\=\\"\\" pantry\\,\\=\\"\\" computer\\=\\"\\" access\\,\\=\\"\\" baby\\=\\"\\" clothes\\,\\=\\"\\" general\\=\\"\\" information\\,\\=\\"\\" referrals\\=\\"\\" to\\=\\"\\" community\\=\\"\\" resources\\"\\}\\"\\=\\"\\" data\\-sheets\\-userformat\\=\\"\\{\\" 2\\"\\:13187\\,\\"3\\"\\:\\[null\\,0\\]\\,\\"4\\"\\:\\[null\\,2\\,16573901\\]\\,\\"10\\"\\:2\\,\\"11\\"\\:4\\,\\"12\\"\\:0\\,\\"15\\"\\:\\"arial\\"\\,\\"16\\"\\:11\\}\\"\\=\\"\\"\\>\\<style type\\=\\"text\\/css\\"\\>\\<\\!\\-\\-td \\{border\\: 1px solid \\#ccc\\;\\}br \\{mso\\-data\\-placement\\:same\\-cell\\;\\}\\-\\-\\>\\<\\/style\\>\\<span data\\-sheets\\-value\\=\\"\\{\\" 1\\"\\:2\\,\\"2\\"\\:\\"all\\=\\"\\" services\\=\\"\\" are\\=\\"\\" free\\"\\}\\"\\=\\"\\" data\\-sheets\\-userformat\\=\\"\\{\\" 2\\"\\:13187\\,\\"3\\"\\:\\[null\\,0\\]\\,\\"4\\"\\:\\[null\\,2\\,16573901\\]\\,\\"10\\"\\:2\\,\\"11\\"\\:4\\,\\"12\\"\\:0\\,\\"15\\"\\:\\"arial\\,sans\\,sans\\-serif\\"\\,\\"16\\"\\:11\\}\\"\\=\\"\\"\\>All services are free\\.\\<\\/span\\>\\<br\\>\\<\\/span\\>\\<\\/div\\>',
					'minor_access'   => '\\<style type\\=\\"text\\/css\\"\\>\\<\\!\\-\\-td \\{border\\: 1px solid \\#ccc\\;\\}br \\{mso\\-data\\-placement\\:same\\-cell\\;\\}\\-\\-\\>\\<\\/style\\>\\<span style\\=\\"font\\-family\\: Arial\\;\\" data\\-sheets\\-value\\=\\"\\{\\" 1\\"\\:2\\,\\"2\\"\\:\\"all\\=\\"\\" services\\=\\"\\" provided\\=\\"\\" are\\=\\"\\" free\\=\\"\\" and\\=\\"\\" confidential\\"\\}\\"\\=\\"\\" data\\-sheets\\-userformat\\=\\"\\{\\" 2\\"\\:13185\\,\\"3\\"\\:\\[null\\,0\\]\\,\\"10\\"\\:2\\,\\"11\\"\\:4\\,\\"12\\"\\:0\\,\\"15\\"\\:\\"arial\\,sans\\,sans\\-serif\\"\\,\\"16\\"\\:11\\}\\"\\=\\"\\"\\>All services provided are free and confidential\\.\\<\\/span\\>',
					'appointments'   => '\\<style type\\=\\"text\\/css\\"\\>\\<\\!\\-\\-td \\{border\\: 1px solid \\#ccc\\;\\}br \\{mso\\-data\\-placement\\:same\\-cell\\;\\}\\-\\-\\>\\<\\/style\\>\\<span style\\=\\"font\\-family\\: Arial\\;\\" data\\-sheets\\-value\\=\\"\\{\\" 1\\"\\:2\\,\\"2\\"\\:\\"same\\=\\"\\" day\\=\\"\\" appointments\\=\\"\\" available\\=\\"\\" \\&\\=\\"\\" walk\\-ins\\=\\"\\" accepted\\"\\}\\"\\=\\"\\" data\\-sheets\\-userformat\\=\\"\\{\\" 2\\"\\:13185\\,\\"3\\"\\:\\[null\\,0\\]\\,\\"10\\"\\:2\\,\\"11\\"\\:4\\,\\"12\\"\\:0\\,\\"15\\"\\:\\"arial\\,sans\\,sans\\-serif\\"\\,\\"16\\"\\:11\\}\\"\\=\\"\\"\\>Same day appointments available \\& walk\\-ins accepted\\.\\<\\/span\\>',
					'order'          => null,
					'notes'          => null,
					'created_at'     => '2017-08-25 13:52:24',
					'updated_at'     => '2017-08-28 16:31:56',
				],
			45 =>
				[
					'id'             => 46,
					'uid'            => null,
					'name'           => 'RENZ Center',
					'tagline'        => null,
					'organization'   => null,
					'address1'       => '2 American Way',
					'address2'       => null,
					'city'           => 'Elgin',
					'state_id'       => 13,
					'zipcode'        => '60120',
					'auto_gps'       => 1,
					'latitude'       => '42.0455370',
					'longitude'      => '-88.2566354',
					'url'            => '',
					'email'          => '',
					'website'        => 'http\\:\\/\\/www\\.renzcenter\\.org',
					'phone'          => '8477423545',
					'247_hotline'    => null,
					'text_line'      => null,
					'live_chat'      => '',
					'youth_approved' => 1,
					'hours'          => '\\<span style\\=\\"font\\-family\\: Arial\\; white\\-space\\: pre\\-wrap\\; background\\-color\\: rgb\\(255\\, 255\\, 255\\)\\;\\"\\>Mon\\-Thu\\: 9AM \\- 9PM\\<\\/span\\>\\<div\\>\\<span style\\=\\"font\\-family\\: Arial\\; white\\-space\\: pre\\-wrap\\; background\\-color\\: rgb\\(255\\, 255\\, 255\\)\\;\\"\\>Fri\\: 9AM \\- 12PM\\<\\/span\\>\\<\\/div\\>',
					'image'          => null,
					'description'    => '\\<span style\\=\\"font\\-family\\: Arial\\; white\\-space\\: pre\\-wrap\\; background\\-color\\: rgb\\(255\\, 255\\, 255\\)\\;\\"\\>HIV\\/AIDS Prevention\\, HIV\\/AIDS Testing\\, Partner Surveillance\\, HIV Support Programs\\, Transgender Support Programs \\<\\/span\\>\\<div\\>\\<span style\\=\\"font\\-family\\: Arial\\; white\\-space\\: pre\\-wrap\\; background\\-color\\: rgb\\(255\\, 255\\, 255\\)\\;\\"\\>\\<br\\>\\<\\/span\\>\\<\\/div\\>\\<div\\>\\<font face\\=\\"Arial\\"\\>\\<span style\\=\\"white\\-space\\: pre\\-wrap\\; background\\-color\\: rgb\\(255\\, 255\\, 255\\)\\;\\"\\>Sliding scale for those who qualify\\.\\<\\/span\\>\\<\\/font\\>\\<\\/div\\>',
					'minor_access'   => '\\<span style\\=\\"font\\-family\\: Arial\\; white\\-space\\: pre\\-wrap\\; background\\-color\\: rgb\\(255\\, 255\\, 255\\)\\;\\"\\>Parent permission not required\\.\\<\\/span\\>',
					'appointments'   => '\\<font face\\=\\"Arial\\"\\>Patients advised to call ahead\\.\\<\\/font\\>',
					'order'          => null,
					'notes'          => null,
					'created_at'     => '2017-08-25 14:00:35',
					'updated_at'     => '2017-08-29 11:06:59',
				],
			46 =>
				[
					'id'             => 61,
					'uid'            => null,
					'name'           => 'Life Span',
					'tagline'        => null,
					'organization'   => null,
					'address1'       => '701 Lee Street \\#700',
					'address2'       => null,
					'city'           => 'Des Plaines',
					'state_id'       => 13,
					'zipcode'        => '60016',
					'auto_gps'       => 1,
					'latitude'       => '42.0403438',
					'longitude'      => '-87.8883466',
					'url'            => '',
					'email'          => '',
					'website'        => 'http\\:\\/\\/www\\.life\\-span\\.org',
					'phone'          => '8478240382',
					'247_hotline'    => null,
					'text_line'      => null,
					'live_chat'      => '',
					'youth_approved' => 0,
					'hours'          => '\\<style type\\=\\"text\\/css\\"\\>\\<\\!\\-\\-td \\{border\\: 1px solid \\#ccc\\;\\}br \\{mso\\-data\\-placement\\:same\\-cell\\;\\}\\-\\-\\>\\<\\/style\\>\\<span style\\=\\"font\\-family\\: Arial\\;\\" data\\-sheets\\-value\\=\\"\\{\\" 1\\"\\:2\\,\\"2\\"\\:\\"mon\\-fri\\:\\=\\"\\" 9am\\-5pm\\"\\}\\"\\=\\"\\" data\\-sheets\\-userformat\\=\\"\\{\\" 2\\"\\:13185\\,\\"3\\"\\:\\[null\\,0\\]\\,\\"10\\"\\:2\\,\\"11\\"\\:4\\,\\"12\\"\\:0\\,\\"15\\"\\:\\"arial\\,sans\\,sans\\-serif\\"\\,\\"16\\"\\:11\\}\\"\\=\\"\\"\\>Mon\\-Fri\\: 9AM \\- 5PM\\<\\/span\\>',
					'image'          => null,
					'description'    => '\\<style type\\=\\"text\\/css\\"\\>\\<\\!\\-\\-td \\{border\\: 1px solid \\#ccc\\;\\}br \\{mso\\-data\\-placement\\:same\\-cell\\;\\}\\-\\-\\>\\<\\/style\\>\\<span style\\=\\"font\\-family\\: Arial\\;\\" data\\-sheets\\-value\\=\\"\\{\\" 1\\"\\:2\\,\\"2\\"\\:\\"legal\\=\\"\\" services\\,\\=\\"\\" teen\\=\\"\\" counseling\\,\\=\\"\\" classroom\\=\\"\\" workshops\\,\\=\\"\\" crisis\\=\\"\\" intervention\\,\\=\\"\\" trauma\\=\\"\\" counseling\\"\\}\\"\\=\\"\\" data\\-sheets\\-userformat\\=\\"\\{\\" 2\\"\\:13187\\,\\"3\\"\\:\\[null\\,0\\]\\,\\"4\\"\\:\\[null\\,2\\,16573901\\]\\,\\"10\\"\\:2\\,\\"11\\"\\:4\\,\\"12\\"\\:0\\,\\"15\\"\\:\\"arial\\"\\,\\"16\\"\\:11\\}\\"\\=\\"\\"\\>Legal Services\\, Teen Counseling\\, Classroom Workshops\\, Crisis Intervention\\, Trauma Counseling\\.\\<\\/span\\>\\<div\\>\\<span style\\=\\"font\\-family\\: Arial\\;\\" data\\-sheets\\-value\\=\\"\\{\\" 1\\"\\:2\\,\\"2\\"\\:\\"legal\\=\\"\\" services\\,\\=\\"\\" teen\\=\\"\\" counseling\\,\\=\\"\\" classroom\\=\\"\\" workshops\\,\\=\\"\\" crisis\\=\\"\\" intervention\\,\\=\\"\\" trauma\\=\\"\\" counseling\\"\\}\\"\\=\\"\\" data\\-sheets\\-userformat\\=\\"\\{\\" 2\\"\\:13187\\,\\"3\\"\\:\\[null\\,0\\]\\,\\"4\\"\\:\\[null\\,2\\,16573901\\]\\,\\"10\\"\\:2\\,\\"11\\"\\:4\\,\\"12\\"\\:0\\,\\"15\\"\\:\\"arial\\"\\,\\"16\\"\\:11\\}\\"\\=\\"\\"\\>\\<br\\>\\<\\/span\\>\\<\\/div\\>\\<div\\>\\<span style\\=\\"font\\-family\\: Arial\\;\\" data\\-sheets\\-value\\=\\"\\{\\" 1\\"\\:2\\,\\"2\\"\\:\\"legal\\=\\"\\" services\\,\\=\\"\\" teen\\=\\"\\" counseling\\,\\=\\"\\" classroom\\=\\"\\" workshops\\,\\=\\"\\" crisis\\=\\"\\" intervention\\,\\=\\"\\" trauma\\=\\"\\" counseling\\"\\}\\"\\=\\"\\" data\\-sheets\\-userformat\\=\\"\\{\\" 2\\"\\:13187\\,\\"3\\"\\:\\[null\\,0\\]\\,\\"4\\"\\:\\[null\\,2\\,16573901\\]\\,\\"10\\"\\:2\\,\\"11\\"\\:4\\,\\"12\\"\\:0\\,\\"15\\"\\:\\"arial\\"\\,\\"16\\"\\:11\\}\\"\\=\\"\\"\\>All services are free\\.\\&nbsp\\;\\<\\/span\\>\\<\\/div\\>',
					'minor_access'   => '\\<style type\\=\\"text\\/css\\"\\>\\<\\!\\-\\-td \\{border\\: 1px solid \\#ccc\\;\\}br \\{mso\\-data\\-placement\\:same\\-cell\\;\\}\\-\\-\\>\\<\\/style\\>\\<span style\\=\\"font\\-family\\: Arial\\;\\" data\\-sheets\\-value\\=\\"\\{\\" 1\\"\\:2\\,\\"2\\"\\:\\"parent\\=\\"\\" permission\\=\\"\\" required\\"\\}\\"\\=\\"\\" data\\-sheets\\-userformat\\=\\"\\{\\" 2\\"\\:13185\\,\\"3\\"\\:\\[null\\,0\\]\\,\\"10\\"\\:2\\,\\"11\\"\\:4\\,\\"12\\"\\:0\\,\\"15\\"\\:\\"arial\\,sans\\,sans\\-serif\\"\\,\\"16\\"\\:11\\}\\"\\=\\"\\"\\>Parent permission required\\.\\<\\/span\\>\\<div\\>\\<span style\\=\\"font\\-family\\: Arial\\;\\" data\\-sheets\\-value\\=\\"\\{\\" 1\\"\\:2\\,\\"2\\"\\:\\"parent\\=\\"\\" permission\\=\\"\\" required\\"\\}\\"\\=\\"\\" data\\-sheets\\-userformat\\=\\"\\{\\" 2\\"\\:13185\\,\\"3\\"\\:\\[null\\,0\\]\\,\\"10\\"\\:2\\,\\"11\\"\\:4\\,\\"12\\"\\:0\\,\\"15\\"\\:\\"arial\\,sans\\,sans\\-serif\\"\\,\\"16\\"\\:11\\}\\"\\=\\"\\"\\>\\<br\\>\\<\\/span\\>\\<\\/div\\>\\<div\\>\\<span style\\=\\"font\\-family\\: Arial\\;\\" data\\-sheets\\-value\\=\\"\\{\\" 1\\"\\:2\\,\\"2\\"\\:\\"parent\\=\\"\\" permission\\=\\"\\" required\\"\\}\\"\\=\\"\\" data\\-sheets\\-userformat\\=\\"\\{\\" 2\\"\\:13185\\,\\"3\\"\\:\\[null\\,0\\]\\,\\"10\\"\\:2\\,\\"11\\"\\:4\\,\\"12\\"\\:0\\,\\"15\\"\\:\\"arial\\,sans\\,sans\\-serif\\"\\,\\"16\\"\\:11\\}\\"\\=\\"\\"\\>\\<style type\\=\\"text\\/css\\"\\>\\<\\!\\-\\-td \\{border\\: 1px solid \\#ccc\\;\\}br \\{mso\\-data\\-placement\\:same\\-cell\\;\\}\\-\\-\\>\\<\\/style\\>\\<span style\\=\\"\\" data\\-sheets\\-value\\=\\"\\{\\" 1\\"\\:2\\,\\"2\\"\\:\\"it\\=\\"\\" is\\=\\"\\" advised\\=\\"\\" that\\=\\"\\" the\\=\\"\\" non\\-abusing\\=\\"\\" parent\\=\\"\\" come\\=\\"\\" to\\=\\"\\" therapy\\.\\"\\}\\"\\=\\"\\" data\\-sheets\\-userformat\\=\\"\\{\\" 2\\"\\:13185\\,\\"3\\"\\:\\[null\\,0\\]\\,\\"10\\"\\:2\\,\\"11\\"\\:4\\,\\"12\\"\\:0\\,\\"15\\"\\:\\"arial\\,sans\\,sans\\-serif\\"\\,\\"16\\"\\:11\\}\\"\\=\\"\\"\\>When applicable\\: It is advised that the non\\-abusing parent come to therapy\\.\\<\\/span\\>\\<br\\>\\<\\/span\\>\\<\\/div\\>',
					'appointments'   => '\\<style type\\=\\"text\\/css\\"\\>\\<\\!\\-\\-td \\{border\\: 1px solid \\#ccc\\;\\}br \\{mso\\-data\\-placement\\:same\\-cell\\;\\}\\-\\-\\>\\<\\/style\\>\\<span style\\=\\"font\\-family\\: Arial\\;\\" data\\-sheets\\-value\\=\\"\\{\\" 1\\"\\:2\\,\\"2\\"\\:\\"patients\\=\\"\\" advised\\=\\"\\" to\\=\\"\\" call\\=\\"\\" ahead\\"\\}\\"\\=\\"\\" data\\-sheets\\-userformat\\=\\"\\{\\" 2\\"\\:13185\\,\\"3\\"\\:\\[null\\,0\\]\\,\\"10\\"\\:2\\,\\"11\\"\\:4\\,\\"12\\"\\:0\\,\\"15\\"\\:\\"arial\\,sans\\,sans\\-serif\\"\\,\\"16\\"\\:11\\}\\"\\=\\"\\"\\>Patients advised to call ahead and make an appointment\\.\\<\\/span\\>',
					'order'          => null,
					'notes'          => null,
					'created_at'     => '2017-08-29 11:07:42',
					'updated_at'     => '2017-08-29 11:47:03',
				],
			47 =>
				[
					'id'             => 47,
					'uid'            => null,
					'name'           => 'Township Riders Initiative Program \\- Schaumburg Township',
					'tagline'        => null,
					'organization'   => null,
					'address1'       => '1 Illinois Boulevard',
					'address2'       => null,
					'city'           => 'Hoffman Estates',
					'state_id'       => 13,
					'zipcode'        => '60169',
					'auto_gps'       => 1,
					'latitude'       => '42.0277687',
					'longitude'      => '-88.0919772',
					'url'            => '',
					'email'          => '',
					'website'        => 'http\\:\\/\\/www\\.schaumburgtownship\\.org\\/departments\\/transportation\\-services\\/township\\-riders\\-initiative\\-program\\/',
					'phone'          => '8478840030',
					'247_hotline'    => null,
					'text_line'      => null,
					'live_chat'      => '',
					'youth_approved' => 0,
					'hours'          => '\\<font face\\=\\"Arial\\"\\>Mon\\-Fri\\: 5AM \\- 9PM\\<\\/font\\>\\<div\\>\\<font face\\=\\"Arial\\"\\>Sat\\: 7AM \\- 4PM\\<\\/font\\>\\<\\/div\\>',
					'image'          => null,
					'description'    => '\\<font face\\=\\"Arial\\"\\>Provides Transit outside Schaumburg Township for medical appointments\\, to those Schaumburg Township residents who are qualified through the TRIP Program\\. May be used for medical purposes only\\, must cross township lines\\, and must not duplicate existing services\\. May go anywhere within these 5 townships\\: Barrington\\, Elk Grove\\, Hanover\\, Maine\\, Palatine\\, Schaumburg\\, and Wheeling\\.\\<\\/font\\>\\<div\\>\\<font face\\=\\"Arial\\"\\>\\<br\\>\\<\\/font\\>\\<\\/div\\>\\<div\\>\\<font face\\=\\"Arial\\"\\>\\$5\\.00 per township line crossed during a single trip\\, with a maximum of \\$10\\.00 per one\\-way\\.\\<\\/font\\>\\<\\/div\\>\\<div\\>\\<div style\\=\\"box\\-sizing\\: border\\-box\\; margin\\-bottom\\: 10px\\; line\\-height\\: 1\\.45\\; font\\-size\\: 13px\\; word\\-wrap\\: break\\-word\\; word\\-break\\: break\\-word\\; text\\-align\\: justify\\; \\-webkit\\-hyphens\\: auto\\; color\\: rgb\\(50\\, 50\\, 50\\)\\; font\\-family\\: Tahoma\\, Geneva\\, sans\\-serif\\;\\"\\>\\<br\\>\\<\\/div\\>\\<div\\>\\<br\\>\\<\\/div\\>\\<div\\>\\<br\\>\\<\\/div\\>\\<div\\>\\<br\\>\\<\\/div\\>\\<\\/div\\>',
					'minor_access'   => null,
					'appointments'   => '\\<font face\\=\\"Arial\\"\\>Patients advised to call ahead\\.\\<\\/font\\>',
					'order'          => null,
					'notes'          => null,
					'created_at'     => '2017-08-25 14:00:44',
					'updated_at'     => '2017-08-29 11:26:57',
				],
			48 =>
				[
					'id'             => 49,
					'uid'            => null,
					'name'           => 'Community Crisis Center',
					'tagline'        => null,
					'organization'   => null,
					'address1'       => '37 S\\. Geneva Street',
					'address2'       => null,
					'city'           => 'Elgin',
					'state_id'       => 13,
					'zipcode'        => '60121',
					'auto_gps'       => 1,
					'latitude'       => '42.0366360',
					'longitude'      => '-88.2791356',
					'url'            => '',
					'email'          => '',
					'website'        => 'http\\:\\/\\/www\\.crisiscenter\\.org',
					'phone'          => '8477424088',
					'247_hotline'    => null,
					'text_line'      => null,
					'live_chat'      => '',
					'youth_approved' => 1,
					'hours'          => '\\<font face\\=\\"Arial\\"\\>Mon\\-Fri\\: 9AM \\- 8PM\\<\\/font\\>',
					'image'          => null,
					'description'    => '\\<span style\\=\\"font\\-family\\: Arial\\; white\\-space\\: pre\\-wrap\\; background\\-color\\: rgb\\(255\\, 255\\, 255\\)\\;\\"\\>24\\/7 Hour Hotline\\, Emergency Shelter for Women and Children\\, Services Specified for\\: Domestic Violence Victims\\, Children\\, Sexual Assault and Economic Crisis\\. Abuse Intervention Program\\, Court Advocacy for Sexual Assault\\/Domestic Violence Victims\\<\\/span\\>\\<div\\>\\<span style\\=\\"font\\-family\\: Arial\\; white\\-space\\: pre\\-wrap\\; background\\-color\\: rgb\\(255\\, 255\\, 255\\)\\;\\"\\>\\<br\\>\\<\\/span\\>\\<\\/div\\>\\<div\\>\\<font face\\=\\"Arial\\"\\>\\<span style\\=\\"white\\-space\\: pre\\-wrap\\; background\\-color\\: rgb\\(255\\, 255\\, 255\\)\\;\\"\\>All services are free\\.\\<\\/span\\>\\<span style\\=\\"white\\-space\\: pre\\-wrap\\; background\\-color\\: rgb\\(252\\, 229\\, 205\\)\\;\\"\\>\\<br\\>\\<\\/span\\>\\<\\/font\\>\\<\\/div\\>\\<div\\>\\<font face\\=\\"Arial\\"\\>For Spanish please call\\: \\(847\\) 697\\-9740\\.\\<\\/font\\>\\<span style\\=\\"font\\-family\\: Arial\\; white\\-space\\: pre\\-wrap\\; background\\-color\\: rgb\\(255\\, 255\\, 255\\)\\;\\"\\>\\<br\\>\\<\\/span\\>\\<\\/div\\>',
					'minor_access'   => '\\<span style\\=\\"font\\-family\\: Arial\\; white\\-space\\: pre\\-wrap\\; background\\-color\\: rgb\\(255\\, 255\\, 255\\)\\;\\"\\>All services provided are free and confidential\\.\\<\\/span\\>\\<div\\>\\<span style\\=\\"font\\-family\\: Arial\\; white\\-space\\: pre\\-wrap\\; background\\-color\\: rgb\\(255\\, 255\\, 255\\)\\;\\"\\>Children must be with a parent\\. No boys over the age of 12 or men allowed\\.\\<\\/span\\>\\<span style\\=\\"font\\-family\\: Arial\\; font\\-size\\: 15px\\; white\\-space\\: pre\\-wrap\\; background\\-color\\: rgb\\(255\\, 255\\, 255\\)\\;\\"\\>\\<br\\>\\<\\/span\\>\\<\\/div\\>',
					'appointments'   => '\\<span style\\=\\"font\\-family\\: Arial\\; white\\-space\\: pre\\-wrap\\; background\\-color\\: rgb\\(255\\, 255\\, 255\\)\\;\\"\\>Patients advised to call ahead\\.\\<\\/span\\>',
					'order'          => null,
					'notes'          => null,
					'created_at'     => '2017-08-25 14:10:34',
					'updated_at'     => '2017-08-29 09:07:48',
				],
			49 =>
				[
					'id'             => 50,
					'uid'            => null,
					'name'           => 'Teen Parent Connection',
					'tagline'        => null,
					'organization'   => null,
					'address1'       => '475 Taft Avenue',
					'address2'       => null,
					'city'           => 'Glen Ellyn',
					'state_id'       => 13,
					'zipcode'        => '60137',
					'auto_gps'       => 1,
					'latitude'       => '41.8581855',
					'longitude'      => '-88.0690546',
					'url'            => '',
					'email'          => '',
					'website'        => 'http\\:\\/\\/teenparentconnection\\.org',
					'phone'          => '6307908433',
					'247_hotline'    => null,
					'text_line'      => null,
					'live_chat'      => '',
					'youth_approved' => 1,
					'hours'          => '\\<span style\\=\\"font\\-family\\: Arial\\; white\\-space\\: pre\\-wrap\\; background\\-color\\: rgb\\(255\\, 255\\, 255\\)\\;\\"\\>Mon\\-Fri\\: 8\\:30AM \\- 4\\:30PM\\<\\/span\\>',
					'image'          => null,
					'description'    => '\\<span style\\=\\"font\\-family\\: Arial\\; white\\-space\\: pre\\-wrap\\; background\\-color\\: rgb\\(255\\, 255\\, 255\\)\\;\\"\\>Prenatal childbirth education classes\\, Baby closet\\, Doula advocates\\, support groups in Glen Ellyn and Naperville\\.\\<\\/span\\>\\<div\\>\\<span style\\=\\"font\\-family\\: Arial\\; white\\-space\\: pre\\-wrap\\; background\\-color\\: rgb\\(255\\, 255\\, 255\\)\\;\\"\\>\\<br\\>\\<\\/span\\>\\<\\/div\\>\\<div\\>\\<span style\\=\\"font\\-family\\: Arial\\; white\\-space\\: pre\\-wrap\\; background\\-color\\: rgb\\(255\\, 255\\, 255\\)\\;\\"\\>All services are free\\: No charge or services and no income requirements\\. Groups cost \\$1\\, but regardless anyone is welcome\\.\\<\\/span\\>\\<span style\\=\\"font\\-family\\: Arial\\; font\\-size\\: 15px\\; white\\-space\\: pre\\-wrap\\; background\\-color\\: rgb\\(255\\, 255\\, 255\\)\\;\\"\\>\\<br\\>\\<\\/span\\>\\<\\/div\\>',
					'minor_access'   => '\\<span style\\=\\"font\\-family\\: Arial\\; white\\-space\\: pre\\-wrap\\; background\\-color\\: rgb\\(255\\, 255\\, 255\\)\\;\\"\\>Parent\\/guardian permission required on a case\\-by\\-case basis\\. All clients must be pregnant or parenting\\. \\<\\/span\\>\\<span style\\=\\"font\\-family\\: Arial\\; white\\-space\\: pre\\-wrap\\; background\\-color\\: rgb\\(255\\, 255\\, 255\\)\\;\\"\\>All services provided are free and confidential\\.\\<\\/span\\>',
					'appointments'   => '\\<font face\\=\\"Arial\\"\\>\\<span style\\=\\"white\\-space\\: pre\\-wrap\\; background\\-color\\: rgb\\(255\\, 255\\, 255\\)\\;\\"\\>Patients advised to call ahead\\.\\<\\/span\\>\\<\\/font\\>',
					'order'          => null,
					'notes'          => null,
					'created_at'     => '2017-08-25 14:17:30',
					'updated_at'     => '2017-08-29 11:18:24',
				],
			50 =>
				[
					'id'             => 60,
					'uid'            => null,
					'name'           => 'Comprehensive Psychological Services',
					'tagline'        => null,
					'organization'   => null,
					'address1'       => '3233 N\\. Arlington Heights Road',
					'address2'       => 'Suite 208',
					'city'           => 'Arlington Heights',
					'state_id'       => 13,
					'zipcode'        => '60004',
					'auto_gps'       => 1,
					'latitude'       => '42.1336524',
					'longitude'      => '-87.9808778',
					'url'            => '',
					'email'          => '',
					'website'        => 'http\\:\\/\\/www\\.psych4kids\\.com',
					'phone'          => '8476320334',
					'247_hotline'    => null,
					'text_line'      => null,
					'live_chat'      => '',
					'youth_approved' => 0,
					'hours'          => '\\<style type\\=\\"text\\/css\\"\\>\\<\\!\\-\\-td \\{border\\: 1px solid \\#ccc\\;\\}br \\{mso\\-data\\-placement\\:same\\-cell\\;\\}\\-\\-\\>\\<\\/style\\>\\<span style\\=\\"font\\-family\\: Arial\\;\\" data\\-sheets\\-value\\=\\"\\{\\" 1\\"\\:2\\,\\"2\\"\\:\\"mon\\-fri\\:\\=\\"\\" 8am\\-9pm\\"\\}\\"\\=\\"\\" data\\-sheets\\-userformat\\=\\"\\{\\" 2\\"\\:13185\\,\\"3\\"\\:\\[null\\,0\\]\\,\\"10\\"\\:2\\,\\"11\\"\\:4\\,\\"12\\"\\:0\\,\\"15\\"\\:\\"arial\\,sans\\,sans\\-serif\\"\\,\\"16\\"\\:11\\}\\"\\=\\"\\"\\>Mon\\-Fri\\: 8am \\- 9pm\\<\\/span\\>',
					'image'          => null,
					'description'    => '\\<style type\\=\\"text\\/css\\"\\>\\<\\!\\-\\-td \\{border\\: 1px solid \\#ccc\\;\\}br \\{mso\\-data\\-placement\\:same\\-cell\\;\\}\\-\\-\\>\\<\\/style\\>\\<span style\\=\\"font\\-family\\: Arial\\;\\" data\\-sheets\\-value\\=\\"\\{\\" 1\\"\\:2\\,\\"2\\"\\:\\"family\\=\\"\\" counseling\\,\\=\\"\\" individual\\=\\"\\" add\\,\\=\\"\\" anxiety\\,\\=\\"\\" aspergers\\,\\=\\"\\" autism\\"\\}\\"\\=\\"\\" data\\-sheets\\-userformat\\=\\"\\{\\" 2\\"\\:13187\\,\\"3\\"\\:\\[null\\,0\\]\\,\\"4\\"\\:\\[null\\,2\\,16573901\\]\\,\\"10\\"\\:2\\,\\"11\\"\\:4\\,\\"12\\"\\:0\\,\\"15\\"\\:\\"arial\\"\\,\\"16\\"\\:11\\}\\"\\=\\"\\"\\>Family Counseling\\, Individual Counseling\\, ADD\\, Anxiety\\, Aspergers\\, Autism\\.\\<\\/span\\>\\<div\\>\\<span style\\=\\"font\\-family\\: Arial\\;\\" data\\-sheets\\-value\\=\\"\\{\\" 1\\"\\:2\\,\\"2\\"\\:\\"family\\=\\"\\" counseling\\,\\=\\"\\" individual\\=\\"\\" add\\,\\=\\"\\" anxiety\\,\\=\\"\\" aspergers\\,\\=\\"\\" autism\\"\\}\\"\\=\\"\\" data\\-sheets\\-userformat\\=\\"\\{\\" 2\\"\\:13187\\,\\"3\\"\\:\\[null\\,0\\]\\,\\"4\\"\\:\\[null\\,2\\,16573901\\]\\,\\"10\\"\\:2\\,\\"11\\"\\:4\\,\\"12\\"\\:0\\,\\"15\\"\\:\\"arial\\"\\,\\"16\\"\\:11\\}\\"\\=\\"\\"\\>\\<br\\>\\<\\/span\\>\\<\\/div\\>\\<div\\>\\<span style\\=\\"font\\-family\\: Arial\\;\\" data\\-sheets\\-value\\=\\"\\{\\" 1\\"\\:2\\,\\"2\\"\\:\\"family\\=\\"\\" counseling\\,\\=\\"\\" individual\\=\\"\\" add\\,\\=\\"\\" anxiety\\,\\=\\"\\" aspergers\\,\\=\\"\\" autism\\"\\}\\"\\=\\"\\" data\\-sheets\\-userformat\\=\\"\\{\\" 2\\"\\:13187\\,\\"3\\"\\:\\[null\\,0\\]\\,\\"4\\"\\:\\[null\\,2\\,16573901\\]\\,\\"10\\"\\:2\\,\\"11\\"\\:4\\,\\"12\\"\\:0\\,\\"15\\"\\:\\"arial\\"\\,\\"16\\"\\:11\\}\\"\\=\\"\\"\\>Sliding scale fee for those who qualify\\.\\<\\/span\\>\\<\\/div\\>\\<div\\>\\<span style\\=\\"font\\-size\\: 11pt\\; font\\-family\\: Arial\\;\\" data\\-sheets\\-value\\=\\"\\{\\" 1\\"\\:2\\,\\"2\\"\\:\\"family\\=\\"\\" counseling\\,\\=\\"\\" individual\\=\\"\\" add\\,\\=\\"\\" anxiety\\,\\=\\"\\" aspergers\\,\\=\\"\\" autism\\"\\}\\"\\=\\"\\" data\\-sheets\\-userformat\\=\\"\\{\\" 2\\"\\:13187\\,\\"3\\"\\:\\[null\\,0\\]\\,\\"4\\"\\:\\[null\\,2\\,16573901\\]\\,\\"10\\"\\:2\\,\\"11\\"\\:4\\,\\"12\\"\\:0\\,\\"15\\"\\:\\"arial\\"\\,\\"16\\"\\:11\\}\\"\\=\\"\\"\\>\\<br\\>\\<\\/span\\>\\<\\/div\\>',
					'minor_access'   => '\\<font face\\=\\"Arial\\"\\>Parent permission required\\.\\<\\/font\\>',
					'appointments'   => '\\<font face\\=\\"Arial\\"\\>Patients advised to call ahead to make an appointment\\.\\<\\/font\\>',
					'order'          => null,
					'notes'          => null,
					'created_at'     => '2017-08-29 11:02:41',
					'updated_at'     => '2017-08-29 11:49:24',
				],
			51 =>
				[
					'id'             => 51,
					'uid'            => null,
					'name'           => 'VNA Health Care',
					'tagline'        => null,
					'organization'   => null,
					'address1'       => '620 Wing Street',
					'address2'       => null,
					'city'           => 'Elgin',
					'state_id'       => 13,
					'zipcode'        => '60123',
					'auto_gps'       => 1,
					'latitude'       => '42.0456150',
					'longitude'      => '-88.2944621',
					'url'            => '',
					'email'          => '',
					'website'        => 'http\\:\\/\\/www\\.vnahealth\\.com',
					'phone'          => '8477176455',
					'247_hotline'    => null,
					'text_line'      => null,
					'live_chat'      => '',
					'youth_approved' => 0,
					'hours'          => '\\<span style\\=\\"white\\-space\\: pre\\-wrap\\; background\\-color\\: rgb\\(255\\, 255\\, 255\\)\\;\\"\\>\\<font face\\=\\"Arial\\"\\>Mon\\: 8AM \\- 8PM\\<\\/font\\>\\<\\/span\\>\\<div\\>\\<span style\\=\\"white\\-space\\: pre\\-wrap\\; background\\-color\\: rgb\\(255\\, 255\\, 255\\)\\;\\"\\>\\<font face\\=\\"Arial\\"\\>Tue\\-Fri\\: 8AM \\- 4PM\\<\\/font\\>\\<\\/span\\>\\<\\/div\\>',
					'image'          => null,
					'description'    => '\\<span style\\=\\"font\\-family\\: Arial\\; font\\-size\\: 15px\\; white\\-space\\: pre\\-wrap\\; background\\-color\\: rgb\\(255\\, 255\\, 255\\)\\;\\"\\>Pediatrics\\, Family Medicine\\, Lab\\, Obstetrics\\/Gynecology\\/Women\\\'s Health\\, Behavioral Health\\, WIC\\.\\<\\/span\\>\\<div\\>\\<span style\\=\\"font\\-family\\: Arial\\; font\\-size\\: 15px\\; white\\-space\\: pre\\-wrap\\; background\\-color\\: rgb\\(255\\, 255\\, 255\\)\\;\\"\\>\\<br\\>\\<\\/span\\>\\<\\/div\\>\\<div\\>\\<span style\\=\\"font\\-family\\: Arial\\; font\\-size\\: 15px\\; white\\-space\\: pre\\-wrap\\; background\\-color\\: rgb\\(255\\, 255\\, 255\\)\\;\\"\\>Patients will not be turned away due to inability to pay\\.\\<\\/span\\>\\<span style\\=\\"font\\-family\\: Arial\\; font\\-size\\: 15px\\; white\\-space\\: pre\\-wrap\\; background\\-color\\: rgb\\(255\\, 255\\, 255\\)\\;\\"\\>\\<br\\>\\<\\/span\\>\\<\\/div\\>',
					'minor_access'   => '\\<span style\\=\\"font\\-family\\: Arial\\; white\\-space\\: pre\\-wrap\\; background\\-color\\: rgb\\(255\\, 255\\, 255\\)\\;\\"\\>Parent permission required\\.\\<\\/span\\>',
					'appointments'   => '\\<span style\\=\\"font\\-family\\: Arial\\; white\\-space\\: pre\\-wrap\\; background\\-color\\: rgb\\(255\\, 255\\, 255\\)\\;\\"\\>Same day appointments available \\&amp\\; walk\\-ins accepted \\- Patients advised to call ahead\\.\\<\\/span\\>',
					'order'          => null,
					'notes'          => null,
					'created_at'     => '2017-08-25 14:26:05',
					'updated_at'     => '2017-08-29 11:29:43',
				],
			52 =>
				[
					'id'             => 53,
					'uid'            => null,
					'name'           => 'Alexian Brothers Behavioral Health Hospital',
					'tagline'        => null,
					'organization'   => null,
					'address1'       => '1650 Moon Lake Boulevard',
					'address2'       => null,
					'city'           => 'Hoffman Estates',
					'state_id'       => 13,
					'zipcode'        => '60169',
					'auto_gps'       => 1,
					'latitude'       => '42.0541580',
					'longitude'      => '-88.1372537',
					'url'            => '',
					'email'          => '',
					'website'        => 'http\\:\\/\\/www\\.alexianbrothershealth\\.org',
					'phone'          => '8553832224',
					'247_hotline'    => '8004325005',
					'text_line'      => null,
					'live_chat'      => '',
					'youth_approved' => 0,
					'hours'          => '\\<font face\\=\\"Arial\\"\\>24\\/7\\<\\/font\\>',
					'image'          => null,
					'description'    => '\\<span style\\=\\"font\\-family\\: Arial\\; white\\-space\\: pre\\-wrap\\; background\\-color\\: rgb\\(255\\, 255\\, 255\\)\\;\\"\\>Adolescent Partial Hospitalization\\, Intensive Outpatient Program\\, Psychiatry\\, Treatment Services for\\: Chemical Dependency\\, Eating Disorders\\, Anorexia Nervosa\\, Bulimia Nervosa\\, Compulsive Overeating\\, Emotional Overeating\\, Binge\\-Eating\\, Obsessive Compulsive Disorder\\, School Refusal\\, Panic Disorder\\, Social Phobia\\, Post Traumatic Stress Disorder\\, Phobias\\, Generalized Anxiety Disorder\\, School Anxiety\\, Self\\-Injury\\, Autism Spectrum\\, Developmental Disorders\\<\\/span\\>\\<div\\>\\<span style\\=\\"font\\-family\\: Arial\\; white\\-space\\: pre\\-wrap\\; background\\-color\\: rgb\\(255\\, 255\\, 255\\)\\;\\"\\>\\<br\\>\\<\\/span\\>\\<\\/div\\>\\<div\\>\\<span style\\=\\"font\\-family\\: Arial\\; white\\-space\\: pre\\-wrap\\; background\\-color\\: rgb\\(255\\, 255\\, 255\\)\\;\\"\\>Sliding scale for those who qualify\\.\\<\\/span\\>\\<\\/div\\>\\<div\\>\\<span style\\=\\"font\\-family\\: Arial\\; white\\-space\\: pre\\-wrap\\; background\\-color\\: rgb\\(255\\, 255\\, 255\\)\\;\\"\\>Cash\\, Credit Card\\, Check\\. Payments can be made online\\, in person\\, or mailed\\. \\<\\/span\\>\\<span style\\=\\"font\\-family\\: Arial\\; font\\-size\\: 15px\\; white\\-space\\: pre\\-wrap\\; background\\-color\\: rgb\\(255\\, 255\\, 255\\)\\;\\"\\>\\<br\\>\\<\\/span\\>\\<\\/div\\>',
					'minor_access'   => '\\<span style\\=\\"font\\-family\\: Arial\\; white\\-space\\: pre\\-wrap\\; background\\-color\\: rgb\\(255\\, 255\\, 255\\)\\;\\"\\>Parent permission required\\. \\<\\/span\\>\\<span style\\=\\"font\\-family\\: Arial\\; white\\-space\\: pre\\-wrap\\; background\\-color\\: rgb\\(255\\, 255\\, 255\\)\\;\\"\\>Anyone 12 or older can access free assessments without a parent\\. Agency requires patients under 18 to be picked up by an adult or sign a release for a Taxi\\/Uber or Medicaid transportation\\.\\<\\/span\\>',
					'appointments'   => '\\<span style\\=\\"font\\-family\\: Arial\\; white\\-space\\: pre\\-wrap\\; background\\-color\\: rgb\\(255\\, 255\\, 255\\)\\;\\"\\>Same day appointments available \\&amp\\; walk\\-ins accepted\\.\\<\\/span\\>',
					'order'          => null,
					'notes'          => null,
					'created_at'     => '2017-08-25 14:38:14',
					'updated_at'     => '2017-08-28 11:56:21',
				],
			53 =>
				[
					'id'             => 54,
					'uid'            => null,
					'name'           => 'Northwest Community Hospital',
					'tagline'        => null,
					'organization'   => null,
					'address1'       => '800 W\\. Central Road',
					'address2'       => null,
					'city'           => 'Arlington Heights',
					'state_id'       => 13,
					'zipcode'        => '60005',
					'auto_gps'       => 1,
					'latitude'       => '42.0680145',
					'longitude'      => '-87.9930501',
					'url'            => '',
					'email'          => '',
					'website'        => 'http\\:\\/\\/www\\.nch\\.org',
					'phone'          => '8474325464',
					'247_hotline'    => '8474325464',
					'text_line'      => null,
					'live_chat'      => '',
					'youth_approved' => 0,
					'hours'          => '\\<span style\\=\\"font\\-family\\: Arial\\; white\\-space\\: pre\\-wrap\\; background\\-color\\: rgb\\(255\\, 255\\, 255\\)\\;\\"\\>24\\/7\\<\\/span\\>\\<div\\>\\<span style\\=\\"font\\-family\\: Arial\\; white\\-space\\: pre\\-wrap\\; background\\-color\\: rgb\\(255\\, 255\\, 255\\)\\;\\"\\>Assessments from 7AM \\- 11PM\\.\\<\\/span\\>\\<\\/div\\>\\<div\\>\\<span style\\=\\"font\\-family\\: Arial\\; white\\-space\\: pre\\-wrap\\; background\\-color\\: rgb\\(255\\, 255\\, 255\\)\\;\\"\\>Last appointment would be 9PM\\.\\<\\/span\\>\\<\\/div\\>',
					'image'          => null,
					'description'    => '\\<span style\\=\\"white\\-space\\: pre\\-wrap\\; background\\-color\\: rgb\\(255\\, 255\\, 255\\)\\;\\"\\>\\<font face\\=\\"Arial\\"\\>Partial Hospitalization Program\\, Intensive Outpatient Program\\, Continuing Care\\, Treatment for\\: Behavioral Health Issues\\, Generalized Anxiety Disorder \\(GAD\\)\\, Panic Disorders\\, Panic Attacks\\, Agoraphobia\\, Specific Phobias\\, Social Anxiety Disorder\\, Social Phobias\\, Separation Anxiety Disorder\\, Selective Mutism\\, Bipolar Disorder\\, Depression\\, Self\\-Injury\\, Eating Disorders\\<\\/font\\>\\<\\/span\\>\\<div\\>\\<span style\\=\\"white\\-space\\: pre\\-wrap\\; background\\-color\\: rgb\\(255\\, 255\\, 255\\)\\;\\"\\>\\<font face\\=\\"Arial\\"\\>\\<br\\>\\<\\/font\\>\\<\\/span\\>\\<\\/div\\>\\<div\\>\\<span style\\=\\"background\\-color\\: rgb\\(255\\, 255\\, 255\\)\\;\\"\\>\\<font face\\=\\"Arial\\"\\>\\<span style\\=\\"white\\-space\\: pre\\-wrap\\;\\"\\>Sliding scale for those who qualify\\.\\<\\/span\\>\\<\\/font\\>\\<\\/span\\>\\<\\/div\\>\\<div\\>\\<span style\\=\\"white\\-space\\: pre\\-wrap\\; background\\-color\\: rgb\\(255\\, 255\\, 255\\)\\;\\"\\>\\<font face\\=\\"Arial\\"\\>Credit card\\, debit card\\, check and money orders are accepted\\.\\<\\/font\\>\\<\\/span\\>\\<span style\\=\\"background\\-color\\: rgb\\(255\\, 255\\, 255\\)\\;\\"\\>\\<font face\\=\\"Arial\\"\\>\\<span style\\=\\"font\\-size\\: 15px\\; white\\-space\\: pre\\-wrap\\;\\"\\>\\<br\\>\\<\\/span\\>\\<\\/font\\>\\<\\/span\\>\\<\\/div\\>',
					'minor_access'   => '\\<span style\\=\\"font\\-family\\: Arial\\; white\\-space\\: pre\\-wrap\\; background\\-color\\: rgb\\(255\\, 255\\, 255\\)\\;\\"\\>Parent permission not required\\.\\<\\/span\\>',
					'appointments'   => '\\<span style\\=\\"white\\-space\\: pre\\-wrap\\; background\\-color\\: rgb\\(255\\, 255\\, 255\\)\\;\\"\\>\\<font face\\=\\"Arial\\"\\>Same day appointments available \\&amp\\; walk\\-ins accepted\\.\\<\\/font\\>\\<\\/span\\>',
					'order'          => null,
					'notes'          => null,
					'created_at'     => '2017-08-25 14:56:11',
					'updated_at'     => '2017-08-29 10:38:12',
				],
			54 =>
				[
					'id'             => 55,
					'uid'            => null,
					'name'           => 'Greater Elgin Family Care Center\\: Lake Health Center',
					'tagline'        => null,
					'organization'   => null,
					'address1'       => '1515 E\\. Lake Street',
					'address2'       => 'Suite 202',
					'city'           => 'Hanover Park',
					'state_id'       => 13,
					'zipcode'        => '60133',
					'auto_gps'       => 1,
					'latitude'       => '41.9839113',
					'longitude'      => '-88.1436169',
					'url'            => '',
					'email'          => '',
					'website'        => 'http\\:\\/\\/www\\.gefcc\\.org',
					'phone'          => '8476081344',
					'247_hotline'    => null,
					'text_line'      => null,
					'live_chat'      => '',
					'youth_approved' => 1,
					'hours'          => '\\<span style\\=\\"font\\-family\\: Arial\\; white\\-space\\: pre\\-wrap\\; background\\-color\\: rgb\\(255\\, 255\\, 255\\)\\;\\"\\>Mon\\-Fri\\: 8\\:30AM \\- 5PM\\<\\/span\\>',
					'image'          => null,
					'description'    => '\\<span style\\=\\"font\\-family\\: Arial\\; white\\-space\\: pre\\-wrap\\; background\\-color\\: rgb\\(255\\, 255\\, 255\\)\\;\\"\\>Free Pregnancy Testing\\, STD\\/STI Testing\\, Family Case Management\\, Home Visits\\.\\<\\/span\\>\\<div\\>\\<span style\\=\\"font\\-family\\: Arial\\; white\\-space\\: pre\\-wrap\\; background\\-color\\: rgb\\(255\\, 255\\, 255\\)\\;\\"\\>\\<br\\>\\<\\/span\\>\\<\\/div\\>\\<div\\>\\<span style\\=\\"background\\-color\\: rgb\\(255\\, 255\\, 255\\)\\;\\"\\>\\<span style\\=\\"font\\-family\\: Arial\\; white\\-space\\: pre\\-wrap\\;\\"\\>Patients will not be turned away due to inability to pay\\<\\/span\\>\\<span style\\=\\"font\\-family\\: Arial\\; white\\-space\\: pre\\-wrap\\;\\"\\>\\<br\\>\\<\\/span\\>\\<\\/span\\>\\<\\/div\\>\\<div\\>\\<span style\\=\\"font\\-family\\: Arial\\; white\\-space\\: pre\\-wrap\\; background\\-color\\: rgb\\(255\\, 255\\, 255\\)\\;\\"\\>\\$25 fee if patient has no insurance\\.\\<\\/span\\>\\<span style\\=\\"font\\-family\\: Arial\\; font\\-size\\: 15px\\; white\\-space\\: pre\\-wrap\\; background\\-color\\: rgb\\(255\\, 255\\, 255\\)\\;\\"\\>\\<br\\>\\<\\/span\\>\\<\\/div\\>',
					'minor_access'   => '\\<span style\\=\\"font\\-family\\: Arial\\; white\\-space\\: pre\\-wrap\\; background\\-color\\: rgb\\(255\\, 255\\, 255\\)\\;\\"\\>Parent permission required\\. \\*\\<\\/span\\>\\<span style\\=\\"font\\-family\\: Arial\\; white\\-space\\: pre\\-wrap\\; background\\-color\\: rgb\\(255\\, 255\\, 255\\)\\;\\"\\>STD\\/STI and pregnancy testing are available without parent permission\\. Walk\\-Ins available for STD\\/STI testing\\.\\<\\/span\\>',
					'appointments'   => '\\<span style\\=\\"font\\-family\\: Arial\\; white\\-space\\: pre\\-wrap\\; background\\-color\\: rgb\\(255\\, 255\\, 255\\)\\;\\"\\>Patients advised to call ahead\\<\\/span\\>',
					'order'          => null,
					'notes'          => null,
					'created_at'     => '2017-08-25 15:06:18',
					'updated_at'     => '2017-08-29 09:30:53',
				],
			55 =>
				[
					'id'             => 56,
					'uid'            => null,
					'name'           => 'Hanover Township',
					'tagline'        => null,
					'organization'   => null,
					'address1'       => '250 S\\. IL Route 59',
					'address2'       => null,
					'city'           => 'Bartlett',
					'state_id'       => 13,
					'zipcode'        => '60103',
					'auto_gps'       => 1,
					'latitude'       => '41.9912100',
					'longitude'      => '-88.2086278',
					'url'            => '',
					'email'          => '',
					'website'        => 'http\\:\\/\\/www\\.hanover\\-township\\.org',
					'phone'          => '6304835799',
					'247_hotline'    => null,
					'text_line'      => null,
					'live_chat'      => '',
					'youth_approved' => 0,
					'hours'          => '\\<span style\\=\\"white\\-space\\: pre\\-wrap\\; background\\-color\\: rgb\\(255\\, 255\\, 255\\)\\;\\"\\>\\<font face\\=\\"Arial\\"\\>Mon\\-Thu\\: 9\\:30AM \\- 9PM\\<\\/font\\>\\<\\/span\\>\\<div\\>\\<span style\\=\\"white\\-space\\: pre\\-wrap\\; background\\-color\\: rgb\\(255\\, 255\\, 255\\)\\;\\"\\>\\<font face\\=\\"Arial\\"\\>Fri\\: 9\\:30AM \\- 4\\:30PM\\<\\/font\\>\\<\\/span\\>\\<\\/div\\>',
					'image'          => null,
					'description'    => '\\<span style\\=\\"font\\-family\\: Arial\\; white\\-space\\: pre\\-wrap\\; background\\-color\\: rgb\\(255\\, 255\\, 255\\)\\;\\"\\>O\\<\\/span\\>\\<span style\\=\\"white\\-space\\: pre\\-wrap\\; background\\-color\\: rgb\\(255\\, 255\\, 255\\)\\;\\"\\>\\<font face\\=\\"Arial\\"\\>utpatient Family Therapy\\, School\\-Based Prevention Services\\, Open Gym\\, Alternative Suspension Program\\, Substance Abuse Prevention\\, Tutoring\\.\\<\\/font\\>\\<\\/span\\>',
					'minor_access'   => '\\<span style\\=\\"font\\-family\\: Arial\\; white\\-space\\: pre\\-wrap\\; background\\-color\\: rgb\\(255\\, 255\\, 255\\)\\;\\"\\>Parent permission required\\.\\<\\/span\\>\\<br\\>',
					'appointments'   => '\\<span style\\=\\"font\\-family\\: Arial\\; white\\-space\\: pre\\-wrap\\; background\\-color\\: rgb\\(255\\, 255\\, 255\\)\\;\\"\\>Patients advised to call ahead\\.\\<\\/span\\>',
					'order'          => null,
					'notes'          => null,
					'created_at'     => '2017-08-25 15:09:41',
					'updated_at'     => '2017-08-29 10:14:47',
				],
			56 =>
				[
					'id'             => 57,
					'uid'            => null,
					'name'           => 'Family Focus',
					'tagline'        => 'literacy\\, family\\, early childhood',
					'organization'   => null,
					'address1'       => '2174 Gladstone Court',
					'address2'       => null,
					'city'           => 'Glendale Heights',
					'state_id'       => 13,
					'zipcode'        => '60139',
					'auto_gps'       => 1,
					'latitude'       => '41.9381659',
					'longitude'      => '-88.0858188',
					'url'            => '',
					'email'          => '',
					'website'        => 'http\\:\\/\\/www\\.family\\-focus\\.org',
					'phone'          => '6305218808',
					'247_hotline'    => null,
					'text_line'      => null,
					'live_chat'      => '',
					'youth_approved' => 0,
					'hours'          => '\\<style type\\=\\"text\\/css\\"\\>\\<\\!\\-\\-td \\{border\\: 1px solid \\#ccc\\;\\}br \\{mso\\-data\\-placement\\:same\\-cell\\;\\}\\-\\-\\>\\<\\/style\\>\\<span style\\=\\"font\\-family\\: Arial\\; font\\-style\\: normal\\;\\" data\\-sheets\\-value\\=\\"\\{\\" 1\\"\\:2\\,\\"2\\"\\:\\"mon\\-fri\\:\\=\\"\\" 9am\\-5pm\\"\\}\\"\\=\\"\\" data\\-sheets\\-userformat\\=\\"\\{\\" 2\\"\\:13185\\,\\"3\\"\\:\\[null\\,0\\]\\,\\"10\\"\\:2\\,\\"11\\"\\:4\\,\\"12\\"\\:0\\,\\"15\\"\\:\\"arial\\,sans\\,sans\\-serif\\"\\,\\"16\\"\\:11\\}\\"\\=\\"\\"\\>Mon\\-Fri\\: 9AM \\- 5PM\\<\\/span\\>',
					'image'          => null,
					'description'    => '\\<style type\\=\\"text\\/css\\"\\>\\<\\!\\-\\-td \\{border\\: 1px solid \\#ccc\\;\\}br \\{mso\\-data\\-placement\\:same\\-cell\\;\\}\\-\\-\\>\\<\\/style\\>\\<span style\\=\\"font\\-family\\: Arial\\; font\\-weight\\: normal\\; font\\-style\\: normal\\; color\\: rgb\\(0\\, 0\\, 0\\)\\;\\" data\\-sheets\\-value\\=\\"\\{\\" 1\\"\\:2\\,\\"2\\"\\:\\"developmental\\=\\"\\" screenings\\,\\=\\"\\" parenting\\=\\"\\" education\\,\\=\\"\\" workshops\\,\\=\\"\\" support\\=\\"\\" groups\\.\\=\\"\\" evidence\\-based\\=\\"\\" early\\=\\"\\" childhood\\=\\"\\" curriculum\\,\\=\\"\\" prenatal\\=\\"\\" birthing\\=\\"\\" support\\,\\=\\"\\" family\\=\\"\\" goal\\-setting\\,\\=\\"\\" pre\\-literacy\\=\\"\\" learning\\=\\"\\" activities\\,\\=\\"\\" father\\=\\"\\" engagement\\"\\}\\"\\=\\"\\" data\\-sheets\\-userformat\\=\\"\\{\\" 2\\"\\:15107\\,\\"3\\"\\:\\[null\\,0\\]\\,\\"4\\"\\:\\[null\\,2\\,16573901\\]\\,\\"11\\"\\:4\\,\\"12\\"\\:0\\,\\"14\\"\\:\\[null\\,2\\,0\\]\\,\\"15\\"\\:\\"arial\\"\\,\\"16\\"\\:11\\}\\"\\=\\"\\"\\>Developmental Screenings\\, Parenting Education\\, Workshops\\, Support Groups\\. Evidence\\-Based Early Childhood Curriculum\\, Prenatal Education\\, Birthing Support\\, Family Goal\\-Setting\\, Pre\\-Literacy Learning Activities\\, Father Engagement\\.\\<\\/span\\>\\<div\\>\\<span style\\=\\"font\\-family\\: Arial\\; font\\-weight\\: normal\\; font\\-style\\: normal\\; color\\: rgb\\(0\\, 0\\, 0\\)\\;\\" data\\-sheets\\-value\\=\\"\\{\\" 1\\"\\:2\\,\\"2\\"\\:\\"developmental\\=\\"\\" screenings\\,\\=\\"\\" parenting\\=\\"\\" education\\,\\=\\"\\" workshops\\,\\=\\"\\" support\\=\\"\\" groups\\.\\=\\"\\" evidence\\-based\\=\\"\\" early\\=\\"\\" childhood\\=\\"\\" curriculum\\,\\=\\"\\" prenatal\\=\\"\\" birthing\\=\\"\\" support\\,\\=\\"\\" family\\=\\"\\" goal\\-setting\\,\\=\\"\\" pre\\-literacy\\=\\"\\" learning\\=\\"\\" activities\\,\\=\\"\\" father\\=\\"\\" engagement\\"\\}\\"\\=\\"\\" data\\-sheets\\-userformat\\=\\"\\{\\" 2\\"\\:15107\\,\\"3\\"\\:\\[null\\,0\\]\\,\\"4\\"\\:\\[null\\,2\\,16573901\\]\\,\\"11\\"\\:4\\,\\"12\\"\\:0\\,\\"14\\"\\:\\[null\\,2\\,0\\]\\,\\"15\\"\\:\\"arial\\"\\,\\"16\\"\\:11\\}\\"\\=\\"\\"\\>\\<br\\>\\<\\/span\\>\\<\\/div\\>\\<div\\>\\<span style\\=\\"font\\-family\\: Arial\\; font\\-weight\\: normal\\; font\\-style\\: normal\\; color\\: rgb\\(0\\, 0\\, 0\\)\\;\\" data\\-sheets\\-value\\=\\"\\{\\" 1\\"\\:2\\,\\"2\\"\\:\\"developmental\\=\\"\\" screenings\\,\\=\\"\\" parenting\\=\\"\\" education\\,\\=\\"\\" workshops\\,\\=\\"\\" support\\=\\"\\" groups\\.\\=\\"\\" evidence\\-based\\=\\"\\" early\\=\\"\\" childhood\\=\\"\\" curriculum\\,\\=\\"\\" prenatal\\=\\"\\" birthing\\=\\"\\" support\\,\\=\\"\\" family\\=\\"\\" goal\\-setting\\,\\=\\"\\" pre\\-literacy\\=\\"\\" learning\\=\\"\\" activities\\,\\=\\"\\" father\\=\\"\\" engagement\\"\\}\\"\\=\\"\\" data\\-sheets\\-userformat\\=\\"\\{\\" 2\\"\\:15107\\,\\"3\\"\\:\\[null\\,0\\]\\,\\"4\\"\\:\\[null\\,2\\,16573901\\]\\,\\"11\\"\\:4\\,\\"12\\"\\:0\\,\\"14\\"\\:\\[null\\,2\\,0\\]\\,\\"15\\"\\:\\"arial\\"\\,\\"16\\"\\:11\\}\\"\\=\\"\\"\\>All services are free and confidential\\.\\<\\/span\\>\\<\\/div\\>',
					'minor_access'   => '\\<span style\\=\\"font\\-family\\: Arial\\; font\\-style\\: normal\\;\\" data\\-sheets\\-value\\=\\"\\{\\" 1\\"\\:2\\,\\"2\\"\\:\\"parent\\=\\"\\" guardian\\=\\"\\" permission\\=\\"\\" is\\=\\"\\" only\\=\\"\\" required\\=\\"\\" for\\=\\"\\" doula\\=\\"\\" services\\.\\"\\}\\"\\=\\"\\" data\\-sheets\\-userformat\\=\\"\\{\\" 2\\"\\:13185\\,\\"3\\"\\:\\[null\\,0\\]\\,\\"10\\"\\:2\\,\\"11\\"\\:4\\,\\"12\\"\\:0\\,\\"15\\"\\:\\"arial\\,sans\\,sans\\-serif\\"\\,\\"16\\"\\:11\\}\\"\\=\\"\\"\\>\\<span style\\=\\"font\\-family\\: Arial\\; font\\-style\\: normal\\;\\" data\\-sheets\\-value\\=\\"\\{\\" 1\\"\\:2\\,\\"2\\"\\:\\"all\\=\\"\\" services\\=\\"\\" provided\\=\\"\\" are\\=\\"\\" free\\=\\"\\" and\\=\\"\\" confidential\\"\\}\\"\\=\\"\\" data\\-sheets\\-userformat\\=\\"\\{\\" 2\\"\\:13185\\,\\"3\\"\\:\\[null\\,0\\]\\,\\"10\\"\\:2\\,\\"11\\"\\:4\\,\\"12\\"\\:0\\,\\"15\\"\\:\\"arial\\,sans\\,sans\\-serif\\"\\,\\"16\\"\\:11\\}\\"\\=\\"\\"\\>Parent\\/guardian permission is only required for doula services\\.\\<br\\>\\<\\/span\\>\\<\\/span\\>',
					'appointments'   => '\\<style type\\=\\"text\\/css\\"\\>\\<\\!\\-\\-td \\{border\\: 1px solid \\#ccc\\;\\}br \\{mso\\-data\\-placement\\:same\\-cell\\;\\}\\-\\-\\>\\<\\/style\\>\\<span style\\=\\"font\\-family\\: Arial\\; font\\-style\\: normal\\;\\" data\\-sheets\\-value\\=\\"\\{\\" 1\\"\\:2\\,\\"2\\"\\:\\"same\\=\\"\\" day\\=\\"\\" appointments\\=\\"\\" available\\=\\"\\" \\&\\=\\"\\" walk\\-ins\\=\\"\\" accepted\\=\\"\\" \\-\\=\\"\\" patients\\=\\"\\" advised\\=\\"\\" to\\=\\"\\" call\\=\\"\\" ahead\\"\\}\\"\\=\\"\\" data\\-sheets\\-userformat\\=\\"\\{\\" 2\\"\\:13185\\,\\"3\\"\\:\\[null\\,0\\]\\,\\"10\\"\\:2\\,\\"11\\"\\:4\\,\\"12\\"\\:0\\,\\"15\\"\\:\\"arial\\,sans\\,sans\\-serif\\"\\,\\"16\\"\\:11\\}\\"\\=\\"\\"\\>Same day appointments available \\&amp\\; walk\\-ins accepted \\- Patients advised to call ahead\\.\\<\\/span\\>',
					'order'          => null,
					'notes'          => null,
					'created_at'     => '2017-08-25 15:14:37',
					'updated_at'     => '2017-08-29 09:26:59',
				],
			57 =>
				[
					'id'             => 58,
					'uid'            => null,
					'name'           => 'Greater Elgin Family Care Center\\: Streamwood Health Center',
					'tagline'        => null,
					'organization'   => null,
					'address1'       => '135 E\\. Irving Park Road',
					'address2'       => null,
					'city'           => 'Streamwood',
					'state_id'       => 13,
					'zipcode'        => '60107',
					'auto_gps'       => 1,
					'latitude'       => '42.0114451',
					'longitude'      => '-88.1789541',
					'url'            => '',
					'email'          => '',
					'website'        => 'http\\:\\/\\/www\\.gefcc\\.org',
					'phone'          => '6302895879',
					'247_hotline'    => null,
					'text_line'      => null,
					'live_chat'      => '',
					'youth_approved' => 1,
					'hours'          => '\\<span style\\=\\"color\\: rgb\\(0\\, 0\\, 0\\)\\; font\\-weight\\: 400\\; text\\-decoration\\: none\\; font\\-style\\: normal\\;\\"\\>\\<font face\\=\\"Arial\\"\\>Mon\\-Thu\\: 8AM \\- 7\\:30PM\\<\\/font\\>\\<\\/span\\>\\<div\\>\\<span style\\=\\"color\\: rgb\\(0\\, 0\\, 0\\)\\; font\\-weight\\: 400\\; text\\-decoration\\: none\\; font\\-style\\: normal\\;\\"\\>\\<font face\\=\\"Arial\\"\\>Fri\\-Sun\\: 8AM \\- 5PM\\<\\/font\\>\\<\\/span\\>\\<\\/div\\>',
					'image'          => null,
					'description'    => '\\<style type\\=\\"text\\/css\\"\\>\\<\\!\\-\\-td \\{border\\: 1px solid \\#ccc\\;\\}br \\{mso\\-data\\-placement\\:same\\-cell\\;\\}\\-\\-\\>\\<\\/style\\>\\<span style\\=\\"font\\-family\\: Arial\\; font\\-weight\\: normal\\; font\\-style\\: normal\\; color\\: rgb\\(0\\, 0\\, 0\\)\\;\\" data\\-sheets\\-value\\=\\"\\{\\" 1\\"\\:2\\,\\"2\\"\\:\\"free\\=\\"\\" pregnancy\\=\\"\\" testing\\,\\=\\"\\" std\\=\\"\\" sti\\=\\"\\" family\\=\\"\\" case\\=\\"\\" management\\,\\=\\"\\" home\\=\\"\\" visits\\"\\}\\"\\=\\"\\" data\\-sheets\\-userformat\\=\\"\\{\\" 2\\"\\:15107\\,\\"3\\"\\:\\[null\\,0\\]\\,\\"4\\"\\:\\[null\\,2\\,16573901\\]\\,\\"11\\"\\:4\\,\\"12\\"\\:0\\,\\"14\\"\\:\\[null\\,2\\,0\\]\\,\\"15\\"\\:\\"arial\\"\\,\\"16\\"\\:11\\}\\"\\=\\"\\"\\>Free Pregnancy Testing\\, STD\\/STI Testing\\, Family Case Management\\, Home Visits\\<br\\>\\<br\\>\\<\\/span\\>\\<div\\>\\<span style\\=\\"background\\-color\\: rgb\\(255\\, 255\\, 255\\)\\;\\"\\>\\<span style\\=\\"font\\-family\\: Arial\\; white\\-space\\: pre\\-wrap\\;\\"\\>Patients will not be turned away due to inability to pay\\.\\<\\/span\\>\\<span style\\=\\"font\\-family\\: Arial\\; white\\-space\\: pre\\-wrap\\;\\"\\>\\<br\\>\\<\\/span\\>\\<\\/span\\>\\<\\/div\\>\\<div\\>\\<span style\\=\\"font\\-family\\: Arial\\; white\\-space\\: pre\\-wrap\\; background\\-color\\: rgb\\(255\\, 255\\, 255\\)\\;\\"\\>\\$25 fee if patient has no insurance\\.\\<\\/span\\>\\<span style\\=\\"font\\-family\\: Arial\\; font\\-size\\: 15px\\; white\\-space\\: pre\\-wrap\\; background\\-color\\: rgb\\(255\\, 255\\, 255\\)\\;\\"\\>\\<br\\>\\<\\/span\\>\\<\\/div\\>',
					'minor_access'   => '\\<font face\\=\\"Arial\\"\\>\\<span style\\=\\"white\\-space\\: pre\\-wrap\\; background\\-color\\: rgb\\(255\\, 255\\, 255\\)\\;\\"\\>Parent permission required\\. \\*\\<\\/span\\>\\<span style\\=\\"white\\-space\\: pre\\-wrap\\; background\\-color\\: rgb\\(255\\, 255\\, 255\\)\\;\\"\\>STD\\/STI and pregnancy testing are available without parent permission\\. Walk\\-Ins available for STD\\/STI testing\\.\\<\\/span\\>\\<\\/font\\>',
					'appointments'   => '\\<span style\\=\\"color\\: rgb\\(0\\, 0\\, 0\\)\\; font\\-weight\\: 400\\; text\\-decoration\\: none\\; font\\-family\\: Arial\\; font\\-style\\: normal\\;\\"\\>Patients advised to call ahead\\.\\<\\/span\\>',
					'order'          => null,
					'notes'          => null,
					'created_at'     => '2017-08-25 15:40:32',
					'updated_at'     => '2017-08-29 09:36:09',
				],
			58 =>
				[
					'id'             => 62,
					'uid'            => null,
					'name'           => 'Personal Growth Associates',
					'tagline'        => null,
					'organization'   => null,
					'address1'       => '919 N\\. Plum Grove Road',
					'address2'       => 'Suite C',
					'city'           => 'Schaumburg',
					'state_id'       => 13,
					'zipcode'        => '60173',
					'auto_gps'       => 1,
					'latitude'       => '42.0455218',
					'longitude'      => '-88.0571969',
					'url'            => '',
					'email'          => '',
					'website'        => 'http\\:\\/\\/www\\.personalgrowthassoc\\.com',
					'phone'          => '8474139700',
					'247_hotline'    => null,
					'text_line'      => null,
					'live_chat'      => '',
					'youth_approved' => 0,
					'hours'          => '\\<style type\\=\\"text\\/css\\"\\>\\<\\!\\-\\-td \\{border\\: 1px solid \\#ccc\\;\\}br \\{mso\\-data\\-placement\\:same\\-cell\\;\\}\\-\\-\\>\\<\\/style\\>\\<span style\\=\\"font\\-family\\: Arial\\;\\" data\\-sheets\\-value\\=\\"\\{\\" 1\\"\\:2\\,\\"2\\"\\:\\"mon\\,\\=\\"\\" thu\\,\\=\\"\\" fri\\:\\=\\"\\" 9am\\-3pm\\,\\=\\"\\" tue\\,\\=\\"\\" wed\\:\\=\\"\\" 9am\\-6pm\\,\\=\\"\\" sat\\,\\=\\"\\" sun\\:\\=\\"\\" closed\\"\\}\\"\\=\\"\\" data\\-sheets\\-userformat\\=\\"\\{\\" 2\\"\\:13185\\,\\"3\\"\\:\\[null\\,0\\]\\,\\"10\\"\\:2\\,\\"11\\"\\:4\\,\\"12\\"\\:0\\,\\"15\\"\\:\\"arial\\"\\,\\"16\\"\\:11\\}\\"\\=\\"\\"\\>Mon\\, Thu\\, Fri\\: 9AM \\- 3PM\\<\\/span\\>\\<div\\>\\<span style\\=\\"font\\-family\\: Arial\\;\\" data\\-sheets\\-value\\=\\"\\{\\" 1\\"\\:2\\,\\"2\\"\\:\\"mon\\,\\=\\"\\" thu\\,\\=\\"\\" fri\\:\\=\\"\\" 9am\\-3pm\\,\\=\\"\\" tue\\,\\=\\"\\" wed\\:\\=\\"\\" 9am\\-6pm\\,\\=\\"\\" sat\\,\\=\\"\\" sun\\:\\=\\"\\" closed\\"\\}\\"\\=\\"\\" data\\-sheets\\-userformat\\=\\"\\{\\" 2\\"\\:13185\\,\\"3\\"\\:\\[null\\,0\\]\\,\\"10\\"\\:2\\,\\"11\\"\\:4\\,\\"12\\"\\:0\\,\\"15\\"\\:\\"arial\\"\\,\\"16\\"\\:11\\}\\"\\=\\"\\"\\>Tue\\, Wed\\: 9AM \\- 6PM\\<\\/span\\>\\<\\/div\\>\\<div\\>\\<span style\\=\\"font\\-family\\: Arial\\;\\" data\\-sheets\\-value\\=\\"\\{\\" 1\\"\\:2\\,\\"2\\"\\:\\"mon\\,\\=\\"\\" thu\\,\\=\\"\\" fri\\:\\=\\"\\" 9am\\-3pm\\,\\=\\"\\" tue\\,\\=\\"\\" wed\\:\\=\\"\\" 9am\\-6pm\\,\\=\\"\\" sat\\,\\=\\"\\" sun\\:\\=\\"\\" closed\\"\\}\\"\\=\\"\\" data\\-sheets\\-userformat\\=\\"\\{\\" 2\\"\\:13185\\,\\"3\\"\\:\\[null\\,0\\]\\,\\"10\\"\\:2\\,\\"11\\"\\:4\\,\\"12\\"\\:0\\,\\"15\\"\\:\\"arial\\"\\,\\"16\\"\\:11\\}\\"\\=\\"\\"\\>Sat\\, Sun\\: Closed\\<\\/span\\>\\<br\\>\\<\\/div\\>',
					'image'          => null,
					'description'    => '\\<style type\\=\\"text\\/css\\"\\>\\<\\!\\-\\-td \\{border\\: 1px solid \\#ccc\\;\\}br \\{mso\\-data\\-placement\\:same\\-cell\\;\\}\\-\\-\\>\\<\\/style\\>\\<span style\\=\\"font\\-family\\: Arial\\;\\" data\\-sheets\\-value\\=\\"\\{\\" 1\\"\\:2\\,\\"2\\"\\:\\"treatment\\=\\"\\" for\\:\\=\\"\\" depression\\,\\=\\"\\" anger\\,\\=\\"\\" anxiety\\=\\"\\" related\\=\\"\\" disorders\\,\\=\\"\\" family\\=\\"\\" therapy\\,\\=\\"\\" medication\\=\\"\\" management\\,\\=\\"\\" adhd\\=\\"\\" evaluations\\,\\=\\"\\" chemical\\=\\"\\" dependency\\=\\"\\" assessments\\,\\=\\"\\" psychiatric\\=\\"\\" services\\,\\=\\"\\" psychological\\=\\"\\" testing\\"\\}\\"\\=\\"\\" data\\-sheets\\-userformat\\=\\"\\{\\" 2\\"\\:13187\\,\\"3\\"\\:\\[null\\,0\\]\\,\\"4\\"\\:\\[null\\,2\\,16573901\\]\\,\\"10\\"\\:2\\,\\"11\\"\\:4\\,\\"12\\"\\:0\\,\\"15\\"\\:\\"arial\\"\\,\\"16\\"\\:11\\}\\"\\=\\"\\"\\>Treatment for\\: Depression\\, Anger\\, Anxiety Related Disorders\\, Family Therapy\\, Medication Management\\, ADHD Evaluations\\, Chemical Dependency Assessments\\, Psychiatric Services\\, Psychological Testing\\<\\/span\\>\\<div\\>\\<span style\\=\\"font\\-size\\: 11pt\\; font\\-family\\: Arial\\;\\" data\\-sheets\\-value\\=\\"\\{\\" 1\\"\\:2\\,\\"2\\"\\:\\"treatment\\=\\"\\" for\\:\\=\\"\\" depression\\,\\=\\"\\" anger\\,\\=\\"\\" anxiety\\=\\"\\" related\\=\\"\\" disorders\\,\\=\\"\\" family\\=\\"\\" therapy\\,\\=\\"\\" medication\\=\\"\\" management\\,\\=\\"\\" adhd\\=\\"\\" evaluations\\,\\=\\"\\" chemical\\=\\"\\" dependency\\=\\"\\" assessments\\,\\=\\"\\" psychiatric\\=\\"\\" services\\,\\=\\"\\" psychological\\=\\"\\" testing\\"\\}\\"\\=\\"\\" data\\-sheets\\-userformat\\=\\"\\{\\" 2\\"\\:13187\\,\\"3\\"\\:\\[null\\,0\\]\\,\\"4\\"\\:\\[null\\,2\\,16573901\\]\\,\\"10\\"\\:2\\,\\"11\\"\\:4\\,\\"12\\"\\:0\\,\\"15\\"\\:\\"arial\\"\\,\\"16\\"\\:11\\}\\"\\=\\"\\"\\>\\<br\\>\\<\\/span\\>\\<\\/div\\>\\<div\\>\\<span style\\=\\"font\\-size\\: 11pt\\; font\\-family\\: Arial\\;\\" data\\-sheets\\-value\\=\\"\\{\\" 1\\"\\:2\\,\\"2\\"\\:\\"treatment\\=\\"\\" for\\:\\=\\"\\" depression\\,\\=\\"\\" anger\\,\\=\\"\\" anxiety\\=\\"\\" related\\=\\"\\" disorders\\,\\=\\"\\" family\\=\\"\\" therapy\\,\\=\\"\\" medication\\=\\"\\" management\\,\\=\\"\\" adhd\\=\\"\\" evaluations\\,\\=\\"\\" chemical\\=\\"\\" dependency\\=\\"\\" assessments\\,\\=\\"\\" psychiatric\\=\\"\\" services\\,\\=\\"\\" psychological\\=\\"\\" testing\\"\\}\\"\\=\\"\\" data\\-sheets\\-userformat\\=\\"\\{\\" 2\\"\\:13187\\,\\"3\\"\\:\\[null\\,0\\]\\,\\"4\\"\\:\\[null\\,2\\,16573901\\]\\,\\"10\\"\\:2\\,\\"11\\"\\:4\\,\\"12\\"\\:0\\,\\"15\\"\\:\\"arial\\"\\,\\"16\\"\\:11\\}\\"\\=\\"\\"\\>\\<br\\>\\<\\/span\\>\\<\\/div\\>',
					'minor_access'   => '\\<div\\>\\<style type\\=\\"text\\/css\\"\\>\\<\\!\\-\\-td \\{border\\: 1px solid \\#ccc\\;\\}br \\{mso\\-data\\-placement\\:same\\-cell\\;\\}\\-\\-\\>\\<\\/style\\>\\<span style\\=\\"font\\-family\\: Arial\\;\\" data\\-sheets\\-value\\=\\"\\{\\" 1\\"\\:2\\,\\"2\\"\\:\\"some\\=\\"\\" therapists\\=\\"\\" do\\=\\"\\" not\\=\\"\\" require\\=\\"\\" parent\\=\\"\\" permission\\=\\"\\" but\\=\\"\\" most\\=\\"\\" do\\.\\"\\}\\"\\=\\"\\" data\\-sheets\\-userformat\\=\\"\\{\\" 2\\"\\:13185\\,\\"3\\"\\:\\[null\\,0\\]\\,\\"10\\"\\:2\\,\\"11\\"\\:4\\,\\"12\\"\\:0\\,\\"15\\"\\:\\"arial\\"\\,\\"16\\"\\:11\\}\\"\\=\\"\\"\\>Some therapists do not require parent permission\\, but most do\\.\\<\\/span\\>\\<br\\>\\<\\/div\\>',
					'appointments'   => '\\<style type\\=\\"text\\/css\\"\\>\\<\\!\\-\\-td \\{border\\: 1px solid \\#ccc\\;\\}br \\{mso\\-data\\-placement\\:same\\-cell\\;\\}\\-\\-\\>\\<\\/style\\>\\<span style\\=\\"font\\-family\\: Arial\\;\\" data\\-sheets\\-value\\=\\"\\{\\" 1\\"\\:2\\,\\"2\\"\\:\\"patients\\=\\"\\" advised\\=\\"\\" to\\=\\"\\" call\\=\\"\\" ahead\\"\\}\\"\\=\\"\\" data\\-sheets\\-userformat\\=\\"\\{\\" 2\\"\\:13185\\,\\"3\\"\\:\\[null\\,0\\]\\,\\"10\\"\\:2\\,\\"11\\"\\:4\\,\\"12\\"\\:0\\,\\"15\\"\\:\\"arial\\"\\,\\"16\\"\\:11\\}\\"\\=\\"\\"\\>Patients advised to call ahead to make an appointment\\.\\<\\/span\\>',
					'order'          => null,
					'notes'          => null,
					'created_at'     => '2017-08-29 11:12:08',
					'updated_at'     => '2017-08-29 11:49:13',
				],
			59 =>
				[
					'id'             => 65,
					'uid'            => null,
					'name'           => 'Northwest Center Against Sexual Assault',
					'tagline'        => null,
					'organization'   => null,
					'address1'       => '415 W\\. Golf Road',
					'address2'       => 'Suite 47',
					'city'           => 'Arlington Heights',
					'state_id'       => 13,
					'zipcode'        => '60005',
					'auto_gps'       => 1,
					'latitude'       => '42.0502049',
					'longitude'      => '-87.9877932',
					'url'            => '',
					'email'          => '',
					'website'        => 'http\\:\\/\\/www\\.nwcasa\\.org',
					'phone'          => '8478066526',
					'247_hotline'    => '8888028890',
					'text_line'      => null,
					'live_chat'      => '',
					'youth_approved' => 0,
					'hours'          => '\\<style type\\=\\"text\\/css\\"\\>\\<\\!\\-\\-td \\{border\\: 1px solid \\#ccc\\;\\}br \\{mso\\-data\\-placement\\:same\\-cell\\;\\}\\-\\-\\>\\<\\/style\\>\\<span style\\=\\"font\\-family\\: Arial\\;\\" data\\-sheets\\-value\\=\\"\\{\\" 1\\"\\:2\\,\\"2\\"\\:\\"mon\\-fri\\:\\=\\"\\" 9am\\-5pm\\=\\"\\" \\"\\}\\"\\=\\"\\" data\\-sheets\\-userformat\\=\\"\\{\\" 2\\"\\:13185\\,\\"3\\"\\:\\[null\\,0\\]\\,\\"10\\"\\:2\\,\\"11\\"\\:4\\,\\"12\\"\\:0\\,\\"15\\"\\:\\"arial\\"\\,\\"16\\"\\:11\\}\\"\\=\\"\\"\\>Mon\\-Fri\\: 9AM \\- 5PM\\&nbsp\\;\\<\\/span\\>',
					'image'          => null,
					'description'    => '\\<style type\\=\\"text\\/css\\"\\>\\<\\!\\-\\-td \\{border\\: 1px solid \\#ccc\\;\\}br \\{mso\\-data\\-placement\\:same\\-cell\\;\\}\\-\\-\\>\\<\\/style\\>\\<span style\\=\\"font\\-family\\: Arial\\;\\" data\\-sheets\\-value\\=\\"\\{\\" 1\\"\\:2\\,\\"2\\"\\:\\"medical\\=\\"\\" advocacy\\,\\=\\"\\" legal\\=\\"\\" crisis\\=\\"\\" intervention\\,\\=\\"\\" counseling\\,\\=\\"\\" prevention\\,\\=\\"\\" community\\=\\"\\" education\\=\\"\\" programs\\"\\}\\"\\=\\"\\" data\\-sheets\\-userformat\\=\\"\\{\\" 2\\"\\:13187\\,\\"3\\"\\:\\[null\\,0\\]\\,\\"4\\"\\:\\[null\\,2\\,16573901\\]\\,\\"10\\"\\:2\\,\\"11\\"\\:4\\,\\"12\\"\\:0\\,\\"15\\"\\:\\"arial\\"\\,\\"16\\"\\:11\\}\\"\\=\\"\\"\\>Medical Advocacy\\, Legal Advocacy\\, Crisis Intervention\\, Counseling\\, Prevention\\, Community Education Programs\\.\\<\\/span\\>\\<div\\>\\<span style\\=\\"font\\-family\\: Arial\\;\\" data\\-sheets\\-value\\=\\"\\{\\" 1\\"\\:2\\,\\"2\\"\\:\\"medical\\=\\"\\" advocacy\\,\\=\\"\\" legal\\=\\"\\" crisis\\=\\"\\" intervention\\,\\=\\"\\" counseling\\,\\=\\"\\" prevention\\,\\=\\"\\" community\\=\\"\\" education\\=\\"\\" programs\\"\\}\\"\\=\\"\\" data\\-sheets\\-userformat\\=\\"\\{\\" 2\\"\\:13187\\,\\"3\\"\\:\\[null\\,0\\]\\,\\"4\\"\\:\\[null\\,2\\,16573901\\]\\,\\"10\\"\\:2\\,\\"11\\"\\:4\\,\\"12\\"\\:0\\,\\"15\\"\\:\\"arial\\"\\,\\"16\\"\\:11\\}\\"\\=\\"\\"\\>\\<br\\>\\<\\/span\\>\\<\\/div\\>\\<div\\>\\<span style\\=\\"font\\-family\\: Arial\\;\\" data\\-sheets\\-value\\=\\"\\{\\" 1\\"\\:2\\,\\"2\\"\\:\\"medical\\=\\"\\" advocacy\\,\\=\\"\\" legal\\=\\"\\" crisis\\=\\"\\" intervention\\,\\=\\"\\" counseling\\,\\=\\"\\" prevention\\,\\=\\"\\" community\\=\\"\\" education\\=\\"\\" programs\\"\\}\\"\\=\\"\\" data\\-sheets\\-userformat\\=\\"\\{\\" 2\\"\\:13187\\,\\"3\\"\\:\\[null\\,0\\]\\,\\"4\\"\\:\\[null\\,2\\,16573901\\]\\,\\"10\\"\\:2\\,\\"11\\"\\:4\\,\\"12\\"\\:0\\,\\"15\\"\\:\\"arial\\"\\,\\"16\\"\\:11\\}\\"\\=\\"\\"\\>All services are free\\.\\<\\/span\\>\\<\\/div\\>',
					'minor_access'   => '\\<style type\\=\\"text\\/css\\"\\>\\<\\!\\-\\-td \\{border\\: 1px solid \\#ccc\\;\\}br \\{mso\\-data\\-placement\\:same\\-cell\\;\\}\\-\\-\\>\\<\\/style\\>\\<span style\\=\\"font\\-family\\: Arial\\;\\" data\\-sheets\\-value\\=\\"\\{\\" 1\\"\\:2\\,\\"2\\"\\:\\"parent\\=\\"\\" permission\\=\\"\\" required\\=\\"\\" after\\=\\"\\" 5\\=\\"\\" sessions\\=\\"\\" of\\=\\"\\" treatment\\.\\"\\}\\"\\=\\"\\" data\\-sheets\\-userformat\\=\\"\\{\\" 2\\"\\:13185\\,\\"3\\"\\:\\[null\\,0\\]\\,\\"10\\"\\:2\\,\\"11\\"\\:4\\,\\"12\\"\\:0\\,\\"15\\"\\:\\"arial\\"\\,\\"16\\"\\:11\\}\\"\\=\\"\\"\\>Parent permission required after 5 sessions of treatment\\.\\<\\/span\\>',
					'appointments'   => '\\<style type\\=\\"text\\/css\\"\\>\\<\\!\\-\\-td \\{border\\: 1px solid \\#ccc\\;\\}br \\{mso\\-data\\-placement\\:same\\-cell\\;\\}\\-\\-\\>\\<\\/style\\>\\<span style\\=\\"font\\-family\\: Arial\\;\\" data\\-sheets\\-value\\=\\"\\{\\" 1\\"\\:2\\,\\"2\\"\\:\\"patients\\=\\"\\" advised\\=\\"\\" to\\=\\"\\" call\\=\\"\\" ahead\\"\\}\\"\\=\\"\\" data\\-sheets\\-userformat\\=\\"\\{\\" 2\\"\\:13185\\,\\"3\\"\\:\\[null\\,0\\]\\,\\"10\\"\\:2\\,\\"11\\"\\:4\\,\\"12\\"\\:0\\,\\"15\\"\\:\\"arial\\"\\,\\"16\\"\\:11\\}\\"\\=\\"\\"\\>Patients advised to call ahead and make an appointment\\.\\<\\/span\\>',
					'order'          => null,
					'notes'          => null,
					'created_at'     => '2017-08-29 11:46:32',
					'updated_at'     => '2017-08-29 11:50:08',
				],
			60 =>
				[
					'id'             => 64,
					'uid'            => null,
					'name'           => 'Hoffman Estates Police Department',
					'tagline'        => null,
					'organization'   => null,
					'address1'       => '411 W Higgins Rd',
					'address2'       => null,
					'city'           => 'Hoffman Estates',
					'state_id'       => 13,
					'zipcode'        => '60169',
					'auto_gps'       => 1,
					'latitude'       => '42.0458271',
					'longitude'      => '-88.0902252',
					'url'            => '',
					'email'          => '',
					'website'        => 'http\\:\\/\\/www\\.hoffmanestates\\.com',
					'phone'          => '8477812800',
					'247_hotline'    => null,
					'text_line'      => null,
					'live_chat'      => '',
					'youth_approved' => 0,
					'hours'          => '\\<font face\\=\\"Arial\\"\\>Open 24\\/7\\<\\/font\\>',
					'image'          => null,
					'description'    => '\\<style type\\=\\"text\\/css\\"\\>\\<\\!\\-\\-td \\{border\\: 1px solid \\#ccc\\;\\}br \\{mso\\-data\\-placement\\:same\\-cell\\;\\}\\-\\-\\>\\<\\/style\\>\\<span style\\=\\"font\\-family\\: Arial\\;\\" data\\-sheets\\-value\\=\\"\\{\\" 1\\"\\:2\\,\\"2\\"\\:\\"law\\=\\"\\" enforcement\\,\\=\\"\\" traffic\\,\\=\\"\\" crime\\=\\"\\" prevention\\,\\=\\"\\" neighborhood\\=\\"\\" watch\\"\\}\\"\\=\\"\\" data\\-sheets\\-userformat\\=\\"\\{\\" 2\\"\\:13187\\,\\"3\\"\\:\\[null\\,0\\]\\,\\"4\\"\\:\\[null\\,2\\,16573901\\]\\,\\"10\\"\\:2\\,\\"11\\"\\:4\\,\\"12\\"\\:0\\,\\"15\\"\\:\\"arial\\"\\,\\"16\\"\\:11\\}\\"\\=\\"\\"\\>Law Enforcement\\, Traffic\\, Crime Prevention\\, Neighborhood Watch\\&nbsp\\;\\<\\/span\\>',
					'minor_access'   => '\\<font face\\=\\"Arial\\"\\>Parent permission is not required\\<\\/font\\>',
					'appointments'   => '\\<font face\\=\\"Arial\\"\\>Walk\\-ins accepted\\<\\/font\\>',
					'order'          => null,
					'notes'          => null,
					'created_at'     => '2017-08-29 11:36:41',
					'updated_at'     => '2017-08-29 11:51:13',
				],
			61 =>
				[
					'id'             => 66,
					'uid'            => null,
					'name'           => 'The Harbour',
					'tagline'        => null,
					'organization'   => null,
					'address1'       => '1440 Renaissance Dr',
					'address2'       => 'Suite 240',
					'city'           => 'Park Ridge',
					'state_id'       => 13,
					'zipcode'        => '60068',
					'auto_gps'       => 1,
					'latitude'       => '42.0328213',
					'longitude'      => '-87.8652363',
					'url'            => '',
					'email'          => '',
					'website'        => 'http\\:\\/\\/www\\.theharbour\\.org',
					'phone'          => '8472978540',
					'247_hotline'    => '8472978541',
					'text_line'      => null,
					'live_chat'      => '',
					'youth_approved' => 0,
					'hours'          => '\\<style type\\=\\"text\\/css\\"\\>\\<\\!\\-\\-td \\{border\\: 1px solid \\#ccc\\;\\}br \\{mso\\-data\\-placement\\:same\\-cell\\;\\}\\-\\-\\>\\<\\/style\\>\\<span style\\=\\"font\\-family\\: Arial\\;\\" data\\-sheets\\-value\\=\\"\\{\\&quot\\;1\\&quot\\;\\:2\\,\\&quot\\;2\\&quot\\;\\:\\&quot\\;Mon\\-Fri\\: 9am\\-5pm\\&quot\\;\\}\\" data\\-sheets\\-userformat\\=\\"\\{\\&quot\\;2\\&quot\\;\\:13185\\,\\&quot\\;3\\&quot\\;\\:\\[null\\,0\\]\\,\\&quot\\;10\\&quot\\;\\:2\\,\\&quot\\;11\\&quot\\;\\:4\\,\\&quot\\;12\\&quot\\;\\:0\\,\\&quot\\;15\\&quot\\;\\:\\&quot\\;Arial\\&quot\\;\\,\\&quot\\;16\\&quot\\;\\:11\\}\\"\\>Mon\\-Fri\\: 9AM\\-5PM\\<\\/span\\>',
					'image'          => null,
					'description'    => '\\<style type\\=\\"text\\/css\\"\\>\\<\\!\\-\\-td \\{border\\: 1px solid \\#ccc\\;\\}br \\{mso\\-data\\-placement\\:same\\-cell\\;\\}\\-\\-\\>\\<\\/style\\>\\<span style\\=\\"font\\-family\\: Arial\\;\\" data\\-sheets\\-value\\=\\"\\{\\&quot\\;1\\&quot\\;\\:2\\,\\&quot\\;2\\&quot\\;\\:\\&quot\\;Emergency Shelter\\, Transitional Housing\\, Pregnant and Parenting Teen Services\\&quot\\;\\}\\" data\\-sheets\\-userformat\\=\\"\\{\\&quot\\;2\\&quot\\;\\:13187\\,\\&quot\\;3\\&quot\\;\\:\\[null\\,0\\]\\,\\&quot\\;4\\&quot\\;\\:\\[null\\,2\\,16573901\\]\\,\\&quot\\;10\\&quot\\;\\:2\\,\\&quot\\;11\\&quot\\;\\:4\\,\\&quot\\;12\\&quot\\;\\:0\\,\\&quot\\;15\\&quot\\;\\:\\&quot\\;Arial\\&quot\\;\\,\\&quot\\;16\\&quot\\;\\:11\\}\\"\\>Emergency Shelter\\, Transitional Housing\\, Pregnant and Parenting Teen Services\\<\\/span\\>\\<div\\>\\<span style\\=\\"font\\-family\\: Arial\\;\\" data\\-sheets\\-value\\=\\"\\{\\&quot\\;1\\&quot\\;\\:2\\,\\&quot\\;2\\&quot\\;\\:\\&quot\\;Emergency Shelter\\, Transitional Housing\\, Pregnant and Parenting Teen Services\\&quot\\;\\}\\" data\\-sheets\\-userformat\\=\\"\\{\\&quot\\;2\\&quot\\;\\:13187\\,\\&quot\\;3\\&quot\\;\\:\\[null\\,0\\]\\,\\&quot\\;4\\&quot\\;\\:\\[null\\,2\\,16573901\\]\\,\\&quot\\;10\\&quot\\;\\:2\\,\\&quot\\;11\\&quot\\;\\:4\\,\\&quot\\;12\\&quot\\;\\:0\\,\\&quot\\;15\\&quot\\;\\:\\&quot\\;Arial\\&quot\\;\\,\\&quot\\;16\\&quot\\;\\:11\\}\\"\\>\\<br\\>\\<\\/span\\>\\<\\/div\\>\\<div\\>\\<span style\\=\\"font\\-family\\: Arial\\;\\" data\\-sheets\\-value\\=\\"\\{\\&quot\\;1\\&quot\\;\\:2\\,\\&quot\\;2\\&quot\\;\\:\\&quot\\;Emergency Shelter\\, Transitional Housing\\, Pregnant and Parenting Teen Services\\&quot\\;\\}\\" data\\-sheets\\-userformat\\=\\"\\{\\&quot\\;2\\&quot\\;\\:13187\\,\\&quot\\;3\\&quot\\;\\:\\[null\\,0\\]\\,\\&quot\\;4\\&quot\\;\\:\\[null\\,2\\,16573901\\]\\,\\&quot\\;10\\&quot\\;\\:2\\,\\&quot\\;11\\&quot\\;\\:4\\,\\&quot\\;12\\&quot\\;\\:0\\,\\&quot\\;15\\&quot\\;\\:\\&quot\\;Arial\\&quot\\;\\,\\&quot\\;16\\&quot\\;\\:11\\}\\"\\>All services are free\\.\\<\\/span\\>\\<\\/div\\>',
					'minor_access'   => '\\<style type\\=\\"text\\/css\\"\\>\\<\\!\\-\\-td \\{border\\: 1px solid \\#ccc\\;\\}br \\{mso\\-data\\-placement\\:same\\-cell\\;\\}\\-\\-\\>\\<\\/style\\>\\<span style\\=\\"font\\-family\\: Arial\\;\\" data\\-sheets\\-value\\=\\"\\{\\&quot\\;1\\&quot\\;\\:2\\,\\&quot\\;2\\&quot\\;\\:\\&quot\\;Emergency housing available for youth without parent permission\\. For services\\, parent permission is required\\.\\&quot\\;\\}\\" data\\-sheets\\-userformat\\=\\"\\{\\&quot\\;2\\&quot\\;\\:13185\\,\\&quot\\;3\\&quot\\;\\:\\[null\\,0\\]\\,\\&quot\\;10\\&quot\\;\\:2\\,\\&quot\\;11\\&quot\\;\\:4\\,\\&quot\\;12\\&quot\\;\\:0\\,\\&quot\\;15\\&quot\\;\\:\\&quot\\;Arial\\&quot\\;\\,\\&quot\\;16\\&quot\\;\\:11\\}\\"\\>Emergency housing available for youth without parent permission\\. For services\\, parent permission is required\\.\\<\\/span\\>',
					'appointments'   => '\\<font face\\=\\"Arial\\"\\>Patients advised to call ahead to make an appointment\\.\\<\\/font\\>',
					'order'          => null,
					'notes'          => 'Intake Coordinator\\: 847\\.297\\.8540 ext\\. 129',
					'created_at'     => '2017-08-29 12:01:21',
					'updated_at'     => '2017-08-29 12:01:21',
				],
			62 =>
				[
					'id'             => 67,
					'uid'            => null,
					'name'           => 'Shelter\\, Inc\\.',
					'tagline'        => null,
					'organization'   => null,
					'address1'       => '1616 Arlington Heights Road',
					'address2'       => null,
					'city'           => 'Arlington Heights',
					'state_id'       => 13,
					'zipcode'        => '60004',
					'auto_gps'       => 1,
					'latitude'       => '42.1062242',
					'longitude'      => '-87.9808555',
					'url'            => '',
					'email'          => '',
					'website'        => 'http\\:\\/\\/www\\.shelter\\-inc\\.org',
					'phone'          => '8472558060',
					'247_hotline'    => '8472558060',
					'text_line'      => null,
					'live_chat'      => '',
					'youth_approved' => 0,
					'hours'          => '\\<font face\\=\\"Arial\\"\\>Open 24\\/7\\<\\/font\\>',
					'image'          => null,
					'description'    => '\\<style type\\=\\"text\\/css\\"\\>\\<\\!\\-\\-td \\{border\\: 1px solid \\#ccc\\;\\}br \\{mso\\-data\\-placement\\:same\\-cell\\;\\}\\-\\-\\>\\<\\/style\\>\\<span style\\=\\"font\\-family\\: Arial\\;\\" data\\-sheets\\-value\\=\\"\\{\\" 1\\"\\:2\\,\\"2\\"\\:\\"emergency\\=\\"\\" housing\\=\\"\\" services\\,\\=\\"\\" referral\\=\\"\\" transportation\\=\\"\\" to\\=\\"\\" school\\,\\=\\"\\" individual\\,\\=\\"\\" group\\,\\=\\"\\" family\\=\\"\\" counseling\\,\\=\\"\\" health\\=\\"\\" screening\\,\\=\\"\\" 30\\-day\\=\\"\\" follow\\-up\\=\\"\\" services\\=\\"\\" \\"\\}\\"\\=\\"\\" data\\-sheets\\-userformat\\=\\"\\{\\" 2\\"\\:13187\\,\\"3\\"\\:\\[null\\,0\\]\\,\\"4\\"\\:\\[null\\,2\\,16573901\\]\\,\\"10\\"\\:2\\,\\"11\\"\\:4\\,\\"12\\"\\:0\\,\\"15\\"\\:\\"arial\\"\\,\\"16\\"\\:11\\}\\"\\=\\"\\"\\>Emergency Housing Services\\, Referral Services\\, Transportation to School\\, Individual\\, Group\\, Family Counseling\\, Health Screening\\, 30\\-Day Follow\\-Up Services\\&nbsp\\;\\<\\/span\\>',
					'minor_access'   => '\\<font face\\=\\"Arial\\"\\>Parent permission required\\<\\/font\\>',
					'appointments'   => '\\<font face\\=\\"Arial\\"\\>Walk\\-ins accepted\\<\\/font\\>',
					'order'          => null,
					'notes'          => null,
					'created_at'     => '2017-08-29 12:09:44',
					'updated_at'     => '2017-08-29 12:09:57',
				],
			63 =>
				[
					'id'             => 68,
					'uid'            => null,
					'name'           => 'Catholic Charities of the Archdiocese of Chicago',
					'tagline'        => null,
					'organization'   => null,
					'address1'       => '1717 Rand Rd',
					'address2'       => null,
					'city'           => 'Des Plaines',
					'state_id'       => 13,
					'zipcode'        => '60016',
					'auto_gps'       => 1,
					'latitude'       => '42.0452218',
					'longitude'      => '-87.8815904',
					'url'            => '',
					'email'          => '',
					'website'        => 'http\\:\\/\\/www\\.catholiccharities\\.net',
					'phone'          => '8473762100',
					'247_hotline'    => null,
					'text_line'      => null,
					'live_chat'      => '',
					'youth_approved' => 0,
					'hours'          => '\\<style type\\=\\"text\\/css\\"\\>\\<\\!\\-\\-td \\{border\\: 1px solid \\#ccc\\;\\}br \\{mso\\-data\\-placement\\:same\\-cell\\;\\}\\-\\-\\>\\<\\/style\\>\\<span style\\=\\"font\\-family\\: Arial\\;\\" data\\-sheets\\-value\\=\\"\\{\\&quot\\;1\\&quot\\;\\:2\\,\\&quot\\;2\\&quot\\;\\:\\&quot\\;Evening Supper\\: Tue\\-Thu\\: 5pm\\-7pm\\, Food Pantry\\: Mon\\: 10am\\-12pm\\, 1pm\\-3pm\\, Wed\\: 10am\\-12pm\\, 1pm\\-6pm\\, Thu\\: 10am\\-12pm\\, 1pm\\-3pm\\&quot\\;\\}\\" data\\-sheets\\-userformat\\=\\"\\{\\&quot\\;2\\&quot\\;\\:13185\\,\\&quot\\;3\\&quot\\;\\:\\[null\\,0\\]\\,\\&quot\\;10\\&quot\\;\\:2\\,\\&quot\\;11\\&quot\\;\\:4\\,\\&quot\\;12\\&quot\\;\\:0\\,\\&quot\\;15\\&quot\\;\\:\\&quot\\;arial\\,sans\\,sans\\-serif\\&quot\\;\\,\\&quot\\;16\\&quot\\;\\:11\\}\\"\\>Evening Supper\\: Tue\\-Thu\\: 5PM\\-7PM\\,\\&nbsp\\;\\<\\/span\\>\\<div\\>\\<span style\\=\\"font\\-family\\: Arial\\;\\" data\\-sheets\\-value\\=\\"\\{\\&quot\\;1\\&quot\\;\\:2\\,\\&quot\\;2\\&quot\\;\\:\\&quot\\;Evening Supper\\: Tue\\-Thu\\: 5pm\\-7pm\\, Food Pantry\\: Mon\\: 10am\\-12pm\\, 1pm\\-3pm\\, Wed\\: 10am\\-12pm\\, 1pm\\-6pm\\, Thu\\: 10am\\-12pm\\, 1pm\\-3pm\\&quot\\;\\}\\" data\\-sheets\\-userformat\\=\\"\\{\\&quot\\;2\\&quot\\;\\:13185\\,\\&quot\\;3\\&quot\\;\\:\\[null\\,0\\]\\,\\&quot\\;10\\&quot\\;\\:2\\,\\&quot\\;11\\&quot\\;\\:4\\,\\&quot\\;12\\&quot\\;\\:0\\,\\&quot\\;15\\&quot\\;\\:\\&quot\\;arial\\,sans\\,sans\\-serif\\&quot\\;\\,\\&quot\\;16\\&quot\\;\\:11\\}\\"\\>Food Pantry\\: Mon\\: 10AM\\-12PM\\, 1PM\\-3PM\\,\\&nbsp\\;\\<\\/span\\>\\<\\/div\\>\\<div\\>\\<span style\\=\\"font\\-family\\: Arial\\;\\" data\\-sheets\\-value\\=\\"\\{\\&quot\\;1\\&quot\\;\\:2\\,\\&quot\\;2\\&quot\\;\\:\\&quot\\;Evening Supper\\: Tue\\-Thu\\: 5pm\\-7pm\\, Food Pantry\\: Mon\\: 10am\\-12pm\\, 1pm\\-3pm\\, Wed\\: 10am\\-12pm\\, 1pm\\-6pm\\, Thu\\: 10am\\-12pm\\, 1pm\\-3pm\\&quot\\;\\}\\" data\\-sheets\\-userformat\\=\\"\\{\\&quot\\;2\\&quot\\;\\:13185\\,\\&quot\\;3\\&quot\\;\\:\\[null\\,0\\]\\,\\&quot\\;10\\&quot\\;\\:2\\,\\&quot\\;11\\&quot\\;\\:4\\,\\&quot\\;12\\&quot\\;\\:0\\,\\&quot\\;15\\&quot\\;\\:\\&quot\\;arial\\,sans\\,sans\\-serif\\&quot\\;\\,\\&quot\\;16\\&quot\\;\\:11\\}\\"\\>Wed\\: 10AM\\-12PM\\, 1PM\\-6PM\\,\\&nbsp\\;\\<\\/span\\>\\<\\/div\\>\\<div\\>\\<span style\\=\\"font\\-family\\: Arial\\;\\" data\\-sheets\\-value\\=\\"\\{\\&quot\\;1\\&quot\\;\\:2\\,\\&quot\\;2\\&quot\\;\\:\\&quot\\;Evening Supper\\: Tue\\-Thu\\: 5pm\\-7pm\\, Food Pantry\\: Mon\\: 10am\\-12pm\\, 1pm\\-3pm\\, Wed\\: 10am\\-12pm\\, 1pm\\-6pm\\, Thu\\: 10am\\-12pm\\, 1pm\\-3pm\\&quot\\;\\}\\" data\\-sheets\\-userformat\\=\\"\\{\\&quot\\;2\\&quot\\;\\:13185\\,\\&quot\\;3\\&quot\\;\\:\\[null\\,0\\]\\,\\&quot\\;10\\&quot\\;\\:2\\,\\&quot\\;11\\&quot\\;\\:4\\,\\&quot\\;12\\&quot\\;\\:0\\,\\&quot\\;15\\&quot\\;\\:\\&quot\\;arial\\,sans\\,sans\\-serif\\&quot\\;\\,\\&quot\\;16\\&quot\\;\\:11\\}\\"\\>Thu\\: 10AM\\-12PM\\, 1PM\\-3PM\\<\\/span\\>\\<\\/div\\>',
					'image'          => null,
					'description'    => '\\<style type\\=\\"text\\/css\\"\\>\\<\\!\\-\\-td \\{border\\: 1px solid \\#ccc\\;\\}br \\{mso\\-data\\-placement\\:same\\-cell\\;\\}\\-\\-\\>\\<\\/style\\>\\<span style\\=\\"font\\-family\\: Arial\\;\\" data\\-sheets\\-value\\=\\"\\{\\&quot\\;1\\&quot\\;\\:2\\,\\&quot\\;2\\&quot\\;\\:\\&quot\\;Food Pantry\\, Dinner for the Homeless\\&quot\\;\\}\\" data\\-sheets\\-userformat\\=\\"\\{\\&quot\\;2\\&quot\\;\\:13187\\,\\&quot\\;3\\&quot\\;\\:\\[null\\,0\\]\\,\\&quot\\;4\\&quot\\;\\:\\[null\\,2\\,16573901\\]\\,\\&quot\\;10\\&quot\\;\\:2\\,\\&quot\\;11\\&quot\\;\\:4\\,\\&quot\\;12\\&quot\\;\\:0\\,\\&quot\\;15\\&quot\\;\\:\\&quot\\;Arial\\&quot\\;\\,\\&quot\\;16\\&quot\\;\\:11\\}\\"\\>Food Pantry\\, Dinner for the Homeless\\<\\/span\\>\\<div\\>\\<span style\\=\\"font\\-family\\: Arial\\;\\" data\\-sheets\\-value\\=\\"\\{\\&quot\\;1\\&quot\\;\\:2\\,\\&quot\\;2\\&quot\\;\\:\\&quot\\;Food Pantry\\, Dinner for the Homeless\\&quot\\;\\}\\" data\\-sheets\\-userformat\\=\\"\\{\\&quot\\;2\\&quot\\;\\:13187\\,\\&quot\\;3\\&quot\\;\\:\\[null\\,0\\]\\,\\&quot\\;4\\&quot\\;\\:\\[null\\,2\\,16573901\\]\\,\\&quot\\;10\\&quot\\;\\:2\\,\\&quot\\;11\\&quot\\;\\:4\\,\\&quot\\;12\\&quot\\;\\:0\\,\\&quot\\;15\\&quot\\;\\:\\&quot\\;Arial\\&quot\\;\\,\\&quot\\;16\\&quot\\;\\:11\\}\\"\\>\\<br\\>\\<\\/span\\>\\<\\/div\\>\\<div\\>\\<span style\\=\\"font\\-family\\: Arial\\;\\" data\\-sheets\\-value\\=\\"\\{\\&quot\\;1\\&quot\\;\\:2\\,\\&quot\\;2\\&quot\\;\\:\\&quot\\;Food Pantry\\, Dinner for the Homeless\\&quot\\;\\}\\" data\\-sheets\\-userformat\\=\\"\\{\\&quot\\;2\\&quot\\;\\:13187\\,\\&quot\\;3\\&quot\\;\\:\\[null\\,0\\]\\,\\&quot\\;4\\&quot\\;\\:\\[null\\,2\\,16573901\\]\\,\\&quot\\;10\\&quot\\;\\:2\\,\\&quot\\;11\\&quot\\;\\:4\\,\\&quot\\;12\\&quot\\;\\:0\\,\\&quot\\;15\\&quot\\;\\:\\&quot\\;Arial\\&quot\\;\\,\\&quot\\;16\\&quot\\;\\:11\\}\\"\\>All services are free\\.\\<\\/span\\>\\<\\/div\\>',
					'minor_access'   => '\\<font face\\=\\"Arial\\"\\>Parent permission not required\\.\\<\\/font\\>',
					'appointments'   => '\\<font face\\=\\"Arial\\"\\>Walk\\-ins accepted\\.\\<\\/font\\>',
					'order'          => null,
					'notes'          => null,
					'created_at'     => '2017-08-29 12:14:44',
					'updated_at'     => '2017-08-29 12:14:44',
				],
		]);
	}

	private function seedFacilityLanguagePivot()
	{
		\DB::table('facility_language')->delete();

		\DB::table('facility_language')->insert([
			0   =>
				[
					'id'          => 1,
					'facility_id' => 11,
					'language_id' => 1,
					'created_at'  => null,
					'updated_at'  => null,
				],
			1   =>
				[
					'id'          => 2,
					'facility_id' => 11,
					'language_id' => 27,
					'created_at'  => null,
					'updated_at'  => null,
				],
			2   =>
				[
					'id'          => 3,
					'facility_id' => 12,
					'language_id' => 1,
					'created_at'  => null,
					'updated_at'  => null,
				],
			3   =>
				[
					'id'          => 4,
					'facility_id' => 12,
					'language_id' => 27,
					'created_at'  => null,
					'updated_at'  => null,
				],
			4   =>
				[
					'id'          => 5,
					'facility_id' => 8,
					'language_id' => 1,
					'created_at'  => null,
					'updated_at'  => null,
				],
			5   =>
				[
					'id'          => 6,
					'facility_id' => 8,
					'language_id' => 40,
					'created_at'  => null,
					'updated_at'  => null,
				],
			6   =>
				[
					'id'          => 7,
					'facility_id' => 8,
					'language_id' => 27,
					'created_at'  => null,
					'updated_at'  => null,
				],
			7   =>
				[
					'id'          => 8,
					'facility_id' => 2,
					'language_id' => 6,
					'created_at'  => null,
					'updated_at'  => null,
				],
			8   =>
				[
					'id'          => 9,
					'facility_id' => 2,
					'language_id' => 1,
					'created_at'  => null,
					'updated_at'  => null,
				],
			9   =>
				[
					'id'          => 10,
					'facility_id' => 2,
					'language_id' => 87,
					'created_at'  => null,
					'updated_at'  => null,
				],
			10  =>
				[
					'id'          => 11,
					'facility_id' => 2,
					'language_id' => 27,
					'created_at'  => null,
					'updated_at'  => null,
				],
			11  =>
				[
					'id'          => 12,
					'facility_id' => 2,
					'language_id' => 119,
					'created_at'  => null,
					'updated_at'  => null,
				],
			12  =>
				[
					'id'          => 13,
					'facility_id' => 3,
					'language_id' => 6,
					'created_at'  => null,
					'updated_at'  => null,
				],
			13  =>
				[
					'id'          => 14,
					'facility_id' => 3,
					'language_id' => 1,
					'created_at'  => null,
					'updated_at'  => null,
				],
			14  =>
				[
					'id'          => 15,
					'facility_id' => 3,
					'language_id' => 87,
					'created_at'  => null,
					'updated_at'  => null,
				],
			15  =>
				[
					'id'          => 16,
					'facility_id' => 3,
					'language_id' => 107,
					'created_at'  => null,
					'updated_at'  => null,
				],
			16  =>
				[
					'id'          => 17,
					'facility_id' => 3,
					'language_id' => 27,
					'created_at'  => null,
					'updated_at'  => null,
				],
			31  =>
				[
					'id'          => 32,
					'facility_id' => 5,
					'language_id' => 1,
					'created_at'  => null,
					'updated_at'  => null,
				],
			32  =>
				[
					'id'          => 33,
					'facility_id' => 6,
					'language_id' => 1,
					'created_at'  => null,
					'updated_at'  => null,
				],
			33  =>
				[
					'id'          => 34,
					'facility_id' => 6,
					'language_id' => 27,
					'created_at'  => null,
					'updated_at'  => null,
				],
			34  =>
				[
					'id'          => 35,
					'facility_id' => 14,
					'language_id' => 1,
					'created_at'  => null,
					'updated_at'  => null,
				],
			35  =>
				[
					'id'          => 36,
					'facility_id' => 14,
					'language_id' => 27,
					'created_at'  => null,
					'updated_at'  => null,
				],
			36  =>
				[
					'id'          => 37,
					'facility_id' => 15,
					'language_id' => 1,
					'created_at'  => null,
					'updated_at'  => null,
				],
			37  =>
				[
					'id'          => 38,
					'facility_id' => 15,
					'language_id' => 27,
					'created_at'  => null,
					'updated_at'  => null,
				],
			38  =>
				[
					'id'          => 39,
					'facility_id' => 16,
					'language_id' => 1,
					'created_at'  => null,
					'updated_at'  => null,
				],
			39  =>
				[
					'id'          => 40,
					'facility_id' => 16,
					'language_id' => 27,
					'created_at'  => null,
					'updated_at'  => null,
				],
			40  =>
				[
					'id'          => 41,
					'facility_id' => 17,
					'language_id' => 1,
					'created_at'  => null,
					'updated_at'  => null,
				],
			41  =>
				[
					'id'          => 42,
					'facility_id' => 17,
					'language_id' => 27,
					'created_at'  => null,
					'updated_at'  => null,
				],
			42  =>
				[
					'id'          => 43,
					'facility_id' => 18,
					'language_id' => 1,
					'created_at'  => null,
					'updated_at'  => null,
				],
			43  =>
				[
					'id'          => 44,
					'facility_id' => 18,
					'language_id' => 27,
					'created_at'  => null,
					'updated_at'  => null,
				],
			44  =>
				[
					'id'          => 45,
					'facility_id' => 19,
					'language_id' => 1,
					'created_at'  => null,
					'updated_at'  => null,
				],
			45  =>
				[
					'id'          => 46,
					'facility_id' => 19,
					'language_id' => 87,
					'created_at'  => null,
					'updated_at'  => null,
				],
			46  =>
				[
					'id'          => 47,
					'facility_id' => 20,
					'language_id' => 1,
					'created_at'  => null,
					'updated_at'  => null,
				],
			47  =>
				[
					'id'          => 48,
					'facility_id' => 20,
					'language_id' => 27,
					'created_at'  => null,
					'updated_at'  => null,
				],
			48  =>
				[
					'id'          => 49,
					'facility_id' => 21,
					'language_id' => 1,
					'created_at'  => null,
					'updated_at'  => null,
				],
			49  =>
				[
					'id'          => 50,
					'facility_id' => 21,
					'language_id' => 87,
					'created_at'  => null,
					'updated_at'  => null,
				],
			50  =>
				[
					'id'          => 51,
					'facility_id' => 21,
					'language_id' => 27,
					'created_at'  => null,
					'updated_at'  => null,
				],
			51  =>
				[
					'id'          => 52,
					'facility_id' => 22,
					'language_id' => 1,
					'created_at'  => null,
					'updated_at'  => null,
				],
			52  =>
				[
					'id'          => 53,
					'facility_id' => 22,
					'language_id' => 27,
					'created_at'  => null,
					'updated_at'  => null,
				],
			53  =>
				[
					'id'          => 54,
					'facility_id' => 23,
					'language_id' => 12,
					'created_at'  => null,
					'updated_at'  => null,
				],
			54  =>
				[
					'id'          => 55,
					'facility_id' => 23,
					'language_id' => 1,
					'created_at'  => null,
					'updated_at'  => null,
				],
			55  =>
				[
					'id'          => 56,
					'facility_id' => 23,
					'language_id' => 40,
					'created_at'  => null,
					'updated_at'  => null,
				],
			56  =>
				[
					'id'          => 57,
					'facility_id' => 23,
					'language_id' => 94,
					'created_at'  => null,
					'updated_at'  => null,
				],
			57  =>
				[
					'id'          => 58,
					'facility_id' => 23,
					'language_id' => 27,
					'created_at'  => null,
					'updated_at'  => null,
				],
			58  =>
				[
					'id'          => 59,
					'facility_id' => 1,
					'language_id' => 1,
					'created_at'  => null,
					'updated_at'  => null,
				],
			59  =>
				[
					'id'          => 60,
					'facility_id' => 2,
					'language_id' => 137,
					'created_at'  => null,
					'updated_at'  => null,
				],
			63  =>
				[
					'id'          => 64,
					'facility_id' => 8,
					'language_id' => 136,
					'created_at'  => null,
					'updated_at'  => null,
				],
			64  =>
				[
					'id'          => 65,
					'facility_id' => 9,
					'language_id' => 1,
					'created_at'  => null,
					'updated_at'  => null,
				],
			65  =>
				[
					'id'          => 66,
					'facility_id' => 10,
					'language_id' => 1,
					'created_at'  => null,
					'updated_at'  => null,
				],
			66  =>
				[
					'id'          => 67,
					'facility_id' => 10,
					'language_id' => 27,
					'created_at'  => null,
					'updated_at'  => null,
				],
			67  =>
				[
					'id'          => 68,
					'facility_id' => 24,
					'language_id' => 1,
					'created_at'  => null,
					'updated_at'  => null,
				],
			68  =>
				[
					'id'          => 69,
					'facility_id' => 24,
					'language_id' => 27,
					'created_at'  => null,
					'updated_at'  => null,
				],
			69  =>
				[
					'id'          => 70,
					'facility_id' => 26,
					'language_id' => 1,
					'created_at'  => null,
					'updated_at'  => null,
				],
			70  =>
				[
					'id'          => 71,
					'facility_id' => 26,
					'language_id' => 42,
					'created_at'  => null,
					'updated_at'  => null,
				],
			71  =>
				[
					'id'          => 72,
					'facility_id' => 26,
					'language_id' => 87,
					'created_at'  => null,
					'updated_at'  => null,
				],
			72  =>
				[
					'id'          => 73,
					'facility_id' => 26,
					'language_id' => 27,
					'created_at'  => null,
					'updated_at'  => null,
				],
			73  =>
				[
					'id'          => 74,
					'facility_id' => 26,
					'language_id' => 127,
					'created_at'  => null,
					'updated_at'  => null,
				],
			74  =>
				[
					'id'          => 75,
					'facility_id' => 28,
					'language_id' => 1,
					'created_at'  => null,
					'updated_at'  => null,
				],
			75  =>
				[
					'id'          => 76,
					'facility_id' => 28,
					'language_id' => 27,
					'created_at'  => null,
					'updated_at'  => null,
				],
			76  =>
				[
					'id'          => 77,
					'facility_id' => 29,
					'language_id' => 1,
					'created_at'  => null,
					'updated_at'  => null,
				],
			77  =>
				[
					'id'          => 78,
					'facility_id' => 29,
					'language_id' => 27,
					'created_at'  => null,
					'updated_at'  => null,
				],
			78  =>
				[
					'id'          => 79,
					'facility_id' => 30,
					'language_id' => 1,
					'created_at'  => null,
					'updated_at'  => null,
				],
			79  =>
				[
					'id'          => 80,
					'facility_id' => 30,
					'language_id' => 87,
					'created_at'  => null,
					'updated_at'  => null,
				],
			80  =>
				[
					'id'          => 81,
					'facility_id' => 30,
					'language_id' => 27,
					'created_at'  => null,
					'updated_at'  => null,
				],
			81  =>
				[
					'id'          => 82,
					'facility_id' => 31,
					'language_id' => 1,
					'created_at'  => null,
					'updated_at'  => null,
				],
			82  =>
				[
					'id'          => 83,
					'facility_id' => 31,
					'language_id' => 87,
					'created_at'  => null,
					'updated_at'  => null,
				],
			83  =>
				[
					'id'          => 84,
					'facility_id' => 31,
					'language_id' => 94,
					'created_at'  => null,
					'updated_at'  => null,
				],
			84  =>
				[
					'id'          => 85,
					'facility_id' => 31,
					'language_id' => 27,
					'created_at'  => null,
					'updated_at'  => null,
				],
			85  =>
				[
					'id'          => 86,
					'facility_id' => 32,
					'language_id' => 1,
					'created_at'  => null,
					'updated_at'  => null,
				],
			86  =>
				[
					'id'          => 87,
					'facility_id' => 32,
					'language_id' => 27,
					'created_at'  => null,
					'updated_at'  => null,
				],
			87  =>
				[
					'id'          => 88,
					'facility_id' => 33,
					'language_id' => 1,
					'created_at'  => null,
					'updated_at'  => null,
				],
			88  =>
				[
					'id'          => 89,
					'facility_id' => 33,
					'language_id' => 27,
					'created_at'  => null,
					'updated_at'  => null,
				],
			89  =>
				[
					'id'          => 90,
					'facility_id' => 34,
					'language_id' => 136,
					'created_at'  => null,
					'updated_at'  => null,
				],
			90  =>
				[
					'id'          => 91,
					'facility_id' => 34,
					'language_id' => 1,
					'created_at'  => null,
					'updated_at'  => null,
				],
			91  =>
				[
					'id'          => 92,
					'facility_id' => 36,
					'language_id' => 1,
					'created_at'  => null,
					'updated_at'  => null,
				],
			92  =>
				[
					'id'          => 93,
					'facility_id' => 37,
					'language_id' => 1,
					'created_at'  => null,
					'updated_at'  => null,
				],
			93  =>
				[
					'id'          => 94,
					'facility_id' => 37,
					'language_id' => 87,
					'created_at'  => null,
					'updated_at'  => null,
				],
			94  =>
				[
					'id'          => 95,
					'facility_id' => 37,
					'language_id' => 27,
					'created_at'  => null,
					'updated_at'  => null,
				],
			95  =>
				[
					'id'          => 96,
					'facility_id' => 37,
					'language_id' => 127,
					'created_at'  => null,
					'updated_at'  => null,
				],
			96  =>
				[
					'id'          => 97,
					'facility_id' => 38,
					'language_id' => 1,
					'created_at'  => null,
					'updated_at'  => null,
				],
			97  =>
				[
					'id'          => 98,
					'facility_id' => 39,
					'language_id' => 1,
					'created_at'  => null,
					'updated_at'  => null,
				],
			98  =>
				[
					'id'          => 99,
					'facility_id' => 39,
					'language_id' => 27,
					'created_at'  => null,
					'updated_at'  => null,
				],
			99  =>
				[
					'id'          => 100,
					'facility_id' => 13,
					'language_id' => 1,
					'created_at'  => null,
					'updated_at'  => null,
				],
			100 =>
				[
					'id'          => 101,
					'facility_id' => 13,
					'language_id' => 27,
					'created_at'  => null,
					'updated_at'  => null,
				],
			101 =>
				[
					'id'          => 102,
					'facility_id' => 40,
					'language_id' => 1,
					'created_at'  => null,
					'updated_at'  => null,
				],
			102 =>
				[
					'id'          => 103,
					'facility_id' => 40,
					'language_id' => 27,
					'created_at'  => null,
					'updated_at'  => null,
				],
			105 =>
				[
					'id'          => 106,
					'facility_id' => 42,
					'language_id' => 1,
					'created_at'  => null,
					'updated_at'  => null,
				],
			106 =>
				[
					'id'          => 107,
					'facility_id' => 42,
					'language_id' => 27,
					'created_at'  => null,
					'updated_at'  => null,
				],
			109 =>
				[
					'id'          => 110,
					'facility_id' => 44,
					'language_id' => 1,
					'created_at'  => null,
					'updated_at'  => null,
				],
			110 =>
				[
					'id'          => 111,
					'facility_id' => 44,
					'language_id' => 27,
					'created_at'  => null,
					'updated_at'  => null,
				],
			113 =>
				[
					'id'          => 114,
					'facility_id' => 46,
					'language_id' => 1,
					'created_at'  => null,
					'updated_at'  => null,
				],
			114 =>
				[
					'id'          => 115,
					'facility_id' => 46,
					'language_id' => 27,
					'created_at'  => null,
					'updated_at'  => null,
				],
			115 =>
				[
					'id'          => 116,
					'facility_id' => 47,
					'language_id' => 1,
					'created_at'  => null,
					'updated_at'  => null,
				],
			116 =>
				[
					'id'          => 117,
					'facility_id' => 47,
					'language_id' => 27,
					'created_at'  => null,
					'updated_at'  => null,
				],
			117 =>
				[
					'id'          => 118,
					'facility_id' => 48,
					'language_id' => 6,
					'created_at'  => null,
					'updated_at'  => null,
				],
			118 =>
				[
					'id'          => 119,
					'facility_id' => 48,
					'language_id' => 1,
					'created_at'  => null,
					'updated_at'  => null,
				],
			119 =>
				[
					'id'          => 120,
					'facility_id' => 48,
					'language_id' => 87,
					'created_at'  => null,
					'updated_at'  => null,
				],
			120 =>
				[
					'id'          => 121,
					'facility_id' => 48,
					'language_id' => 107,
					'created_at'  => null,
					'updated_at'  => null,
				],
			121 =>
				[
					'id'          => 122,
					'facility_id' => 48,
					'language_id' => 27,
					'created_at'  => null,
					'updated_at'  => null,
				],
			122 =>
				[
					'id'          => 123,
					'facility_id' => 49,
					'language_id' => 1,
					'created_at'  => null,
					'updated_at'  => null,
				],
			123 =>
				[
					'id'          => 124,
					'facility_id' => 49,
					'language_id' => 27,
					'created_at'  => null,
					'updated_at'  => null,
				],
			124 =>
				[
					'id'          => 125,
					'facility_id' => 50,
					'language_id' => 1,
					'created_at'  => null,
					'updated_at'  => null,
				],
			125 =>
				[
					'id'          => 126,
					'facility_id' => 50,
					'language_id' => 27,
					'created_at'  => null,
					'updated_at'  => null,
				],
			126 =>
				[
					'id'          => 127,
					'facility_id' => 51,
					'language_id' => 1,
					'created_at'  => null,
					'updated_at'  => null,
				],
			127 =>
				[
					'id'          => 128,
					'facility_id' => 51,
					'language_id' => 27,
					'created_at'  => null,
					'updated_at'  => null,
				],
			128 =>
				[
					'id'          => 129,
					'facility_id' => 52,
					'language_id' => 1,
					'created_at'  => null,
					'updated_at'  => null,
				],
			129 =>
				[
					'id'          => 130,
					'facility_id' => 52,
					'language_id' => 27,
					'created_at'  => null,
					'updated_at'  => null,
				],
			130 =>
				[
					'id'          => 131,
					'facility_id' => 53,
					'language_id' => 140,
					'created_at'  => null,
					'updated_at'  => null,
				],
			131 =>
				[
					'id'          => 132,
					'facility_id' => 54,
					'language_id' => 140,
					'created_at'  => null,
					'updated_at'  => null,
				],
			132 =>
				[
					'id'          => 133,
					'facility_id' => 55,
					'language_id' => 1,
					'created_at'  => null,
					'updated_at'  => null,
				],
			133 =>
				[
					'id'          => 134,
					'facility_id' => 55,
					'language_id' => 27,
					'created_at'  => null,
					'updated_at'  => null,
				],
			134 =>
				[
					'id'          => 135,
					'facility_id' => 57,
					'language_id' => 1,
					'created_at'  => null,
					'updated_at'  => null,
				],
			135 =>
				[
					'id'          => 136,
					'facility_id' => 57,
					'language_id' => 27,
					'created_at'  => null,
					'updated_at'  => null,
				],
			136 =>
				[
					'id'          => 137,
					'facility_id' => 58,
					'language_id' => 1,
					'created_at'  => null,
					'updated_at'  => null,
				],
			137 =>
				[
					'id'          => 138,
					'facility_id' => 58,
					'language_id' => 27,
					'created_at'  => null,
					'updated_at'  => null,
				],
			138 =>
				[
					'id'          => 139,
					'facility_id' => 56,
					'language_id' => 1,
					'created_at'  => null,
					'updated_at'  => null,
				],
			139 =>
				[
					'id'          => 140,
					'facility_id' => 56,
					'language_id' => 27,
					'created_at'  => null,
					'updated_at'  => null,
				],
			140 =>
				[
					'id'          => 141,
					'facility_id' => 25,
					'language_id' => 1,
					'created_at'  => null,
					'updated_at'  => null,
				],
			141 =>
				[
					'id'          => 142,
					'facility_id' => 59,
					'language_id' => 1,
					'created_at'  => null,
					'updated_at'  => null,
				],
			142 =>
				[
					'id'          => 143,
					'facility_id' => 59,
					'language_id' => 27,
					'created_at'  => null,
					'updated_at'  => null,
				],
			143 =>
				[
					'id'          => 144,
					'facility_id' => 60,
					'language_id' => 1,
					'created_at'  => null,
					'updated_at'  => null,
				],
			144 =>
				[
					'id'          => 145,
					'facility_id' => 61,
					'language_id' => 6,
					'created_at'  => null,
					'updated_at'  => null,
				],
			145 =>
				[
					'id'          => 146,
					'facility_id' => 61,
					'language_id' => 1,
					'created_at'  => null,
					'updated_at'  => null,
				],
			146 =>
				[
					'id'          => 147,
					'facility_id' => 61,
					'language_id' => 87,
					'created_at'  => null,
					'updated_at'  => null,
				],
			147 =>
				[
					'id'          => 148,
					'facility_id' => 61,
					'language_id' => 27,
					'created_at'  => null,
					'updated_at'  => null,
				],
			148 =>
				[
					'id'          => 149,
					'facility_id' => 62,
					'language_id' => 1,
					'created_at'  => null,
					'updated_at'  => null,
				],
			149 =>
				[
					'id'          => 150,
					'facility_id' => 62,
					'language_id' => 94,
					'created_at'  => null,
					'updated_at'  => null,
				],
			150 =>
				[
					'id'          => 151,
					'facility_id' => 63,
					'language_id' => 1,
					'created_at'  => null,
					'updated_at'  => null,
				],
			151 =>
				[
					'id'          => 152,
					'facility_id' => 63,
					'language_id' => 27,
					'created_at'  => null,
					'updated_at'  => null,
				],
			152 =>
				[
					'id'          => 153,
					'facility_id' => 64,
					'language_id' => 1,
					'created_at'  => null,
					'updated_at'  => null,
				],
			153 =>
				[
					'id'          => 154,
					'facility_id' => 64,
					'language_id' => 27,
					'created_at'  => null,
					'updated_at'  => null,
				],
			154 =>
				[
					'id'          => 155,
					'facility_id' => 65,
					'language_id' => 1,
					'created_at'  => null,
					'updated_at'  => null,
				],
			155 =>
				[
					'id'          => 156,
					'facility_id' => 65,
					'language_id' => 27,
					'created_at'  => null,
					'updated_at'  => null,
				],
			156 =>
				[
					'id'          => 157,
					'facility_id' => 66,
					'language_id' => 1,
					'created_at'  => null,
					'updated_at'  => null,
				],
			157 =>
				[
					'id'          => 158,
					'facility_id' => 66,
					'language_id' => 27,
					'created_at'  => null,
					'updated_at'  => null,
				],
			158 =>
				[
					'id'          => 159,
					'facility_id' => 67,
					'language_id' => 1,
					'created_at'  => null,
					'updated_at'  => null,
				],
			159 =>
				[
					'id'          => 160,
					'facility_id' => 68,
					'language_id' => 1,
					'created_at'  => null,
					'updated_at'  => null,
				],
			160 =>
				[
					'id'          => 161,
					'facility_id' => 68,
					'language_id' => 27,
					'created_at'  => null,
					'updated_at'  => null,
				],
		]);
	}

	private function seedFacilityServicePivot()
	{
		\DB::table('facility_service')->delete();

		\DB::table('facility_service')->insert([
			0   =>
				[
					'id'          => 1,
					'facility_id' => 1,
					'service_id'  => 3,
					'created_at'  => null,
					'updated_at'  => null,
				],
			1   =>
				[
					'id'          => 49,
					'facility_id' => 1,
					'service_id'  => 14,
					'created_at'  => null,
					'updated_at'  => null,
				],
			2   =>
				[
					'id'          => 48,
					'facility_id' => 1,
					'service_id'  => 74,
					'created_at'  => null,
					'updated_at'  => null,
				],
			3   =>
				[
					'id'          => 4,
					'facility_id' => 2,
					'service_id'  => 5,
					'created_at'  => null,
					'updated_at'  => null,
				],
			4   =>
				[
					'id'          => 5,
					'facility_id' => 2,
					'service_id'  => 6,
					'created_at'  => null,
					'updated_at'  => null,
				],
			5   =>
				[
					'id'          => 6,
					'facility_id' => 3,
					'service_id'  => 6,
					'created_at'  => null,
					'updated_at'  => null,
				],
			6   =>
				[
					'id'          => 7,
					'facility_id' => 4,
					'service_id'  => 1,
					'created_at'  => null,
					'updated_at'  => null,
				],
			7   =>
				[
					'id'          => 8,
					'facility_id' => 4,
					'service_id'  => 6,
					'created_at'  => null,
					'updated_at'  => null,
				],
			8   =>
				[
					'id'          => 9,
					'facility_id' => 4,
					'service_id'  => 8,
					'created_at'  => null,
					'updated_at'  => null,
				],
			9   =>
				[
					'id'          => 10,
					'facility_id' => 4,
					'service_id'  => 9,
					'created_at'  => null,
					'updated_at'  => null,
				],
			10  =>
				[
					'id'          => 11,
					'facility_id' => 4,
					'service_id'  => 10,
					'created_at'  => null,
					'updated_at'  => null,
				],
			11  =>
				[
					'id'          => 50,
					'facility_id' => 5,
					'service_id'  => 30,
					'created_at'  => null,
					'updated_at'  => null,
				],
			12  =>
				[
					'id'          => 13,
					'facility_id' => 6,
					'service_id'  => 6,
					'created_at'  => null,
					'updated_at'  => null,
				],
			14  =>
				[
					'id'          => 45,
					'facility_id' => 8,
					'service_id'  => 36,
					'created_at'  => null,
					'updated_at'  => null,
				],
			15  =>
				[
					'id'          => 44,
					'facility_id' => 8,
					'service_id'  => 15,
					'created_at'  => null,
					'updated_at'  => null,
				],
			16  =>
				[
					'id'          => 43,
					'facility_id' => 8,
					'service_id'  => 16,
					'created_at'  => null,
					'updated_at'  => null,
				],
			17  =>
				[
					'id'          => 54,
					'facility_id' => 9,
					'service_id'  => 96,
					'created_at'  => null,
					'updated_at'  => null,
				],
			18  =>
				[
					'id'          => 53,
					'facility_id' => 9,
					'service_id'  => 25,
					'created_at'  => null,
					'updated_at'  => null,
				],
			19  =>
				[
					'id'          => 20,
					'facility_id' => 10,
					'service_id'  => 6,
					'created_at'  => null,
					'updated_at'  => null,
				],
			20  =>
				[
					'id'          => 21,
					'facility_id' => 11,
					'service_id'  => 13,
					'created_at'  => null,
					'updated_at'  => null,
				],
			21  =>
				[
					'id'          => 22,
					'facility_id' => 11,
					'service_id'  => 5,
					'created_at'  => null,
					'updated_at'  => null,
				],
			22  =>
				[
					'id'          => 33,
					'facility_id' => 11,
					'service_id'  => 16,
					'created_at'  => null,
					'updated_at'  => null,
				],
			23  =>
				[
					'id'          => 24,
					'facility_id' => 12,
					'service_id'  => 1,
					'created_at'  => null,
					'updated_at'  => null,
				],
			24  =>
				[
					'id'          => 25,
					'facility_id' => 12,
					'service_id'  => 2,
					'created_at'  => null,
					'updated_at'  => null,
				],
			25  =>
				[
					'id'          => 26,
					'facility_id' => 12,
					'service_id'  => 3,
					'created_at'  => null,
					'updated_at'  => null,
				],
			26  =>
				[
					'id'          => 27,
					'facility_id' => 12,
					'service_id'  => 4,
					'created_at'  => null,
					'updated_at'  => null,
				],
			27  =>
				[
					'id'          => 28,
					'facility_id' => 12,
					'service_id'  => 7,
					'created_at'  => null,
					'updated_at'  => null,
				],
			28  =>
				[
					'id'          => 29,
					'facility_id' => 12,
					'service_id'  => 24,
					'created_at'  => null,
					'updated_at'  => null,
				],
			29  =>
				[
					'id'          => 30,
					'facility_id' => 12,
					'service_id'  => 20,
					'created_at'  => null,
					'updated_at'  => null,
				],
			30  =>
				[
					'id'          => 31,
					'facility_id' => 12,
					'service_id'  => 11,
					'created_at'  => null,
					'updated_at'  => null,
				],
			31  =>
				[
					'id'          => 32,
					'facility_id' => 12,
					'service_id'  => 12,
					'created_at'  => null,
					'updated_at'  => null,
				],
			32  =>
				[
					'id'          => 34,
					'facility_id' => 11,
					'service_id'  => 76,
					'created_at'  => null,
					'updated_at'  => null,
				],
			33  =>
				[
					'id'          => 35,
					'facility_id' => 11,
					'service_id'  => 19,
					'created_at'  => null,
					'updated_at'  => null,
				],
			34  =>
				[
					'id'          => 36,
					'facility_id' => 11,
					'service_id'  => 17,
					'created_at'  => null,
					'updated_at'  => null,
				],
			35  =>
				[
					'id'          => 37,
					'facility_id' => 11,
					'service_id'  => 20,
					'created_at'  => null,
					'updated_at'  => null,
				],
			36  =>
				[
					'id'          => 38,
					'facility_id' => 13,
					'service_id'  => 38,
					'created_at'  => null,
					'updated_at'  => null,
				],
			37  =>
				[
					'id'          => 39,
					'facility_id' => 13,
					'service_id'  => 8,
					'created_at'  => null,
					'updated_at'  => null,
				],
			38  =>
				[
					'id'          => 40,
					'facility_id' => 13,
					'service_id'  => 24,
					'created_at'  => null,
					'updated_at'  => null,
				],
			39  =>
				[
					'id'          => 41,
					'facility_id' => 13,
					'service_id'  => 20,
					'created_at'  => null,
					'updated_at'  => null,
				],
			40  =>
				[
					'id'          => 42,
					'facility_id' => 13,
					'service_id'  => 36,
					'created_at'  => null,
					'updated_at'  => null,
				],
			41  =>
				[
					'id'          => 46,
					'facility_id' => 3,
					'service_id'  => 75,
					'created_at'  => null,
					'updated_at'  => null,
				],
			43  =>
				[
					'id'          => 51,
					'facility_id' => 5,
					'service_id'  => 81,
					'created_at'  => null,
					'updated_at'  => null,
				],
			44  =>
				[
					'id'          => 52,
					'facility_id' => 5,
					'service_id'  => 41,
					'created_at'  => null,
					'updated_at'  => null,
				],
			45  =>
				[
					'id'          => 55,
					'facility_id' => 9,
					'service_id'  => 61,
					'created_at'  => null,
					'updated_at'  => null,
				],
			46  =>
				[
					'id'          => 56,
					'facility_id' => 9,
					'service_id'  => 55,
					'created_at'  => null,
					'updated_at'  => null,
				],
			47  =>
				[
					'id'          => 57,
					'facility_id' => 6,
					'service_id'  => 84,
					'created_at'  => null,
					'updated_at'  => null,
				],
			48  =>
				[
					'id'          => 58,
					'facility_id' => 3,
					'service_id'  => 89,
					'created_at'  => null,
					'updated_at'  => null,
				],
			49  =>
				[
					'id'          => 59,
					'facility_id' => 14,
					'service_id'  => 75,
					'created_at'  => null,
					'updated_at'  => null,
				],
			50  =>
				[
					'id'          => 60,
					'facility_id' => 15,
					'service_id'  => 75,
					'created_at'  => null,
					'updated_at'  => null,
				],
			51  =>
				[
					'id'          => 61,
					'facility_id' => 16,
					'service_id'  => 75,
					'created_at'  => null,
					'updated_at'  => null,
				],
			52  =>
				[
					'id'          => 62,
					'facility_id' => 17,
					'service_id'  => 75,
					'created_at'  => null,
					'updated_at'  => null,
				],
			53  =>
				[
					'id'          => 63,
					'facility_id' => 18,
					'service_id'  => 16,
					'created_at'  => null,
					'updated_at'  => null,
				],
			54  =>
				[
					'id'          => 64,
					'facility_id' => 18,
					'service_id'  => 39,
					'created_at'  => null,
					'updated_at'  => null,
				],
			55  =>
				[
					'id'          => 65,
					'facility_id' => 18,
					'service_id'  => 23,
					'created_at'  => null,
					'updated_at'  => null,
				],
			56  =>
				[
					'id'          => 66,
					'facility_id' => 18,
					'service_id'  => 22,
					'created_at'  => null,
					'updated_at'  => null,
				],
			57  =>
				[
					'id'          => 67,
					'facility_id' => 18,
					'service_id'  => 15,
					'created_at'  => null,
					'updated_at'  => null,
				],
			58  =>
				[
					'id'          => 68,
					'facility_id' => 18,
					'service_id'  => 24,
					'created_at'  => null,
					'updated_at'  => null,
				],
			59  =>
				[
					'id'          => 69,
					'facility_id' => 18,
					'service_id'  => 20,
					'created_at'  => null,
					'updated_at'  => null,
				],
			60  =>
				[
					'id'          => 70,
					'facility_id' => 18,
					'service_id'  => 36,
					'created_at'  => null,
					'updated_at'  => null,
				],
			61  =>
				[
					'id'          => 71,
					'facility_id' => 19,
					'service_id'  => 39,
					'created_at'  => null,
					'updated_at'  => null,
				],
			62  =>
				[
					'id'          => 72,
					'facility_id' => 19,
					'service_id'  => 21,
					'created_at'  => null,
					'updated_at'  => null,
				],
			63  =>
				[
					'id'          => 73,
					'facility_id' => 19,
					'service_id'  => 97,
					'created_at'  => null,
					'updated_at'  => null,
				],
			64  =>
				[
					'id'          => 74,
					'facility_id' => 20,
					'service_id'  => 87,
					'created_at'  => null,
					'updated_at'  => null,
				],
			65  =>
				[
					'id'          => 75,
					'facility_id' => 20,
					'service_id'  => 16,
					'created_at'  => null,
					'updated_at'  => null,
				],
			66  =>
				[
					'id'          => 76,
					'facility_id' => 20,
					'service_id'  => 71,
					'created_at'  => null,
					'updated_at'  => null,
				],
			67  =>
				[
					'id'          => 77,
					'facility_id' => 20,
					'service_id'  => 21,
					'created_at'  => null,
					'updated_at'  => null,
				],
			68  =>
				[
					'id'          => 78,
					'facility_id' => 21,
					'service_id'  => 39,
					'created_at'  => null,
					'updated_at'  => null,
				],
			69  =>
				[
					'id'          => 79,
					'facility_id' => 21,
					'service_id'  => 21,
					'created_at'  => null,
					'updated_at'  => null,
				],
			70  =>
				[
					'id'          => 80,
					'facility_id' => 21,
					'service_id'  => 97,
					'created_at'  => null,
					'updated_at'  => null,
				],
			71  =>
				[
					'id'          => 81,
					'facility_id' => 22,
					'service_id'  => 48,
					'created_at'  => null,
					'updated_at'  => null,
				],
			72  =>
				[
					'id'          => 82,
					'facility_id' => 22,
					'service_id'  => 39,
					'created_at'  => null,
					'updated_at'  => null,
				],
			73  =>
				[
					'id'          => 83,
					'facility_id' => 22,
					'service_id'  => 45,
					'created_at'  => null,
					'updated_at'  => null,
				],
			74  =>
				[
					'id'          => 84,
					'facility_id' => 22,
					'service_id'  => 21,
					'created_at'  => null,
					'updated_at'  => null,
				],
			75  =>
				[
					'id'          => 85,
					'facility_id' => 23,
					'service_id'  => 16,
					'created_at'  => null,
					'updated_at'  => null,
				],
			76  =>
				[
					'id'          => 86,
					'facility_id' => 23,
					'service_id'  => 76,
					'created_at'  => null,
					'updated_at'  => null,
				],
			77  =>
				[
					'id'          => 87,
					'facility_id' => 23,
					'service_id'  => 39,
					'created_at'  => null,
					'updated_at'  => null,
				],
			78  =>
				[
					'id'          => 88,
					'facility_id' => 23,
					'service_id'  => 50,
					'created_at'  => null,
					'updated_at'  => null,
				],
			79  =>
				[
					'id'          => 89,
					'facility_id' => 23,
					'service_id'  => 15,
					'created_at'  => null,
					'updated_at'  => null,
				],
			80  =>
				[
					'id'          => 90,
					'facility_id' => 23,
					'service_id'  => 36,
					'created_at'  => null,
					'updated_at'  => null,
				],
			81  =>
				[
					'id'          => 91,
					'facility_id' => 1,
					'service_id'  => 96,
					'created_at'  => null,
					'updated_at'  => null,
				],
			82  =>
				[
					'id'          => 92,
					'facility_id' => 2,
					'service_id'  => 55,
					'created_at'  => null,
					'updated_at'  => null,
				],
			83  =>
				[
					'id'          => 93,
					'facility_id' => 2,
					'service_id'  => 13,
					'created_at'  => null,
					'updated_at'  => null,
				],
			84  =>
				[
					'id'          => 94,
					'facility_id' => 24,
					'service_id'  => 39,
					'created_at'  => null,
					'updated_at'  => null,
				],
			85  =>
				[
					'id'          => 95,
					'facility_id' => 24,
					'service_id'  => 45,
					'created_at'  => null,
					'updated_at'  => null,
				],
			86  =>
				[
					'id'          => 96,
					'facility_id' => 24,
					'service_id'  => 61,
					'created_at'  => null,
					'updated_at'  => null,
				],
			87  =>
				[
					'id'          => 97,
					'facility_id' => 24,
					'service_id'  => 60,
					'created_at'  => null,
					'updated_at'  => null,
				],
			88  =>
				[
					'id'          => 98,
					'facility_id' => 25,
					'service_id'  => 64,
					'created_at'  => null,
					'updated_at'  => null,
				],
			89  =>
				[
					'id'          => 99,
					'facility_id' => 25,
					'service_id'  => 39,
					'created_at'  => null,
					'updated_at'  => null,
				],
			90  =>
				[
					'id'          => 100,
					'facility_id' => 25,
					'service_id'  => 96,
					'created_at'  => null,
					'updated_at'  => null,
				],
			91  =>
				[
					'id'          => 101,
					'facility_id' => 25,
					'service_id'  => 99,
					'created_at'  => null,
					'updated_at'  => null,
				],
			92  =>
				[
					'id'          => 102,
					'facility_id' => 25,
					'service_id'  => 61,
					'created_at'  => null,
					'updated_at'  => null,
				],
			93  =>
				[
					'id'          => 103,
					'facility_id' => 25,
					'service_id'  => 49,
					'created_at'  => null,
					'updated_at'  => null,
				],
			94  =>
				[
					'id'          => 104,
					'facility_id' => 26,
					'service_id'  => 46,
					'created_at'  => null,
					'updated_at'  => null,
				],
			95  =>
				[
					'id'          => 105,
					'facility_id' => 26,
					'service_id'  => 98,
					'created_at'  => null,
					'updated_at'  => null,
				],
			96  =>
				[
					'id'          => 106,
					'facility_id' => 26,
					'service_id'  => 39,
					'created_at'  => null,
					'updated_at'  => null,
				],
			97  =>
				[
					'id'          => 107,
					'facility_id' => 26,
					'service_id'  => 45,
					'created_at'  => null,
					'updated_at'  => null,
				],
			98  =>
				[
					'id'          => 108,
					'facility_id' => 26,
					'service_id'  => 26,
					'created_at'  => null,
					'updated_at'  => null,
				],
			99  =>
				[
					'id'          => 109,
					'facility_id' => 26,
					'service_id'  => 50,
					'created_at'  => null,
					'updated_at'  => null,
				],
			100 =>
				[
					'id'          => 110,
					'facility_id' => 26,
					'service_id'  => 51,
					'created_at'  => null,
					'updated_at'  => null,
				],
			101 =>
				[
					'id'          => 111,
					'facility_id' => 26,
					'service_id'  => 57,
					'created_at'  => null,
					'updated_at'  => null,
				],
			102 =>
				[
					'id'          => 112,
					'facility_id' => 26,
					'service_id'  => 55,
					'created_at'  => null,
					'updated_at'  => null,
				],
			103 =>
				[
					'id'          => 113,
					'facility_id' => 26,
					'service_id'  => 52,
					'created_at'  => null,
					'updated_at'  => null,
				],
			104 =>
				[
					'id'          => 114,
					'facility_id' => 26,
					'service_id'  => 15,
					'created_at'  => null,
					'updated_at'  => null,
				],
			105 =>
				[
					'id'          => 115,
					'facility_id' => 26,
					'service_id'  => 17,
					'created_at'  => null,
					'updated_at'  => null,
				],
			106 =>
				[
					'id'          => 116,
					'facility_id' => 26,
					'service_id'  => 18,
					'created_at'  => null,
					'updated_at'  => null,
				],
			107 =>
				[
					'id'          => 117,
					'facility_id' => 26,
					'service_id'  => 56,
					'created_at'  => null,
					'updated_at'  => null,
				],
			108 =>
				[
					'id'          => 118,
					'facility_id' => 27,
					'service_id'  => 59,
					'created_at'  => null,
					'updated_at'  => null,
				],
			109 =>
				[
					'id'          => 119,
					'facility_id' => 28,
					'service_id'  => 46,
					'created_at'  => null,
					'updated_at'  => null,
				],
			110 =>
				[
					'id'          => 120,
					'facility_id' => 28,
					'service_id'  => 81,
					'created_at'  => null,
					'updated_at'  => null,
				],
			111 =>
				[
					'id'          => 121,
					'facility_id' => 29,
					'service_id'  => 46,
					'created_at'  => null,
					'updated_at'  => null,
				],
			112 =>
				[
					'id'          => 122,
					'facility_id' => 29,
					'service_id'  => 81,
					'created_at'  => null,
					'updated_at'  => null,
				],
			113 =>
				[
					'id'          => 123,
					'facility_id' => 30,
					'service_id'  => 48,
					'created_at'  => null,
					'updated_at'  => null,
				],
			114 =>
				[
					'id'          => 124,
					'facility_id' => 30,
					'service_id'  => 39,
					'created_at'  => null,
					'updated_at'  => null,
				],
			115 =>
				[
					'id'          => 125,
					'facility_id' => 30,
					'service_id'  => 30,
					'created_at'  => null,
					'updated_at'  => null,
				],
			116 =>
				[
					'id'          => 126,
					'facility_id' => 30,
					'service_id'  => 45,
					'created_at'  => null,
					'updated_at'  => null,
				],
			117 =>
				[
					'id'          => 127,
					'facility_id' => 30,
					'service_id'  => 27,
					'created_at'  => null,
					'updated_at'  => null,
				],
			118 =>
				[
					'id'          => 128,
					'facility_id' => 30,
					'service_id'  => 84,
					'created_at'  => null,
					'updated_at'  => null,
				],
			119 =>
				[
					'id'          => 129,
					'facility_id' => 31,
					'service_id'  => 29,
					'created_at'  => null,
					'updated_at'  => null,
				],
			120 =>
				[
					'id'          => 130,
					'facility_id' => 31,
					'service_id'  => 87,
					'created_at'  => null,
					'updated_at'  => null,
				],
			121 =>
				[
					'id'          => 131,
					'facility_id' => 31,
					'service_id'  => 34,
					'created_at'  => null,
					'updated_at'  => null,
				],
			122 =>
				[
					'id'          => 132,
					'facility_id' => 31,
					'service_id'  => 39,
					'created_at'  => null,
					'updated_at'  => null,
				],
			123 =>
				[
					'id'          => 133,
					'facility_id' => 31,
					'service_id'  => 45,
					'created_at'  => null,
					'updated_at'  => null,
				],
			124 =>
				[
					'id'          => 134,
					'facility_id' => 31,
					'service_id'  => 67,
					'created_at'  => null,
					'updated_at'  => null,
				],
			125 =>
				[
					'id'          => 135,
					'facility_id' => 32,
					'service_id'  => 29,
					'created_at'  => null,
					'updated_at'  => null,
				],
			126 =>
				[
					'id'          => 136,
					'facility_id' => 32,
					'service_id'  => 39,
					'created_at'  => null,
					'updated_at'  => null,
				],
			127 =>
				[
					'id'          => 137,
					'facility_id' => 32,
					'service_id'  => 45,
					'created_at'  => null,
					'updated_at'  => null,
				],
			128 =>
				[
					'id'          => 138,
					'facility_id' => 32,
					'service_id'  => 21,
					'created_at'  => null,
					'updated_at'  => null,
				],
			129 =>
				[
					'id'          => 139,
					'facility_id' => 32,
					'service_id'  => 59,
					'created_at'  => null,
					'updated_at'  => null,
				],
			130 =>
				[
					'id'          => 140,
					'facility_id' => 33,
					'service_id'  => 87,
					'created_at'  => null,
					'updated_at'  => null,
				],
			131 =>
				[
					'id'          => 141,
					'facility_id' => 33,
					'service_id'  => 88,
					'created_at'  => null,
					'updated_at'  => null,
				],
			132 =>
				[
					'id'          => 142,
					'facility_id' => 34,
					'service_id'  => 87,
					'created_at'  => null,
					'updated_at'  => null,
				],
			133 =>
				[
					'id'          => 143,
					'facility_id' => 34,
					'service_id'  => 16,
					'created_at'  => null,
					'updated_at'  => null,
				],
			134 =>
				[
					'id'          => 144,
					'facility_id' => 34,
					'service_id'  => 39,
					'created_at'  => null,
					'updated_at'  => null,
				],
			135 =>
				[
					'id'          => 145,
					'facility_id' => 34,
					'service_id'  => 71,
					'created_at'  => null,
					'updated_at'  => null,
				],
			136 =>
				[
					'id'          => 146,
					'facility_id' => 34,
					'service_id'  => 26,
					'created_at'  => null,
					'updated_at'  => null,
				],
			137 =>
				[
					'id'          => 147,
					'facility_id' => 34,
					'service_id'  => 68,
					'created_at'  => null,
					'updated_at'  => null,
				],
			138 =>
				[
					'id'          => 148,
					'facility_id' => 34,
					'service_id'  => 60,
					'created_at'  => null,
					'updated_at'  => null,
				],
			139 =>
				[
					'id'          => 149,
					'facility_id' => 34,
					'service_id'  => 41,
					'created_at'  => null,
					'updated_at'  => null,
				],
			140 =>
				[
					'id'          => 150,
					'facility_id' => 35,
					'service_id'  => 34,
					'created_at'  => null,
					'updated_at'  => null,
				],
			141 =>
				[
					'id'          => 151,
					'facility_id' => 35,
					'service_id'  => 67,
					'created_at'  => null,
					'updated_at'  => null,
				],
			142 =>
				[
					'id'          => 152,
					'facility_id' => 35,
					'service_id'  => 51,
					'created_at'  => null,
					'updated_at'  => null,
				],
			143 =>
				[
					'id'          => 153,
					'facility_id' => 35,
					'service_id'  => 66,
					'created_at'  => null,
					'updated_at'  => null,
				],
			144 =>
				[
					'id'          => 154,
					'facility_id' => 36,
					'service_id'  => 16,
					'created_at'  => null,
					'updated_at'  => null,
				],
			145 =>
				[
					'id'          => 155,
					'facility_id' => 36,
					'service_id'  => 39,
					'created_at'  => null,
					'updated_at'  => null,
				],
			146 =>
				[
					'id'          => 156,
					'facility_id' => 36,
					'service_id'  => 71,
					'created_at'  => null,
					'updated_at'  => null,
				],
			147 =>
				[
					'id'          => 157,
					'facility_id' => 37,
					'service_id'  => 2,
					'created_at'  => null,
					'updated_at'  => null,
				],
			148 =>
				[
					'id'          => 158,
					'facility_id' => 37,
					'service_id'  => 71,
					'created_at'  => null,
					'updated_at'  => null,
				],
			149 =>
				[
					'id'          => 159,
					'facility_id' => 37,
					'service_id'  => 69,
					'created_at'  => null,
					'updated_at'  => null,
				],
			150 =>
				[
					'id'          => 160,
					'facility_id' => 37,
					'service_id'  => 82,
					'created_at'  => null,
					'updated_at'  => null,
				],
			151 =>
				[
					'id'          => 161,
					'facility_id' => 37,
					'service_id'  => 68,
					'created_at'  => null,
					'updated_at'  => null,
				],
			152 =>
				[
					'id'          => 162,
					'facility_id' => 37,
					'service_id'  => 70,
					'created_at'  => null,
					'updated_at'  => null,
				],
			153 =>
				[
					'id'          => 163,
					'facility_id' => 37,
					'service_id'  => 72,
					'created_at'  => null,
					'updated_at'  => null,
				],
			154 =>
				[
					'id'          => 164,
					'facility_id' => 37,
					'service_id'  => 47,
					'created_at'  => null,
					'updated_at'  => null,
				],
			155 =>
				[
					'id'          => 165,
					'facility_id' => 38,
					'service_id'  => 67,
					'created_at'  => null,
					'updated_at'  => null,
				],
			156 =>
				[
					'id'          => 166,
					'facility_id' => 38,
					'service_id'  => 61,
					'created_at'  => null,
					'updated_at'  => null,
				],
			157 =>
				[
					'id'          => 167,
					'facility_id' => 38,
					'service_id'  => 3,
					'created_at'  => null,
					'updated_at'  => null,
				],
			158 =>
				[
					'id'          => 168,
					'facility_id' => 39,
					'service_id'  => 29,
					'created_at'  => null,
					'updated_at'  => null,
				],
			159 =>
				[
					'id'          => 169,
					'facility_id' => 39,
					'service_id'  => 89,
					'created_at'  => null,
					'updated_at'  => null,
				],
			160 =>
				[
					'id'          => 170,
					'facility_id' => 39,
					'service_id'  => 45,
					'created_at'  => null,
					'updated_at'  => null,
				],
			161 =>
				[
					'id'          => 171,
					'facility_id' => 39,
					'service_id'  => 75,
					'created_at'  => null,
					'updated_at'  => null,
				],
			162 =>
				[
					'id'          => 172,
					'facility_id' => 39,
					'service_id'  => 84,
					'created_at'  => null,
					'updated_at'  => null,
				],
			163 =>
				[
					'id'          => 173,
					'facility_id' => 13,
					'service_id'  => 94,
					'created_at'  => null,
					'updated_at'  => null,
				],
			164 =>
				[
					'id'          => 174,
					'facility_id' => 13,
					'service_id'  => 95,
					'created_at'  => null,
					'updated_at'  => null,
				],
			165 =>
				[
					'id'          => 175,
					'facility_id' => 13,
					'service_id'  => 13,
					'created_at'  => null,
					'updated_at'  => null,
				],
			166 =>
				[
					'id'          => 176,
					'facility_id' => 13,
					'service_id'  => 37,
					'created_at'  => null,
					'updated_at'  => null,
				],
			167 =>
				[
					'id'          => 177,
					'facility_id' => 40,
					'service_id'  => 48,
					'created_at'  => null,
					'updated_at'  => null,
				],
			168 =>
				[
					'id'          => 178,
					'facility_id' => 40,
					'service_id'  => 26,
					'created_at'  => null,
					'updated_at'  => null,
				],
			169 =>
				[
					'id'          => 179,
					'facility_id' => 40,
					'service_id'  => 74,
					'created_at'  => null,
					'updated_at'  => null,
				],
			170 =>
				[
					'id'          => 180,
					'facility_id' => 40,
					'service_id'  => 14,
					'created_at'  => null,
					'updated_at'  => null,
				],
			173 =>
				[
					'id'          => 183,
					'facility_id' => 42,
					'service_id'  => 1,
					'created_at'  => null,
					'updated_at'  => null,
				],
			174 =>
				[
					'id'          => 184,
					'facility_id' => 42,
					'service_id'  => 2,
					'created_at'  => null,
					'updated_at'  => null,
				],
			175 =>
				[
					'id'          => 185,
					'facility_id' => 42,
					'service_id'  => 3,
					'created_at'  => null,
					'updated_at'  => null,
				],
			176 =>
				[
					'id'          => 186,
					'facility_id' => 42,
					'service_id'  => 4,
					'created_at'  => null,
					'updated_at'  => null,
				],
			177 =>
				[
					'id'          => 187,
					'facility_id' => 42,
					'service_id'  => 5,
					'created_at'  => null,
					'updated_at'  => null,
				],
			178 =>
				[
					'id'          => 188,
					'facility_id' => 42,
					'service_id'  => 6,
					'created_at'  => null,
					'updated_at'  => null,
				],
			179 =>
				[
					'id'          => 189,
					'facility_id' => 42,
					'service_id'  => 7,
					'created_at'  => null,
					'updated_at'  => null,
				],
			180 =>
				[
					'id'          => 190,
					'facility_id' => 42,
					'service_id'  => 8,
					'created_at'  => null,
					'updated_at'  => null,
				],
			181 =>
				[
					'id'          => 191,
					'facility_id' => 42,
					'service_id'  => 24,
					'created_at'  => null,
					'updated_at'  => null,
				],
			182 =>
				[
					'id'          => 192,
					'facility_id' => 42,
					'service_id'  => 20,
					'created_at'  => null,
					'updated_at'  => null,
				],
			183 =>
				[
					'id'          => 193,
					'facility_id' => 42,
					'service_id'  => 11,
					'created_at'  => null,
					'updated_at'  => null,
				],
			186 =>
				[
					'id'          => 196,
					'facility_id' => 44,
					'service_id'  => 98,
					'created_at'  => null,
					'updated_at'  => null,
				],
			187 =>
				[
					'id'          => 197,
					'facility_id' => 44,
					'service_id'  => 14,
					'created_at'  => null,
					'updated_at'  => null,
				],
			188 =>
				[
					'id'          => 198,
					'facility_id' => 44,
					'service_id'  => 4,
					'created_at'  => null,
					'updated_at'  => null,
				],
			191 =>
				[
					'id'          => 201,
					'facility_id' => 46,
					'service_id'  => 19,
					'created_at'  => null,
					'updated_at'  => null,
				],
			192 =>
				[
					'id'          => 202,
					'facility_id' => 46,
					'service_id'  => 95,
					'created_at'  => null,
					'updated_at'  => null,
				],
			193 =>
				[
					'id'          => 203,
					'facility_id' => 46,
					'service_id'  => 40,
					'created_at'  => null,
					'updated_at'  => null,
				],
			194 =>
				[
					'id'          => 204,
					'facility_id' => 46,
					'service_id'  => 84,
					'created_at'  => null,
					'updated_at'  => null,
				],
			195 =>
				[
					'id'          => 205,
					'facility_id' => 46,
					'service_id'  => 41,
					'created_at'  => null,
					'updated_at'  => null,
				],
			196 =>
				[
					'id'          => 206,
					'facility_id' => 47,
					'service_id'  => 80,
					'created_at'  => null,
					'updated_at'  => null,
				],
			197 =>
				[
					'id'          => 207,
					'facility_id' => 47,
					'service_id'  => 79,
					'created_at'  => null,
					'updated_at'  => null,
				],
			198 =>
				[
					'id'          => 208,
					'facility_id' => 48,
					'service_id'  => 89,
					'created_at'  => null,
					'updated_at'  => null,
				],
			199 =>
				[
					'id'          => 209,
					'facility_id' => 48,
					'service_id'  => 75,
					'created_at'  => null,
					'updated_at'  => null,
				],
			200 =>
				[
					'id'          => 210,
					'facility_id' => 49,
					'service_id'  => 29,
					'created_at'  => null,
					'updated_at'  => null,
				],
			201 =>
				[
					'id'          => 211,
					'facility_id' => 49,
					'service_id'  => 30,
					'created_at'  => null,
					'updated_at'  => null,
				],
			202 =>
				[
					'id'          => 212,
					'facility_id' => 49,
					'service_id'  => 45,
					'created_at'  => null,
					'updated_at'  => null,
				],
			203 =>
				[
					'id'          => 213,
					'facility_id' => 49,
					'service_id'  => 26,
					'created_at'  => null,
					'updated_at'  => null,
				],
			204 =>
				[
					'id'          => 214,
					'facility_id' => 49,
					'service_id'  => 28,
					'created_at'  => null,
					'updated_at'  => null,
				],
			205 =>
				[
					'id'          => 215,
					'facility_id' => 49,
					'service_id'  => 25,
					'created_at'  => null,
					'updated_at'  => null,
				],
			206 =>
				[
					'id'          => 216,
					'facility_id' => 49,
					'service_id'  => 27,
					'created_at'  => null,
					'updated_at'  => null,
				],
			207 =>
				[
					'id'          => 217,
					'facility_id' => 50,
					'service_id'  => 44,
					'created_at'  => null,
					'updated_at'  => null,
				],
			208 =>
				[
					'id'          => 218,
					'facility_id' => 50,
					'service_id'  => 32,
					'created_at'  => null,
					'updated_at'  => null,
				],
			209 =>
				[
					'id'          => 219,
					'facility_id' => 50,
					'service_id'  => 43,
					'created_at'  => null,
					'updated_at'  => null,
				],
			210 =>
				[
					'id'          => 220,
					'facility_id' => 50,
					'service_id'  => 31,
					'created_at'  => null,
					'updated_at'  => null,
				],
			211 =>
				[
					'id'          => 221,
					'facility_id' => 50,
					'service_id'  => 84,
					'created_at'  => null,
					'updated_at'  => null,
				],
			212 =>
				[
					'id'          => 222,
					'facility_id' => 11,
					'service_id'  => 95,
					'created_at'  => null,
					'updated_at'  => null,
				],
			213 =>
				[
					'id'          => 223,
					'facility_id' => 11,
					'service_id'  => 18,
					'created_at'  => null,
					'updated_at'  => null,
				],
			214 =>
				[
					'id'          => 224,
					'facility_id' => 51,
					'service_id'  => 16,
					'created_at'  => null,
					'updated_at'  => null,
				],
			215 =>
				[
					'id'          => 225,
					'facility_id' => 51,
					'service_id'  => 50,
					'created_at'  => null,
					'updated_at'  => null,
				],
			216 =>
				[
					'id'          => 226,
					'facility_id' => 51,
					'service_id'  => 15,
					'created_at'  => null,
					'updated_at'  => null,
				],
			217 =>
				[
					'id'          => 227,
					'facility_id' => 51,
					'service_id'  => 53,
					'created_at'  => null,
					'updated_at'  => null,
				],
			218 =>
				[
					'id'          => 228,
					'facility_id' => 51,
					'service_id'  => 36,
					'created_at'  => null,
					'updated_at'  => null,
				],
			219 =>
				[
					'id'          => 229,
					'facility_id' => 52,
					'service_id'  => 92,
					'created_at'  => null,
					'updated_at'  => null,
				],
			220 =>
				[
					'id'          => 230,
					'facility_id' => 52,
					'service_id'  => 2,
					'created_at'  => null,
					'updated_at'  => null,
				],
			221 =>
				[
					'id'          => 231,
					'facility_id' => 52,
					'service_id'  => 98,
					'created_at'  => null,
					'updated_at'  => null,
				],
			222 =>
				[
					'id'          => 232,
					'facility_id' => 52,
					'service_id'  => 42,
					'created_at'  => null,
					'updated_at'  => null,
				],
			223 =>
				[
					'id'          => 233,
					'facility_id' => 52,
					'service_id'  => 5,
					'created_at'  => null,
					'updated_at'  => null,
				],
			224 =>
				[
					'id'          => 234,
					'facility_id' => 52,
					'service_id'  => 8,
					'created_at'  => null,
					'updated_at'  => null,
				],
			225 =>
				[
					'id'          => 235,
					'facility_id' => 52,
					'service_id'  => 24,
					'created_at'  => null,
					'updated_at'  => null,
				],
			226 =>
				[
					'id'          => 236,
					'facility_id' => 52,
					'service_id'  => 20,
					'created_at'  => null,
					'updated_at'  => null,
				],
			227 =>
				[
					'id'          => 237,
					'facility_id' => 52,
					'service_id'  => 84,
					'created_at'  => null,
					'updated_at'  => null,
				],
			228 =>
				[
					'id'          => 238,
					'facility_id' => 52,
					'service_id'  => 11,
					'created_at'  => null,
					'updated_at'  => null,
				],
			229 =>
				[
					'id'          => 239,
					'facility_id' => 53,
					'service_id'  => 16,
					'created_at'  => null,
					'updated_at'  => null,
				],
			230 =>
				[
					'id'          => 240,
					'facility_id' => 53,
					'service_id'  => 100,
					'created_at'  => null,
					'updated_at'  => null,
				],
			231 =>
				[
					'id'          => 241,
					'facility_id' => 53,
					'service_id'  => 102,
					'created_at'  => null,
					'updated_at'  => null,
				],
			232 =>
				[
					'id'          => 242,
					'facility_id' => 53,
					'service_id'  => 103,
					'created_at'  => null,
					'updated_at'  => null,
				],
			233 =>
				[
					'id'          => 243,
					'facility_id' => 54,
					'service_id'  => 16,
					'created_at'  => null,
					'updated_at'  => null,
				],
			234 =>
				[
					'id'          => 244,
					'facility_id' => 54,
					'service_id'  => 100,
					'created_at'  => null,
					'updated_at'  => null,
				],
			235 =>
				[
					'id'          => 245,
					'facility_id' => 54,
					'service_id'  => 101,
					'created_at'  => null,
					'updated_at'  => null,
				],
			236 =>
				[
					'id'          => 246,
					'facility_id' => 54,
					'service_id'  => 103,
					'created_at'  => null,
					'updated_at'  => null,
				],
			237 =>
				[
					'id'          => 247,
					'facility_id' => 55,
					'service_id'  => 48,
					'created_at'  => null,
					'updated_at'  => null,
				],
			238 =>
				[
					'id'          => 248,
					'facility_id' => 55,
					'service_id'  => 24,
					'created_at'  => null,
					'updated_at'  => null,
				],
			239 =>
				[
					'id'          => 249,
					'facility_id' => 55,
					'service_id'  => 20,
					'created_at'  => null,
					'updated_at'  => null,
				],
			240 =>
				[
					'id'          => 250,
					'facility_id' => 56,
					'service_id'  => 21,
					'created_at'  => null,
					'updated_at'  => null,
				],
			241 =>
				[
					'id'          => 251,
					'facility_id' => 56,
					'service_id'  => 97,
					'created_at'  => null,
					'updated_at'  => null,
				],
			242 =>
				[
					'id'          => 252,
					'facility_id' => 56,
					'service_id'  => 18,
					'created_at'  => null,
					'updated_at'  => null,
				],
			243 =>
				[
					'id'          => 253,
					'facility_id' => 56,
					'service_id'  => 33,
					'created_at'  => null,
					'updated_at'  => null,
				],
			244 =>
				[
					'id'          => 254,
					'facility_id' => 57,
					'service_id'  => 32,
					'created_at'  => null,
					'updated_at'  => null,
				],
			245 =>
				[
					'id'          => 255,
					'facility_id' => 57,
					'service_id'  => 46,
					'created_at'  => null,
					'updated_at'  => null,
				],
			246 =>
				[
					'id'          => 256,
					'facility_id' => 57,
					'service_id'  => 89,
					'created_at'  => null,
					'updated_at'  => null,
				],
			247 =>
				[
					'id'          => 257,
					'facility_id' => 57,
					'service_id'  => 98,
					'created_at'  => null,
					'updated_at'  => null,
				],
			248 =>
				[
					'id'          => 258,
					'facility_id' => 57,
					'service_id'  => 43,
					'created_at'  => null,
					'updated_at'  => null,
				],
			249 =>
				[
					'id'          => 259,
					'facility_id' => 57,
					'service_id'  => 67,
					'created_at'  => null,
					'updated_at'  => null,
				],
			250 =>
				[
					'id'          => 260,
					'facility_id' => 57,
					'service_id'  => 51,
					'created_at'  => null,
					'updated_at'  => null,
				],
			251 =>
				[
					'id'          => 261,
					'facility_id' => 57,
					'service_id'  => 4,
					'created_at'  => null,
					'updated_at'  => null,
				],
			252 =>
				[
					'id'          => 262,
					'facility_id' => 57,
					'service_id'  => 6,
					'created_at'  => null,
					'updated_at'  => null,
				],
			253 =>
				[
					'id'          => 263,
					'facility_id' => 57,
					'service_id'  => 59,
					'created_at'  => null,
					'updated_at'  => null,
				],
			254 =>
				[
					'id'          => 264,
					'facility_id' => 58,
					'service_id'  => 48,
					'created_at'  => null,
					'updated_at'  => null,
				],
			255 =>
				[
					'id'          => 265,
					'facility_id' => 58,
					'service_id'  => 8,
					'created_at'  => null,
					'updated_at'  => null,
				],
			256 =>
				[
					'id'          => 266,
					'facility_id' => 58,
					'service_id'  => 20,
					'created_at'  => null,
					'updated_at'  => null,
				],
			257 =>
				[
					'id'          => 267,
					'facility_id' => 8,
					'service_id'  => 78,
					'created_at'  => null,
					'updated_at'  => null,
				],
			258 =>
				[
					'id'          => 268,
					'facility_id' => 8,
					'service_id'  => 76,
					'created_at'  => null,
					'updated_at'  => null,
				],
			259 =>
				[
					'id'          => 269,
					'facility_id' => 23,
					'service_id'  => 18,
					'created_at'  => null,
					'updated_at'  => null,
				],
			260 =>
				[
					'id'          => 270,
					'facility_id' => 34,
					'service_id'  => 2,
					'created_at'  => null,
					'updated_at'  => null,
				],
			261 =>
				[
					'id'          => 271,
					'facility_id' => 34,
					'service_id'  => 76,
					'created_at'  => null,
					'updated_at'  => null,
				],
			262 =>
				[
					'id'          => 272,
					'facility_id' => 34,
					'service_id'  => 13,
					'created_at'  => null,
					'updated_at'  => null,
				],
			263 =>
				[
					'id'          => 273,
					'facility_id' => 34,
					'service_id'  => 103,
					'created_at'  => null,
					'updated_at'  => null,
				],
			264 =>
				[
					'id'          => 274,
					'facility_id' => 21,
					'service_id'  => 51,
					'created_at'  => null,
					'updated_at'  => null,
				],
			265 =>
				[
					'id'          => 275,
					'facility_id' => 18,
					'service_id'  => 102,
					'created_at'  => null,
					'updated_at'  => null,
				],
			266 =>
				[
					'id'          => 276,
					'facility_id' => 44,
					'service_id'  => 44,
					'created_at'  => null,
					'updated_at'  => null,
				],
			267 =>
				[
					'id'          => 277,
					'facility_id' => 2,
					'service_id'  => 84,
					'created_at'  => null,
					'updated_at'  => null,
				],
			268 =>
				[
					'id'          => 278,
					'facility_id' => 57,
					'service_id'  => 71,
					'created_at'  => null,
					'updated_at'  => null,
				],
			269 =>
				[
					'id'          => 279,
					'facility_id' => 57,
					'service_id'  => 31,
					'created_at'  => null,
					'updated_at'  => null,
				],
			270 =>
				[
					'id'          => 280,
					'facility_id' => 57,
					'service_id'  => 84,
					'created_at'  => null,
					'updated_at'  => null,
				],
			271 =>
				[
					'id'          => 281,
					'facility_id' => 26,
					'service_id'  => 48,
					'created_at'  => null,
					'updated_at'  => null,
				],
			272 =>
				[
					'id'          => 282,
					'facility_id' => 26,
					'service_id'  => 102,
					'created_at'  => null,
					'updated_at'  => null,
				],
			273 =>
				[
					'id'          => 283,
					'facility_id' => 26,
					'service_id'  => 54,
					'created_at'  => null,
					'updated_at'  => null,
				],
			274 =>
				[
					'id'          => 284,
					'facility_id' => 26,
					'service_id'  => 53,
					'created_at'  => null,
					'updated_at'  => null,
				],
			275 =>
				[
					'id'          => 285,
					'facility_id' => 26,
					'service_id'  => 36,
					'created_at'  => null,
					'updated_at'  => null,
				],
			276 =>
				[
					'id'          => 286,
					'facility_id' => 22,
					'service_id'  => 102,
					'created_at'  => null,
					'updated_at'  => null,
				],
			277 =>
				[
					'id'          => 287,
					'facility_id' => 22,
					'service_id'  => 18,
					'created_at'  => null,
					'updated_at'  => null,
				],
			278 =>
				[
					'id'          => 288,
					'facility_id' => 37,
					'service_id'  => 18,
					'created_at'  => null,
					'updated_at'  => null,
				],
			279 =>
				[
					'id'          => 289,
					'facility_id' => 39,
					'service_id'  => 98,
					'created_at'  => null,
					'updated_at'  => null,
				],
			280 =>
				[
					'id'          => 290,
					'facility_id' => 39,
					'service_id'  => 91,
					'created_at'  => null,
					'updated_at'  => null,
				],
			281 =>
				[
					'id'          => 291,
					'facility_id' => 39,
					'service_id'  => 97,
					'created_at'  => null,
					'updated_at'  => null,
				],
			282 =>
				[
					'id'          => 292,
					'facility_id' => 59,
					'service_id'  => 6,
					'created_at'  => null,
					'updated_at'  => null,
				],
			283 =>
				[
					'id'          => 293,
					'facility_id' => 59,
					'service_id'  => 97,
					'created_at'  => null,
					'updated_at'  => null,
				],
			284 =>
				[
					'id'          => 294,
					'facility_id' => 59,
					'service_id'  => 84,
					'created_at'  => null,
					'updated_at'  => null,
				],
			285 =>
				[
					'id'          => 295,
					'facility_id' => 60,
					'service_id'  => 16,
					'created_at'  => null,
					'updated_at'  => null,
				],
			286 =>
				[
					'id'          => 296,
					'facility_id' => 60,
					'service_id'  => 39,
					'created_at'  => null,
					'updated_at'  => null,
				],
			287 =>
				[
					'id'          => 297,
					'facility_id' => 61,
					'service_id'  => 39,
					'created_at'  => null,
					'updated_at'  => null,
				],
			288 =>
				[
					'id'          => 298,
					'facility_id' => 61,
					'service_id'  => 55,
					'created_at'  => null,
					'updated_at'  => null,
				],
			289 =>
				[
					'id'          => 299,
					'facility_id' => 62,
					'service_id'  => 16,
					'created_at'  => null,
					'updated_at'  => null,
				],
			290 =>
				[
					'id'          => 300,
					'facility_id' => 62,
					'service_id'  => 21,
					'created_at'  => null,
					'updated_at'  => null,
				],
			291 =>
				[
					'id'          => 301,
					'facility_id' => 62,
					'service_id'  => 102,
					'created_at'  => null,
					'updated_at'  => null,
				],
			292 =>
				[
					'id'          => 302,
					'facility_id' => 20,
					'service_id'  => 103,
					'created_at'  => null,
					'updated_at'  => null,
				],
			293 =>
				[
					'id'          => 303,
					'facility_id' => 10,
					'service_id'  => 98,
					'created_at'  => null,
					'updated_at'  => null,
				],
			294 =>
				[
					'id'          => 304,
					'facility_id' => 63,
					'service_id'  => 39,
					'created_at'  => null,
					'updated_at'  => null,
				],
			295 =>
				[
					'id'          => 305,
					'facility_id' => 63,
					'service_id'  => 26,
					'created_at'  => null,
					'updated_at'  => null,
				],
			296 =>
				[
					'id'          => 306,
					'facility_id' => 63,
					'service_id'  => 3,
					'created_at'  => null,
					'updated_at'  => null,
				],
			297 =>
				[
					'id'          => 307,
					'facility_id' => 64,
					'service_id'  => 75,
					'created_at'  => null,
					'updated_at'  => null,
				],
			298 =>
				[
					'id'          => 308,
					'facility_id' => 64,
					'service_id'  => 97,
					'created_at'  => null,
					'updated_at'  => null,
				],
			299 =>
				[
					'id'          => 309,
					'facility_id' => 65,
					'service_id'  => 39,
					'created_at'  => null,
					'updated_at'  => null,
				],
			300 =>
				[
					'id'          => 310,
					'facility_id' => 65,
					'service_id'  => 45,
					'created_at'  => null,
					'updated_at'  => null,
				],
			301 =>
				[
					'id'          => 311,
					'facility_id' => 65,
					'service_id'  => 55,
					'created_at'  => null,
					'updated_at'  => null,
				],
			302 =>
				[
					'id'          => 312,
					'facility_id' => 65,
					'service_id'  => 97,
					'created_at'  => null,
					'updated_at'  => null,
				],
			303 =>
				[
					'id'          => 313,
					'facility_id' => 66,
					'service_id'  => 25,
					'created_at'  => null,
					'updated_at'  => null,
				],
			304 =>
				[
					'id'          => 314,
					'facility_id' => 66,
					'service_id'  => 3,
					'created_at'  => null,
					'updated_at'  => null,
				],
			305 =>
				[
					'id'          => 315,
					'facility_id' => 67,
					'service_id'  => 39,
					'created_at'  => null,
					'updated_at'  => null,
				],
			306 =>
				[
					'id'          => 316,
					'facility_id' => 67,
					'service_id'  => 25,
					'created_at'  => null,
					'updated_at'  => null,
				],
			307 =>
				[
					'id'          => 317,
					'facility_id' => 67,
					'service_id'  => 3,
					'created_at'  => null,
					'updated_at'  => null,
				],
			308 =>
				[
					'id'          => 318,
					'facility_id' => 68,
					'service_id'  => 14,
					'created_at'  => null,
					'updated_at'  => null,
				],
		]);
	}

	private function seedFacilityInsuranceTypePivot()
	{
		\DB::table('facility_insurance_type')->delete();

		\DB::table('facility_insurance_type')->insert([
			0  =>
				[
					'id'                => 1,
					'facility_id'       => 18,
					'insurance_type_id' => 3,
					'created_at'        => null,
					'updated_at'        => null,
				],
			1  =>
				[
					'id'                => 2,
					'facility_id'       => 18,
					'insurance_type_id' => 2,
					'created_at'        => null,
					'updated_at'        => null,
				],
			2  =>
				[
					'id'                => 3,
					'facility_id'       => 18,
					'insurance_type_id' => 1,
					'created_at'        => null,
					'updated_at'        => null,
				],
			3  =>
				[
					'id'                => 4,
					'facility_id'       => 19,
					'insurance_type_id' => 1,
					'created_at'        => null,
					'updated_at'        => null,
				],
			4  =>
				[
					'id'                => 5,
					'facility_id'       => 20,
					'insurance_type_id' => 3,
					'created_at'        => null,
					'updated_at'        => null,
				],
			5  =>
				[
					'id'                => 6,
					'facility_id'       => 20,
					'insurance_type_id' => 1,
					'created_at'        => null,
					'updated_at'        => null,
				],
			6  =>
				[
					'id'                => 7,
					'facility_id'       => 21,
					'insurance_type_id' => 1,
					'created_at'        => null,
					'updated_at'        => null,
				],
			7  =>
				[
					'id'                => 8,
					'facility_id'       => 22,
					'insurance_type_id' => 3,
					'created_at'        => null,
					'updated_at'        => null,
				],
			8  =>
				[
					'id'                => 9,
					'facility_id'       => 22,
					'insurance_type_id' => 2,
					'created_at'        => null,
					'updated_at'        => null,
				],
			9  =>
				[
					'id'                => 10,
					'facility_id'       => 22,
					'insurance_type_id' => 1,
					'created_at'        => null,
					'updated_at'        => null,
				],
			10 =>
				[
					'id'                => 11,
					'facility_id'       => 23,
					'insurance_type_id' => 3,
					'created_at'        => null,
					'updated_at'        => null,
				],
			11 =>
				[
					'id'                => 12,
					'facility_id'       => 23,
					'insurance_type_id' => 2,
					'created_at'        => null,
					'updated_at'        => null,
				],
			12 =>
				[
					'id'                => 13,
					'facility_id'       => 23,
					'insurance_type_id' => 1,
					'created_at'        => null,
					'updated_at'        => null,
				],
			13 =>
				[
					'id'                => 14,
					'facility_id'       => 2,
					'insurance_type_id' => 3,
					'created_at'        => null,
					'updated_at'        => null,
				],
			14 =>
				[
					'id'                => 15,
					'facility_id'       => 2,
					'insurance_type_id' => 1,
					'created_at'        => null,
					'updated_at'        => null,
				],
			15 =>
				[
					'id'                => 16,
					'facility_id'       => 6,
					'insurance_type_id' => 3,
					'created_at'        => null,
					'updated_at'        => null,
				],
			16 =>
				[
					'id'                => 17,
					'facility_id'       => 6,
					'insurance_type_id' => 2,
					'created_at'        => null,
					'updated_at'        => null,
				],
			17 =>
				[
					'id'                => 18,
					'facility_id'       => 6,
					'insurance_type_id' => 1,
					'created_at'        => null,
					'updated_at'        => null,
				],
			19 =>
				[
					'id'                => 20,
					'facility_id'       => 8,
					'insurance_type_id' => 3,
					'created_at'        => null,
					'updated_at'        => null,
				],
			20 =>
				[
					'id'                => 21,
					'facility_id'       => 8,
					'insurance_type_id' => 2,
					'created_at'        => null,
					'updated_at'        => null,
				],
			21 =>
				[
					'id'                => 22,
					'facility_id'       => 8,
					'insurance_type_id' => 1,
					'created_at'        => null,
					'updated_at'        => null,
				],
			22 =>
				[
					'id'                => 23,
					'facility_id'       => 24,
					'insurance_type_id' => 3,
					'created_at'        => null,
					'updated_at'        => null,
				],
			23 =>
				[
					'id'                => 24,
					'facility_id'       => 24,
					'insurance_type_id' => 1,
					'created_at'        => null,
					'updated_at'        => null,
				],
			24 =>
				[
					'id'                => 25,
					'facility_id'       => 26,
					'insurance_type_id' => 3,
					'created_at'        => null,
					'updated_at'        => null,
				],
			25 =>
				[
					'id'                => 26,
					'facility_id'       => 26,
					'insurance_type_id' => 2,
					'created_at'        => null,
					'updated_at'        => null,
				],
			26 =>
				[
					'id'                => 27,
					'facility_id'       => 26,
					'insurance_type_id' => 1,
					'created_at'        => null,
					'updated_at'        => null,
				],
			27 =>
				[
					'id'                => 28,
					'facility_id'       => 31,
					'insurance_type_id' => 3,
					'created_at'        => null,
					'updated_at'        => null,
				],
			28 =>
				[
					'id'                => 29,
					'facility_id'       => 31,
					'insurance_type_id' => 1,
					'created_at'        => null,
					'updated_at'        => null,
				],
			29 =>
				[
					'id'                => 30,
					'facility_id'       => 32,
					'insurance_type_id' => 3,
					'created_at'        => null,
					'updated_at'        => null,
				],
			30 =>
				[
					'id'                => 31,
					'facility_id'       => 32,
					'insurance_type_id' => 1,
					'created_at'        => null,
					'updated_at'        => null,
				],
			31 =>
				[
					'id'                => 32,
					'facility_id'       => 33,
					'insurance_type_id' => 3,
					'created_at'        => null,
					'updated_at'        => null,
				],
			32 =>
				[
					'id'                => 33,
					'facility_id'       => 33,
					'insurance_type_id' => 2,
					'created_at'        => null,
					'updated_at'        => null,
				],
			33 =>
				[
					'id'                => 34,
					'facility_id'       => 34,
					'insurance_type_id' => 1,
					'created_at'        => null,
					'updated_at'        => null,
				],
			34 =>
				[
					'id'                => 35,
					'facility_id'       => 36,
					'insurance_type_id' => 1,
					'created_at'        => null,
					'updated_at'        => null,
				],
			35 =>
				[
					'id'                => 36,
					'facility_id'       => 37,
					'insurance_type_id' => 3,
					'created_at'        => null,
					'updated_at'        => null,
				],
			36 =>
				[
					'id'                => 37,
					'facility_id'       => 37,
					'insurance_type_id' => 2,
					'created_at'        => null,
					'updated_at'        => null,
				],
			37 =>
				[
					'id'                => 38,
					'facility_id'       => 37,
					'insurance_type_id' => 1,
					'created_at'        => null,
					'updated_at'        => null,
				],
			38 =>
				[
					'id'                => 39,
					'facility_id'       => 13,
					'insurance_type_id' => 3,
					'created_at'        => null,
					'updated_at'        => null,
				],
			39 =>
				[
					'id'                => 40,
					'facility_id'       => 13,
					'insurance_type_id' => 1,
					'created_at'        => null,
					'updated_at'        => null,
				],
			40 =>
				[
					'id'                => 41,
					'facility_id'       => 44,
					'insurance_type_id' => 3,
					'created_at'        => null,
					'updated_at'        => null,
				],
			41 =>
				[
					'id'                => 42,
					'facility_id'       => 44,
					'insurance_type_id' => 2,
					'created_at'        => null,
					'updated_at'        => null,
				],
			42 =>
				[
					'id'                => 43,
					'facility_id'       => 46,
					'insurance_type_id' => 3,
					'created_at'        => null,
					'updated_at'        => null,
				],
			43 =>
				[
					'id'                => 44,
					'facility_id'       => 46,
					'insurance_type_id' => 2,
					'created_at'        => null,
					'updated_at'        => null,
				],
			44 =>
				[
					'id'                => 45,
					'facility_id'       => 46,
					'insurance_type_id' => 1,
					'created_at'        => null,
					'updated_at'        => null,
				],
			45 =>
				[
					'id'                => 46,
					'facility_id'       => 11,
					'insurance_type_id' => 3,
					'created_at'        => null,
					'updated_at'        => null,
				],
			46 =>
				[
					'id'                => 47,
					'facility_id'       => 11,
					'insurance_type_id' => 2,
					'created_at'        => null,
					'updated_at'        => null,
				],
			47 =>
				[
					'id'                => 48,
					'facility_id'       => 11,
					'insurance_type_id' => 1,
					'created_at'        => null,
					'updated_at'        => null,
				],
			48 =>
				[
					'id'                => 49,
					'facility_id'       => 51,
					'insurance_type_id' => 3,
					'created_at'        => null,
					'updated_at'        => null,
				],
			49 =>
				[
					'id'                => 50,
					'facility_id'       => 51,
					'insurance_type_id' => 2,
					'created_at'        => null,
					'updated_at'        => null,
				],
			50 =>
				[
					'id'                => 51,
					'facility_id'       => 51,
					'insurance_type_id' => 1,
					'created_at'        => null,
					'updated_at'        => null,
				],
			51 =>
				[
					'id'                => 52,
					'facility_id'       => 53,
					'insurance_type_id' => 3,
					'created_at'        => null,
					'updated_at'        => null,
				],
			52 =>
				[
					'id'                => 53,
					'facility_id'       => 53,
					'insurance_type_id' => 2,
					'created_at'        => null,
					'updated_at'        => null,
				],
			53 =>
				[
					'id'                => 54,
					'facility_id'       => 53,
					'insurance_type_id' => 1,
					'created_at'        => null,
					'updated_at'        => null,
				],
			54 =>
				[
					'id'                => 55,
					'facility_id'       => 54,
					'insurance_type_id' => 3,
					'created_at'        => null,
					'updated_at'        => null,
				],
			55 =>
				[
					'id'                => 56,
					'facility_id'       => 54,
					'insurance_type_id' => 2,
					'created_at'        => null,
					'updated_at'        => null,
				],
			56 =>
				[
					'id'                => 57,
					'facility_id'       => 54,
					'insurance_type_id' => 1,
					'created_at'        => null,
					'updated_at'        => null,
				],
			57 =>
				[
					'id'                => 58,
					'facility_id'       => 55,
					'insurance_type_id' => 3,
					'created_at'        => null,
					'updated_at'        => null,
				],
			58 =>
				[
					'id'                => 59,
					'facility_id'       => 55,
					'insurance_type_id' => 2,
					'created_at'        => null,
					'updated_at'        => null,
				],
			59 =>
				[
					'id'                => 60,
					'facility_id'       => 55,
					'insurance_type_id' => 1,
					'created_at'        => null,
					'updated_at'        => null,
				],
			60 =>
				[
					'id'                => 61,
					'facility_id'       => 56,
					'insurance_type_id' => 3,
					'created_at'        => null,
					'updated_at'        => null,
				],
			61 =>
				[
					'id'                => 62,
					'facility_id'       => 56,
					'insurance_type_id' => 2,
					'created_at'        => null,
					'updated_at'        => null,
				],
			62 =>
				[
					'id'                => 63,
					'facility_id'       => 56,
					'insurance_type_id' => 1,
					'created_at'        => null,
					'updated_at'        => null,
				],
			63 =>
				[
					'id'                => 64,
					'facility_id'       => 58,
					'insurance_type_id' => 3,
					'created_at'        => null,
					'updated_at'        => null,
				],
			64 =>
				[
					'id'                => 65,
					'facility_id'       => 58,
					'insurance_type_id' => 2,
					'created_at'        => null,
					'updated_at'        => null,
				],
			65 =>
				[
					'id'                => 66,
					'facility_id'       => 58,
					'insurance_type_id' => 1,
					'created_at'        => null,
					'updated_at'        => null,
				],
			66 =>
				[
					'id'                => 67,
					'facility_id'       => 60,
					'insurance_type_id' => 1,
					'created_at'        => null,
					'updated_at'        => null,
				],
			67 =>
				[
					'id'                => 68,
					'facility_id'       => 62,
					'insurance_type_id' => 1,
					'created_at'        => null,
					'updated_at'        => null,
				],
		]);
	}

	private function seedFacilityTransitOptionPivot()
	{
		\DB::table('facility_transit_option')->delete();

		\DB::table('facility_transit_option')->insert([
			0   =>
				[
					'id'                => 1,
					'facility_id'       => 18,
					'transit_option_id' => 4,
					'created_at'        => null,
					'updated_at'        => null,
				],
			1   =>
				[
					'id'                => 2,
					'facility_id'       => 18,
					'transit_option_id' => 1,
					'created_at'        => null,
					'updated_at'        => null,
				],
			2   =>
				[
					'id'                => 3,
					'facility_id'       => 19,
					'transit_option_id' => 4,
					'created_at'        => null,
					'updated_at'        => null,
				],
			3   =>
				[
					'id'                => 4,
					'facility_id'       => 19,
					'transit_option_id' => 1,
					'created_at'        => null,
					'updated_at'        => null,
				],
			4   =>
				[
					'id'                => 5,
					'facility_id'       => 20,
					'transit_option_id' => 4,
					'created_at'        => null,
					'updated_at'        => null,
				],
			5   =>
				[
					'id'                => 6,
					'facility_id'       => 20,
					'transit_option_id' => 1,
					'created_at'        => null,
					'updated_at'        => null,
				],
			6   =>
				[
					'id'                => 7,
					'facility_id'       => 21,
					'transit_option_id' => 5,
					'created_at'        => null,
					'updated_at'        => null,
				],
			7   =>
				[
					'id'                => 8,
					'facility_id'       => 21,
					'transit_option_id' => 1,
					'created_at'        => null,
					'updated_at'        => null,
				],
			8   =>
				[
					'id'                => 9,
					'facility_id'       => 22,
					'transit_option_id' => 4,
					'created_at'        => null,
					'updated_at'        => null,
				],
			9   =>
				[
					'id'                => 10,
					'facility_id'       => 22,
					'transit_option_id' => 1,
					'created_at'        => null,
					'updated_at'        => null,
				],
			10  =>
				[
					'id'                => 11,
					'facility_id'       => 23,
					'transit_option_id' => 4,
					'created_at'        => null,
					'updated_at'        => null,
				],
			11  =>
				[
					'id'                => 12,
					'facility_id'       => 23,
					'transit_option_id' => 1,
					'created_at'        => null,
					'updated_at'        => null,
				],
			12  =>
				[
					'id'                => 13,
					'facility_id'       => 1,
					'transit_option_id' => 4,
					'created_at'        => null,
					'updated_at'        => null,
				],
			13  =>
				[
					'id'                => 14,
					'facility_id'       => 1,
					'transit_option_id' => 1,
					'created_at'        => null,
					'updated_at'        => null,
				],
			14  =>
				[
					'id'                => 15,
					'facility_id'       => 2,
					'transit_option_id' => 4,
					'created_at'        => null,
					'updated_at'        => null,
				],
			15  =>
				[
					'id'                => 16,
					'facility_id'       => 2,
					'transit_option_id' => 1,
					'created_at'        => null,
					'updated_at'        => null,
				],
			16  =>
				[
					'id'                => 17,
					'facility_id'       => 3,
					'transit_option_id' => 4,
					'created_at'        => null,
					'updated_at'        => null,
				],
			17  =>
				[
					'id'                => 18,
					'facility_id'       => 3,
					'transit_option_id' => 1,
					'created_at'        => null,
					'updated_at'        => null,
				],
			18  =>
				[
					'id'                => 19,
					'facility_id'       => 5,
					'transit_option_id' => 4,
					'created_at'        => null,
					'updated_at'        => null,
				],
			19  =>
				[
					'id'                => 20,
					'facility_id'       => 5,
					'transit_option_id' => 1,
					'created_at'        => null,
					'updated_at'        => null,
				],
			20  =>
				[
					'id'                => 21,
					'facility_id'       => 6,
					'transit_option_id' => 1,
					'created_at'        => null,
					'updated_at'        => null,
				],
			21  =>
				[
					'id'                => 22,
					'facility_id'       => 8,
					'transit_option_id' => 1,
					'created_at'        => null,
					'updated_at'        => null,
				],
			22  =>
				[
					'id'                => 23,
					'facility_id'       => 9,
					'transit_option_id' => 1,
					'created_at'        => null,
					'updated_at'        => null,
				],
			23  =>
				[
					'id'                => 24,
					'facility_id'       => 24,
					'transit_option_id' => 4,
					'created_at'        => null,
					'updated_at'        => null,
				],
			24  =>
				[
					'id'                => 25,
					'facility_id'       => 24,
					'transit_option_id' => 1,
					'created_at'        => null,
					'updated_at'        => null,
				],
			25  =>
				[
					'id'                => 26,
					'facility_id'       => 26,
					'transit_option_id' => 4,
					'created_at'        => null,
					'updated_at'        => null,
				],
			26  =>
				[
					'id'                => 27,
					'facility_id'       => 26,
					'transit_option_id' => 1,
					'created_at'        => null,
					'updated_at'        => null,
				],
			27  =>
				[
					'id'                => 28,
					'facility_id'       => 28,
					'transit_option_id' => 4,
					'created_at'        => null,
					'updated_at'        => null,
				],
			28  =>
				[
					'id'                => 29,
					'facility_id'       => 28,
					'transit_option_id' => 1,
					'created_at'        => null,
					'updated_at'        => null,
				],
			29  =>
				[
					'id'                => 30,
					'facility_id'       => 29,
					'transit_option_id' => 4,
					'created_at'        => null,
					'updated_at'        => null,
				],
			30  =>
				[
					'id'                => 31,
					'facility_id'       => 29,
					'transit_option_id' => 1,
					'created_at'        => null,
					'updated_at'        => null,
				],
			31  =>
				[
					'id'                => 32,
					'facility_id'       => 30,
					'transit_option_id' => 4,
					'created_at'        => null,
					'updated_at'        => null,
				],
			32  =>
				[
					'id'                => 33,
					'facility_id'       => 30,
					'transit_option_id' => 1,
					'created_at'        => null,
					'updated_at'        => null,
				],
			33  =>
				[
					'id'                => 34,
					'facility_id'       => 31,
					'transit_option_id' => 4,
					'created_at'        => null,
					'updated_at'        => null,
				],
			34  =>
				[
					'id'                => 35,
					'facility_id'       => 31,
					'transit_option_id' => 1,
					'created_at'        => null,
					'updated_at'        => null,
				],
			35  =>
				[
					'id'                => 36,
					'facility_id'       => 32,
					'transit_option_id' => 4,
					'created_at'        => null,
					'updated_at'        => null,
				],
			36  =>
				[
					'id'                => 37,
					'facility_id'       => 32,
					'transit_option_id' => 1,
					'created_at'        => null,
					'updated_at'        => null,
				],
			37  =>
				[
					'id'                => 38,
					'facility_id'       => 33,
					'transit_option_id' => 1,
					'created_at'        => null,
					'updated_at'        => null,
				],
			38  =>
				[
					'id'                => 39,
					'facility_id'       => 34,
					'transit_option_id' => 4,
					'created_at'        => null,
					'updated_at'        => null,
				],
			39  =>
				[
					'id'                => 40,
					'facility_id'       => 34,
					'transit_option_id' => 1,
					'created_at'        => null,
					'updated_at'        => null,
				],
			40  =>
				[
					'id'                => 41,
					'facility_id'       => 35,
					'transit_option_id' => 4,
					'created_at'        => null,
					'updated_at'        => null,
				],
			41  =>
				[
					'id'                => 42,
					'facility_id'       => 35,
					'transit_option_id' => 1,
					'created_at'        => null,
					'updated_at'        => null,
				],
			42  =>
				[
					'id'                => 43,
					'facility_id'       => 36,
					'transit_option_id' => 4,
					'created_at'        => null,
					'updated_at'        => null,
				],
			43  =>
				[
					'id'                => 44,
					'facility_id'       => 36,
					'transit_option_id' => 1,
					'created_at'        => null,
					'updated_at'        => null,
				],
			44  =>
				[
					'id'                => 45,
					'facility_id'       => 37,
					'transit_option_id' => 1,
					'created_at'        => null,
					'updated_at'        => null,
				],
			45  =>
				[
					'id'                => 46,
					'facility_id'       => 37,
					'transit_option_id' => 6,
					'created_at'        => null,
					'updated_at'        => null,
				],
			46  =>
				[
					'id'                => 47,
					'facility_id'       => 38,
					'transit_option_id' => 1,
					'created_at'        => null,
					'updated_at'        => null,
				],
			47  =>
				[
					'id'                => 48,
					'facility_id'       => 39,
					'transit_option_id' => 4,
					'created_at'        => null,
					'updated_at'        => null,
				],
			48  =>
				[
					'id'                => 49,
					'facility_id'       => 39,
					'transit_option_id' => 1,
					'created_at'        => null,
					'updated_at'        => null,
				],
			49  =>
				[
					'id'                => 50,
					'facility_id'       => 13,
					'transit_option_id' => 1,
					'created_at'        => null,
					'updated_at'        => null,
				],
			50  =>
				[
					'id'                => 51,
					'facility_id'       => 40,
					'transit_option_id' => 4,
					'created_at'        => null,
					'updated_at'        => null,
				],
			51  =>
				[
					'id'                => 52,
					'facility_id'       => 40,
					'transit_option_id' => 1,
					'created_at'        => null,
					'updated_at'        => null,
				],
			52  =>
				[
					'id'                => 53,
					'facility_id'       => 42,
					'transit_option_id' => 4,
					'created_at'        => null,
					'updated_at'        => null,
				],
			53  =>
				[
					'id'                => 54,
					'facility_id'       => 42,
					'transit_option_id' => 1,
					'created_at'        => null,
					'updated_at'        => null,
				],
			54  =>
				[
					'id'                => 55,
					'facility_id'       => 44,
					'transit_option_id' => 4,
					'created_at'        => null,
					'updated_at'        => null,
				],
			55  =>
				[
					'id'                => 56,
					'facility_id'       => 44,
					'transit_option_id' => 1,
					'created_at'        => null,
					'updated_at'        => null,
				],
			58  =>
				[
					'id'                => 59,
					'facility_id'       => 46,
					'transit_option_id' => 4,
					'created_at'        => null,
					'updated_at'        => null,
				],
			59  =>
				[
					'id'                => 60,
					'facility_id'       => 46,
					'transit_option_id' => 5,
					'created_at'        => null,
					'updated_at'        => null,
				],
			60  =>
				[
					'id'                => 61,
					'facility_id'       => 46,
					'transit_option_id' => 1,
					'created_at'        => null,
					'updated_at'        => null,
				],
			61  =>
				[
					'id'                => 62,
					'facility_id'       => 47,
					'transit_option_id' => 4,
					'created_at'        => null,
					'updated_at'        => null,
				],
			62  =>
				[
					'id'                => 63,
					'facility_id'       => 47,
					'transit_option_id' => 1,
					'created_at'        => null,
					'updated_at'        => null,
				],
			63  =>
				[
					'id'                => 64,
					'facility_id'       => 48,
					'transit_option_id' => 4,
					'created_at'        => null,
					'updated_at'        => null,
				],
			64  =>
				[
					'id'                => 65,
					'facility_id'       => 48,
					'transit_option_id' => 1,
					'created_at'        => null,
					'updated_at'        => null,
				],
			65  =>
				[
					'id'                => 66,
					'facility_id'       => 49,
					'transit_option_id' => 4,
					'created_at'        => null,
					'updated_at'        => null,
				],
			66  =>
				[
					'id'                => 67,
					'facility_id'       => 49,
					'transit_option_id' => 1,
					'created_at'        => null,
					'updated_at'        => null,
				],
			67  =>
				[
					'id'                => 68,
					'facility_id'       => 50,
					'transit_option_id' => 5,
					'created_at'        => null,
					'updated_at'        => null,
				],
			68  =>
				[
					'id'                => 69,
					'facility_id'       => 50,
					'transit_option_id' => 1,
					'created_at'        => null,
					'updated_at'        => null,
				],
			69  =>
				[
					'id'                => 70,
					'facility_id'       => 11,
					'transit_option_id' => 4,
					'created_at'        => null,
					'updated_at'        => null,
				],
			70  =>
				[
					'id'                => 71,
					'facility_id'       => 11,
					'transit_option_id' => 1,
					'created_at'        => null,
					'updated_at'        => null,
				],
			71  =>
				[
					'id'                => 72,
					'facility_id'       => 51,
					'transit_option_id' => 4,
					'created_at'        => null,
					'updated_at'        => null,
				],
			72  =>
				[
					'id'                => 73,
					'facility_id'       => 51,
					'transit_option_id' => 1,
					'created_at'        => null,
					'updated_at'        => null,
				],
			73  =>
				[
					'id'                => 74,
					'facility_id'       => 52,
					'transit_option_id' => 4,
					'created_at'        => null,
					'updated_at'        => null,
				],
			74  =>
				[
					'id'                => 75,
					'facility_id'       => 52,
					'transit_option_id' => 1,
					'created_at'        => null,
					'updated_at'        => null,
				],
			75  =>
				[
					'id'                => 76,
					'facility_id'       => 53,
					'transit_option_id' => 4,
					'created_at'        => null,
					'updated_at'        => null,
				],
			76  =>
				[
					'id'                => 77,
					'facility_id'       => 53,
					'transit_option_id' => 5,
					'created_at'        => null,
					'updated_at'        => null,
				],
			77  =>
				[
					'id'                => 78,
					'facility_id'       => 53,
					'transit_option_id' => 1,
					'created_at'        => null,
					'updated_at'        => null,
				],
			78  =>
				[
					'id'                => 79,
					'facility_id'       => 54,
					'transit_option_id' => 4,
					'created_at'        => null,
					'updated_at'        => null,
				],
			79  =>
				[
					'id'                => 80,
					'facility_id'       => 54,
					'transit_option_id' => 1,
					'created_at'        => null,
					'updated_at'        => null,
				],
			80  =>
				[
					'id'                => 81,
					'facility_id'       => 55,
					'transit_option_id' => 5,
					'created_at'        => null,
					'updated_at'        => null,
				],
			81  =>
				[
					'id'                => 82,
					'facility_id'       => 55,
					'transit_option_id' => 1,
					'created_at'        => null,
					'updated_at'        => null,
				],
			82  =>
				[
					'id'                => 83,
					'facility_id'       => 56,
					'transit_option_id' => 5,
					'created_at'        => null,
					'updated_at'        => null,
				],
			83  =>
				[
					'id'                => 84,
					'facility_id'       => 56,
					'transit_option_id' => 1,
					'created_at'        => null,
					'updated_at'        => null,
				],
			84  =>
				[
					'id'                => 85,
					'facility_id'       => 57,
					'transit_option_id' => 5,
					'created_at'        => null,
					'updated_at'        => null,
				],
			85  =>
				[
					'id'                => 86,
					'facility_id'       => 57,
					'transit_option_id' => 1,
					'created_at'        => null,
					'updated_at'        => null,
				],
			86  =>
				[
					'id'                => 87,
					'facility_id'       => 58,
					'transit_option_id' => 5,
					'created_at'        => null,
					'updated_at'        => null,
				],
			87  =>
				[
					'id'                => 88,
					'facility_id'       => 58,
					'transit_option_id' => 1,
					'created_at'        => null,
					'updated_at'        => null,
				],
			88  =>
				[
					'id'                => 89,
					'facility_id'       => 12,
					'transit_option_id' => 4,
					'created_at'        => null,
					'updated_at'        => null,
				],
			89  =>
				[
					'id'                => 90,
					'facility_id'       => 12,
					'transit_option_id' => 1,
					'created_at'        => null,
					'updated_at'        => null,
				],
			90  =>
				[
					'id'                => 91,
					'facility_id'       => 16,
					'transit_option_id' => 4,
					'created_at'        => null,
					'updated_at'        => null,
				],
			91  =>
				[
					'id'                => 92,
					'facility_id'       => 16,
					'transit_option_id' => 1,
					'created_at'        => null,
					'updated_at'        => null,
				],
			92  =>
				[
					'id'                => 93,
					'facility_id'       => 25,
					'transit_option_id' => 1,
					'created_at'        => null,
					'updated_at'        => null,
				],
			93  =>
				[
					'id'                => 94,
					'facility_id'       => 59,
					'transit_option_id' => 4,
					'created_at'        => null,
					'updated_at'        => null,
				],
			94  =>
				[
					'id'                => 95,
					'facility_id'       => 59,
					'transit_option_id' => 1,
					'created_at'        => null,
					'updated_at'        => null,
				],
			95  =>
				[
					'id'                => 96,
					'facility_id'       => 60,
					'transit_option_id' => 1,
					'created_at'        => null,
					'updated_at'        => null,
				],
			96  =>
				[
					'id'                => 97,
					'facility_id'       => 61,
					'transit_option_id' => 4,
					'created_at'        => null,
					'updated_at'        => null,
				],
			97  =>
				[
					'id'                => 98,
					'facility_id'       => 61,
					'transit_option_id' => 5,
					'created_at'        => null,
					'updated_at'        => null,
				],
			98  =>
				[
					'id'                => 99,
					'facility_id'       => 61,
					'transit_option_id' => 1,
					'created_at'        => null,
					'updated_at'        => null,
				],
			99  =>
				[
					'id'                => 100,
					'facility_id'       => 62,
					'transit_option_id' => 4,
					'created_at'        => null,
					'updated_at'        => null,
				],
			100 =>
				[
					'id'                => 101,
					'facility_id'       => 62,
					'transit_option_id' => 1,
					'created_at'        => null,
					'updated_at'        => null,
				],
			101 =>
				[
					'id'                => 102,
					'facility_id'       => 63,
					'transit_option_id' => 1,
					'created_at'        => null,
					'updated_at'        => null,
				],
			102 =>
				[
					'id'                => 103,
					'facility_id'       => 15,
					'transit_option_id' => 1,
					'created_at'        => null,
					'updated_at'        => null,
				],
			103 =>
				[
					'id'                => 104,
					'facility_id'       => 64,
					'transit_option_id' => 4,
					'created_at'        => null,
					'updated_at'        => null,
				],
			104 =>
				[
					'id'                => 105,
					'facility_id'       => 64,
					'transit_option_id' => 1,
					'created_at'        => null,
					'updated_at'        => null,
				],
			105 =>
				[
					'id'                => 106,
					'facility_id'       => 65,
					'transit_option_id' => 5,
					'created_at'        => null,
					'updated_at'        => null,
				],
			106 =>
				[
					'id'                => 107,
					'facility_id'       => 65,
					'transit_option_id' => 1,
					'created_at'        => null,
					'updated_at'        => null,
				],
			107 =>
				[
					'id'                => 108,
					'facility_id'       => 66,
					'transit_option_id' => 1,
					'created_at'        => null,
					'updated_at'        => null,
				],
			108 =>
				[
					'id'                => 109,
					'facility_id'       => 66,
					'transit_option_id' => 6,
					'created_at'        => null,
					'updated_at'        => null,
				],
			109 =>
				[
					'id'                => 110,
					'facility_id'       => 67,
					'transit_option_id' => 1,
					'created_at'        => null,
					'updated_at'        => null,
				],
			110 =>
				[
					'id'                => 111,
					'facility_id'       => 67,
					'transit_option_id' => 6,
					'created_at'        => null,
					'updated_at'        => null,
				],
			111 =>
				[
					'id'                => 112,
					'facility_id'       => 68,
					'transit_option_id' => 4,
					'created_at'        => null,
					'updated_at'        => null,
				],
			112 =>
				[
					'id'                => 113,
					'facility_id'       => 68,
					'transit_option_id' => 1,
					'created_at'        => null,
					'updated_at'        => null,
				],
		]);
	}

	private function seedFacilityAgeGroupPivot()
	{
		\DB::table('age_group_facility')->delete();
		\DB::table('age_group_facility')->insert([
			0  =>
				[
					'id'           => 1,
					'facility_id'  => 1,
					'age_group_id' => 1,
					'created_at'   => null,
					'updated_at'   => null,
				],
			1  =>
				[
					'id'           => 2,
					'facility_id'  => 2,
					'age_group_id' => 2,
					'created_at'   => null,
					'updated_at'   => null,
				],
			2  =>
				[
					'id'           => 3,
					'facility_id'  => 3,
					'age_group_id' => 1,
					'created_at'   => null,
					'updated_at'   => null,
				],
			3  =>
				[
					'id'           => 4,
					'facility_id'  => 3,
					'age_group_id' => 2,
					'created_at'   => null,
					'updated_at'   => null,
				],
			4  =>
				[
					'id'           => 5,
					'facility_id'  => 5,
					'age_group_id' => 2,
					'created_at'   => null,
					'updated_at'   => null,
				],
			5  =>
				[
					'id'           => 6,
					'facility_id'  => 6,
					'age_group_id' => 2,
					'created_at'   => null,
					'updated_at'   => null,
				],
			7  =>
				[
					'id'           => 8,
					'facility_id'  => 8,
					'age_group_id' => 2,
					'created_at'   => null,
					'updated_at'   => null,
				],
			8  =>
				[
					'id'           => 9,
					'facility_id'  => 9,
					'age_group_id' => 2,
					'created_at'   => null,
					'updated_at'   => null,
				],
			9  =>
				[
					'id'           => 10,
					'facility_id'  => 10,
					'age_group_id' => 1,
					'created_at'   => null,
					'updated_at'   => null,
				],
			10 =>
				[
					'id'           => 11,
					'facility_id'  => 10,
					'age_group_id' => 2,
					'created_at'   => null,
					'updated_at'   => null,
				],
			11 =>
				[
					'id'           => 12,
					'facility_id'  => 63,
					'age_group_id' => 1,
					'created_at'   => null,
					'updated_at'   => null,
				],
			12 =>
				[
					'id'           => 13,
					'facility_id'  => 63,
					'age_group_id' => 2,
					'created_at'   => null,
					'updated_at'   => null,
				],
			13 =>
				[
					'id'           => 14,
					'facility_id'  => 11,
					'age_group_id' => 2,
					'created_at'   => null,
					'updated_at'   => null,
				],
			14 =>
				[
					'id'           => 15,
					'facility_id'  => 12,
					'age_group_id' => 2,
					'created_at'   => null,
					'updated_at'   => null,
				],
			15 =>
				[
					'id'           => 16,
					'facility_id'  => 13,
					'age_group_id' => 2,
					'created_at'   => null,
					'updated_at'   => null,
				],
			16 =>
				[
					'id'           => 17,
					'facility_id'  => 14,
					'age_group_id' => 1,
					'created_at'   => null,
					'updated_at'   => null,
				],
			17 =>
				[
					'id'           => 18,
					'facility_id'  => 14,
					'age_group_id' => 2,
					'created_at'   => null,
					'updated_at'   => null,
				],
			18 =>
				[
					'id'           => 19,
					'facility_id'  => 15,
					'age_group_id' => 1,
					'created_at'   => null,
					'updated_at'   => null,
				],
			19 =>
				[
					'id'           => 20,
					'facility_id'  => 15,
					'age_group_id' => 2,
					'created_at'   => null,
					'updated_at'   => null,
				],
			20 =>
				[
					'id'           => 21,
					'facility_id'  => 17,
					'age_group_id' => 1,
					'created_at'   => null,
					'updated_at'   => null,
				],
			21 =>
				[
					'id'           => 22,
					'facility_id'  => 17,
					'age_group_id' => 2,
					'created_at'   => null,
					'updated_at'   => null,
				],
			22 =>
				[
					'id'           => 23,
					'facility_id'  => 16,
					'age_group_id' => 1,
					'created_at'   => null,
					'updated_at'   => null,
				],
			23 =>
				[
					'id'           => 24,
					'facility_id'  => 16,
					'age_group_id' => 2,
					'created_at'   => null,
					'updated_at'   => null,
				],
			24 =>
				[
					'id'           => 25,
					'facility_id'  => 18,
					'age_group_id' => 2,
					'created_at'   => null,
					'updated_at'   => null,
				],
			25 =>
				[
					'id'           => 26,
					'facility_id'  => 19,
					'age_group_id' => 2,
					'created_at'   => null,
					'updated_at'   => null,
				],
			26 =>
				[
					'id'           => 27,
					'facility_id'  => 20,
					'age_group_id' => 2,
					'created_at'   => null,
					'updated_at'   => null,
				],
			27 =>
				[
					'id'           => 28,
					'facility_id'  => 21,
					'age_group_id' => 1,
					'created_at'   => null,
					'updated_at'   => null,
				],
			28 =>
				[
					'id'           => 29,
					'facility_id'  => 21,
					'age_group_id' => 2,
					'created_at'   => null,
					'updated_at'   => null,
				],
			29 =>
				[
					'id'           => 30,
					'facility_id'  => 22,
					'age_group_id' => 1,
					'created_at'   => null,
					'updated_at'   => null,
				],
			30 =>
				[
					'id'           => 31,
					'facility_id'  => 23,
					'age_group_id' => 2,
					'created_at'   => null,
					'updated_at'   => null,
				],
			31 =>
				[
					'id'           => 32,
					'facility_id'  => 24,
					'age_group_id' => 1,
					'created_at'   => null,
					'updated_at'   => null,
				],
			32 =>
				[
					'id'           => 33,
					'facility_id'  => 25,
					'age_group_id' => 1,
					'created_at'   => null,
					'updated_at'   => null,
				],
			33 =>
				[
					'id'           => 34,
					'facility_id'  => 26,
					'age_group_id' => 1,
					'created_at'   => null,
					'updated_at'   => null,
				],
			34 =>
				[
					'id'           => 35,
					'facility_id'  => 26,
					'age_group_id' => 2,
					'created_at'   => null,
					'updated_at'   => null,
				],
			35 =>
				[
					'id'           => 36,
					'facility_id'  => 27,
					'age_group_id' => 1,
					'created_at'   => null,
					'updated_at'   => null,
				],
			36 =>
				[
					'id'           => 37,
					'facility_id'  => 28,
					'age_group_id' => 1,
					'created_at'   => null,
					'updated_at'   => null,
				],
			37 =>
				[
					'id'           => 38,
					'facility_id'  => 29,
					'age_group_id' => 1,
					'created_at'   => null,
					'updated_at'   => null,
				],
			38 =>
				[
					'id'           => 39,
					'facility_id'  => 30,
					'age_group_id' => 1,
					'created_at'   => null,
					'updated_at'   => null,
				],
			39 =>
				[
					'id'           => 40,
					'facility_id'  => 31,
					'age_group_id' => 2,
					'created_at'   => null,
					'updated_at'   => null,
				],
			40 =>
				[
					'id'           => 41,
					'facility_id'  => 59,
					'age_group_id' => 1,
					'created_at'   => null,
					'updated_at'   => null,
				],
			41 =>
				[
					'id'           => 42,
					'facility_id'  => 59,
					'age_group_id' => 2,
					'created_at'   => null,
					'updated_at'   => null,
				],
			42 =>
				[
					'id'           => 43,
					'facility_id'  => 33,
					'age_group_id' => 1,
					'created_at'   => null,
					'updated_at'   => null,
				],
			43 =>
				[
					'id'           => 44,
					'facility_id'  => 32,
					'age_group_id' => 2,
					'created_at'   => null,
					'updated_at'   => null,
				],
			44 =>
				[
					'id'           => 45,
					'facility_id'  => 34,
					'age_group_id' => 2,
					'created_at'   => null,
					'updated_at'   => null,
				],
			45 =>
				[
					'id'           => 46,
					'facility_id'  => 35,
					'age_group_id' => 1,
					'created_at'   => null,
					'updated_at'   => null,
				],
			46 =>
				[
					'id'           => 47,
					'facility_id'  => 37,
					'age_group_id' => 1,
					'created_at'   => null,
					'updated_at'   => null,
				],
			47 =>
				[
					'id'           => 48,
					'facility_id'  => 38,
					'age_group_id' => 1,
					'created_at'   => null,
					'updated_at'   => null,
				],
			48 =>
				[
					'id'           => 49,
					'facility_id'  => 39,
					'age_group_id' => 1,
					'created_at'   => null,
					'updated_at'   => null,
				],
			49 =>
				[
					'id'           => 50,
					'facility_id'  => 40,
					'age_group_id' => 1,
					'created_at'   => null,
					'updated_at'   => null,
				],
			52 =>
				[
					'id'           => 53,
					'facility_id'  => 52,
					'age_group_id' => 2,
					'created_at'   => null,
					'updated_at'   => null,
				],
			54 =>
				[
					'id'           => 55,
					'facility_id'  => 44,
					'age_group_id' => 1,
					'created_at'   => null,
					'updated_at'   => null,
				],
			56 =>
				[
					'id'           => 57,
					'facility_id'  => 46,
					'age_group_id' => 2,
					'created_at'   => null,
					'updated_at'   => null,
				],
			57 =>
				[
					'id'           => 58,
					'facility_id'  => 61,
					'age_group_id' => 1,
					'created_at'   => null,
					'updated_at'   => null,
				],
			58 =>
				[
					'id'           => 59,
					'facility_id'  => 61,
					'age_group_id' => 2,
					'created_at'   => null,
					'updated_at'   => null,
				],
			59 =>
				[
					'id'           => 60,
					'facility_id'  => 47,
					'age_group_id' => 1,
					'created_at'   => null,
					'updated_at'   => null,
				],
			60 =>
				[
					'id'           => 61,
					'facility_id'  => 49,
					'age_group_id' => 2,
					'created_at'   => null,
					'updated_at'   => null,
				],
			61 =>
				[
					'id'           => 62,
					'facility_id'  => 50,
					'age_group_id' => 2,
					'created_at'   => null,
					'updated_at'   => null,
				],
			62 =>
				[
					'id'           => 63,
					'facility_id'  => 60,
					'age_group_id' => 1,
					'created_at'   => null,
					'updated_at'   => null,
				],
			63 =>
				[
					'id'           => 64,
					'facility_id'  => 60,
					'age_group_id' => 2,
					'created_at'   => null,
					'updated_at'   => null,
				],
			64 =>
				[
					'id'           => 65,
					'facility_id'  => 51,
					'age_group_id' => 2,
					'created_at'   => null,
					'updated_at'   => null,
				],
			65 =>
				[
					'id'           => 66,
					'facility_id'  => 53,
					'age_group_id' => 2,
					'created_at'   => null,
					'updated_at'   => null,
				],
			66 =>
				[
					'id'           => 67,
					'facility_id'  => 54,
					'age_group_id' => 2,
					'created_at'   => null,
					'updated_at'   => null,
				],
			67 =>
				[
					'id'           => 68,
					'facility_id'  => 55,
					'age_group_id' => 2,
					'created_at'   => null,
					'updated_at'   => null,
				],
			68 =>
				[
					'id'           => 69,
					'facility_id'  => 56,
					'age_group_id' => 2,
					'created_at'   => null,
					'updated_at'   => null,
				],
			69 =>
				[
					'id'           => 70,
					'facility_id'  => 57,
					'age_group_id' => 2,
					'created_at'   => null,
					'updated_at'   => null,
				],
			70 =>
				[
					'id'           => 71,
					'facility_id'  => 58,
					'age_group_id' => 2,
					'created_at'   => null,
					'updated_at'   => null,
				],
			71 =>
				[
					'id'           => 72,
					'facility_id'  => 62,
					'age_group_id' => 1,
					'created_at'   => null,
					'updated_at'   => null,
				],
			72 =>
				[
					'id'           => 73,
					'facility_id'  => 62,
					'age_group_id' => 2,
					'created_at'   => null,
					'updated_at'   => null,
				],
			73 =>
				[
					'id'           => 74,
					'facility_id'  => 65,
					'age_group_id' => 1,
					'created_at'   => null,
					'updated_at'   => null,
				],
			74 =>
				[
					'id'           => 75,
					'facility_id'  => 65,
					'age_group_id' => 2,
					'created_at'   => null,
					'updated_at'   => null,
				],
			75 =>
				[
					'id'           => 76,
					'facility_id'  => 64,
					'age_group_id' => 1,
					'created_at'   => null,
					'updated_at'   => null,
				],
			76 =>
				[
					'id'           => 77,
					'facility_id'  => 64,
					'age_group_id' => 2,
					'created_at'   => null,
					'updated_at'   => null,
				],
			77 =>
				[
					'id'           => 78,
					'facility_id'  => 66,
					'age_group_id' => 2,
					'created_at'   => null,
					'updated_at'   => null,
				],
			78 =>
				[
					'id'           => 79,
					'facility_id'  => 67,
					'age_group_id' => 1,
					'created_at'   => null,
					'updated_at'   => null,
				],
			79 =>
				[
					'id'           => 80,
					'facility_id'  => 67,
					'age_group_id' => 2,
					'created_at'   => null,
					'updated_at'   => null,
				],
			80 =>
				[
					'id'           => 81,
					'facility_id'  => 68,
					'age_group_id' => 1,
					'created_at'   => null,
					'updated_at'   => null,
				],
			81 =>
				[
					'id'           => 82,
					'facility_id'  => 68,
					'age_group_id' => 2,
					'created_at'   => null,
					'updated_at'   => null,
				],
		]);
	}
}