<?php

use App\Models\Options\TransitOption;
use Illuminate\Database\Seeder;

class BaseTransitOptionSeeder extends Seeder
{
	public function run()
	{
		$names = [
			'Parking Lot Or Garage Available',
			'Valet Parking',
			'No Parking',
			'Close To Public Transportation',
			'Monetary Reimbursement For Those Who Qualify (Please Call Ahead)',
			'Transportation Provided By Agency (Please Call Ahead)',
		];

		foreach($names as $name)
		{
			TransitOption::create([
				'name' => $name,
			]);
		}
	}
}
