<?php

use App\Models\User;
use App\Role;
use Illuminate\Database\Seeder;

class BaseInvexDevUserSeeder extends Seeder
{
	public function run()
	{
		$faker = \Faker\Factory::create();

		$overlord_role = Role::get('overlord', true);
		$master_role = Role::get('master', true);
		$administrator_role = Role::get('administrator', true);

		$is_local_environment = config('app.env') == 'local';
		$password = $is_local_environment ? 'password' : '79gdsa32obja3g4trgdfs';

		//Create Overlord
		$user = User::create([
			'username'   => 'dev',
			'password'   => $password,
			'first_name' => 'Babay',
			'last_name'  => 'Lettuce',
			'email'      => 'andrew@invexdesign.com'
		]);
		$user->attachRole($overlord_role);

//		$user = User::create([
//			'username'   => 'pdhp-admin',
//			'password'   => $password,
//			'first_name' => 'PDHP',
//			'last_name'  => 'Admin',
//			'email'      => 'brian@invexdesign.com'
//		]);
//		$user->attachRole($administrator_role);

		$user = User::create([
			'username'   => 'spd-admin',
			'password'   => $password,
			'first_name' => 'SPD',
			'last_name'  => 'Admin',
			'email'      => 'brian@invexdesign.com'
		]);
		$user->attachRole($administrator_role);
	}
}
