<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAllTables extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
		/* If there was an error during "up()", the migration will not have completed
		and will NOT run a "down()" before running "up()" which will error out
		since each table before the error will already exist.
		Use this to clear all tables beforehand.
		*/
		$this->down();

		Schema::create('password_resets', function (Blueprint $table)
		{
			$table->increments('id');
			$table->string('email')->index();
			$table->string('token')->index();
			$table->timestamp('created_at')->nullable();
		});

		Schema::create('users', function (Blueprint $table)
		{
			$table->increments('id');
			$table->string('username')->unique();
			$table->string('email');
			$table->string('first_name')->nullable();
			$table->string('last_name')->nullable();
			$table->string('password');
			$table->dateTime('password_updated_at')->nullable();
			$table->boolean('is_active')->default(true);
			$table->rememberToken();
			$table->nullableTimestamps();
		});

		Schema::create('settings', function (Blueprint $table)
		{
			$table->increments('id');
			$table->string('key')->unique();
			$table->text('value')->nullable();
			$table->string('type', 25)->default('string');
			$table->text('description')->nullable();
			$table->integer('user_id')->unsigned()->nullable();
			$table->foreign('user_id')->references('id')->on('users');
			$table->nullableTimestamps();
		});

		Schema::create('states', function (Blueprint $table)
		{
			$table->increments('id');
			$table->string('name');
			$table->string('short_name');
			$table->nullableTimestamps();
		});

		Schema::create('languages', function (Blueprint $table)
		{
			$table->increments('id');
			$table->string('name');
			$table->string('short_name')->nullable();
			$table->nullableTimestamps();
		});

		Schema::create('services', function (Blueprint $table)
		{
			$table->increments('id');
			$table->string('uid')->nullable();
			$table->string('name')->nullable();
			$table->string('tagline')->nullable();
			$table->text('description')->nullable();
			$table->string('image')->nullable();
			$table->integer('order')->nullable();
			$table->text('notes')->nullable();
			$table->nullableTimestamps();
		});

		Schema::create('age_groups', function (Blueprint $table)
		{
			$table->increments('id');
			$table->string('name');
			$table->nullableTimestamps();
		});

		Schema::create('insurance_types', function (Blueprint $table)
		{
			$table->increments('id');
			$table->string('name');
			$table->nullableTimestamps();
		});

		Schema::create('transit_options', function (Blueprint $table)
		{
			$table->increments('id');
			$table->string('name');
			$table->nullableTimestamps();
		});

		Schema::create('facilities', function (Blueprint $table)
		{
			$table->increments('id');
			$table->string('uid')->nullable();
			$table->string('name')->nullable();
			$table->string('tagline')->nullable();
			$table->string('organization')->nullable();
			$table->string('address1')->nullable();
			$table->string('address2')->nullable()->default(null);
			$table->string('city')->nullable();
			$table->integer('state_id')->unsigned();
			$table->foreign('state_id')->references('id')->on('states');
			$table->string('zipcode')->nullable();
			$table->boolean('auto_gps')->default(false);
			$table->decimal('latitude', 10, 7)->nullable();
			$table->decimal('longitude', 10, 7)->nullable();
			$table->string('url')->nullable();
			$table->string('email')->nullable();
			$table->string('website')->nullable();
			$table->string('phone')->nullable();
			$table->string('247_hotline')->nullable();
			$table->string('text_line')->nullable();
			$table->string('live_chat')->nullable();
			$table->boolean('youth_approved')->default(false);
			$table->text('hours')->nullable();
			$table->string('image')->nullable();
			$table->text('description')->nullable();
			$table->text('minor_access')->nullable();
			$table->text('appointments')->nullable();
			$table->integer('order')->nullable();
			$table->text('notes')->nullable();
			$table->nullableTimestamps();
		});

		Schema::create('facility_service', function (Blueprint $table)
		{
			$table->increments('id')->unsigned()->index();
			$table->integer('facility_id')->unsigned();
			$table->foreign('facility_id')->references('id')->on('facilities')->onDelete('cascade');
			$table->integer('service_id')->unsigned();
			$table->foreign('service_id')->references('id')->on('services')->onDelete('cascade');
			$table->nullableTimestamps();
		});

		Schema::create('facility_language', function (Blueprint $table)
		{
			$table->increments('id')->unsigned()->index();
			$table->integer('facility_id')->unsigned();
			$table->foreign('facility_id')->references('id')->on('facilities')->onDelete('cascade');
			$table->integer('language_id')->unsigned();
			$table->foreign('language_id')->references('id')->on('languages')->onDelete('cascade');
			$table->nullableTimestamps();
		});

		Schema::create('facility_insurance_type', function (Blueprint $table)
		{
			$table->increments('id')->unsigned()->index();
			$table->integer('facility_id')->unsigned();
			$table->foreign('facility_id')->references('id')->on('facilities')->onDelete('cascade');
			$table->integer('insurance_type_id')->unsigned();
			$table->foreign('insurance_type_id')->references('id')->on('insurance_types')->onDelete('cascade');
			$table->nullableTimestamps();
		});

		Schema::create('facility_transit_option', function (Blueprint $table)
		{
			$table->increments('id')->unsigned()->index();
			$table->integer('facility_id')->unsigned();
			$table->foreign('facility_id')->references('id')->on('facilities')->onDelete('cascade');
			$table->integer('transit_option_id')->unsigned();
			$table->foreign('transit_option_id')->references('id')->on('transit_options')->onDelete('cascade');
			$table->nullableTimestamps();
		});

		Schema::create('age_group_facility', function (Blueprint $table)
		{
			$table->increments('id')->unsigned()->index();
			$table->integer('age_group_id')->unsigned();
			$table->foreign('age_group_id')->references('id')->on('age_groups')->onDelete('cascade');
			$table->integer('facility_id')->unsigned();
			$table->foreign('facility_id')->references('id')->on('facilities')->onDelete('cascade');
			$table->nullableTimestamps();
		});

		Schema::create('login_attempts', function (Blueprint $table)
		{
			$table->increments('id')->unsigned()->index();
			$table->string('username')->nullable();
			$table->string('ip_address')->nullable();
			$table->dateTime('timestamp');
			$table->boolean('success')->nullable();
			$table->string('country')->nullable();
			$table->string('region')->nullable();
			$table->string('city')->nullable();
			$table->string('postal')->nullable();
			$table->decimal('latitude', 10, 7)->nullable();
			$table->decimal('longitude', 10, 7)->nullable();
			$table->string('organization')->nullable();
			$table->string('hostname')->nullable();
			$table->string('browser')->nullable();
			$table->string('referrer')->nullable();
			$table->integer('user_id')->unsigned()->nullable();
			$table->nullableTimestamps();
		});

		Schema::create('analytics__monthly_service_searches', function (Blueprint $table)
		{
			$table->increments('id')->unsigned()->index();
			$table->integer('service_id')->unsigned();
			$table->foreign('service_id')->references('id')->on('services')->onDelete('cascade');
			$table->dateTime('month')->nullable();
			$table->bigInteger('hits')->unsigned();
			$table->nullableTimestamps();
		});

		Schema::create('analytics__monthly_age_group_searches', function (Blueprint $table)
		{
			$table->increments('id')->unsigned()->index();
			$table->integer('age_group_id')->unsigned();
			$table->foreign('age_group_id')->references('id')->on('age_groups')->onDelete('cascade');
			$table->dateTime('month')->nullable();
			$table->bigInteger('hits')->unsigned();
			$table->nullableTimestamps();
		});

		Schema::create('analytics__monthly_facility_clicks', function (Blueprint $table)
		{
			$table->increments('id')->unsigned()->index();
			$table->integer('facility_id')->unsigned();
			$table->foreign('facility_id')->references('id')->on('facilities')->onDelete('cascade');
			$table->dateTime('month')->nullable();
			$table->bigInteger('hits')->unsigned();
			$table->nullableTimestamps();
		});

		Schema::create('notifications', function (Blueprint $table)
		{
			$table->increments('id');
			$table->string('type')->default('SYSTEM');
			$table->string('class');
			$table->string('description');
			$table->integer('model_id')->unsigned()->nullable();
			$table->dateTime('remind_at')->nullable();
			$table->boolean('is_severe')->default(false);
			$table->boolean('is_email')->default(true);
			$table->boolean('is_active')->default(true);
			$table->timestamps();
		});
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
		DB::statement('SET FOREIGN_KEY_CHECKS=0;');

		Schema::dropIfExists('notifications');
		Schema::dropIfExists('analytics__monthly_facility_clicks');
		Schema::dropIfExists('analytics__monthly_age_group_searches');
		Schema::dropIfExists('analytics__monthly_service_searches');
		Schema::dropIfExists('login_attempts');
		Schema::dropIfExists('age_group_facility');
		Schema::dropIfExists('facility_transit_option');
		Schema::dropIfExists('facility_insurance_type');
		Schema::dropIfExists('facility_language');
		Schema::dropIfExists('facility_service');
		Schema::dropIfExists('facilities');
		Schema::dropIfExists('facility_types');
		Schema::dropIfExists('transit_options');
		Schema::dropIfExists('insurance_types');
		Schema::dropIfExists('age_groups');
		Schema::dropIfExists('services');
		Schema::dropIfExists('languages');
		Schema::dropIfExists('states');
		Schema::dropIfExists('settings');
		Schema::dropIfExists('users');
		Schema::dropIfExists('password_resets');

		DB::statement('SET FOREIGN_KEY_CHECKS=1;');
    }
}
