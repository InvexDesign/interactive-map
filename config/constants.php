<?php

// These are semicolon separated lists used to specify the user types allowed to bypass certain middleware/permissions restrictions.
define('EXECUTIVES', 'overlord;master');
define('ADMINISTRATORS', 'overlord;master;administrator');

//use App\Models\Access\Role;
//define('EXECUTIVES', implode(';', Role::executives()));
//define('ADMINISTRATORS', implode(';', Role::administrators()));

define('BASIC_SQL_REGEX', '/^[A-Za-z0-9\-_: \/@!\.,\(\)]*$/');
define('PHONE_REGEX', '/^[0-9]{3}-[0-9]{3}-[0-9]{4}(?:x[0-9]+){0,1}$/');
define('ADDRESS_REGEX', '/^[A-Za-z0-9\- \.,]*$/');
define('CITY_REGEX', '/^[A-Za-z\- ]*$/');

define('ZIPCODE_REGEX', '/^[0-9]{5}$/');
define('SSN_REGEX', '/^[0-9]{9}$/');
define('TIN_REGEX', '/^[0-9]{9}$/');
define('INTEGER_REGEX', '/^[0-9]*$/');

define('INTEGER', '/^[1-9]+[0-9]*$/');
define('SERIALIZED', '/^[a-zA-Z0-9\-_;:\{\}\'"]*$/');
define('VIEW_REGEX_SINGLE', '/^[a-zA-Z0-9\-_]*$/');
define('VIEW_REGEX', '/^[a-zA-Z0-9\-_\.]*$/');
define('HTML', '/^[A-Za-z0-9\-_: !\.,\(\)]*$/');
define('ALPHA_DASH_EXPRESSIVE_PLUS', '/^[A-Za-z0-9\-_: !\.,\(\)]*$/');
define('ALPHA_DASH_EXPRESSIVE', '/^[A-Za-z0-9\-:_ !\.,]*$/');
define('ALPHA_DASH_PLUS', '/^[A-Za-z0-9\-_: \(\)]*$/');
define('ALPHA_DASH', '/^[A-Za-z0-9\-_ ]*$/');
define('ALPHA_DASH_NO_SPACE', '/^[A-Za-z0-9\-_]*$/');

define('DELIMITED', '/^[A-Za-z0-9\-_ ,\(\)\*<>]*$/');
define('DELIMITER', '*<>*');