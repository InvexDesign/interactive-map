<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', ['as' => 'frontend.index', 'uses' => 'FrontendController@interactiveMap']);
Route::post('/filter', ['as' => 'frontend.filter', 'uses' => 'FrontendController@filter']);
Route::post('/process-facility-click', ['as' => 'frontend.process_facility_click', 'uses' => 'FrontendController@processFacilityClick']);

if(config('app.env') == 'local')
{
	Route::get('test', function ()
	{
		return 'done';
	});
	Route::get('debug', function ()
	{
		return 'debugging';
	});
}

Route::get('/login', ['as' => 'session.login', 'uses' => 'SessionController@create']);
Route::post('/login', ['as' => 'session.store', 'uses' => 'SessionController@store']);
Route::get('/logout', ['as' => 'session.destroy', 'uses' => 'SessionController@destroy']);

Route::get('/password/forgot', ['as' => 'password.reset.email.get', 'uses' => 'Auth\PasswordController@getEmail']);
Route::post('/password/forgot', ['as' => 'password.reset.email.post', 'uses' => 'Auth\PasswordController@postEmail']);
Route::get('/password/reset/{token}', ['as' => 'password.reset.get', 'uses' => 'Auth\PasswordController@getReset']);
Route::post('/password/reset', ['as' => 'password.reset.post', 'uses' => 'Auth\PasswordController@postReset']);

Route::get('/unauthorized', ['as' => 'errors.unauthorized', 'uses' => 'ErrorController@unauthorized']);

Route::group(['prefix' => '/manage/', 'namespace' => 'Backend'], function ()
{
	Route::get('/', ['as' => 'backend.dashboard.index', 'uses' => 'DashboardController@index']);
//	Route::get('/', ['as' => 'backend.dashboard.index', 'uses' => '\App\Http\Controllers\FrontendController@interactiveMap']);

	Route::group(['prefix' => '/my-account/'], function ()
	{
		Route::get('/', ['as' => 'backend.my_account.show', 'uses' => 'MyAccountController@show']);
		Route::get('/edit', ['as' => 'backend.my_account.edit.get', 'uses' => 'MyAccountController@getEdit']);
		Route::post('/edit', ['as' => 'backend.my_account.edit.post', 'uses' => 'MyAccountController@postEdit']);
		Route::get('/change-password', ['as' => 'backend.my_account.change_password.get', 'uses' => 'MyAccountController@getChangePassword']);
		Route::post('/change-password', ['as' => 'backend.my_account.change_password.post', 'uses' => 'MyAccountController@postChangePassword']);
	});

	Route::group(['prefix' => '/services/'], function ()
	{
		Route::get('/', ['as' => 'backend.services.index', 'uses' => 'ServiceController@index']);
		Route::get('/export', ['as' => 'backend.services.export', 'uses' => 'ServiceController@export']);
		Route::get('/create', ['as' => 'backend.services.create.get', 'uses' => 'ServiceController@getCreate']);
		Route::post('/create', ['as' => 'backend.services.create.post', 'uses' => 'ServiceController@postCreate']);
		Route::get('/{id}', ['as' => 'backend.services.show', 'uses' => 'ServiceController@show']);
		Route::get('/{id}/edit', ['as' => 'backend.services.edit.get', 'uses' => 'ServiceController@getEdit']);
		Route::post('/{id}/edit', ['as' => 'backend.services.edit.post', 'uses' => 'ServiceController@postEdit']);
		Route::get('/{id}/destroy', ['as' => 'backend.services.destroy', 'uses' => 'ServiceController@destroy']);
	});

	Route::group(['prefix' => '/languages/'], function ()
	{
		Route::get('/', ['as' => 'backend.languages.index', 'uses' => 'LanguageController@index']);
		Route::get('/export', ['as' => 'backend.languages.export', 'uses' => 'LanguageController@export']);
		Route::get('/create', ['as' => 'backend.languages.create.get', 'uses' => 'LanguageController@getCreate']);
		Route::post('/create', ['as' => 'backend.languages.create.post', 'uses' => 'LanguageController@postCreate']);
		Route::get('/{id}', ['as' => 'backend.languages.show', 'uses' => 'LanguageController@show']);
		Route::get('/{id}/edit', ['as' => 'backend.languages.edit.get', 'uses' => 'LanguageController@getEdit']);
		Route::post('/{id}/edit', ['as' => 'backend.languages.edit.post', 'uses' => 'LanguageController@postEdit']);
		Route::get('/{id}/destroy', ['as' => 'backend.languages.destroy', 'uses' => 'LanguageController@destroy']);
	});

	Route::group(['prefix' => '/facilities/'], function ()
	{
		Route::get('/', ['as' => 'backend.facilities.index', 'uses' => 'FacilityController@index']);
		Route::get('/export', ['as' => 'backend.facilities.export', 'uses' => 'FacilityController@export']);
		Route::get('/create', ['as' => 'backend.facilities.create.get', 'uses' => 'FacilityController@getCreate']);
		Route::post('/create', ['as' => 'backend.facilities.create.post', 'uses' => 'FacilityController@postCreate']);
		Route::get('/{id}', ['as' => 'backend.facilities.show', 'uses' => 'FacilityController@show']);
		Route::get('/{id}/duplicate', ['as' => 'backend.facilities.duplicate.get', 'uses' => 'FacilityController@getDuplicate']);
		Route::get('/{id}/edit', ['as' => 'backend.facilities.edit.get', 'uses' => 'FacilityController@getEdit']);
		Route::post('/{id}/edit', ['as' => 'backend.facilities.edit.post', 'uses' => 'FacilityController@postEdit']);
		Route::get('/{id}/destroy', ['as' => 'backend.facilities.destroy', 'uses' => 'FacilityController@destroy']);
	});

	Route::group(['prefix' => '/facility-types/'], function ()
	{
		Route::get('/', ['as' => 'backend.facility_types.index', 'uses' => 'FacilityTypeController@index']);
		Route::get('/create', ['as' => 'backend.facility_types.create.get', 'uses' => 'FacilityTypeController@getCreate']);
		Route::post('/create', ['as' => 'backend.facility_types.create.post', 'uses' => 'FacilityTypeController@postCreate']);
		Route::get('/{id}', ['as' => 'backend.facility_types.show', 'uses' => 'FacilityTypeController@show']);
		Route::get('/{id}/edit', ['as' => 'backend.facility_types.edit.get', 'uses' => 'FacilityTypeController@getEdit']);
		Route::post('/{id}/edit', ['as' => 'backend.facility_types.edit.post', 'uses' => 'FacilityTypeController@postEdit']);
		Route::get('/{id}/destroy', ['as' => 'backend.facility_types.destroy', 'uses' => 'FacilityTypeController@destroy']);
	});

	Route::group(['prefix' => '/age-groups/'], function ()
	{
		Route::get('/', ['as' => 'backend.age_groups.index', 'uses' => 'AgeGroupController@index']);
		Route::get('/create', ['as' => 'backend.age_groups.create.get', 'uses' => 'AgeGroupController@getCreate']);
		Route::post('/create', ['as' => 'backend.age_groups.create.post', 'uses' => 'AgeGroupController@postCreate']);
		Route::get('/{id}', ['as' => 'backend.age_groups.show', 'uses' => 'AgeGroupController@show']);
		Route::get('/{id}/edit', ['as' => 'backend.age_groups.edit.get', 'uses' => 'AgeGroupController@getEdit']);
		Route::post('/{id}/edit', ['as' => 'backend.age_groups.edit.post', 'uses' => 'AgeGroupController@postEdit']);
		Route::get('/{id}/destroy', ['as' => 'backend.age_groups.destroy', 'uses' => 'AgeGroupController@destroy']);
	});

	Route::group(['prefix' => '/insurance-types/'], function ()
	{
		Route::get('/', ['as' => 'backend.insurance_types.index', 'uses' => 'InsuranceTypeController@index']);
		Route::get('/create', ['as' => 'backend.insurance_types.create.get', 'uses' => 'InsuranceTypeController@getCreate']);
		Route::post('/create', ['as' => 'backend.insurance_types.create.post', 'uses' => 'InsuranceTypeController@postCreate']);
		Route::get('/{id}', ['as' => 'backend.insurance_types.show', 'uses' => 'InsuranceTypeController@show']);
		Route::get('/{id}/edit', ['as' => 'backend.insurance_types.edit.get', 'uses' => 'InsuranceTypeController@getEdit']);
		Route::post('/{id}/edit', ['as' => 'backend.insurance_types.edit.post', 'uses' => 'InsuranceTypeController@postEdit']);
		Route::get('/{id}/destroy', ['as' => 'backend.insurance_types.destroy', 'uses' => 'InsuranceTypeController@destroy']);
	});

	Route::group(['prefix' => '/transit-options/'], function ()
	{
		Route::get('/', ['as' => 'backend.transit_options.index', 'uses' => 'TransitOptionController@index']);
		Route::get('/create', ['as' => 'backend.transit_options.create.get', 'uses' => 'TransitOptionController@getCreate']);
		Route::post('/create', ['as' => 'backend.transit_options.create.post', 'uses' => 'TransitOptionController@postCreate']);
		Route::get('/{id}', ['as' => 'backend.transit_options.show', 'uses' => 'TransitOptionController@show']);
		Route::get('/{id}/edit', ['as' => 'backend.transit_options.edit.get', 'uses' => 'TransitOptionController@getEdit']);
		Route::post('/{id}/edit', ['as' => 'backend.transit_options.edit.post', 'uses' => 'TransitOptionController@postEdit']);
		Route::get('/{id}/destroy', ['as' => 'backend.transit_options.destroy', 'uses' => 'TransitOptionController@destroy']);
	});

	Route::group(['prefix' => '/users/'], function ()
	{
		Route::get('/', ['as' => 'backend.users.index', 'uses' => 'UserController@index']);
		Route::get('/create', ['as' => 'backend.users.create.get', 'uses' => 'UserController@getCreate']);
		Route::post('/create', ['as' => 'backend.users.create.post', 'uses' => 'UserController@postCreate']);
		Route::get('/{id}', ['as' => 'backend.users.show', 'uses' => 'UserController@show']);
		Route::get('/{id}/edit', ['as' => 'backend.users.edit.get', 'uses' => 'UserController@getEdit']);
		Route::post('/{id}/edit', ['as' => 'backend.users.edit.post', 'uses' => 'UserController@postEdit']);
		Route::get('/{id}/change-password', ['as' => 'backend.users.change_password.get', 'uses' => 'UserController@getChangePassword']);
		Route::post('/{id}/change-password', ['as' => 'backend.users.change_password.post', 'uses' => 'UserController@postChangePassword']);
		Route::get('/{id}/destroy', ['as' => 'backend.users.destroy', 'uses' => 'UserController@destroy']);
	});

	Route::group(['prefix' => '/settings/'], function ()
	{
		Route::get('/', ['as' => 'backend.settings.index', 'uses' => 'SettingController@index']);
//		Route::get('/create', ['as' => 'backend.settings.create.get', 'uses' => 'SettingController@getCreate']);
//		Route::post('/create', ['as' => 'backend.settings.create.post', 'uses' => 'SettingController@postCreate']);
		Route::get('/{id}', ['as' => 'backend.settings.show', 'uses' => 'SettingController@show']);
		Route::get('/{id}/edit', ['as' => 'backend.settings.edit.get', 'uses' => 'SettingController@getEdit']);
		Route::post('/{id}/edit', ['as' => 'backend.settings.edit.post', 'uses' => 'SettingController@postEdit']);
//		Route::get('/{id}/destroy', ['as' => 'backend.settings.destroy', 'uses' => 'SettingController@destroy']);
	});

	Route::group(['prefix' => '/login-attempts/'], function ()
	{
		Route::get('/', ['as' => 'backend.login_attempts.index', 'uses' => 'LoginAttemptController@index']);
		Route::get('/timeline', ['as' => 'backend.login_attempts.timeline', 'uses' => 'LoginAttemptController@timeline']);
		Route::get('/{id}', ['as' => 'backend.login_attempts.show', 'uses' => 'LoginAttemptController@show']);
//		Route::get('/{id}/edit', ['as' => 'backend.login_attempts.edit.get', 'uses' => 'LoginAttemptController@getEdit']);
//		Route::post('/{id}/edit', ['as' => 'backend.login_attempts.edit.post', 'uses' => 'LoginAttemptController@postEdit']);
	});

	Route::group(['prefix' => '/analytics/'], function ()
	{
		Route::group(['prefix' => '/facilities/'], function ()
		{
			Route::get('/', ['as' => 'backend.analytics.facilities.timeline', 'uses' => 'AnalyticsController@facilityClickTimeline']);
			Route::get('/timeline', ['as' => 'backend.analytics.facilities.timeline', 'uses' => 'AnalyticsController@facilityClickTimeline']);
			Route::get('/monthly', ['as' => 'backend.analytics.facilities.monthly', 'uses' => 'AnalyticsController@facilityClickMonthly']);
		});

		Route::group(['prefix' => '/services/'], function ()
		{
			Route::get('/', ['as' => 'backend.analytics.services.timeline', 'uses' => 'AnalyticsController@ServiceSearchTimeline']);
			Route::get('/timeline', ['as' => 'backend.analytics.services.timeline', 'uses' => 'AnalyticsController@ServiceSearchTimeline']);
			Route::get('/monthly', ['as' => 'backend.analytics.services.monthly', 'uses' => 'AnalyticsController@ServiceSearchMonthly']);
		});
	});
});